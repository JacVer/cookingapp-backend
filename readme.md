# How to build on the VM

````shell script
sudo docker ps # to get current container id
sudo docker stop <previous_container_id>
sudo docker images # to get previous image id
sudo docker image rm <previous_image_id> # optional : to delete previous image 

git pull origin release/0.2 # or any release branch
sudo sbt test
sudo sbt docker:publishLocal
sudo docker images # to get image id
sudo docker run -p 8080:8080 <new_image_id> &
````