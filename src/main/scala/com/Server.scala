package com

import akka.actor
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.{ActorSystem, Behavior}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import com.route.HttpRoute
import com.storage.{FavouriteRecipeStorage, FoodStorage, RecipeStorage, AccountStorage}
import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

/* Swagger links
  - https://blog.codecentric.de/en/2016/04/swagger-akka-http/
    - https://github.com/pjfanning/swagger-akka-http-sample

 */

object Server extends App {

  val rootLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME)

  private def startHttpServer(host: String, port: Int, routes: Route, system: ActorSystem[_]): Unit = {
    implicit val classicSystem: actor.ActorSystem = system.toClassic
    import system.executionContext

    Http().newServerAt(host, port).bindFlow(routes)
      .onComplete {
        case Success(binding) =>
          val address = binding.localAddress
          rootLogger.info("Server online at http://{}:{}/", address.getHostString, address.getPort)
        case Failure(exception) =>
          rootLogger.error("Failed to bind HTTP endpoint, terminating system", exception)
          system.terminate()
      }
  }

  val conf: Config = ConfigFactory.load

  val rootBehaviour: Behavior[Nothing] = Behaviors.setup[Nothing] { context =>
    implicit val actorSystem: ActorSystem[Nothing] = context.system
    implicit val executionContext: ExecutionContext = actorSystem.executionContext

    AccountStorage.createTables()
    FoodStorage.createTables()
    RecipeStorage.createTables()
    FavouriteRecipeStorage.createTables()

    startHttpServer(
      conf.getString("http.host"),
      conf.getInt("http.port"),
      HttpRoute.route,
      actorSystem
    )
    Behaviors.empty
  }
  val system = ActorSystem[Nothing](rootBehaviour, "CookingAppAkkaHttpServer")
}

