package com.api

import com.api.NutritionApi.NutritionalInformation

object RecipeApi {

  // INPUT
  case class NewRecipeIngredient(food_id: Int, unit_id: Int, quantity: Double)

  case class NewRecipe(
    title: String,
    description: String,
    instructions: List[String],
    ingredients: List[NewRecipeIngredient],
    servings: Int,
    preparation_time_minutes: Double,
    sourceURL: Option[String],
    base_64_encoded_picture: Option[String]
  )

  // OUTPUT
  case class NewRecipeId(recipe_id: Int)

  case class Recipe(
    recipe_id: Int,
    title: String,
    description: String,
    instructions: Seq[String],
    servings: Int,
    nutri_score_label: String,
    preparation_time_minutes: Double,
    creator_pseudo: String,
    pescetarian: Boolean,
    vegetarian: Boolean,
    vegan: Boolean,
    gluten_free: Boolean,
    january: Boolean,
    february: Boolean,
    march: Boolean,
    april: Boolean,
    may: Boolean,
    june: Boolean,
    july: Boolean,
    august: Boolean,
    september: Boolean,
    october: Boolean,
    november: Boolean,
    december: Boolean,
    nutritional_information: NutritionalInformation,
    base_64_picture: Option[String],
    is_favourite: Boolean,
  )

  case class RecipeIngredient(
    food_id: Int,
    name: String,
    unit_id: Int,
    unit_name: String,
    unit_grams: Double,
    quantity: Double,
    pescetarian: Boolean,
    vegetarian: Boolean,
    vegan: Boolean,
    glutenFree: Boolean,
    january: Boolean,
    february: Boolean,
    march: Boolean,
    april: Boolean,
    may: Boolean,
    june: Boolean,
    july: Boolean,
    august: Boolean,
    september: Boolean,
    october: Boolean,
    november: Boolean,
    december: Boolean,
    nutritional_information: NutritionalInformation
  )
}
