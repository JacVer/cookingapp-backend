package com.api

import com.api.NutritionApi.NutritionalInformation

object FoodApi {

  // OUTPUT

  case class FoodUnit(unit_id: Int, name: String, grams: Double)

  case class Food(
    food_id: Int,
    name: String,
    pescetarian: Boolean,
    vegetarian: Boolean,
    vegan: Boolean,
    gluten_free: Boolean,
    january: Boolean,
    february: Boolean,
    march: Boolean,
    april: Boolean,
    may: Boolean,
    june: Boolean,
    july: Boolean,
    august: Boolean,
    september: Boolean,
    october: Boolean,
    november: Boolean,
    december: Boolean,
    nutritional_information: NutritionalInformation
  )
}
