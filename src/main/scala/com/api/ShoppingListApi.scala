package com.api

import java.time.LocalDateTime

object ShoppingListApi {


  // INPUT
  case class NewShoppingList(shopping_list_title: String)

  case class NewShoppingListItem(ticked: Boolean, indented: Boolean, text: String)

  case class AddShoppingListItems(items: List[NewShoppingListItem])

  case class RenameShoppingList(shopping_list_title: String)

  // OUTPUT
  case class ShoppingListCreatedId(shopping_list_id: Int)

  case class ShoppingListItem(item_id: Int, ticked: Boolean, indented: Boolean, text: String)

  case class ShoppingList(shopping_list_id: Int, time_created: LocalDateTime, title: String, items: List[ShoppingListItem])

  case class ShoppingListSummary(shopping_list_id: Int, time_created: LocalDateTime, title: String)
}
