package com.api

object NutritionApi {
  case class NutritionalInformation(
    energyKiloCalories: Double,
    lipid_g: Double,
    saturated_fatty_acid_g: Double,
    carbohydrate_g: Double,
    sugar_g: Double,
    dietary_fiber_g: Double,
    protein_g: Double,
    salt_g: Double,
    sodium_mg: Double
  )
}
