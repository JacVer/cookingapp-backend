package com.api

import java.time.LocalDateTime

object RecipeRatingApi {

  // OUTPUT
  case class RecipeRating(rating: Option[Double], count: Int)

  case class RecipeComment(added_timestamp: LocalDateTime, pseudo: Option[String], comment: String)
}
