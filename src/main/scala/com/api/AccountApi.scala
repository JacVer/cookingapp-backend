package com.api

object AccountApi {

  // INPUT
  case class Credentials(email: String, password: String)

  case class CreateAccount(email: String, pseudo: String, password: String)


  // OUTPUT
  case class JsonWebToken(token: String)
}
