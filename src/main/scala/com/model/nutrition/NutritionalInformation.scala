package com.model.nutrition

import com.api.NutritionApi.{NutritionalInformation => NutritionalInformationApi}

case class NutritionalInformation(
  energyKiloCalories: Double,
  lipidGram: Double,
  saturatedFattyAcidGram: Double,
  carbohydrateGram: Double,
  sugarGram: Double,
  dietaryFiberGram: Double,
  proteinGram: Double,
  saltGram: Double,
  sodiumMilliGram: Double,
) {
  def toNutritionalInformationApi: NutritionalInformationApi = {
    NutritionalInformationApi(energyKiloCalories, lipidGram, saturatedFattyAcidGram, carbohydrateGram, sugarGram, dietaryFiberGram, proteinGram, saltGram, sodiumMilliGram)
  }
}
