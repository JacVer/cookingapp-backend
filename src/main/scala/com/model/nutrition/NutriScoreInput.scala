package com.model.nutrition

case class NutriScoreInput(
  isTEV: Boolean,
  energyKiloCalories: Double,
  lipidGram: Double,
  saturatedFattyAcidGram: Double,
  carbohydrateGram: Double,
  sugarGram: Double,
  dietaryFiberGram: Double,
  proteinGram: Double,
  saltGram: Double,
  sodiumMilliGram: Double,
  weightGram: Double
)
