package com.model

import com.typesafe.config.ConfigFactory
import io.circe._, io.circe.generic.auto._
import org.mindrot.jbcrypt.BCrypt
import pdi.jwt.{JwtAlgorithm, JwtCirce}



case class Account(accountId: Int, email: String, pseudo: String, passwordHash: String) {
  def checkPassword(password: String): Boolean = BCrypt.checkpw(password, passwordHash)
}

object Account {

  def isEmailValid(email: String): Boolean = email.contains("@") && !email.contains(";")

  def isPseudoValid(pseudo: String): Boolean = !pseudo.contains(";") && !pseudo.contains(" ") && !pseudo.contains("@") && pseudo.length > 3

  def checkPasswordLength(password: String): Boolean = password.length >= 8

  def encryptPassword(password: String): String = {
    BCrypt.hashpw(password, BCrypt.gensalt(ConfigFactory.load.getInt("encryptionSalt")))
  }

  def getJsonWebToken(accountId: Int): String = {
    JwtCirce.encode(
      claim = Json.obj(("accountId", Json.fromInt(accountId))),
      key = ConfigFactory.load.getString("jsonWebTokenKey"),
      algorithm = JwtAlgorithm.HS256
    )
  }

  case class AccountId(accountId: Int)

  def decodeJsonWebToken(jsonWebToken: String): Either[Throwable, Int] = {

    JwtCirce.decodeJson(
      jsonWebToken,
      ConfigFactory.load.getString("jsonWebTokenKey"),
      Seq(JwtAlgorithm.HS256)
    )
      .toEither
      .flatMap(json => json.as[AccountId].map(_.accountId))

  }
}
