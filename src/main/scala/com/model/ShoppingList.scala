package com.model

import com.api.ShoppingListApi.{ShoppingList => ShoppingListApi, ShoppingListItem => ShoppingListItemApi}
import java.time.LocalDateTime

case class ShoppingListItem(shoppingListId: Int, itemId: Int, ticked: Boolean, indented: Boolean, text: String) {
  def toShoppingListItemApi: ShoppingListItemApi = ShoppingListItemApi(itemId, ticked, indented, text)
}

case class ShoppingList(accountId: Int, shoppingListId: Int, addedTime: LocalDateTime, title: String) {
  def toShoppingListApi(shoppingListItems: List[ShoppingListItem]): ShoppingListApi = {
    ShoppingListApi(shoppingListId, addedTime, title, shoppingListItems.map(_.toShoppingListItemApi))
  }
}

sealed trait ShoppingListOrdering
case object Alphabetical extends ShoppingListOrdering
case object CreatedDateTime extends ShoppingListOrdering

object ShoppingListOrdering {
  def fromString(ordering: String): Either[String, ShoppingListOrdering] = ordering match {
    case "alphabetical" => Right(Alphabetical)
    case "created_time" => Right(CreatedDateTime)
    case _ => Left("UNKNOWN_ORDERING")
  }
}