package com.model.recipe

import com.api.RecipeApi.{NewRecipeIngredient => NewRecipeIngredientApi}

case class NewRecipeIngredient(recipeId: Int, foodId: Int, unitId: Int, quantity: Double) {
  def toNewRecipeIngredientApi: NewRecipeIngredientApi = NewRecipeIngredientApi(foodId, unitId, quantity)
}

