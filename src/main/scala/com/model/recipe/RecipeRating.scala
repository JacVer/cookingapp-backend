package com.model.recipe

import com.api.RecipeRatingApi.{RecipeRating => RecipeRatingApi}

case class RecipeRating(averageRating: Option[Double], count: Int) {
  def toRecipeRatingApi: RecipeRatingApi = RecipeRatingApi(averageRating, count)
}
