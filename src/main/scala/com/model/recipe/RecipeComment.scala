package com.model.recipe

import com.api.RecipeRatingApi.{RecipeComment => RecipeCommentApi}
import java.time.LocalDateTime

case class RecipeComment(recipeId: Int, pseudo: Option[String], addedTimestamp: LocalDateTime, comment: String) {
  def toRecipeCommentApi: RecipeCommentApi = RecipeCommentApi(addedTimestamp, pseudo, comment)
}
