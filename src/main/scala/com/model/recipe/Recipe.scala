package com.model.recipe

import com.api.NutritionApi.NutritionalInformation
import com.api.RecipeApi.{Recipe => RecipeApi}
import com.helpers.Tools.getDuplicatesFromList

case class Recipe(
  recipeId: Int,
  accountId: Int,
  accountPseudo: String,
  title: String,
  description: String,
  instructions: List[String],
  sourceURL: Option[String],
  servings: Int,
  nutriScore: String,
  preparationTimeMinutes: Double,
  pescetarian: Boolean,
  vegetarian: Boolean,
  vegan: Boolean,
  glutenFree: Boolean,
  january: Boolean,
  february: Boolean,
  march: Boolean,
  april: Boolean,
  may: Boolean,
  june: Boolean,
  july: Boolean,
  august: Boolean,
  september: Boolean,
  october: Boolean,
  november: Boolean,
  december: Boolean,
  energyKiloCalories: Double,
  lipidGram: Double,
  saturatedFattyAcidGram: Double,
  carbohydrateGram: Double,
  sugarGram: Double,
  dietaryFiberGram: Double,
  proteinGram: Double,
  saltGram: Double,
  sodiumMilliGram: Double,
  picture: Option[String],
  isFavourite: Boolean
) {
  require(List("A", "B", "C", "D", "E").contains(nutriScore), "nutriScore should be A, B, C, D or E")

  def toRecipeApi: RecipeApi = RecipeApi(
    recipeId, title, description, instructions, servings, nutriScore, preparationTimeMinutes, accountPseudo
    , pescetarian, vegetarian, vegan, glutenFree
    , january, february, march, april, may, june, july, august, september, october, november, december
    , this.toNutritionalInformationApi, picture, isFavourite
  )

  def toNutritionalInformationApi: NutritionalInformation = {
    NutritionalInformation(energyKiloCalories, lipidGram, saturatedFattyAcidGram, carbohydrateGram, sugarGram, dietaryFiberGram, proteinGram, saltGram, sodiumMilliGram)
  }
}

object Recipe {
  def isTitleValid(title: String): Boolean = !title.contains(";")

  def getDuplicatedFoodIdUnitId(foodIds: List[(Int, Int)]): List[(Int, Int)] = getDuplicatesFromList(foodIds)
}
