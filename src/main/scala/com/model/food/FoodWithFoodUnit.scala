package com.model.food

case class FoodWithFoodUnit(
  foodId: Int,
  foodNameFr: String,
  isTEV: Boolean,
  pescetarian: Boolean,
  vegetarian: Boolean,
  vegan: Boolean,
  glutenFree: Boolean,
  january: Boolean,
  february: Boolean,
  march: Boolean,
  april: Boolean,
  may: Boolean,
  june: Boolean,
  july: Boolean,
  august: Boolean,
  september: Boolean,
  october: Boolean,
  november: Boolean,
  december: Boolean,
  energyKiloCalories: Double,
  lipidGram: Double,
  saturatedFattyAcidGram: Double,
  carbohydrateGram: Double,
  sugarGram: Double,
  dietaryFiberGram: Double,
  proteinGram: Double,
  saltGram: Double,
  sodiumMilliGram: Double,
  unitId: Int,
  unitNameFr: String,
  weightGrams: Double
) {
  def toFood: Food = Food(foodId, foodNameFr, isTEV, pescetarian, vegetarian, vegan, glutenFree, january, february, march, april, may, june, july, august, september, october, november, december, energyKiloCalories, lipidGram, saturatedFattyAcidGram, carbohydrateGram, sugarGram, dietaryFiberGram, proteinGram, saltGram, sodiumMilliGram)
  def toFoodUnit: FoodUnit = FoodUnit(unitId, unitNameFr, weightGrams)
}
