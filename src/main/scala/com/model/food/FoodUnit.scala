package com.model.food

import com.api.FoodApi.{FoodUnit => FoodUnitApi}

case class FoodUnit(unitId: Int, unitNameFr: String, weightGrams: Double) {
  def toFoodUnitApi: FoodUnitApi = FoodUnitApi(unitId, unitNameFr, weightGrams)
}
