package com.model.food

import com.model.nutrition.NutriScoreInput
import com.api.FoodApi.{Food => FoodApi}
import com.api.NutritionApi.NutritionalInformation
import com.api.RecipeApi.RecipeIngredient

case class Food(
  foodId: Int,
  foodNameFr: String,
  isTEV: Boolean,
  pescetarian: Boolean,
  vegetarian: Boolean,
  vegan: Boolean,
  glutenFree: Boolean,
  january: Boolean,
  february: Boolean,
  march: Boolean,
  april: Boolean,
  may: Boolean,
  june: Boolean,
  july: Boolean,
  august: Boolean,
  september: Boolean,
  october: Boolean,
  november: Boolean,
  december: Boolean,
  energyKiloCalories: Double,
  lipidGram: Double,
  saturatedFattyAcidGram: Double,
  carbohydrateGram: Double,
  sugarGram: Double,
  dietaryFiberGram: Double,
  proteinGram: Double,
  saltGram: Double,
  sodiumMilliGram: Double,
) {
  def toNutriScoreInput(weightGram: Double): NutriScoreInput = NutriScoreInput(isTEV, energyKiloCalories, lipidGram, saturatedFattyAcidGram, carbohydrateGram, sugarGram, dietaryFiberGram, proteinGram, saltGram, sodiumMilliGram, weightGram)

  def toFoodApi: FoodApi = {
    FoodApi(
      foodId, foodNameFr,
      pescetarian, vegetarian, vegan, glutenFree,
      january, february, march, april, may, june, july, august, september, october, november, december,
      NutritionalInformation(energyKiloCalories, lipidGram, saturatedFattyAcidGram, carbohydrateGram, sugarGram, dietaryFiberGram, proteinGram, saltGram, sodiumMilliGram)
    )
  }

  def toRecipeIngredient(unit: FoodUnit, quantity: Double): RecipeIngredient = {
    RecipeIngredient(
      foodId, foodNameFr,
      unit.unitId, unit.unitNameFr, unit.weightGrams, quantity,
      pescetarian, vegetarian, vegan, glutenFree,
      january, february, march, april, may, june, july, august, september, october, november, december,
      NutritionalInformation(energyKiloCalories, lipidGram, saturatedFattyAcidGram, carbohydrateGram, sugarGram, dietaryFiberGram, proteinGram, saltGram, sodiumMilliGram)
    )
  }
}

object Food {
  def isNameValid(name: String): Boolean = !name.contains(";")
}
