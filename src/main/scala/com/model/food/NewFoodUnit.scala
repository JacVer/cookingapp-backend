package com.model.food

case class NewFoodUnit(foodId: Int, unitId: Int, unitNameFr: String, weightGrams: Double)
