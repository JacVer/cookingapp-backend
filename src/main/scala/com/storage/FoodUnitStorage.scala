package com.storage

import com.github.tototoshi.csv.CSVReader
import com.model.food.{FoodUnit, NewFoodUnit}
import doobie.implicits._
import doobie.util.fragment.Fragment

import scala.concurrent.Future

object FoodUnitStorage extends BaseStorage {


  private def createFoodUnitTable(): Unit = {
    (sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(foodUnitTableName) ++
      sql"(food_id INT REFERENCES " ++ Fragment.const(foodTableName) ++ sql"(food_id), " ++
      sql"unit_id INT NOT NULL, " ++
      sql"unit_name_fr VARCHAR NOT NULL, " ++
      sql"grams REAL NOT NULL, " ++
      sql"UNIQUE(food_id, unit_id), " ++
      sql"UNIQUE(food_id, unit_name_fr), " ++
      sql"PRIMARY KEY (food_id, unit_id, unit_name_fr))"
      ).update.run.transact(xa).unsafeRunSync()
  }

  private def dropFoodUnitTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(foodUnitTableName)).update.run.transact(xa).unsafeRunSync()
  }

  def createTables(): Unit = createFoodUnitTable()

  def dropTables(): Unit = dropFoodUnitTable()


  // MAIN FUNCTIONS


  def readFoodUnitCsv(csvPath: String): Iterator[NewFoodUnit] = {

    def strToDouble(str: String): Double = str.replace(",", ".").toDouble

    val reader: CSVReader = CSVReader.open(csvPath)
    val (_, rows: Iterator[Seq[String]]) = {
      val it = reader.iterator
      (it.next, it)
    }

    rows.map( row => NewFoodUnit(row.head.toInt, row(1).toInt, row(2), strToDouble(row(3))))
  }

  def insertFoodUnits(foodUnits: List[NewFoodUnit]): Either[Throwable, Int] = {
    val query = sql"INSERT INTO " ++ Fragment.const(foodUnitTableName) ++ sql" VALUES " ++
      foodUnits
        .map(f => sql"(${f.foodId}, ${f.unitId}, ${f.unitNameFr}, ${f.weightGrams})")
        .reduce(_ ++ sql"," ++ _)
    query.update.run.transact(xa).attempt.unsafeRunSync()
  }

  def selectFoodUnit(foodId: Int): Future[Either[Throwable, List[FoodUnit]]] = {
    (sql"SELECT unit_id, unit_name_fr, grams " ++
      sql"FROM " ++ Fragment.const(foodUnitTableName) ++ sql" " ++
      sql"WHERE food_id = $foodId"
      )
      .query[FoodUnit].to[List].transact(xa).attempt.unsafeToFuture()
  }

  def selectFoodIdWithoutUnit(): Either[Throwable, List[Int]] = {
    (sql"WITH FoodUnitByFoodId AS ( " ++
      sql"SELECT f.food_id, SUM(CASE WHEN fu.unit_id IS NOT NULL THEN 1 ELSE 0 END) AS unit_id_count " ++
      sql"FROM " ++ Fragment.const(foodTableName) ++ sql" f " ++
      sql"LEFT OUTER JOIN " ++ Fragment.const(foodUnitTableName) ++ sql" fu ON f.food_id = fu.food_id " ++
      sql"GROUP BY f.food_id) " ++
      sql"SELECT food_id FROM FoodUnitByFoodId WHERE unit_id_count = 0 ORDER BY food_id"
      ).query[Int].to[List].transact(xa).attempt.unsafeRunSync()
  }

  def selectUnitIdWithMultipleUnitNameFr(): Either[Throwable, List[Int]] = {
    (sql"WITH UnitNameByUnitId AS (" ++
      sql"SELECT unit_id, count(distinct unit_name_fr) AS count_distinct_name " ++
      sql"FROM " ++ Fragment.const(foodUnitTableName) ++ sql" " ++
      sql"GROUP BY unit_id) " ++
      sql"SELECT unit_id FROM UnitNameByUnitId WHERE count_distinct_name > 1 ORDER BY unit_id"
      ).query[Int].to[List].transact(xa).attempt.unsafeRunSync()
  }

  def selectFoodIdWithoutGramUnit(): Either[Throwable, List[Int]] = {
    val gramme = "gramme"
    (sql"WITH FoodUnitByFoodId AS ( " ++
      sql"SELECT f.food_id, SUM(CASE WHEN fu.unit_id = 0 AND fu.unit_name_fr = $gramme AND fu.grams = 1 THEN 1 ELSE 0 END) AS grammes_unit_count " ++
      sql"FROM " ++ Fragment.const(foodTableName) ++ sql" f " ++
      sql"LEFT OUTER JOIN " ++ Fragment.const(foodUnitTableName) ++ sql" fu ON f.food_id = fu.food_id " ++
      sql"GROUP BY f.food_id) " ++
      sql"SELECT food_id FROM FoodUnitByFoodId WHERE grammes_unit_count = 0 ORDER BY food_id"
      ).query[Int].to[List].transact(xa).attempt.unsafeRunSync()
  }
}
