package com.storage

import java.time.Month

import com.model.food.{Food, FoodUnit, FoodWithFoodUnit}
import com.model.recipe.{NewRecipeIngredient, Recipe}
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import doobie.util.fragment.Fragment

import scala.concurrent.{ExecutionContext, Future}

object RecipeStorage extends BaseStorage {

  // TODO : add time_tsp column for recipe creation time
  private def createRecipeTable(): Unit = {
    (sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(recipeTableName) ++
      sql"(recipe_id SERIAL PRIMARY KEY, " ++
      sql"account_id INT NOT NULL REFERENCES " ++ Fragment.const(accountTableName) ++ sql"""(account_id),
      title VARCHAR NOT NULL,
      description VARCHAR NOT NULL,
      instructions text ARRAY NOT NULL,
      source_url VARCHAR,
      servings INT NOT NULL CHECK(servings > 0),
      nutri_score CHAR(1) NOT NULL CHECK(nutri_score IN ('A', 'B', 'C', 'D', 'E')),
      preparation_time_minutes REAL NOT NULL CHECK(preparation_time_minutes > 0.0),
      energy_kilo_calories REAL NOT NULL CHECK(energy_kilo_calories >= 0.0),
      pescetarian BOOLEAN NOT NULL,
      vegetarian BOOLEAN NOT NULL,
      vegan BOOLEAN NOT NULL,
      gluten_free BOOLEAN NOT NULL,
      january BOOLEAN NOT NULL,
      february BOOLEAN NOT NULL,
      march BOOLEAN NOT NULL,
      april BOOLEAN NOT NULL,
      may BOOLEAN NOT NULL,
      june BOOLEAN NOT NULL,
      july BOOLEAN NOT NULL,
      august BOOLEAN NOT NULL,
      september BOOLEAN NOT NULL,
      october BOOLEAN NOT NULL,
      november BOOLEAN NOT NULL,
      december BOOLEAN NOT NULL,
      lipid_g REAL NOT NULL CHECK(lipid_g >= 0.0),
      saturated_fatty_acid_g REAL NOT NULL CHECK(saturated_fatty_acid_g >= 0.0),
      carbohydrate_g REAL NOT NULL CHECK(carbohydrate_g >= 0.0),
      sugar_g REAL NOT NULL CHECK(sugar_g >= 0.0),
      dietary_fiber_g REAL NOT NULL CHECK(dietary_fiber_g >= 0.0),
      protein_g REAL NOT NULL CHECK(protein_g >= 0.0),
      salt_g REAL NOT NULL CHECK(salt_g >= 0.0),
      sodium_mg REAL NOT NULL CHECK(sodium_mg >= 0.0),
      base_64_encoded_picture VARCHAR,
      deleted BOOLEAN NOT NULL DEFAULT false)
      """
      ).update.run.transact(xa).unsafeRunSync()
  }

  private def createIngredientTable(): Unit = {
    (sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(ingredientTableName) ++
      sql"(recipe_id INT REFERENCES " ++ Fragment.const(recipeTableName) ++ sql"(recipe_id)," ++
      sql"food_id INT NOT NULL," ++
      sql"unit_id INT NOT NULL," ++
      sql"quantity REAL NOT NULL," ++
      sql"FOREIGN KEY (food_id, unit_id) REFERENCES " ++ Fragment.const(foodUnitTableName) ++ sql" (food_id, unit_id)," ++
      sql"PRIMARY KEY (recipe_id, food_id, unit_id))"
      ).update.run.transact(xa).unsafeRunSync()
  }

  private def dropRecipeTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(recipeTableName)).update.run.transact(xa).unsafeRunSync()
  }

  private def dropIngredientTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(ingredientTableName)).update.run.transact(xa).unsafeRunSync()
  }

  def createTables(): Unit = {
    createRecipeTable()
    createIngredientTable()
  }

  def dropTables(): Unit = {
    dropIngredientTable()
    dropRecipeTable()
  }


  // MAIN FUNCTIONS


  private def monthToColumn(month: Month): Fragment = month match {
    case Month.JANUARY => sql"r.january"
    case Month.FEBRUARY => sql"r.february"
    case Month.MARCH => sql"r.march"
    case Month.APRIL => sql"r.april"
    case Month.MAY => sql"r.may"
    case Month.JUNE => sql"r.june"
    case Month.JULY => sql"r.july"
    case Month.AUGUST => sql"r.august"
    case Month.SEPTEMBER => sql"r.september"
    case Month.OCTOBER => sql"r.october"
    case Month.NOVEMBER => sql"r.november"
    case Month.DECEMBER => sql"r.december"
  }

  // TODO : this architecture doesn't look right, how should it be improved ?
  def selectRecipe(
    title: Option[String] = None,
    pseudo: Option[String] = None,
    accountId: Option[Int] = None,
    recipeId: Option[Int] = None,
    foodIds: Option[List[Int]] = None,
    pescetarian: Boolean = false,
    vegetarian: Boolean = false,
    vegan: Boolean = false,
    glutenFree: Boolean = false,
    month: Option[Month] = None,
  )(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[Recipe]]] = {
    if(title.isEmpty && recipeId.isEmpty && pseudo.isEmpty && foodIds.isEmpty && month.isEmpty) throw new Error("At least one parameter must be defined")

    foodIds match {
      case Some(foodIds) =>
        selectRecipeWithIngredients(foodIds, title, pseudo, accountId, pescetarian, vegetarian, vegan, glutenFree, month)
      case None =>
        selectRecipeWithoutIngredients(title, pseudo, accountId, recipeId, pescetarian, vegetarian, vegan, glutenFree, month)
    }
  }

  def selectRecipeWithoutIngredients(
    title: Option[String] = None,
    pseudo: Option[String] = None,
    accountId: Option[Int] = None,
    recipeId: Option[Int] = None,
    pescetarian: Boolean = false,
    vegetarian: Boolean = false,
    vegan: Boolean = false,
    glutenFree: Boolean = false,
    month: Option[Month] = None
  )(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[Recipe]]] = {

      if (title.isEmpty && recipeId.isEmpty && pseudo.isEmpty && month.isEmpty) throw new Error("At least one parameter must be defined")
    val seasonalFilter: Fragment = month match {
      case Some(month) => monthToColumn(month)
      case None => sql"TRUE"
    }

    (sql"SELECT " ++
      sql"  r.recipe_id" ++
      sql"  , r.account_id" ++
      sql"  , u.pseudo" ++
      sql"  , r.title" ++
      sql"  , r.description" ++
      sql"  , r.instructions" ++
      sql"  , r.source_url" ++
      sql"  , r.servings" ++
      sql"  , r.nutri_score" ++
      sql"  , r.preparation_time_minutes" ++
      sql"  , r.pescetarian, r.vegetarian, r.vegan, r.gluten_free" ++
      sql"  , r.january, r.february, r.march, r.april, r.may, r.june, r.july, r.august, r.september, r.october, r.november, r.december" ++
      sql"  , r.energy_kilo_calories " ++
      sql"  , r.lipid_g " ++
      sql"  , r.saturated_fatty_acid_g " ++
      sql"  , r.carbohydrate_g " ++
      sql"  , r.sugar_g " ++
      sql"  , r.dietary_fiber_g " ++
      sql"  , r.protein_g " ++
      sql"  , r.salt_g " ++
      sql"  , r.sodium_mg " ++
      sql"  , r.base_64_encoded_picture" ++
      sql"  , " ++ accountId.map(_ => sql"f.added_tsp IS NOT NULL").getOrElse(sql"FALSE") ++ sql" as isFavourite " ++
      sql"FROM " ++ Fragment.const(recipeTableName) ++ sql" r " ++
      sql"INNER JOIN " ++ Fragment.const(accountTableName) ++ sql" u ON r.account_id = u.account_id " ++
      accountId.map(u => sql"LEFT OUTER JOIN " ++ Fragment.const(favouriteRecipeTableName) ++ sql" f ON r.recipe_id = f.recipe_id AND f.account_id = $u ").getOrElse(sql"") ++
      sql"WHERE " ++
      title.map("%" + _.toLowerCase + "%").map(t => sql"LOWER(r.title) LIKE $t").getOrElse(sql"TRUE") ++
      sql" AND " ++ recipeId.map(r => sql"r.recipe_id = $r").getOrElse(sql"TRUE") ++
      sql" AND " ++ pseudo.map(u => sql"u.pseudo = $u").getOrElse(sql"TRUE") ++
      sql" AND " ++ seasonalFilter ++
      sql" AND (NOT u.deleted)" ++
      sql" AND (NOT r.deleted)" ++
      (if(pescetarian) sql" AND r.pescetarian" else sql"") ++
      (if(vegetarian) sql" AND r.vegetarian" else sql"") ++
      (if(vegan) sql" AND r.vegan" else sql"") ++
      (if(glutenFree) sql" AND r.gluten_free" else sql"") ++
      sql" ORDER BY title"
      )
      .query[Recipe]
      .to[List]
      .transact(xa)
      .attempt.unsafeToFuture()
  }

  def selectRecipeWithIngredients(
    foodIds: List[Int],
    title: Option[String] = None,
    pseudo: Option[String] = None,
    accountId: Option[Int] = None,
    pescetarian: Boolean = false,
    vegetarian: Boolean = false,
    vegan: Boolean = false,
    glutenFree: Boolean = false,
    month: Option[Month] = None
  )(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[Recipe]]] = {

    val foodIdsArray = "Array[" + foodIds.mkString(", ") + "]"
    val seasonalFilter: Fragment = month match {
      case Some(month) => monthToColumn(month)
      case None => sql"TRUE"
    }

    (sql"with Recipes_With_Agg_Ingredients as ( " ++
      sql"  SELECT " ++
      sql"    r.recipe_id" ++
      sql"    , r.account_id" ++
      sql"    , u.pseudo" ++
      sql"    , r.title" ++
      sql"    , r.description" ++
      sql"    , r.instructions" ++
      sql"    , r.source_url" ++
      sql"    , r.servings" ++
      sql"    , r.nutri_score" ++
      sql"    , r.preparation_time_minutes" ++
      sql"    , r.pescetarian, r.vegetarian, r.vegan, r.gluten_free" ++
      sql"    , r.january, r.february, r.march, r.april, r.may, r.june, r.july, r.august, r.september, r.october, r.november, r.december" ++
      sql"    , r.energy_kilo_calories " ++
      sql"    , r.lipid_g " ++
      sql"    , r.saturated_fatty_acid_g " ++
      sql"    , r.carbohydrate_g " ++
      sql"    , r.sugar_g " ++
      sql"    , r.dietary_fiber_g " ++
      sql"    , r.protein_g " ++
      sql"    , r.salt_g " ++
      sql"    , r.sodium_mg " ++
      sql"    , r.base_64_encoded_picture" ++
      sql"    , array_agg(i.food_id) as food_ids " ++
      sql"  FROM " ++ Fragment.const(recipeTableName) ++ sql" r " ++
      sql"    INNER JOIN " ++ Fragment.const(accountTableName) ++ sql" u ON r.account_id = u.account_id " ++
      sql"    INNER JOIN " ++ Fragment.const(ingredientTableName) ++ sql" i ON r.recipe_id = i.recipe_id " ++
      sql"  WHERE " ++
      title.map("%" + _.toLowerCase + "%").map(t => sql"LOWER(title) LIKE $t").getOrElse(sql"TRUE") ++
      sql"    AND " ++ pseudo.map(u => sql"u.pseudo = $u").getOrElse(sql"TRUE") ++
      sql"    AND " ++ seasonalFilter ++
      sql"    AND (NOT u.deleted)" ++
      sql"    AND NOT r.deleted" ++
      (if(pescetarian) sql" AND r.pescetarian" else sql"") ++
      (if(vegetarian) sql" AND r.vegetarian" else sql"") ++
      (if(vegan) sql" AND r.vegan" else sql"") ++
      (if(glutenFree) sql" AND r.gluten_free" else sql"") ++
      sql"  GROUP BY r.recipe_id, r.account_id, u.pseudo, r.title, r.instructions " ++
      sql")" ++
      sql"SELECT " ++
      sql"  r.recipe_id" ++
      sql"  , r.account_id" ++
      sql"  , r.pseudo" ++
      sql"  , r.title" ++
      sql"  , r.description" ++
      sql"  , r.instructions" ++
      sql"  , r.source_url" ++
      sql"  , r.servings" ++
      sql"  , r.nutri_score" ++
      sql"  , r.preparation_time_minutes" ++
      sql"  , r.pescetarian, r.vegetarian, r.vegan, r.gluten_free" ++
      sql"  , r.january, r.february, r.march, r.april, r.may, r.june, r.july, r.august, r.september, r.october, r.november, r.december" ++
      sql"  , r.energy_kilo_calories " ++
      sql"  , r.lipid_g " ++
      sql"  , r.saturated_fatty_acid_g " ++
      sql"  , r.carbohydrate_g " ++
      sql"  , r.sugar_g " ++
      sql"  , r.dietary_fiber_g " ++
      sql"  , r.protein_g " ++
      sql"  , r.salt_g " ++
      sql"  , r.sodium_mg " ++
      sql"  , r.base_64_encoded_picture" ++
      sql"  , " ++ accountId.map(_ => sql"f.added_tsp IS NOT NULL").getOrElse(sql"FALSE") ++ sql" as isFavourite " ++
      sql"FROM Recipes_With_Agg_Ingredients r " ++
      accountId.map(u => sql"LEFT OUTER JOIN " ++ Fragment.const(favouriteRecipeTableName) ++ sql" f ON r.recipe_id = f.recipe_id AND f.account_id = $u ").getOrElse(sql"") ++
      sql"WHERE r.food_ids @> " ++ Fragment.const(foodIdsArray) ++
      sql" ORDER BY r.title"
      )
      .query[Recipe]
      .to[List]
      .transact(xa)
      .attempt.unsafeToFuture()
  }

  def selectRecipeIngredients(recipeId: Int)(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[(Food, FoodUnit, Double)]]] = {
    (sql"SELECT " ++
      sql"  f.food_id " ++
      sql"  , f.food_name_fr " ++
      sql"  , f.nutri_score_tev" ++
      sql"  , f.pescetarian, f.vegetarian, f.vegan, f.gluten_free" ++
      sql"  , f.january, f.february, f.march, f.april, f.may, f.june, f.july, f.august, f.september, f.october, f.november, f.december" ++
      sql"  , f.energy_kilo_calories " ++
      sql"  , f.lipid_g " ++
      sql"  , f.saturated_fatty_acid_g " ++
      sql"  , f.carbohydrate_g " ++
      sql"  , f.sugar_g " ++
      sql"  , f.dietary_fiber_g " ++
      sql"  , f.protein_g " ++
      sql"  , f.salt_g " ++
      sql"  , f.sodium_mg " ++
      sql"  , fu.unit_id " ++
      sql"  , fu.unit_name_fr " ++
      sql"  , fu.grams " ++
      sql"  , i.quantity " ++
      sql"FROM " ++ Fragment.const(ingredientTableName) ++ sql" i " ++
      sql"INNER JOIN " ++ Fragment.const(foodUnitTableName) ++ sql" fu ON i.food_id = fu.food_id AND i.unit_id = fu.unit_id " ++
      sql"INNER JOIN " ++ Fragment.const(foodTableName) ++ sql" f ON fu.food_id = f.food_id " ++
      sql"INNER JOIN " ++ Fragment.const(recipeTableName) ++ sql" r ON i.recipe_id = r.recipe_id " ++
      sql"INNER JOIN " ++ Fragment.const(accountTableName) ++ sql" a ON r.account_id = a.account_id " ++
      sql"WHERE " ++
      sql"  r.recipe_id = $recipeId " ++
      sql"  AND not (r.deleted) " ++
      sql"  AND not (a.deleted) " ++
      sql"ORDER BY f.food_name_fr"
      )
      .query[(FoodWithFoodUnit, Double)]
      .to[List]
      .transact(xa)
      .attempt
      .unsafeToFuture()
      .map(_.map(_.map(t => (t._1.toFood, t._1.toFoodUnit, t._2))))
  }

  def insertRecipe(
    accountId: Int,
    title: String,
    description: String,
    instructions: List[String],
    sourceURL: Option[String],
    servings: Int,
    nutriScore: String,
    preparationTimeMinutes: Double,
    base64EncodedPicture: Option[String],
    energyKiloCalories: Double,
    lipidGram: Double,
    saturatedFattyAcidGram: Double,
    carbohydrateGram: Double,
    sugarGram: Double,
    dietaryFiberGram: Double,
    proteinGram: Double,
    saltGram: Double,
    sodiumMilliGram: Double,
    pescetarian: Boolean,
    vegetarian: Boolean,
    vegan: Boolean,
    glutenFree: Boolean,
    january: Boolean,
    february: Boolean,
    march: Boolean,
    april: Boolean,
    may: Boolean,
    june: Boolean,
    july: Boolean,
    august: Boolean,
    september: Boolean,
    october: Boolean,
    november: Boolean,
    december: Boolean
  ): Future[Either[Throwable, List[Int]]] = {

    val formattedSourceURL = sourceURL.map(s => sql"$s").getOrElse(sql"null")
    val formattedPicture = base64EncodedPicture.map(s => sql"$s").getOrElse(sql"null")
    (sql"INSERT INTO " ++ Fragment.const(recipeTableName) ++ sql" (account_id, title, description, instructions, source_url, servings, nutri_score, preparation_time_minutes, pescetarian, vegetarian, vegan, gluten_free, january, february, march, april, may, june, july, august, september, october, november, december, energy_kilo_calories, lipid_g, saturated_fatty_acid_g, carbohydrate_g, sugar_g, dietary_fiber_g, protein_g, salt_g, sodium_mg, base_64_encoded_picture) " ++
      sql"VALUES ($accountId, $title, $description, $instructions, " ++ formattedSourceURL ++ sql", $servings, ${nutriScore.toString}, $preparationTimeMinutes, $pescetarian, $vegetarian, $vegan, $glutenFree, $january, $february, $march, $april, $may, $june, $july, $august, $september, $october, $november, $december, $energyKiloCalories, $lipidGram, $saturatedFattyAcidGram, $carbohydrateGram, $sugarGram, $dietaryFiberGram, $proteinGram, $saltGram, $sodiumMilliGram, " ++ formattedPicture ++ sql")" ++
      sql"RETURNING recipe_id"
      )
      .query[Int]
      .to[List]
      .transact(xa)
      .attempt.unsafeToFuture()
  }

  def hideRecipe(recipeId: Int): Future[Either[Throwable, Int]] = {
    (sql"UPDATE " ++ Fragment.const(recipeTableName) ++ sql" SET deleted = true WHERE recipe_id = $recipeId")
      .update.run.transact(xa).attempt.unsafeToFuture()
  }

  def updateRecipe(
    recipeId: Int,
    title: String,
    description: String,
    instructions: List[String],
    sourceURL: Option[String],
    servings: Int,
    nutriScore: String,
    preparationTimeMinutes: Double,
    base64EncodedPicture: Option[String],
    energyKiloCalories: Double,
    lipidGram: Double,
    saturatedFattyAcidGram: Double,
    carbohydrateGram: Double,
    sugarGram: Double,
    dietaryFiberGram: Double,
    proteinGram: Double,
    saltGram: Double,
    sodiumMilliGram: Double,
    pescetarian: Boolean,
    vegetarian: Boolean,
    vegan: Boolean,
    glutenFree: Boolean,
    january: Boolean,
    february: Boolean,
    march: Boolean,
    april: Boolean,
    may: Boolean,
    june: Boolean,
    july: Boolean,
    august: Boolean,
    september: Boolean,
    october: Boolean,
    november: Boolean,
    december: Boolean
  ): Future[Either[Throwable, Int]] = {

    val formattedSourceURL = sourceURL.map(s => sql"$s").getOrElse(sql"null")
    val formattedPicture = base64EncodedPicture.map(s => sql"$s").getOrElse(sql"null")

    (sql"UPDATE " ++ Fragment.const(recipeTableName) ++ sql" " ++
      sql"SET title = $title, description = $description, instructions = $instructions, source_url = " ++ formattedSourceURL ++
      sql", servings = $servings, nutri_score = ${nutriScore.toString}, preparation_time_minutes = $preparationTimeMinutes " ++
      sql", pescetarian = $pescetarian, vegetarian = $vegetarian, vegan = $vegan, gluten_free = $glutenFree" ++
      sql", january = $january, february = $february, march = $march, april = $april, may = $march, june = $june, july = $july" ++
      sql", august = $august, september = $september, october = $october, november = $november, december = $december" ++
      sql", energy_kilo_calories = $energyKiloCalories, lipid_g = $lipidGram, saturated_fatty_acid_g =  $saturatedFattyAcidGram" ++
      sql", carbohydrate_g = $carbohydrateGram, sugar_g = $sugarGram, dietary_fiber_g = $dietaryFiberGram, protein_g = $proteinGram" ++
      sql", salt_g = $saltGram, sodium_mg = $sodiumMilliGram, base_64_encoded_picture = " ++ formattedPicture ++ sql" " ++
      sql"WHERE " ++
      sql"  recipe_id = $recipeId " ++
      sql"  AND NOT (deleted)"
    ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def insertIngredients(ingredients: List[NewRecipeIngredient]): Future[Either[Throwable, Int]] = {
    (sql"INSERT INTO " ++ Fragment.const(ingredientTableName) ++ sql"(recipe_id, food_id, unit_id, quantity) VALUES" ++
      ingredients.map(i => sql"(${i.recipeId}, ${i.foodId}, ${i.unitId}, ${i.quantity})").reduce(_ ++ sql"," ++ _)
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def deleteRecipeIngredients(recipeId: Int): Future[Either[Throwable, Int]] = {
    (sql"DELETE FROM " ++ Fragment.const(ingredientTableName) ++ sql" " ++
      sql"WHERE " ++
      sql"  recipe_id = $recipeId " ++
      sql"  AND recipe_id IN (SELECT recipe_id FROM " ++ Fragment.const(recipeTableName) ++ sql" WHERE not deleted)"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }


  // TEST FUNCTIONS


  def insertTestRecipes(recipes: List[Recipe]): Int = {
    (sql"INSERT INTO " ++ Fragment.const(recipeTableName) ++ sql"(recipe_id, account_id, title, description, instructions, source_url, servings, nutri_score, preparation_time_minutes, pescetarian, vegetarian, vegan, gluten_free, january, february, march, april, may, june, july, august, september, october, november, december, energy_kilo_calories, lipid_g, saturated_fatty_acid_g, carbohydrate_g, sugar_g, dietary_fiber_g, protein_g, salt_g, sodium_mg, base_64_encoded_picture) VALUES" ++
      recipes
        .map { r =>
          val formattedSourceURL = r.sourceURL.map(s => sql"$s").getOrElse(sql"null")
          val formattedPicture = r.picture.map(s => sql"$s").getOrElse(sql"null")
          sql"(${r.recipeId}, ${r.accountId}, ${r.title}, ${r.description}, ${r.instructions}, " ++ formattedSourceURL ++ sql", ${r.servings}, ${r.nutriScore.toString}, ${r.preparationTimeMinutes}, ${r.pescetarian}, ${r.vegetarian}, ${r.vegan}, ${r.glutenFree}, ${r.january}, ${r.february}, ${r.march}, ${r.april}, ${r.may}, ${r.june}, ${r.july}, ${r.august}, ${r.september}, ${r.october}, ${r.november}, ${r.december}, ${r.energyKiloCalories}, ${r.lipidGram}, ${r.saturatedFattyAcidGram}, ${r.carbohydrateGram}, ${r.sugarGram}, ${r.dietaryFiberGram}, ${r.proteinGram}, ${r.saltGram}, ${r.sodiumMilliGram}, " ++ formattedPicture ++ sql")"
        }
        .reduce(_ ++ sql"," ++ _)
      ).update.run.transact(xa).unsafeRunSync()
  }

  def insertTestIngredients(ingredients: List[NewRecipeIngredient]): Int = {
    (sql"INSERT INTO " ++ Fragment.const(ingredientTableName) ++ sql"(recipe_id, food_id, unit_id, quantity) VALUES" ++
      ingredients.map(i => sql"(${i.recipeId}, ${i.foodId}, ${i.unitId}, ${i.quantity})").reduce(_ ++ sql"," ++ _)
      ).update.run.transact(xa).unsafeRunSync()
  }
}
