package com.storage

import java.sql.Timestamp
import java.time.{LocalDateTime, ZoneOffset}

import com.model.{Alphabetical, CreatedDateTime, ShoppingList, ShoppingListItem, ShoppingListOrdering}
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import doobie.util.fragment.Fragment

import scala.concurrent.{ExecutionContext, Future}

object ShoppingListStorage extends BaseStorage {

  private def createShoppingListTable(): Unit = {
    (sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(shoppingListTableName) ++
      sql"(shopping_list_id SERIAL PRIMARY KEY, " ++
      sql"account_id INT NOT NULL REFERENCES " ++ Fragment.const(accountTableName) ++ sql"(account_id), " ++
      sql"added_tsp TIMESTAMP NOT NULL, " ++
      sql"title VARCHAR NOT NULL)"
      ).update.run.transact(xa).unsafeRunSync()
  }

  private def createShoppingListItemsTable(): Unit = {
    (sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(shoppingListItemsTableName) ++
      sql"(shopping_list_id INT REFERENCES " ++ Fragment.const(shoppingListTableName) ++ sql"(shopping_list_id), " ++
      sql"item_id INT NOT NULL, " ++
      sql"ticked BOOLEAN NOT NULL, " ++
      sql"indented BOOLEAN NOT NULL, " ++
      sql"item_text VARCHAR NOT NULL, " ++
      sql"PRIMARY KEY (shopping_list_id, item_id))"
      ).update.run.transact(xa).unsafeRunSync()
  }

  private def dropShoppingListTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(shoppingListTableName)).update.run.transact(xa).unsafeRunSync()
  }

  private def dropShoppingListItemsTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(shoppingListItemsTableName)).update.run.transact(xa).unsafeRunSync()
  }

  def createTables(): Unit = {
    createShoppingListTable()
    createShoppingListItemsTable()
  }

  def dropTables(): Unit = {
    dropShoppingListItemsTable()
    dropShoppingListTable()
  }


  // MAIN FUNCTIONS


  def insertShoppingList(accountId: Int, added_timestamp: LocalDateTime, title: String): Future[Either[Throwable, List[Int]]] = {
    (sql"INSERT INTO " ++ Fragment.const(shoppingListTableName) ++ sql"(account_id, added_tsp, title) " ++
      sql"VALUES ($accountId, " ++ Fragment.const(s"\'${Timestamp.valueOf(added_timestamp).toString}\'") ++ sql", $title)" ++
      sql"RETURNING shopping_list_id"
      ).query[Int].to[List].transact(xa).attempt.unsafeToFuture()
  }

  def insertShoppingListItems(shoppingListItems: List[ShoppingListItem]): Future[Either[Throwable, Int]] = {

    val shoppingListItemsFormatted = shoppingListItems
      .map { i => sql"(${i.shoppingListId}, ${i.itemId}, ${i.ticked}, ${i.indented}, ${i.text})" }
      .reduce(_ ++ sql"," ++ _)

    (sql"INSERT INTO " ++ Fragment.const(shoppingListItemsTableName) ++ sql"(shopping_list_id, item_id, ticked, indented, item_text) VALUES " ++ shoppingListItemsFormatted
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def selectShoppingListByAccountId(accountId: Int, ordering: ShoppingListOrdering)(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[ShoppingList]]] = {
    val orderingStatement = ordering match {
      case Alphabetical => sql"ORDER BY title"
      case CreatedDateTime => sql"ORDER BY added_tsp DESC"
    }

    (sql"SELECT account_id, shopping_list_id, EXTRACT(EPOCH FROM added_tsp), title " ++
      sql"FROM " ++ Fragment.const(shoppingListTableName) ++ sql" " ++
      sql"WHERE account_id = $accountId " ++
      orderingStatement
      )
      .query[(Int, Int, Long, String)].to[List].transact(xa).attempt.unsafeToFuture()
      .map(_.map(_.map { case (accountId, shoppingListId, addedTimestampEpoch, title) =>
        ShoppingList(accountId, shoppingListId, LocalDateTime.ofEpochSecond(addedTimestampEpoch, 0, ZoneOffset.UTC), title)
      }))
  }

  def selectShoppingListByShoppingListId(shoppingListId: Int)(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[ShoppingList]]] = {
    (sql"SELECT account_id, shopping_list_id, EXTRACT(EPOCH FROM added_tsp), title " ++
      sql"FROM " ++ Fragment.const(shoppingListTableName) ++ sql" " ++
      sql"WHERE shopping_list_id = $shoppingListId"
      )
      .query[(Int, Int, Long, String)].to[List].transact(xa).attempt.unsafeToFuture()
      .map(_.map(_.map { case (accountId, shoppingListId, addedTimestampEpoch, title) =>
        ShoppingList(accountId, shoppingListId, LocalDateTime.ofEpochSecond(addedTimestampEpoch, 0, ZoneOffset.UTC), title)
      }))
  }

  def selectShoppingListItems(shoppingListId: Int): Future[Either[Throwable, List[ShoppingListItem]]] = {
    (sql"SELECT * FROM " ++ Fragment.const(shoppingListItemsTableName) ++ sql" WHERE shopping_list_id = $shoppingListId ORDER BY item_id"
      ).query[ShoppingListItem].to[List].transact(xa).attempt.unsafeToFuture()
  }

  def selectMaxItemIdByShoppingListId(shoppingListId: Int): Future[Either[Throwable, List[(Int, Int, Int)]]] = {
    (sql"WITH SHOPPING_LIST_MAX_ITEM_ID AS ( " ++
      sql"SELECT s.shopping_list_id, s.account_id, max(si.item_id) as max_item_id " ++
      sql"FROM " ++ Fragment.const(shoppingListTableName) ++ sql" s " ++
      sql"LEFT OUTER JOIN " ++ Fragment.const(shoppingListItemsTableName) ++ sql" si ON s.shopping_list_id = si.shopping_list_id " ++
      sql"WHERE s.shopping_list_id = $shoppingListId " ++
      sql"GROUP BY s.shopping_list_id, account_id" ++
      sql")" ++
      sql"SELECT shopping_list_id, account_id, COALESCE(max_item_id, 0) FROM SHOPPING_LIST_MAX_ITEM_ID"
      ).query[(Int, Int, Int)].to[List].transact(xa).attempt.unsafeToFuture()
  }

  def deleteShoppingListItem(shoppingListId: Int, itemId: Int): Future[Either[Throwable, Int]] = {
    (sql"DELETE FROM " ++ Fragment.const(shoppingListItemsTableName) ++ sql" * " ++
      sql"WHERE shopping_list_id = $shoppingListId AND item_id = $itemId"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def deleteShoppingListItems(shoppingListId: Int): Future[Either[Throwable, Int]] = {
    (sql"DELETE FROM " ++ Fragment.const(shoppingListItemsTableName) ++ sql" * " ++
      sql"WHERE shopping_list_id = $shoppingListId"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def deleteShoppingList(shoppingListId: Int): Future[Either[Throwable, Int]] = {
    (sql"DELETE FROM " ++ Fragment.const(shoppingListTableName) ++ sql" * " ++
      sql"WHERE shopping_list_id = $shoppingListId"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def updateShoppingListItem(shoppingListId: Int, itemId: Int, ticked: Option[Boolean], indented: Option[Boolean], text: Option[String])
    (implicit executionContext: ExecutionContext): Future[Either[Throwable, Int]] = {

    val setStatement: Fragment = (indented, ticked, text) match {
      case (None, None, None) => throw new IllegalArgumentException("indented, ticked or text must be defined")
      case (Some(i), None, None) => sql" SET indented = $i "
      case (None, Some(t), None) => sql" SET ticked = $t "
      case (Some(i), Some(t), None) => sql" SET (indented, ticked) = ($i, $t) "
      case (None, None, Some(txt)) => sql"SET item_text = $txt"
      case (Some(i), None, Some(txt)) => sql"SET (indented, item_text) = ($i, $txt)"
      case (None, Some(t), Some(txt)) => sql"SET (ticked, item_text) = ($t, $txt)"
      case (Some(i), Some(t), Some(txt)) => sql"SET (indented, ticked, item_text) = ($i, $t, $txt)"
    }

    (sql"UPDATE " ++ Fragment.const(shoppingListItemsTableName) ++ setStatement ++
      sql"WHERE shopping_list_id = $shoppingListId AND item_id = $itemId"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def updateShoppingListTitle(shoppingListId: Int, title: String): Future[Either[Throwable, Int]] = {
    (sql"UPDATE " ++ Fragment.const(shoppingListTableName) ++ sql" SET title = $title " ++
      sql"WHERE shopping_list_id = $shoppingListId"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }


  // TEST FUNCTIONS


  def insertTestShoppingLists(shoppingLists: List[ShoppingList]): Int = {

    val shoppingListsFormatted = shoppingLists
      .map(s => sql"(${s.shoppingListId}, ${s.accountId}, " ++ Fragment.const(s"\'${Timestamp.valueOf(s.addedTime).toString}\'") ++ sql", ${s.title})")
      .reduce(_ ++ sql"," ++ _)

    (sql"INSERT INTO " ++ Fragment.const(shoppingListTableName) ++ sql"(shopping_list_id, account_id, added_tsp, title) VALUES " ++ shoppingListsFormatted
      ).update.run.transact(xa).unsafeRunSync()
  }

  def insertTestShoppingListItems(shoppingListItems: List[ShoppingListItem]): Int = {

    val shoppingListItemsFormatted = shoppingListItems
      .map(i => sql"(${i.shoppingListId}, ${i.itemId}, ${i.ticked}, ${i.indented}, ${i.text})")
      .reduce(_ ++ sql"," ++ _)

    (sql"INSERT INTO " ++ Fragment.const(shoppingListItemsTableName) ++ sql"(shopping_list_id, item_id, ticked, indented, item_text) VALUES " ++ shoppingListItemsFormatted
      ).update.run.transact(xa).unsafeRunSync()
  }
}
