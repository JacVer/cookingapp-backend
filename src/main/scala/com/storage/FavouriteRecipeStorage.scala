package com.storage

import java.sql.Timestamp
import java.time.LocalDateTime

import com.model.recipe.Recipe
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import doobie.util.fragment.Fragment

import scala.concurrent.{ExecutionContext, Future}

object FavouriteRecipeStorage extends BaseStorage {

  private def createFavouriteRecipeTable(): Unit = {
    (sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(favouriteRecipeTableName)) ++
      sql"(account_id INT NOT NULL REFERENCES " ++ Fragment.const(accountTableName) ++ sql"(account_id), " ++
      sql"recipe_id INT NOT NULL REFERENCES " ++ Fragment.const(recipeTableName) ++ sql"(recipe_id), " ++
      sql"added_tsp TIMESTAMP NOT NULL, " ++
      sql"PRIMARY KEY (account_id, recipe_id))"
  }.update.run.transact(xa).unsafeRunSync()

  private def dropFavouriteRecipeTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(favouriteRecipeTableName)).update.run.transact(xa).unsafeRunSync()
  }

  def createTables(): Unit = createFavouriteRecipeTable()

  def dropTables(): Unit = dropFavouriteRecipeTable()

  def insertFavouriteRecipe(accountId: Int, recipeId: Int, addedDateTime: LocalDateTime): Future[Either[Throwable, Int]] = {
    val formattedTimestamp = Fragment.const(s"\'${Timestamp.valueOf(addedDateTime).toString}\'")
    (sql"INSERT INTO " ++ Fragment.const(favouriteRecipeTableName) ++ sql" (account_id, recipe_id, added_tsp) " ++
      sql"VALUES ($accountId, $recipeId, " ++ formattedTimestamp ++ sql")"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def deleteFavouriteRecipe(accountId: Int, recipeId: Int): Future[Either[Throwable, Int]] = {
    (sql"DELETE FROM " ++ Fragment.const(favouriteRecipeTableName) ++ sql" * WHERE account_id = $accountId AND recipe_id = $recipeId"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def selectFavouriteRecipe(accountId: Int)(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[Recipe]]] = {
    (sql"SELECT " ++
      sql"  r.recipe_id" ++
      sql"  , r.account_id" ++
      sql"  , u.pseudo" ++
      sql"  , r.title" ++
      sql"  , r.description" ++
      sql"  , r.instructions" ++
      sql"  , r.source_url" ++
      sql"  , r.servings" ++
      sql"  , r.nutri_score" ++
      sql"  , r.preparation_time_minutes" ++
      sql"  , r.pescetarian, r.vegetarian, r.vegan, r.gluten_free" ++
      sql"  , r.january, r.february, r.march, r.april, r.may, r.june, r.july, r.august, r.september, r.october, r.november, r.december" ++
      sql"  , r.energy_kilo_calories " ++
      sql"  , r.lipid_g " ++
      sql"  , r.saturated_fatty_acid_g " ++
      sql"  , r.carbohydrate_g " ++
      sql"  , r.sugar_g " ++
      sql"  , r.dietary_fiber_g " ++
      sql"  , r.protein_g " ++
      sql"  , r.salt_g " ++
      sql"  , r.sodium_mg " ++
      sql"  , r.base_64_encoded_picture" ++
      sql"  , TRUE as is_favourite " ++
      sql"FROM " ++ Fragment.const(recipeTableName) ++ sql" r " ++
      sql"  INNER JOIN " ++ Fragment.const(accountTableName) ++ sql" u ON r.account_id = u.account_id " ++
      sql"  INNER JOIN " ++ Fragment.const(favouriteRecipeTableName) ++ sql" f ON r.recipe_id = f.recipe_id " ++
      sql"WHERE " ++
      sql"  f.account_id = $accountId " ++
      sql"  AND r.deleted = FALSE " ++
      sql"ORDER BY f.added_tsp"
      )
      .query[Recipe]
      .to[List]
      .transact(xa)
      .attempt.unsafeToFuture()
  }


  // TEST FUNCTIONS


  def insertTestFavouriteRecipes(favouriteRecipes: List[(Int, Int, LocalDateTime)]): Int = {
    val favouriteRecipesFormatted = favouriteRecipes.map { case (accountId, recipeId, addedDateTime) =>
      sql"($accountId, $recipeId, " ++ Fragment.const(s"\'${Timestamp.valueOf(addedDateTime).toString}\'") ++ sql")"
    }.reduce(_ ++ sql"," ++ _)
    (sql"INSERT INTO " ++ Fragment.const(favouriteRecipeTableName) ++ sql"(account_id, recipe_id, added_tsp) VALUES " ++ favouriteRecipesFormatted
      ).update.run.transact(xa).unsafeRunSync()
  }
}
