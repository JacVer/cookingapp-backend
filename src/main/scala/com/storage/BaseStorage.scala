package com.storage

import cats.effect.{Blocker, ContextShift, IO}
import com.typesafe.config.{Config, ConfigFactory}
import doobie.util.transactor.Transactor
import doobie.util.transactor.Transactor.Aux

trait BaseStorage {

  implicit val cs: ContextShift[IO] = IO.contextShift(scala.concurrent.ExecutionContext.global)

  val conf: Config = ConfigFactory.load
  val xa: Aux[IO, Unit] = Transactor.fromDriverManager[IO](
    driver = "org.postgresql.Driver",
    url = conf.getString("postgres.url"),
    user = conf.getString("postgres.userName"),
    pass = conf.getString("postgres.password"),
    Blocker.liftExecutionContext(scala.concurrent.ExecutionContext.global)
  )

  val accountTableName = "account"
  val recipeTableName = "recipe"
  val foodTableName = "food"
  val ingredientTableName = "ingredient"
  val favouriteRecipeTableName = "favourite_recipe"
  val foodUnitTableName = "food_unit"
  val shoppingListTableName = "shopping_list"
  val shoppingListItemsTableName = "shopping_list_items"
  val recipeRatingTableName = "recipe_rating"
  val recipeCommentTableName = "recipe_comment"
}
