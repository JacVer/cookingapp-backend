package com.storage

import com.model.Account
import doobie.implicits._
import doobie.util.fragment.Fragment

import scala.concurrent.Future

object AccountStorage extends BaseStorage {

  private def createAccountTable(): Unit = {
    (sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(accountTableName) ++
      sql"(account_id SERIAL PRIMARY KEY, " ++
      sql"email VARCHAR NOT NULL UNIQUE, " ++
      sql"pseudo VARCHAR NOT NULL UNIQUE, " ++
      sql"password_hash VARCHAR NOT NULL," ++
      sql"deleted BOOLEAN NOT NULL DEFAULT false)"
      ).update.run.transact(xa).unsafeRunSync()
  }

  private def dropAccountTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(accountTableName)).update.run.transact(xa).unsafeRunSync()
  }

  def createTables(): Unit = createAccountTable()

  def dropTables(): Unit = dropAccountTable()

  def selectAccountWithEmail(email: String): Future[Either[Throwable, List[Account]]] = {
    (sql"SELECT account_id, email, pseudo, password_hash FROM " ++ Fragment.const(accountTableName) ++ sql"WHERE email = $email AND NOT deleted")
      .query[Account].to[List].transact(xa).attempt.unsafeToFuture()
  }

  def selectAccountWithPseudo(pseudo: String): Future[Either[Throwable, List[Account]]] = {
    (sql"SELECT account_id, email, pseudo, password_hash FROM " ++ Fragment.const(accountTableName) ++ sql"WHERE pseudo = $pseudo AND NOT deleted")
      .query[Account].to[List].transact(xa).attempt.unsafeToFuture()
  }

  def selectAccountWithAccountId(accountId: Int): Future[Either[Throwable, List[Account]]] = {
    (sql"SELECT account_id, email, pseudo, password_hash FROM " ++ Fragment.const(accountTableName) ++ sql"WHERE account_id = $accountId AND NOT deleted")
      .query[Account].to[List].transact(xa).attempt.unsafeToFuture()
  }

  def insertAccount(email: String, pseudo: String, passwordHash: String): Future[Either[Throwable, List[Account]]] = {
    (sql"INSERT INTO " ++ Fragment.const(accountTableName) ++ sql" (email, pseudo, password_hash) VALUES ($email, $pseudo, $passwordHash) RETURNING account_id, email, pseudo, password_hash")
      .query[Account].to[List].transact(xa).attempt.unsafeToFuture()
  }

  def deleteAccount(email: String): Future[Either[Throwable, Int]] = {
    (sql"DELETE FROM " ++ Fragment.const(accountTableName) ++ sql" WHERE email = $email")
      .update.run.transact(xa).attempt.unsafeToFuture()
  }

  def hideAccount(accountId: Int): Future[Either[Throwable, Int]] = {
    (sql"UPDATE " ++ Fragment.const(accountTableName) ++ sql" SET deleted = true WHERE account_id = $accountId")
      .update.run.transact(xa).attempt.unsafeToFuture()
  }

  def insertTestAccounts(accounts: List[Account]): Int = {
    (sql"INSERT INTO " ++ Fragment.const(accountTableName) ++ sql"(account_id, email, pseudo, password_hash) VALUES" ++
      accounts.map(u => sql"(${u.accountId}, ${u.email}, ${u.pseudo}, ${u.passwordHash})").reduce(_ ++ sql"," ++ _)
      ).update.run.transact(xa).unsafeRunSync()
  }
}
