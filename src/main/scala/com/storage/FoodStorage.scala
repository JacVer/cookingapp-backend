package com.storage

import java.sql.Timestamp
import java.time.LocalDateTime

import com.github.tototoshi.csv.CSVReader
import com.model.food.{CalnutFood, Food, FoodUnit, FoodWithFoodUnit}
import doobie.implicits._
import doobie.implicits.javasql.TimestampMeta
import doobie.util.fragment.Fragment

import scala.concurrent.{ExecutionContext, Future}

object FoodStorage extends BaseStorage {

  private def createFoodTable(): Unit = {
    val query = sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(foodTableName) ++ sql"""(
      added_tsp TIMESTAMP NOT NULL,
      category_id INT,
      category_name_fr VARCHAR,
      food_id SERIAL PRIMARY KEY,
      food_name_fr VARCHAR NOT NULL UNIQUE,
      nutri_score_tev BOOLEAN NOT NULL,
      pescetarian BOOLEAN,
      vegetarian BOOLEAN,
      vegan BOOLEAN,
      gluten_free BOOLEAN,
      january BOOLEAN,
      february BOOLEAN,
      march BOOLEAN,
      april BOOLEAN,
      may BOOLEAN,
      june BOOLEAN,
      july BOOLEAN,
      august BOOLEAN,
      september BOOLEAN,
      october BOOLEAN,
      november BOOLEAN,
      december BOOLEAN,
      energy_kilo_calories REAL NOT NULL,
      water_g REAL,
      salt_g REAL,
      sodium_mg REAL NOT NULL,
      magnesium_mg REAL,
      phosphorus_mg REAL,
      potassium_mg REAL,
      calcium_mg REAL,
      manganese_mg REAL,
      iron_mg REAL,
      copper_mg REAL,
      zinc_mg REAL,
      selenium_mcg REAL,
      iodine_mcg REAL,
      protein_g REAL NOT NULL,
      carbohydrate_g REAL,
      sugar_g REAL NOT NULL,
      fructose_g REAL,
      galactose_g REAL,
      lactose_g REAL,
      glucose_g REAL,
      maltose_g REAL,
      sucrose_g REAL,
      starch_g REAL,
      polyalcohol_g REAL,
      dietary_fiber_g REAL NOT NULL,
      lipid_g REAL,
      saturated_fatty_acid_g REAL NOT NULL,
      mono_unsaturated_fatty_acid_g REAL,
      poly_unsaturated_fatty_acid_g REAL,
      butyric_acid_g REAL,
      caproic_acid_g REAL,
      caorylic_acid_g REAL,
      capric_acid_g REAL,
      lauric_acid_g REAL,
      mystiric_acid_g REAL,
      palmitic_acid_g REAL,
      stearic_acid_g REAL,
      oleic_acid_g REAL,
      linoleic_acid_g REAL,
      alpha_linoleic_acid_g REAL,
      arachidonic_acid_g REAL,
      eicosapentaenoic_acid_g REAL,
      docosahexaenoic_acid_g REAL,
      retinol_mcg REAL,
      beta_carotene_mcg REAL,
      vitamin_d_mcg REAL,
      vitamin_e_mg REAL,
      vitamin_k1_mcg REAL,
      vitamin_k2_mcg REAL,
      vitamin_c_mg REAL,
      vitamin_b1_mg REAL,
      vitamin_b2_mg REAL,
      vitamin_b3_mg REAL,
      vitamin_b5_mg REAL,
      vitamin_b6_mg REAL,
      vitamin_b12_mcg REAL,
      vitamin_b9_mcg REAL,
      alcohol_g REAL,
      organic_acid_g REAL,
      cholesterol_mg REAL)"""
    query.update.run.transact(xa).unsafeRunSync()
  }

  private def dropFoodTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(foodTableName)).update.run.transact(xa).unsafeRunSync()
  }

  def createTables(): Unit = createFoodTable()

  def dropTables(): Unit = dropFoodTable()


  // MAIN FUNCTIONS


  // TODO : do something about accents and "œ" characters
  def readCalnutCsv(csvPath: String): Iterator[CalnutFood] = {

    def strToBoolean(str: String): Boolean =
      if(str == "0") false
      else if(str == "1") true
      else throw new Error(s"Unexpected value $str should be either 0 or 1")

    def strToDouble(str: String): Double = str.replace(",", ".").toDouble

    val reader: CSVReader = CSVReader.open(csvPath)
    val (_, rows: Iterator[Seq[String]]) = {
      val it = reader.iterator
      (it.next, it)
    }

    rows.map(row =>
      CalnutFood.apply(
        row.head.toInt, row(1), row(2).toInt, row(3), strToBoolean(row(4)), strToBoolean(row(5)), strToBoolean(row(6)), strToBoolean(row(7)), strToBoolean(row(8)), strToBoolean(row(9)), strToBoolean(row(10)), strToBoolean(row(11)), strToBoolean(row(12)), strToBoolean(row(13)), strToBoolean(row(14)), strToBoolean(row(15)), strToBoolean(row(16)), strToBoolean(row(17)), strToBoolean(row(18)), strToBoolean(row(19)), strToBoolean(row(20)), strToDouble(row(21)), strToDouble(row(22)), strToDouble(row(23)), strToDouble(row(24)), strToDouble(row(25)), strToDouble(row(26)), strToDouble(row(27)), strToDouble(row(28)), strToDouble(row(29)), strToDouble(row(30)), strToDouble(row(31)), strToDouble(row(32)), strToDouble(row(33)), strToDouble(row(34)), strToDouble(row(35)), strToDouble(row(36)), strToDouble(row(37)), strToDouble(row(38)), strToDouble(row(39)), strToDouble(row(40)), strToDouble(row(41)), strToDouble(row(42)), strToDouble(row(43)), strToDouble(row(44)), strToDouble(row(45)), strToDouble(row(46)), strToDouble(row(47)), strToDouble(row(48)), strToDouble(row(49)), strToDouble(row(50)), strToDouble(row(51)), strToDouble(row(52)), strToDouble(row(53)), strToDouble(row(54)), strToDouble(row(55)), strToDouble(row(56)), strToDouble(row(57)), strToDouble(row(58)), strToDouble(row(59)), strToDouble(row(60)), strToDouble(row(61)), strToDouble(row(62)), strToDouble(row(63)), strToDouble(row(64)), strToDouble(row(65)), strToDouble(row(66)), strToDouble(row(67)), strToDouble(row(68)), strToDouble(row(69)), strToDouble(row(70)), strToDouble(row(71)), strToDouble(row(72)), strToDouble(row(73)), strToDouble(row(74)), strToDouble(row(75)), strToDouble(row(76)), strToDouble(row(77)), strToDouble(row(78)), strToDouble(row(79)), strToDouble(row(79)), strToDouble(row(80))
      )
    )
  }

  def insertCalnutFoods(timeAdded: LocalDateTime, calnutFoods: List[CalnutFood]): Either[Throwable, Int] = {
    val query = sql"INSERT INTO " ++ Fragment.const(foodTableName) ++ sql" VALUES " ++
      calnutFoods
        .map(f => sql"(${Timestamp.valueOf(timeAdded)}, ${f.categoryId}, ${f.categoryNameFr}, ${f.foodId}, ${f.foodNameFr}, ${f.nutriScoreTEV}, ${f.pescetarian}, ${f.vegetarian}, ${f.vegan}, ${f.glutenFree}, ${f.january}, ${f.february}, ${f.march}, ${f.april}, ${f.may}, ${f.june}, ${f.july}, ${f.august}, ${f.september}, ${f.october}, ${f.november}, ${f.december}, ${f.energyKiloCalories}, ${f.waterGram}, ${f.saltGram}, ${f.sodiumMilliGram}, ${f.magnesiumMilliGram}, ${f.phosphorusMilliGram}, ${f.potassiumMilliGram}, ${f.calciumMilliGram}, ${f.manganeseMilliGram}, ${f.ironMilliGram}, ${f.copperMilliGram}, ${f.zincMilliGram}, ${f.seleniumMicroGram}, ${f.iodineMicroGram}, ${f.proteinGram}, ${f.carbohydrateGram}, ${f.sugarGram}, ${f.fructoseGram}, ${f.galactoseGram}, ${f.lactoseGram}, ${f.glucoseGram}, ${f.maltoseGram}, ${f.sucroseGram}, ${f.starchGram}, ${f.polyalcoholGram}, ${f.dietaryFiberGram}, ${f.lipidGram}, ${f.saturatedFattyAcidGram}, ${f.monoUnsaturatedFattyAcidGram}, ${f.polyUnsaturatedFattyAcidGram}, ${f.butyricAcidGram}, ${f.caproicAcidGram}, ${f.caorylicAcidGram}, ${f.capricAcidGram}, ${f.lauricAcidGram}, ${f.mystiricAcidGram}, ${f.palmiticAcidGram}, ${f.stearicAcidGram}, ${f.oleicAcidGram}, ${f.linoleicAcidGram}, ${f.alphaLinoleicAcidGram}, ${f.arachidonicAcidGram}, ${f.eicosapentaenoicAcidGram}, ${f.docosahexaenoicAcidGram}, ${f.retinolMicroGram}, ${f.betaCaroteneMicroGram}, ${f.vitaminDMicroGram}, ${f.vitaminEMilliGram}, ${f.vitaminK1MicroGram}, ${f.vitaminK2MicroGram}, ${f.vitaminCMilliGram}, ${f.vitaminB1MilliGram}, ${f.vitaminB2MilliGram}, ${f.vitaminB3MilliGram}, ${f.vitaminB5MilliGram}, ${f.vitaminB6MilliGram}, ${f.vitaminB12MicroGram}, ${f.vitaminB9MicroGram}, ${f.alcoholGram}, ${f.organicAcidGram}, ${f.cholesterolMilliGram})")
        .reduce(_ ++ sql"," ++ _)
    query.update.run.transact(xa).attempt.unsafeRunSync()
  }

  def selectFoodWithName(foodNameContains: String)(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[Food]]] = {
    val nameWithPercentages = "%" + foodNameContains.toLowerCase + "%"
    (sql"SELECT " ++
      sql"  food_id" ++
      sql"  , food_name_fr" ++
      sql"  , nutri_score_tev" ++
      sql"  , pescetarian, vegetarian, vegan, gluten_free" ++
      sql"  , january, february, march, april, may, june, july, august, september, october, november, december" ++
      sql"  , energy_kilo_calories" ++
      sql"  , lipid_g" ++
      sql"  , saturated_fatty_acid_g" ++
      sql"  , carbohydrate_g" ++
      sql"  , sugar_g" ++
      sql"  , dietary_fiber_g" ++
      sql"  , protein_g" ++
      sql"  , salt_g " ++
      sql"  , sodium_mg " ++
      sql"FROM " ++ Fragment.const(foodTableName) ++ sql" " ++
      sql"WHERE LOWER(food_name_fr) LIKE $nameWithPercentages " ++
      sql"ORDER BY food_name_fr"
      )
      .query[Food]
      .to[List]
      .transact(xa)
      .attempt
      .unsafeToFuture()
  }

  def selectFoodWithFoodId(foodId: Int)(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[Food]]] = {
    (sql"SELECT " ++
      sql"  food_id" ++
      sql"  , food_name_fr" ++
      sql"  , nutri_score_tev" ++
      sql"  , pescetarian, vegetarian, vegan, gluten_free" ++
      sql"  , january, february, march, april, may, june, july, august, september, october, november, december" ++
      sql"  , energy_kilo_calories" ++
      sql"  , lipid_g" ++
      sql"  , saturated_fatty_acid_g" ++
      sql"  , carbohydrate_g" ++
      sql"  , sugar_g" ++
      sql"  , dietary_fiber_g" ++
      sql"  , protein_g" ++
      sql"  , salt_g " ++
      sql"  , sodium_mg " ++
      sql"FROM " ++ Fragment.const(foodTableName) ++ sql" " ++
      sql"WHERE food_id = $foodId "
      )
      .query[Food]
      .to[List]
      .transact(xa)
      .attempt
      .unsafeToFuture()
  }

  def selectFoodWithFoodUnit(foodUnitId: List[(Int, Int)])(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[(Food, FoodUnit)]]] = {
    (sql"SELECT " ++
      sql"  f.food_id " ++
      sql"  , f.food_name_fr " ++
      sql"  , nutri_score_tev" ++
      sql"  , pescetarian, vegetarian, vegan, gluten_free" ++
      sql"  , january, february, march, april, may, june, july, august, september, october, november, december" ++
      sql"  , energy_kilo_calories " ++
      sql"  , lipid_g " ++
      sql"  , saturated_fatty_acid_g " ++
      sql"  , carbohydrate_g " ++
      sql"  , sugar_g " ++
      sql"  , dietary_fiber_g " ++
      sql"  , protein_g " ++
      sql"  , salt_g " ++
      sql"  , sodium_mg " ++
      sql"  , fu.unit_id " ++
      sql"  , fu.unit_name_fr " ++
      sql"  , fu.grams " ++
      sql"FROM " ++ Fragment.const(foodTableName) ++ sql" f " ++
      sql"INNER JOIN " ++ Fragment.const(foodUnitTableName) ++ sql" fu ON f.food_id = fu.food_id " ++
      sql"WHERE" ++ foodUnitId.map { case (foodId, unitId) => sql" (fu.food_id = $foodId AND fu.unit_id = $unitId) " }.reduce(_ ++ sql"OR" ++ _) ++
      sql"ORDER BY food_id"
      )
      .query[FoodWithFoodUnit]
      .to[List]
      .transact(xa)
      .attempt
      .unsafeToFuture()
      .map(_.map(_.map(f => (f.toFood, f.toFoodUnit))))
  }
}
