package com.storage

import java.sql.Timestamp
import java.time.{LocalDateTime, ZoneOffset}

import com.model.recipe.{RecipeComment, RecipeRating}
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import doobie.util.fragment.Fragment

import scala.concurrent.{ExecutionContext, Future}

object RecipeRatingStorage extends BaseStorage {

  private def createRecipeRatingTable(): Unit = {
    (sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(recipeRatingTableName) ++ sql"(" ++
      sql"recipe_id INT NOT NULL REFERENCES " ++ Fragment.const(recipeTableName) ++ sql"(recipe_id)," ++
      sql"account_id INT NOT NULL REFERENCES " ++ Fragment.const(accountTableName) ++ sql"(account_id)," ++
      sql"added_tsp TIMESTAMP NOT NULL," ++
      sql"rating INT NOT NULL CHECK(rating IN(1, 2, 3, 4, 5))," ++
      sql"PRIMARY KEY (recipe_id, account_id))"
      ).update.run.transact(xa).unsafeRunSync()
  }

  private def createRecipeCommentTable(): Unit = {
    (sql"CREATE TABLE IF NOT EXISTS " ++ Fragment.const(recipeCommentTableName) ++ sql"(" ++
      sql"recipe_id INT NOT NULL REFERENCES " ++ Fragment.const(recipeTableName) ++ sql"(recipe_id)," ++
      sql"account_id INT NOT NULL REFERENCES " ++ Fragment.const(accountTableName) ++ sql"(account_id)," ++
      sql"added_tsp TIMESTAMP NOT NULL," ++
      sql"comment VARCHAR NOT NULL CHECK(length(comment) > 0))"
      ).update.run.transact(xa).unsafeRunSync()
  }

  private def dropRecipeRatingTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(recipeRatingTableName)).update.run.transact(xa).unsafeRunSync()
  }

  private def dropRecipeCommentTable(): Unit = {
    (sql"DROP TABLE IF EXISTS " ++ Fragment.const(recipeCommentTableName)).update.run.transact(xa).unsafeRunSync()
  }

  def createTables(): Unit = {
    createRecipeRatingTable()
    createRecipeCommentTable()
  }

  def dropTables(): Unit = {
    dropRecipeRatingTable()
    dropRecipeCommentTable()
  }


  // MAIN FUNCTIONS


  def insertRating(recipeId: Int, accountId: Int, timeAdded: LocalDateTime, rating: Int): Future[Either[Throwable, Int]] = {
    val formattedTimestamp = Fragment.const(s"\'${Timestamp.valueOf(timeAdded).toString}\'")
    (sql"INSERT INTO " ++ Fragment.const(recipeRatingTableName) ++ sql" (recipe_id, account_id, added_tsp, rating) " ++
      sql"VALUES ($recipeId, $accountId, " ++ formattedTimestamp ++ sql", $rating)"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def deleteRating(recipeId: Int, accountId: Int): Future[Either[Throwable, Int]] = {
    (sql"DELETE FROM " ++ Fragment.const(recipeRatingTableName) ++ sql" * WHERE recipe_id = $recipeId AND account_id = $accountId"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def selectRecipeRating(recipeId: Int): Future[Either[Throwable, List[RecipeRating]]] = {
    (sql"SELECT AVG(rating), COUNT(*) " ++
      sql"FROM " ++ Fragment.const(recipeRatingTableName) ++ sql" " ++
      sql"WHERE recipe_id = $recipeId" ++
      sql"GROUP BY recipe_id"
      )
      .query[RecipeRating]
      .to[List]
      .transact(xa)
      .attempt.unsafeToFuture()
  }

  def insertComment(recipeId: Int, accountId: Int, timeAdded: LocalDateTime, comment: String): Future[Either[Throwable, Int]] = {
    val formattedTimestamp = Fragment.const(s"\'${Timestamp.valueOf(timeAdded).toString}\'")
    (sql"INSERT INTO " ++ Fragment.const(recipeCommentTableName) ++ sql" (recipe_id, account_id, added_tsp, comment) " ++
      sql"VALUES ($recipeId, $accountId, " ++ formattedTimestamp ++ sql", $comment)"
      ).update.run.transact(xa).attempt.unsafeToFuture()
  }

  def selectRecipeComment(recipeId: Int)(implicit executionContext: ExecutionContext): Future[Either[Throwable, List[RecipeComment]]] = {
    (
      sql"SELECT " ++
        sql"  c.recipe_id " ++
        sql"  , CASE WHEN a.deleted THEN NULL ELSE a.pseudo END as pseudo " ++
        sql"  , EXTRACT(EPOCH FROM c.added_tsp) " ++
        sql"  , c.comment " ++
        sql"FROM " ++ Fragment.const(recipeCommentTableName) ++ sql" c " ++
        sql"INNER JOIN " ++ Fragment.const(accountTableName) ++ sql" a ON c.account_id = a.account_id " ++
        sql"WHERE c.recipe_id = $recipeId" ++
        sql"ORDER BY added_tsp"
      )
      .query[(Int, Option[String], Long, String)]
      .to[List]
      .transact(xa)
      .attempt.unsafeToFuture()
      .map(_.map(_.map { case (recipeId, pseudo, epoch, comment) =>
        RecipeComment(recipeId, pseudo, LocalDateTime.ofEpochSecond(epoch, 0, ZoneOffset.UTC), comment)
      }))
  }
}
