package com.helpers

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.helpers.RouteLogging.{ErrorEvent, ExceptionEvent, RequestReceivedEvent, SuccessEvent, formatRouteLog}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import org.slf4j.Logger

import scala.concurrent.Future
import scala.util.{Failure, Success}

object RouteWrapper {
  def wrapRoute[A, B](serviceFunction: => Future[Either[String, A]])(
    toApi: A => io.circe.Json,
    logger: Logger,
    token: Option[String] = None,
    params: Map[String, Any] = Map.empty
  ): Route = {
    extractRequestContext { context =>

      val method = context.request.method
      val uri = context.request.uri.path.toString() + context.request.uri.rawQueryString.map("?" + _).getOrElse("")

      logger.info(formatRouteLog(method, uri, RequestReceivedEvent(), token, params))
      onComplete(serviceFunction) {
        case Success(Left(exception)) =>
          logger.info(formatRouteLog(method, uri, ExceptionEvent(exception), token, params))
          complete((StatusCodes.BadRequest, exception))
        case Success(Right(serviceResult)) =>
          val output = toApi(serviceResult)
          logger.info(formatRouteLog(method, uri, SuccessEvent(), token, params))
          complete((StatusCodes.OK, output))
        case Failure(error) =>
          complete {
            logger.error(formatRouteLog(method, uri, ErrorEvent(error.getMessage), token, params), error)
            (StatusCodes.InternalServerError, "An unexpected error happened, please bear with us for the moment.")
          }
      }
    }
  }
}
