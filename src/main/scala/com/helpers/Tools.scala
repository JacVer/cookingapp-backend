package com.helpers

object Tools {
  def getDuplicatesFromList[A](list: List[A]): List[A] = {

    case class Acc(itemsEncountered: List[A], duplicates: List[A])

    @scala.annotation.tailrec
    def loop(l: List[A], acc: Acc): List[A] = l match {
      case x :: xs =>
        if (acc.itemsEncountered.contains(x))
          loop(xs, Acc(acc.itemsEncountered, x :: acc.duplicates))
        else loop(xs, Acc(x :: acc.itemsEncountered, acc.duplicates))
      case Nil =>
        acc.duplicates
    }

    loop(list, Acc(List.empty[A], List.empty[A]))
  }
}
