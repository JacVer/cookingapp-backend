package com.helpers

import akka.event.Logging.LogLevel
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.RouteResult.{Complete, Rejected}
import akka.http.scaladsl.server.directives.{DebuggingDirectives, LogEntry, LoggingMagnet}
import akka.http.scaladsl.server.{Directive0, RouteResult}

object RouteLogging {

  sealed trait RouteEvent
  case class ErrorEvent(errorMessage: String) extends RouteEvent { override def toString: String = "ERROR"}
  case class ExceptionEvent(exception: String) extends RouteEvent { override def toString: String = "EXCEPTION"}
  case class RequestReceivedEvent() extends RouteEvent { override def toString: String = "RECEIVED"}
  case class SuccessEvent() extends RouteEvent  { override def toString: String = "SUCCESS"}

  def formatRouteLog(httpMethod: akka.http.scaladsl.model.HttpMethod, route: String, event: RouteEvent, token: Option[String], param: Map[String, Any]): String = {
    val paramWithPotentialError = event match {
      case RequestReceivedEvent() => param
      case SuccessEvent() => param
      case ErrorEvent(errorMessage) => param + ("error" -> errorMessage)
      case ExceptionEvent(exception) => param + ("exception" -> exception)
    }

    val formattedParam = paramWithPotentialError.map { case (key, value) => s"'$key':'${value.toString}'" }.mkString(", ").replace("\n", "")
    s"{'http_method':'${httpMethod.value}', 'route':'$route', 'event':'$event', 'token':'${token.getOrElse("null")}', 'param':{$formattedParam}"
  }

  def akkaResponseTimeLoggingFunction(
    loggingAdapter: LoggingAdapter,
    requestTimestamp: Long,
    level: LogLevel = Logging.InfoLevel
  )(req: HttpRequest)(res: RouteResult): Unit = {
    val entry = res match {
      case Complete(resp) =>
        val responseTimestamp: Long = System.nanoTime
        val elapsedTime: Long = (responseTimestamp - requestTimestamp) / 1000000
        val loggingString = s"""Log Performance:${req.method.value}:${req.uri.path}:${resp.status}:${elapsedTime}ms"""
        LogEntry(loggingString, level)
      case Rejected(reason) =>
        LogEntry(s"Rejected Reason: ${reason.mkString(",")}", level)
    }
    entry.logTo(loggingAdapter)
  }

  def printResponseTime(log: LoggingAdapter): HttpRequest => RouteResult => Unit = {
    val requestTimestamp = System.nanoTime
    akkaResponseTimeLoggingFunction(log, requestTimestamp)
  }

  val logResponseTime: Directive0 = DebuggingDirectives.logRequestResult(LoggingMagnet(printResponseTime))
}
