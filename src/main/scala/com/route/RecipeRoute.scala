package com.route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.api.NutritionApi.NutritionalInformation
import com.api.RecipeApi.{NewRecipe, NewRecipeId, NewRecipeIngredient, Recipe, RecipeIngredient}
import com.helpers.PATCH
import com.helpers.RouteWrapper.wrapRoute
import com.service.RecipeService
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import io.circe.syntax._
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.{ArraySchema, Content, Schema}
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import javax.ws.rs.core.MediaType
import javax.ws.rs.{Consumes, DELETE, GET, POST, Path, Produces}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext

trait RecipeRoute {

  implicit val ec: ExecutionContext
  val logger: Logger = LoggerFactory.getLogger("com.route.RecipeRoute")

  def route: Route = concat(
    recipeCreateRoute,
    recipeListerRoute,
    recipeIngredientGetterRoute,
    recipeGetterRoute,
    recipeDeleteRoute,
    recipeUpdateRoute,
    recipeSimulateRoute
  )

  @Path("/api/recipe")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Search recipes",
    tags = Array("recipe"),
    parameters = Array(
      new Parameter(
        name = "ingredients",
        description = "Food_ids separated by commas. Example : 11,22,33",
        in = ParameterIn.QUERY,
        required = false,
        schema = new Schema(implementation = classOf[String])
      ),
      new Parameter(
        name = "title",
        description = "Get recipes containing this in their title",
        in = ParameterIn.QUERY,
        required = false,
        schema = new Schema(implementation = classOf[String])
      ),
      new Parameter(
        name = "pseudo",
        description = "Get recipes containing from this user's pseudo",
        in = ParameterIn.QUERY,
        required = false,
        schema = new Schema(implementation = classOf[String])
      ),
      new Parameter(name = "pescetarian", description = "If true, only returns pescetarian recipes", in = ParameterIn.QUERY, required = false, schema = new Schema(implementation = classOf[Boolean])),
      new Parameter(name = "vegetarian", description = "If true, only returns vegetarian recipes", in = ParameterIn.QUERY, required = false, schema = new Schema(implementation = classOf[Boolean])),
      new Parameter(name = "vegan", description = "If true, only returns vegan recipes", in = ParameterIn.QUERY, required = false, schema = new Schema(implementation = classOf[Boolean])),
      new Parameter(name = "gluten_free", description = "If true, only returns gluten free recipes", in = ParameterIn.QUERY, required = false, schema = new Schema(implementation = classOf[Boolean])),
      new Parameter(name = "month", description = "Keep only recipes that have vegetables and fruits available in this month", in = ParameterIn.QUERY, required = false, schema = new Schema(implementation = classOf[String], allowableValues = Array("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"))),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = false, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "List of found recipes",
        content = Array(new Content(array = new ArraySchema(schema = new Schema(implementation = classOf[Recipe]))))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid title, No parameters, Invalid pseudo", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def recipeListerRoute: Route = optionalHeaderValueByName("jwt") { token =>
    (path("api" / "recipe") & get & parameters(
      "ingredients".as[String].optional,
      "title".as[String].optional,
      "pseudo".as[String].optional,
      "pescetarian".as[Boolean].optional,
      "vegetarian".as[Boolean].optional,
      "vegan".as[Boolean].optional,
      "gluten_free".as[Boolean].optional,
      "month".as[String].optional
    )) { (ingredients: Option[String], title: Option[String], pseudo: Option[String], pescetarian: Option[Boolean], vegetarian: Option[Boolean], vegan: Option[Boolean], glutenFree: Option[Boolean], month: Option[String]) =>

      val routeParams =
        List(
          ingredients.map(i => s"ingredients=$i"),
          title.map(t => s"title=$t"),
          pseudo.map(p => s"pseudo=$p"),
          pescetarian.map(p => s"pescetarian=$p"),
          vegetarian.map(p => s"vegetarian=$p"),
          vegan.map(p => s"vegan=$p"),
          glutenFree.map(p => s"gluten_free=$p"),
          month.map(p => s"month=$p")
        ).filter(_.isDefined) match { case Nil => ""; case l => l.map(_.getOrElse("")).reduce(_ + "&" + _)}
      val routePath = s"/api/recipe?" + routeParams
      wrapRoute(RecipeService.listRecipes(title, ingredients, pseudo, token, pescetarian, vegetarian, vegan, glutenFree, month) _)(
        _.map(_.toRecipeApi).asJson,
        logger,
        token
      )
    }
  }

  @Path("/api/recipe/{recipe_id}")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get one recipe by recipe_id",
    tags = Array("recipe"),
    parameters = Array(
      new Parameter(name = "recipe_id", description = "Id of the recipe", in = ParameterIn.PATH, required = true),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = false, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "A recipe",
        content = Array(new Content(schema = new Schema(implementation = classOf[Recipe])))
      ),
      new ApiResponse(responseCode = "400", description = "Recipe not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def recipeGetterRoute: Route = optionalHeaderValueByName("jwt") { token: Option[String] =>
    path("api" / "recipe" / IntNumber) { recipeId: Int =>
      get {
        wrapRoute(RecipeService.getRecipe(recipeId, token) _)(
          _.toRecipeApi.asJson,
          logger,
          token
        )
      }
    }
  }

  @Path("/api/recipe/{recipe_id}/ingredients")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get one recipe ingredients",
    tags = Array("recipe"),
    parameters = Array(
      new Parameter(name = "recipe_id", description = "Id of the recipe", in = ParameterIn.PATH, required = true),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = false, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Recipe information",
        content = Array(new Content(array = new ArraySchema(schema = new Schema(implementation = classOf[RecipeIngredient]))))
      ),
      new ApiResponse(responseCode = "400", description = "Recipe not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def recipeIngredientGetterRoute: Route = optionalHeaderValueByName("jwt") { token: Option[String] =>
    pathPrefix("api" / "recipe" / IntNumber) { recipeId: Int =>
      (path("ingredients") & get) {
        wrapRoute(RecipeService.getRecipeIngredients(recipeId) _)(
          _.map {case (food, foodUnit, quantity) => food.toRecipeIngredient(foodUnit, quantity)}.asJson,
          logger,
          token
        )
      }
    }
  }

  @Path("/api/recipe/{recipe_id}")
  @DELETE
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Delete a recipe",
    tags = Array("recipe"),
    parameters = Array(
      new Parameter(name = "recipe_id", description = "Id of the recipe", in = ParameterIn.PATH, required = true),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Success !",
        content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, User not owner of recipe, Recipe not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def recipeDeleteRoute: Route = headerValueByName("jwt") { token =>
    (path("api" / "recipe" / IntNumber) & delete) { recipeId: Int =>
      wrapRoute(RecipeService.deleteRecipe(token, recipeId) _)(
        b => b.asJson,
        logger,
        Some(token)
      )
    }
  }

  @Path("/api/recipe")
  @POST
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Create a recipe",
    tags = Array("recipe"),
    parameters = Array(
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    requestBody = new RequestBody(description = "Recipe to create", content = Array(new Content(schema = new Schema(implementation = classOf[NewRecipe])))),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Created recipe's recipe_id",
        content = Array(new Content(schema = new Schema(implementation = classOf[NewRecipeId])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, Invalid title, No ingredients, No instructions, Food unit not found, Duplicated food", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def recipeCreateRoute: Route = headerValueByName("jwt") { token =>
    (path("api" / "recipe") & post) {
      entity(as[NewRecipe]) { nr =>
        wrapRoute(
          RecipeService.createRecipe(
            token,
            nr.title,
            nr.description,
            nr.instructions,
            nr.ingredients.map(i => (i.food_id, i.unit_id, i.quantity)),
            nr.servings,
            nr.preparation_time_minutes,
            nr.sourceURL,
            nr.base_64_encoded_picture
          ) _)(
          recipeId => NewRecipeId(recipeId).asJson,
          logger,
          Some(token),
          Map("new_recipe" -> nr)
        )
      }
    }
  }

  @Path("/api/recipe/{recipe_id}")
  @PATCH
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Update a recipe",
    tags = Array("recipe"),
    parameters = Array(
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String])),
      new Parameter(name = "recipe_id", description = "id the recipe to update", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
    ),
    requestBody = new RequestBody(description = "Recipe to create", content = Array(new Content(schema = new Schema(implementation = classOf[NewRecipe])))),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Recipe was updated successfully",
        content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, Recipe not found, Invalid title, No ingredients, No instructions, Food unit not found, Duplicated food", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def recipeUpdateRoute: Route = headerValueByName("jwt") { token =>
    (path("api" / "recipe" / IntNumber) & patch) { recipeId: Int =>
      entity(as[NewRecipe]) { nr: NewRecipe =>
        wrapRoute(
          RecipeService.updateRecipe(
            recipeId,
            token,
            nr.title,
            nr.description,
            nr.instructions,
            nr.ingredients.map(i => (i.food_id, i.unit_id, i.quantity)),
            nr.servings,
            nr.preparation_time_minutes,
            nr.sourceURL,
            nr.base_64_encoded_picture
          ) _)(
          b => b.asJson,
          logger,
          Some(token),
          Map("new_recipe" -> nr)
        )
      }
    }
  }

  @Path("/api/recipe/simulate")
  @POST
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get nutritional information based on ingredients",
    tags = Array("recipe"),
    parameters = Array(
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    requestBody = new RequestBody(description = "List of food with unit and quantity ", content = Array(new Content(array = new ArraySchema(schema = new Schema(implementation = classOf[NewRecipeIngredient]))))),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Nutritional information of the ingredients",
        content = Array(new Content(schema = new Schema(implementation = classOf[NutritionalInformation])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, No ingredients, Food unit not found,  Duplicated food", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def recipeSimulateRoute: Route = headerValueByName("jwt") { token =>
    (path("api" / "recipe" / "simulate") & post) {
      entity(as[List[NewRecipeIngredient]]) { ingredients =>
        wrapRoute(RecipeService.getNutritionalInformation(token, ingredients.map(i => (i.food_id, i.unit_id, i.quantity))) _)(
          _.toNutritionalInformationApi.asJson,
          logger,
          Some(token),
          Map("ingredients" -> ingredients)
        )
      }
    }
  }
}

object RecipeRoute {
  def apply()(implicit executionContext: ExecutionContext): RecipeRoute = {
    new RecipeRoute {
      override implicit val ec: ExecutionContext = executionContext
    }
  }
}