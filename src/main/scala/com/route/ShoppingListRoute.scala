package com.route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.api.ShoppingListApi.{AddShoppingListItems, NewShoppingList, RenameShoppingList, ShoppingList, ShoppingListCreatedId, ShoppingListSummary}
import com.helpers.PATCH
import com.helpers.RouteWrapper.wrapRoute
import com.service.ShoppingListService
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import io.circe.syntax._
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import io.swagger.v3.oas.annotations.media.{ArraySchema, Content, Schema}
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.ws.rs.core.MediaType
import javax.ws.rs.{Consumes, DELETE, GET, POST, PUT, Path, Produces}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext

trait ShoppingListRoute {

  implicit val ec: ExecutionContext
  val logger: Logger = LoggerFactory.getLogger("com.route.ShoppingListRoute")

  def route: Route = concat(
    shoppingListItemCreateRoute,
    shoppingListItemDeleteRoute,
    shoppingListItemUpdateRoute,
    shoppingListCreateRoute,
    shoppingListDeleteRoute,
    shoppingListListerRoute,
    shoppingListGetterRoute,
    shoppingListUpdateRoute,
  )

  @Path("/api/shopping-list")
  @POST
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Create a new shopping list",
    tags = Array("shopping-list"),
    parameters = Array(new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))),
    requestBody = new RequestBody(content = Array(new Content(schema = new Schema(implementation = classOf[NewShoppingList])))),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Shopping list's id", content = Array(new Content(schema = new Schema(implementation = classOf[ShoppingListCreatedId])))),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, Empty shopping list title", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def shoppingListCreateRoute: Route = headerValueByName("jwt") { token: String =>
    (path("api" / "shopping-list") & post) {
      entity(as[NewShoppingList]) { case NewShoppingList(shoppingListTitle) =>
        wrapRoute(ShoppingListService.createShoppingList(token, shoppingListTitle) _)(
          shoppingListId => ShoppingListCreatedId(shoppingListId).asJson,
          logger,
          Some(token),
          Map("shopping_list_title" -> shoppingListTitle)
        )
      }
    }
  }

  @Path("/api/shopping-list")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get user's shopping lists",
    tags = Array("shopping-list"),
    parameters = Array(
      new Parameter(
        name = "ordering",
        description = "Ordering of the shopping lists",
        in = ParameterIn.QUERY,
        required = true,
        schema = new Schema(implementation = classOf[String], allowableValues = Array("alphabetical", "created_time"))
      ),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "User's shopping lists",
        content = Array(new Content(array = new ArraySchema(schema = new Schema(implementation = classOf[ShoppingListCreatedId]))))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, Unknown ordering", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def shoppingListListerRoute: Route = headerValueByName("jwt") { token: String =>
    (path("api" / "shopping-list") & get & parameters("ordering")) { ordering: String =>
      wrapRoute(ShoppingListService.listAccountShoppingLists(token, ordering) _)(
        _.map(s => ShoppingListSummary(s.shoppingListId, s.addedTime, s.title)).asJson,
        logger,
        Some(token)
      )
    }
  }

  @Path("/api/shopping-list/{shopping_list_id}")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get a shopping list by its id",
    tags = Array("shopping-list"),
    parameters = Array(
      new Parameter(name = "shopping_list_id", description = "Id of the shopping list", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "The shopping list and its items",
        content = Array(new Content(schema = new Schema(implementation = classOf[ShoppingList])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, User not owner of shopping list, Shopping list not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def shoppingListGetterRoute: Route = headerValueByName("jwt") { token: String =>
    pathPrefix("api" / "shopping-list" / IntNumber) { shoppingListId: Int =>
      get {
        wrapRoute(ShoppingListService.getShoppingList(token, shoppingListId) _)(
          t => t._1.toShoppingListApi(t._2).asJson,
          logger,
          Some(token)
        )
      }
    }
  }

  @Path("/api/shopping-list/{shopping_list_id}")
  @DELETE
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Delete a shopping list",
    tags = Array("shopping-list"),
    parameters = Array(
      new Parameter(name = "shopping_list_id", description = "Id of the shopping list", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Success !",
        content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, User not owner of shopping list, Shopping list not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def shoppingListDeleteRoute: Route = headerValueByName("jwt") { token: String =>
    pathPrefix("api" / "shopping-list" / IntNumber) { shoppingListId: Int =>
      delete {
        wrapRoute(ShoppingListService.deleteShoppingList(token, shoppingListId) _)(
          b => b.asJson,
          logger,
          Some(token)
        )
      }
    }
  }

  @Path("/api/shopping-list/{shopping_list_id}")
  @PATCH
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Update a shopping list",
    tags = Array("shopping-list"),
    parameters = Array(
      new Parameter(name = "shopping_list_id", description = "Id of the shopping list", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    requestBody = new RequestBody(content = Array(new Content(schema = new Schema(implementation = classOf[RenameShoppingList])))),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Success !",
        content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, User not owner of shopping list, Shopping list not found, Empty title", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def shoppingListUpdateRoute: Route = headerValueByName("jwt") { token: String =>
    pathPrefix("api" / "shopping-list" / IntNumber) { shoppingListId: Int =>
      patch {
        entity(as[RenameShoppingList]) { case RenameShoppingList(title) =>
          wrapRoute(ShoppingListService.renameShoppingList(token, shoppingListId, title) _)(
            b => b.asJson,
            logger,
            Some(token),
            Map("shopping_list_title" -> title)
          )
        }
      }
    }
  }


  @Path("/api/shopping-list/{shopping_list_id}/items")
  @POST
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Add items to the shopping list",
    tags = Array("shopping-list"),
    parameters = Array(
      new Parameter(name = "shopping_list_id", description = "Id of the shopping list", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    requestBody = new RequestBody(content = Array(new Content(schema = new Schema(implementation = classOf[AddShoppingListItems])))),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Success !",
        content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, User not owner of shopping list, Shopping list not found, Empty item list", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def shoppingListItemCreateRoute: Route = headerValueByName("jwt") { token: String =>
    pathPrefix("api" / "shopping-list" / IntNumber) { shoppingListId: Int =>
      (path("items") & post) {
        entity(as[AddShoppingListItems]) { case AddShoppingListItems(items) =>
          wrapRoute(ShoppingListService.addItemsToShoppingList(token, shoppingListId, items) _)(
            b => b.asJson,
            logger,
            Some(token),
            Map("items" -> items)
          )
        }
      }
    }
  }

  @Path("/api/shopping-list/{shopping_list_id}/items/{item_id}")
  @DELETE
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Delete a shopping list item",
    tags = Array("shopping-list"),
    parameters = Array(
      new Parameter(name = "shopping_list_id", description = "Id of the shopping list", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "item_id", description = "Id of the item in the list", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Success !",
        content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, User not owner of shopping list, Shopping list not found, Item not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def shoppingListItemDeleteRoute: Route = headerValueByName("jwt") { token: String =>
    pathPrefix("api" / "shopping-list" / IntNumber) { shoppingListId: Int =>
      path("items" / IntNumber) { itemId: Int =>
        delete {
          wrapRoute(ShoppingListService.deleteItemFromShoppingList(token, shoppingListId, itemId) _)(
            b => b.asJson,
            logger,
            Some(token)
          )
        }
      }
    }
  }

  @Path("/api/shopping-list/{shopping_list_id}/items/{item_id}")
  @PATCH
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Update a shopping list item",
    tags = Array("shopping-list"),
    parameters = Array(
      new Parameter(name = "shopping_list_id", description = "Id of the shopping list", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "item_id", description = "Id of the item in the list", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "ticked", description = "Tick/Untick the item", in = ParameterIn.QUERY, required = false, schema = new Schema(implementation = classOf[Boolean])),
      new Parameter(name = "indented", description = "Indent/De-ident the item", in = ParameterIn.QUERY, required = false, schema = new Schema(implementation = classOf[Boolean])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    requestBody = new RequestBody(required = false, content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Success !",
        content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))
      ),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, User not owner of shopping list, Shopping list not found, Item not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def shoppingListItemUpdateRoute: Route = headerValueByName("jwt") { token: String =>
    pathPrefix("api" / "shopping-list" / IntNumber) { shoppingListId: Int =>
      path("items" / IntNumber) { itemId =>
        parameters("ticked".as[Boolean].optional, "indented".as[Boolean].optional) { (ticked: Option[Boolean], indented: Option[Boolean]) =>
          patch {
            entity(as[Option[String]]) { text: Option[String] =>
              wrapRoute(ShoppingListService.updateShoppingListItem(token, shoppingListId, itemId, ticked, indented, text) _)(
                b => b.asJson,
                logger,
                Some(token)
              )
            }
          }
        }
      }
    }
  }
}

object ShoppingListRoute {
  def apply()(implicit executionContext: ExecutionContext): ShoppingListRoute = {
    new ShoppingListRoute {
      override implicit val ec: ExecutionContext = executionContext
    }
  }
}
