package com.route

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.helpers.CORSHandler
import com.helpers.RouteLogging.logResponseTime
import com.service.SwaggerDocService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.ws.rs.core.MediaType
import javax.ws.rs.{GET, Path, Produces}

import scala.concurrent.ExecutionContext

object HttpRoute {

  @Path("/heartbeat")
  @GET
  @Produces(Array(MediaType.TEXT_PLAIN))
  @Operation(
    summary = "Checks if the backend is running",
    tags = Array("heartbeat"),
    responses = Array(new ApiResponse(responseCode = "200", description = "I'm looking for a heartbeat !"))
  )
  def heartbeatRoute: Route =
    (path("heartbeat") & get) {
      complete((StatusCodes.OK, "I'm looking for a heartbeat !"))
    }

  def route(implicit executionContext: ExecutionContext): Route = concat(
    pathPrefix("swagger") {
      getFromResourceDirectory("swagger") ~ pathSingleSlash(get(redirect("index.html", StatusCodes.PermanentRedirect)))
    },
    CORSHandler.handle(
      logResponseTime(
        concat(
          heartbeatRoute,
          FavouriteRecipeRoute().route,
          FoodRoute().route,
          RecipeRoute().route,
          ShoppingListRoute().route,
          AccountRoute().route,
          RecipeRatingRoute().route,
          SwaggerDocService.routes
        )
      )
    )
  )
}
