package com.route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.api.RecipeApi.Recipe
import com.helpers.RouteWrapper.wrapRoute
import com.service.FavouriteRecipeService
import io.circe.generic.auto._
import io.circe.syntax._
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import io.swagger.v3.oas.annotations.media.{ArraySchema, Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.ws.rs.core.MediaType
import javax.ws.rs.{DELETE, GET, PUT, Path, Produces}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext

trait FavouriteRecipeRoute {

  implicit val ec: ExecutionContext
  val logger: Logger = LoggerFactory.getLogger("com.route.FavouriteRecipeRoute")

  def route: Route = getFavouriteRecipeGetRoute ~ getFavouriteRecipeAddRoute ~ getFavouriteRecipeDeleteRoute

  @Path("/api/recipe/favourite")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get user's favourite recipes",
    tags = Array("favourite"),
    parameters = Array(new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "List of user's favourite recipes",
        content = Array(new Content(array = new ArraySchema(schema = new Schema(implementation = classOf[Recipe]))))
      ),
      new ApiResponse(
        responseCode = "400",
        description = "Invalid token, User not found",
        content = Array(new Content(schema = new Schema(implementation = classOf[String])))
      ),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def getFavouriteRecipeGetRoute: Route = headerValueByName("jwt") { token =>
    (path("api" / "recipe" / "favourite") & get) {
      wrapRoute(FavouriteRecipeService.findFavouriteRecipe(token))(
        _.map(r => r.toRecipeApi).asJson,
        logger,
        Some(token)
      )
    }
  }

  @Path("/api/recipe/favourite/{recipe_id}")
  @PUT
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Add a recipe to user's favourite recipes",
    tags = Array("favourite"),
    parameters = Array(
      new Parameter(name = "recipe_id", description = "Id of the recipe", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Recipe was successfully added as a favourite", content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, Already a favourite", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def getFavouriteRecipeAddRoute: Route = headerValueByName("jwt") { token =>
    path("api" / "recipe" / "favourite" / IntNumber) { recipeId =>
      put {
        wrapRoute(FavouriteRecipeService.addFavouriteRecipe(token, recipeId))(
          b => b.asJson,
          logger,
          Some(token)
        )
      }
    }
  }

  @Path("/api/recipe/favourite/{recipe_id}")
  @DELETE
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Delete a recipe from user's favourite recipes",
    tags = Array("favourite"),
    parameters = Array(
      new Parameter(name = "recipe_id", description = "Id of the recipe", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[Int])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Recipe was successfully removed from the favourites", content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))),
      new ApiResponse(responseCode = "400", description = "Invalid token, User not found, Not a favourite", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def getFavouriteRecipeDeleteRoute: Route = headerValueByName("jwt") { token =>
    path("api" / "recipe" / "favourite" / IntNumber) { recipeId =>
      delete {
        wrapRoute(FavouriteRecipeService.deleteFavouriteRecipe(token, recipeId))(
          b => b.asJson,
          logger,
          Some(token)
        )
      }
    }
  }
}

object FavouriteRecipeRoute {
  def apply()(implicit executionContext: ExecutionContext): FavouriteRecipeRoute = {
    new FavouriteRecipeRoute {
      override implicit val ec: ExecutionContext = executionContext
    }
  }
}