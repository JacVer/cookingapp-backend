package com.route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.api.RecipeRatingApi.{RecipeComment, RecipeRating => RecipeRatingApi}
import com.helpers.RouteWrapper.wrapRoute
import com.service.RecipeRatingService
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import io.circe.syntax._
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.{ArraySchema, Content, Schema}
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import javax.ws.rs.core.MediaType
import javax.ws.rs.{GET, PUT, Path, Produces}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext

trait RecipeRatingRoute {

  implicit val ec: ExecutionContext
  val logger: Logger = LoggerFactory.getLogger("com.route.RecipeGradeRoute")

  def route: Route = ratingAddRoute ~ ratingGetRoute ~ commentAddRoute ~ commentGetRoute

  @Path("/api/recipe/{recipe_id}/rating")
  @PUT
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Rate a recipe",
    tags = Array("recipe-rating"),
    parameters = Array(
      new Parameter(name = "recipe_id", description = "Id of the recipe", in = ParameterIn.PATH, required = true),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    requestBody = new RequestBody(description = "Rating of the recipe", content = Array(new Content(schema = new Schema(implementation = classOf[Int], allowableValues = Array("1", "2", "3", "4", "5"))))),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Updated recipe rating", content = Array(new Content(schema = new Schema(implementation = classOf[RecipeRatingApi])))),
      new ApiResponse(responseCode = "400", description = "Invalid token, Invalid rating, Recipe not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def ratingAddRoute: Route = headerValueByName("jwt") { token =>
    path("api" / "recipe" / IntNumber / "rating") { recipeId: Int =>
      (put & entity(as[Int])) { rating: Int =>
        wrapRoute(RecipeRatingService.addRecipeRating(recipeId, rating, token) _)(
          _.toRecipeRatingApi.asJson,
          logger,
          Some(token),
          Map("rating" -> rating)
        )
      }
    }
  }

  @Path("/api/recipe/{recipe_id}/rating")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get a recipe's rating",
    tags = Array("recipe-rating"),
    parameters = Array(
      new Parameter(name = "recipe_id", description = "Id of the recipe", in = ParameterIn.PATH, required = true),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = false, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Recipe rating", content = Array(new Content(schema = new Schema(implementation = classOf[RecipeRatingApi])))),
      new ApiResponse(responseCode = "400", description = "Recipe not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def ratingGetRoute: Route = optionalHeaderValueByName("jwt") { token =>
    path("api" / "recipe" / IntNumber / "rating") { recipeId: Int =>
      get {
        wrapRoute(RecipeRatingService.getRecipeRating(recipeId) _)(
          _.toRecipeRatingApi.asJson,
          logger,
          token
        )
      }
    }
  }

  @Path("/api/recipe/{recipe_id}/comment")
  @PUT
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Add a comment to a recipe",
    tags = Array("recipe-rating"),
    parameters = Array(
      new Parameter(name = "recipe_id", description = "Id of the recipe", in = ParameterIn.PATH, required = true),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = true, schema = new Schema(implementation = classOf[String]))
    ),
    requestBody = new RequestBody(description = "Comment text", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Success"),
      new ApiResponse(responseCode = "400", description = "Invalid token, Recipe not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def commentAddRoute: Route = headerValueByName("jwt") { token =>
    path("api" / "recipe" / IntNumber / "comment") { recipeId: Int =>
      (put & entity(as[String])) { comment =>
        wrapRoute(RecipeRatingService.addRecipeComment(recipeId, comment, token) _)(
          b => b.asJson,
          logger,
          Some(token),
          Map("comment" -> comment)
        )
      }
    }
  }

  @Path("/api/recipe/{recipe_id}/comment")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get a recipe's comments",
    tags = Array("recipe-rating"),
    parameters = Array(
      new Parameter(name = "recipe_id", description = "Id of the recipe", in = ParameterIn.PATH, required = true),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = false, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Recipe comments", content = Array(new Content(array = new ArraySchema(schema = new Schema(implementation = classOf[RecipeComment]))))),
      new ApiResponse(responseCode = "400", description = "Recipe not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def commentGetRoute: Route = optionalHeaderValueByName("jwt") { token =>
    path("api" / "recipe" / IntNumber / "comment") { recipeId: Int =>
      get {
        wrapRoute(RecipeRatingService.getRecipeComments(recipeId) _)(
          _.map(_.toRecipeCommentApi).asJson,
          logger,
          token
        )
      }
    }
  }
}

object RecipeRatingRoute {
  def apply()(implicit executionContext: ExecutionContext): RecipeRatingRoute = {
    new RecipeRatingRoute {
      override implicit val ec: ExecutionContext = executionContext
    }
  }
}