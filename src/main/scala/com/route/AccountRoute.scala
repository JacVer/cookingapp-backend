package com.route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.api.AccountApi.{CreateAccount, Credentials, JsonWebToken}
import com.helpers.RouteWrapper.wrapRoute
import com.service.AccountService
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import io.circe.syntax._
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.{Content, Schema}
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.ws.rs.core.MediaType
import javax.ws.rs.{Consumes, POST, Path, Produces}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext

trait AccountRoute {

  implicit val ec: ExecutionContext
  val logger: Logger = LoggerFactory.getLogger("com.route.AccountRoute")

  def route: Route = pathPrefix("api" / "account") {
    getAccountLoginRoute ~ getAccountCreateRoute ~ getAccountDeleteRoute
  }

  @Path("/api/account/login")
  @POST
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Login user and get a Json Web Token (JWT)",
    tags = Array("account"),
    requestBody = new RequestBody(content = Array(new Content(schema = new Schema(implementation = classOf[Credentials])))),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "User's JWT", content = Array(new Content(schema = new Schema(implementation = classOf[JsonWebToken])))),
      new ApiResponse(responseCode = "400", description = "Invalid email error, Email not found, Wrong password", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def getAccountLoginRoute: Route = {
    (path("login") & post) {
      entity(as[Credentials]) { case Credentials(email, password) =>
        wrapRoute(AccountService.loginAccount(email, password) _)(
          token => JsonWebToken(token).asJson,
          logger,
          params = Map("email" -> email)
        )
      }
    }
  }

  @Path("/api/account/create")
  @POST
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Create user's account and get his Json Web Token (JWT)",
    tags = Array("account"),
    requestBody = new RequestBody(content = Array(new Content(schema = new Schema(implementation = classOf[Credentials])))),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "User's JWT", content = Array(new Content(schema = new Schema(implementation = classOf[JsonWebToken])))),
      new ApiResponse(responseCode = "400", description = "Invalid email, Invalid pseudo, Password too short, Email already used, Pseudo already used", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def getAccountCreateRoute: Route = (path("create") & post) {
    entity(as[CreateAccount]) { case CreateAccount(email, pseudo, password) =>
      wrapRoute(AccountService.createAccount(email, pseudo, password) _)(
        token => JsonWebToken(token).asJson,
        logger,
        params = Map("email" -> email, "pseudo" -> pseudo)
      )
    }
  }

  @Path("/api/account/delete")
  @POST
  @Consumes(Array(MediaType.APPLICATION_JSON))
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Delete user's account",
    tags = Array("account"),
    requestBody = new RequestBody(content = Array(new Content(schema = new Schema(implementation = classOf[Credentials])))),
    responses = Array(
      new ApiResponse(responseCode = "200", description = "Success", content = Array(new Content(schema = new Schema(implementation = classOf[Boolean])))),
      new ApiResponse(responseCode = "400", description = "Invalid email, Email not found, Wrong password", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def getAccountDeleteRoute: Route  = {
    (path("delete") & post) {
      entity(as[Credentials]) { case Credentials(email, password) =>
        wrapRoute(AccountService.hideAccount(email, password) _)(
          b => b.asJson,
          logger,
          params = Map("email" -> email)
        )
      }
    }
  }
}

object AccountRoute {
  def apply()(implicit executionContext: ExecutionContext): AccountRoute = {
    new AccountRoute {
      override implicit val ec: ExecutionContext = executionContext
    }
  }
}