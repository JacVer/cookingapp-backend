package com.route

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.api.FoodApi.{FoodUnit, Food => FoodApi}
import com.helpers.RouteWrapper.wrapRoute
import com.service.FoodService
import io.circe.generic.auto._
import io.circe.syntax._
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.{Operation, Parameter}
import io.swagger.v3.oas.annotations.media.{ArraySchema, Content, Schema}
import io.swagger.v3.oas.annotations.responses.ApiResponse
import javax.ws.rs.core.MediaType
import javax.ws.rs.{GET, Path, Produces}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext

trait FoodRoute {

  implicit val ec: ExecutionContext
  val logger: Logger = LoggerFactory.getLogger("com.route.FoodRoute")

  def route: Route = foodListerRoute ~ foodGetterRoute ~ foodUnitGetterRoute

  @Path("/api/food")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "List food whose name contains the name parameter",
    tags = Array("food"),
    parameters = Array(
      new Parameter(name = "name", description = "Name to search food with", in = ParameterIn.QUERY, required = true, schema = new Schema(implementation = classOf[String])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = false, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Food informations",
        content = Array(new Content(array = new ArraySchema(schema = new Schema(implementation = classOf[FoodApi]))))
      ),
      new ApiResponse(
        responseCode = "400",
        description = "Invalid food name",
        content = Array(new Content(schema = new Schema(implementation = classOf[String])))
      ),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def foodListerRoute: Route = optionalHeaderValueByName("jwt") { token =>
    // Not a real error
    (path("api" / "food") & parameter("name".as[String])) { name: String =>
      wrapRoute(FoodService.listFoodByNameContains(name) _)(
        _.map(_.toFoodApi).asJson,
        logger,
        token
      )
    }
  }

  @Path("/api/food/{food_id}")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get one food information",
    tags = Array("food"),
    parameters = Array(
      new Parameter(name = "food_id", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[String])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = false, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Food information",
        content = Array(new Content(schema = new Schema(implementation = classOf[FoodApi])))
      ),
      new ApiResponse(responseCode = "400", description = "Food not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def foodGetterRoute: Route = optionalHeaderValueByName("jwt") { token =>
    path("api" / "food" / IntNumber) { foodId =>
      get {
        wrapRoute(FoodService.getFood(foodId) _)(
          _.toFoodApi.asJson,
          logger,
          token
        )
      }
    }
  }

  @Path("/api/food/{food_id}/units")
  @GET
  @Produces(Array(MediaType.APPLICATION_JSON))
  @Operation(
    summary = "Get one food's units",
    tags = Array("food"),
    parameters = Array(
      new Parameter(name = "food_id", in = ParameterIn.PATH, required = true, schema = new Schema(implementation = classOf[String])),
      new Parameter(name = "jwt", description = "Json Web Token", in = ParameterIn.HEADER, required = false, schema = new Schema(implementation = classOf[String]))
    ),
    responses = Array(
      new ApiResponse(
        responseCode = "200",
        description = "Food units",
        content = Array(new Content(array = new ArraySchema(schema = new Schema(implementation = classOf[FoodUnit]))))
      ),
      new ApiResponse(responseCode = "400", description = "Food not found", content = Array(new Content(schema = new Schema(implementation = classOf[String])))),
      new ApiResponse(responseCode = "500", description = "Internal server error")
    )
  )
  def foodUnitGetterRoute: Route = optionalHeaderValueByName("jwt") { token =>
    path("api" / "food" / IntNumber / "units") { foodId =>
      get {
        wrapRoute(FoodService.getFoodUnits(foodId) _)(
          _.map(_.toFoodUnitApi).asJson,
          logger,
          token
        )
      }
    }
  }
}

object FoodRoute {
  def apply()(implicit executionContext: ExecutionContext): FoodRoute = {
    new FoodRoute {
      override implicit val ec: ExecutionContext = executionContext
    }
  }
}