package com.service

import com.github.swagger.akka.SwaggerHttpService
import com.github.swagger.akka.model.Info
import com.route.{AccountRoute, FavouriteRecipeRoute, FoodRoute, HttpRoute, RecipeRatingRoute, RecipeRoute, ShoppingListRoute}

object SwaggerDocService extends SwaggerHttpService {
  override val apiClasses = Set(
    HttpRoute.getClass,
    classOf[FavouriteRecipeRoute],
    classOf[FoodRoute],
    classOf[RecipeRoute],
    classOf[ShoppingListRoute],
    classOf[AccountRoute],
    classOf[RecipeRatingRoute]
  )
  override val host = "51.195.45.145:8080"
  override val info: Info = Info(version = "0.7")
  override val unwantedDefinitions = Seq("Function1", "Function1RequestContextFutureRouteResult")

}