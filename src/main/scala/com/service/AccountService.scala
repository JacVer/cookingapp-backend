package com.service

import com.model.Account
import com.storage.AccountStorage

import scala.concurrent.{ExecutionContext, Future}

object AccountService {

  def getFirstAccount(accounts: Either[Throwable, List[Account]]): Option[Account] = accounts match {
    case Left(psqlException) => throw psqlException
    case Right(Nil) => None
    case Right(account :: _) => Some(account)
  }

  def getAccountByAccountId(accountId: Int)(implicit executionContext: ExecutionContext): Future[Option[Account]] =
    AccountStorage.selectAccountWithAccountId(accountId).map(getFirstAccount)

  def decodeToken(token: String): Either[String, Int] = {
    Account.decodeJsonWebToken(token).left.map(_ => "INVALID_TOKEN")
  }

  // TODO : this function is not used at the moment
  def getAccountByPseudo(pseudo: String)(implicit executionContext: ExecutionContext): Future[Either[String, Account]] = {
    if(Account.isPseudoValid(pseudo)) {
      AccountStorage.selectAccountWithPseudo(pseudo).map {
        case Left(psqlException) => throw psqlException
        case Right(Nil) => Left("PSEUDO_NOT_FOUND")
        case Right(account :: _) => Right(account)
      }
    }
    else Future(Left("INVALID_PSEUDO"))
  }

  def loginAccount(email: String, password: String)(implicit executionContext: ExecutionContext): Future[Either[String, String]] = {
    Future(Account.isEmailValid(email))
      .zip(AccountStorage.selectAccountWithEmail(email).map(getFirstAccount))
      .map { case (isEmailValid, account) =>
        if (!isEmailValid) Left("INVALID_EMAIL")
        else account match {
          case None => Left("EMAIL_NOT_FOUND")
          case Some(account) if !account.checkPassword(password) => Left("WRONG_PASSWORD")
          case Some(account) => Right(Account.getJsonWebToken(account.accountId))
        }
      }
  }

  def createAccount(email: String, pseudo: String, password: String)(implicit executionContext: ExecutionContext): Future[Either[String, String]] = {
    Future((Account.isEmailValid(email), Account.isPseudoValid(pseudo), Account.checkPasswordLength(password)))
      .zip(AccountStorage.selectAccountWithEmail(email).map(getFirstAccount))
      .map { case ((isEmailValid, isPseudoValid, isPasswordValid), account) =>
        if (!isEmailValid) Some("INVALID_EMAIL")
        else if (!isPseudoValid) Some("INVALID_PSEUDO")
        else if (!isPasswordValid) Some("PASSWORD_TOO_SHORT")
        else if (account.isDefined) Some("ALREADY_USED_EMAIL")
        else None
      }
      .flatMap {
        case Some(exception) => Future(Left(exception))
        case None =>
          AccountStorage.selectAccountWithEmail(email).map(getFirstAccount)
            .zip(AccountStorage.selectAccountWithPseudo(pseudo).map(getFirstAccount))
            .flatMap {
            case (Some(_), _) => Future(Left("ALREADY_USED_EMAIL"))
            case (_, Some(_)) => Future(Left("ALREADY_USED_PSEUDO"))
            case (None, None) =>
              AccountStorage.insertAccount(email, pseudo, Account.encryptPassword(password)).map(getFirstAccount).map {
                case Some(account) => Right(Account.getJsonWebToken(account.accountId))
                case None => throw new Error("Unexpected Nil when creating the account")
              }
          }
      }
  }

  def hideAccount(email: String, password: String)(implicit executionContext: ExecutionContext): Future[Either[String, Boolean]] = {
    Future(Account.isEmailValid(email))
      .zip(AccountStorage.selectAccountWithEmail(email).map(getFirstAccount))
      .map { case (isEmailValid, account) =>
        if (!isEmailValid) Left("INVALID_EMAIL")
        else account match {
          case None => Left("EMAIL_NOT_FOUND")
          case Some(account) if !account.checkPassword(password) => Left("WRONG_PASSWORD")
          case Some(account) => Right(account.accountId)
        }
      }
      .flatMap {
        case Left(exception) => Future(Left(exception))
        case Right(accountId) =>
          AccountStorage.hideAccount(accountId).map {
            case Left(psqlException) => throw psqlException
            case Right(_) => Right(true)
          }
      }
  }
}
