package com.service

import com.model.food.{Food, FoodUnit}
import com.storage.{FoodStorage, FoodUnitStorage}

import scala.concurrent.{ExecutionContext, Future}

object FoodService {

  def listFoodByNameContains(foodPartialName: String)(implicit executionContext: ExecutionContext): Future[Either[String, List[Food]]] = {
    Future(Food.isNameValid(foodPartialName))
      .flatMap {
        case false => Future(Left("INVALID_FOOD_NAME"))
        case true =>
          FoodStorage
            .selectFoodWithName(foodPartialName)
            .map {
              case Left(psqlException) => throw psqlException
              case Right(foods) => Right(foods)
            }
      }
  }

  def getFood(foodId: Int)(implicit executionContext: ExecutionContext): Future[Either[String, Food]] = {

    FoodStorage
      .selectFoodWithFoodId(foodId)
      .map {
        case Left(psqlException) => throw psqlException
        case Right(Nil) => Left("FOOD_NOT_FOUND")
        case Right(food :: _) => Right(food)
      }
  }

  def getFoodUnits(foodId: Int)(implicit executionContext: ExecutionContext): Future[Either[String, List[FoodUnit]]] = {

    FoodUnitStorage
      .selectFoodUnit(foodId)
      .map {
        case Left(psqlException) => throw psqlException
        case Right(Nil) => Left("FOOD_UNITS_NOT_FOUND")
        case Right(units) => Right(units)
      }
  }

  def getFoodWithUnit(foodUnitIds: List[(Int, Int)])(implicit executionContext: ExecutionContext): Future[Either[String, List[(Food, FoodUnit)]]] = {
    FoodStorage
      .selectFoodWithFoodUnit(foodUnitIds)
      .map {
        case Left(psqlException) => throw psqlException
        case Right(foodWithUnits) =>
          if (foodUnitIds.length == foodWithUnits.length)
            Right(foodWithUnits)
          else {
            val missingFoodUnitIds = foodUnitIds
              .filter(fu =>
                !foodWithUnits.map { case (food, foodUnit) => (food.foodId, foodUnit.unitId)}.contains(fu)
              )
              .sortBy(_._1)
            Left(s"FOOD_UNIT_NOT_FOUND:${missingFoodUnitIds.mkString(",")}")
          }
      }
  }
}
