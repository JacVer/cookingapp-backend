package com.service

import java.time.LocalDateTime

import com.api.ShoppingListApi.NewShoppingListItem
import com.model.{Alphabetical, CreatedDateTime, ShoppingList, ShoppingListItem, ShoppingListOrdering}
import com.storage.ShoppingListStorage

import scala.concurrent.{ExecutionContext, Future}

// TODO : find a way to encapsulate the logic Future[Either[X,Y]] => Future[Either[X, Future[Z]] => Future[Either[X, Z]
object ShoppingListService {

  def createShoppingList(token: String, title: String)
    (implicit executionContext: ExecutionContext): Future[Either[String, Int]] = {
    if(title.isEmpty) Future(Left("EMPTY_SHOPPING_LIST_TITLE"))
    else
      AccountService.decodeToken(token) match {
        case Left(e) => Future(Left(e))
        case Right(accountId) =>
          ShoppingListStorage
            .insertShoppingList(accountId, LocalDateTime.now(), title)
            .map {
              case Left(psqlException) => throw psqlException
              case Right(Nil) => throw new Error(s"Couldn't get shopping_list_id on shopping-list creation.")
              case Right(shoppingListId :: _) => Right(shoppingListId)
            }
      }
  }

  private def decodeTokenAndGetShoppingList(token: String, shoppingListId: Int)(implicit executionContext: ExecutionContext): Future[Either[String, ShoppingList]] = {
    AccountService.decodeToken(token) match {
      case Left(e) => Future(Left(e))
      case Right(accountId) =>
        ShoppingListStorage
          .selectShoppingListByShoppingListId(shoppingListId)
          .map {
            case Left(psqlException) => throw psqlException
            case Right(shoppingList :: _) if shoppingList.accountId != accountId => Left("USER_NOT_OWNER_OF_SHOPPING_LIST")
            case Right(Nil) => Left("SHOPPING_LIST_NOT_FOUND")
            case Right(shoppingList :: _) => Right(shoppingList)
          }
    }
  }

  def getShoppingList(token: String, shoppingListId: Int)
    (implicit executionContext: ExecutionContext): Future[Either[String, (ShoppingList, List[ShoppingListItem])]] = {
    decodeTokenAndGetShoppingList(token, shoppingListId).flatMap {
      case Left(e) => Future(Left(e))
      case Right(shoppingList) =>
        ShoppingListStorage
          .selectShoppingListItems(shoppingList.shoppingListId)
          .map {
            case Left(psqlException) => throw psqlException
            case Right(shoppingListItems) => Right((shoppingList, shoppingListItems))
          }
    }
  }

  def listAccountShoppingLists(token: String, ordering: String)(implicit executionContext: ExecutionContext): Future[Either[String, List[ShoppingList]]] = {
    Future(ShoppingListOrdering.fromString(ordering))
      .flatMap {
        case Left(e) => Future(Left(e))
        case Right(shoppingListOrdering) =>
          AccountService.decodeToken(token) match {
            case Left(e) => Future(Left(e))
            case Right(accountId) =>
              ShoppingListStorage
                .selectShoppingListByAccountId(accountId, shoppingListOrdering)
                .map(_.left.map(throw _))
            }
      }
  }

  def addItemsToShoppingList(token: String, shoppingListId: Int, items: List[NewShoppingListItem])
    (implicit executionContext: ExecutionContext): Future[Either[String, Boolean]] = {
    if(items.isEmpty) Future(Left("EMPTY_ITEM_LIST"))
    else
      AccountService.decodeToken(token) match {
        case Left(e) => Future(Left(e))
        case Right(accountId) =>
          ShoppingListStorage
            .selectMaxItemIdByShoppingListId(shoppingListId)
            .flatMap {
              case Left(psqlException) => throw psqlException
              case Right((_, creatorAccountId, _) :: _) if creatorAccountId != accountId => Future(Left("USER_NOT_OWNER_OF_SHOPPING_LIST"))
              case Right(Nil) => Future(Left("SHOPPING_LIST_NOT_FOUND"))
              case Right((_, _, maxItemId) :: _) =>
                ShoppingListStorage
                  .insertShoppingListItems(
                    items
                      .zipWithIndex
                      .map { case (NewShoppingListItem(ticked, indented, text), index) =>
                        ShoppingListItem(shoppingListId, index + maxItemId, ticked, indented, text)
                      }
                  )
                  .map {
                    case Left(psqlException) => throw psqlException
                    case Right(i) if i != items.length => throw new Error(s"Unexpected number of created items. Expected ${items.length}. Found $i")
                    case Right(_) => Right(true)
                  }
            }
      }
  }

  def deleteItemFromShoppingList(token: String, shoppingListId: Int, itemId: Int)
    (implicit executionContext: ExecutionContext): Future[Either[String, Boolean]] = {
    decodeTokenAndGetShoppingList(token, shoppingListId).flatMap {
      case Left(e) => Future(Left(e))
      case Right(_) =>
        ShoppingListStorage
          .selectShoppingListItems(shoppingListId)
          .flatMap {
            case Left(psqlException) => throw psqlException
            case Right(items) if !items.exists(i => i.itemId == itemId) => Future(Left("ITEM_NOT_FOUND"))
            case Right(_) =>
              ShoppingListStorage.deleteShoppingListItem(shoppingListId, itemId).map(_.left.map(throw _).map(_ => true))
          }
    }
  }

  def deleteShoppingList(token: String, shoppingListId: Int)
    (implicit executionContext: ExecutionContext): Future[Either[String, Boolean]] = {
    decodeTokenAndGetShoppingList(token, shoppingListId).flatMap {
      case Left(e) => Future(Left(e))
      case Right(_) =>
        ShoppingListStorage.deleteShoppingListItems(shoppingListId)
          .flatMap {
            case Left(psqlException) => throw psqlException
            case _ => ShoppingListStorage.deleteShoppingList(shoppingListId).map {_.left.map(throw _).map(_ => true)}
          }
    }
  }

  def updateShoppingListItem(token: String, shoppingListId: Int, itemId: Int, ticked: Option[Boolean], indented: Option[Boolean], text: Option[String])
    (implicit executionContext: ExecutionContext): Future[Either[String, Boolean]] = {
    if(indented.isEmpty && ticked.isEmpty && text.isEmpty) Future(Left("AT_LEAST_ONE_PARAMETER_MUST_BE_DEFINED"))
    else
      decodeTokenAndGetShoppingList(token, shoppingListId).flatMap {
        case Left(e) => Future(Left(e))
        case Right(_) =>
          ShoppingListStorage
            .selectShoppingListItems(shoppingListId)
            .flatMap {
              case Left(psqlException) => throw psqlException
              case Right(items) if !items.exists(i => i.itemId == itemId) => Future(Left("ITEM_NOT_FOUND"))
              case Right(_) =>
                ShoppingListStorage
                  .updateShoppingListItem(shoppingListId, itemId, ticked, indented, text)
                  .map {
                    case Left(psqlException) => throw psqlException
                    case Right(0) => Left("ITEM_NOT_FOUND")
                    case Right(1) => Right(true)
                    case Right(i) => throw new Error(s"Unexpected value of updated items. Expected 1. Found $i")
                  }
            }
      }
  }

  def renameShoppingList(token: String, shoppingListId: Int, title: String)
    (implicit executionContext: ExecutionContext): Future[Either[String, Boolean]] = {
    if (title.isEmpty) Future(Left("EMPTY_SHOPPING_LIST_TITLE"))
    else
      decodeTokenAndGetShoppingList(token, shoppingListId).flatMap {
        case Left(e) => Future(Left(e))
        case Right(_) => ShoppingListStorage.updateShoppingListTitle(shoppingListId, title).map(_.left.map(throw _).map(_ => true))
      }
  }
}
