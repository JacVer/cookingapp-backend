package com.service

import com.model.nutrition.{NutriScoreInput, NutritionalInformation}

object NutritionService {

  def computeEnergyScore(nutriScoreInput: List[NutriScoreInput]): Int = {

    def getEnergyDensityScore(energyDensity: Double): Int = energyDensity match {
      case e if e % 335.0 == 0 => (e / 335).toInt - 1
      case e => scala.math.floor(e / 335.0).toInt
    }

    val kiloCalorieToKiloJoule = 4.184
    val hundredGramsPortions = nutriScoreInput.map(_.weightGram).sum / 100.0
    val energyDensityKJ = nutriScoreInput.map { n => n.energyKiloCalories * kiloCalorieToKiloJoule * n.weightGram / 100.0}.sum / hundredGramsPortions
    getEnergyDensityScore(energyDensityKJ)
  }

  def computeSaturatedFatScore(nutriScoreInput: List[NutriScoreInput]): Int = {

    def getSaturatedFatDensityScore(saturatedFatDensity: Double): Int = saturatedFatDensity match {
      case s if s % 1.0 == 0 => s.toInt - 1
      case s => scala.math.floor(s).toInt
    }

    val hundredGramsPortions = nutriScoreInput.map(_.weightGram).sum / 100.0
    val saturatedFatDensity = nutriScoreInput.map { n => n.saturatedFattyAcidGram * n.weightGram / 100.0}.sum / hundredGramsPortions
    getSaturatedFatDensityScore(saturatedFatDensity)
  }

  def computeSugarScore(nutriScoreInput: List[NutriScoreInput]): Int = {

    def getSugarScore(sugarDensity: Double): Int = sugarDensity match {
      case s if s <= 4.5 => 0
      case s if s > 45 => 10
      case s if s > 40 => 9
      case s if s > 36 => 8
      case s if s > 31 => 7
      case s if s > 27 => 6
      case s if s > 22.5 => 5
      case s if s > 18 => 4
      case s if s > 13.5 => 3
      case s if s > 9 => 2
      case s if s > 4.5 => 1
    }

    val hundredGramsPortions = nutriScoreInput.map(_.weightGram).sum / 100.0
    val sugarDensity = nutriScoreInput.map { n => n.sugarGram * n.weightGram / 100.0}.sum / hundredGramsPortions
    getSugarScore(sugarDensity)
  }

  def computeSodiumScore(nutriScoreInput: List[NutriScoreInput]): Int = {

    def getSodiumScore(sodiumDensity: Double): Int = sodiumDensity match {
      case s if s % 90.0 == 0 => s.toInt - 1
      case s => scala.math.floor(s / 90.0).toInt
    }

    val hundredGramsPortions = nutriScoreInput.map(_.weightGram).sum / 100.0
    val sodiumDensity = nutriScoreInput.map { n => n.sodiumMilliGram * n.weightGram / 100.0}.sum / hundredGramsPortions
    getSodiumScore(sodiumDensity)
  }

  def computeTEVScore(nutriScoreInput: List[NutriScoreInput]): Int = {

    def getTEVScore(TEV: Double): Int = TEV match {
      case t if t <= 0.4 => 0
      case t if t > 0.8 => 5
      case t if t > 0.6 => 2
      case t if t > 0.4 => 1
    }

    val hundredGramsPortions = nutriScoreInput.map(_.weightGram).sum / 100.0
    val TEV = nutriScoreInput.map { n => if(n.isTEV) n.weightGram / 100.0 else 0.0}.sum / hundredGramsPortions
    getTEVScore(TEV)
  }

  def computeDietaryFiberScore(nutriScoreInput: List[NutriScoreInput]): Int = {

    def getDietaryFiberScore(dietaryFiberDensity: Double): Int = dietaryFiberDensity match {
      case d if d <= 0.9 => 0
      case d if d > 4.7 => 5
      case d if d > 3.7 => 4
      case d if d > 2.8 => 3
      case d if d > 1.9 => 2
      case d if d > 0.9 => 1
    }

    val hundredGramsPortions = nutriScoreInput.map(_.weightGram).sum / 100.0
    val dietaryFiberDensity = nutriScoreInput.map { n => n.dietaryFiberGram * n.weightGram / 100.0}.sum / hundredGramsPortions
    getDietaryFiberScore(dietaryFiberDensity)
  }

  def computeProteinScore(nutriScoreInput: List[NutriScoreInput]): Int = {

    def getProteinScore(proteinDensity: Double): Int = proteinDensity match {
      case p if p <= 1.6 => 0
      case p if p > 8 => 5
      case p if p > 6.4 => 4
      case p if p > 4.8 => 3
      case p if p > 3.2 => 2
      case p if p > 1.6 => 1
    }

    val hundredGramsPortions = nutriScoreInput.map(_.weightGram).sum / 100.0
    val proteinDensity = nutriScoreInput.map { n => n.proteinGram * n.weightGram / 100.0}.sum / hundredGramsPortions
    getProteinScore(proteinDensity)
  }

  def computeRecipeNutritionalScore(nutriScoreInput: List[NutriScoreInput]): Int = {

    val energyScore = computeEnergyScore(nutriScoreInput)
    val saturatedFatScore = computeSaturatedFatScore(nutriScoreInput)
    val sugarScore = computeSugarScore(nutriScoreInput)
    val sodiumScore = computeSodiumScore(nutriScoreInput)
    val N = energyScore + saturatedFatScore + sugarScore + sodiumScore

    val TEVScore = computeTEVScore(nutriScoreInput)
    val dietaryFiberScore = computeDietaryFiberScore(nutriScoreInput)
    val proteinScore = computeProteinScore(nutriScoreInput)

    val nutritionalScore =
      if(N < 11) N - TEVScore - dietaryFiberScore - proteinScore
      else if(N >= 11 && TEVScore == 5) N - TEVScore - dietaryFiberScore - proteinScore
      else N - TEVScore - dietaryFiberScore

    nutritionalScore
  }

  def getNutriScoreLabel(nutritionalScore: Int): String = nutritionalScore match {
    case n if n <= -1 => "A"
    case n if n <= 2 => "B"
    case n if n <= 10 => "C"
    case n if n <= 18 => "D"
    case n if n <= 40 => "E"
  }

  def computeRecipeNutritionalInformation(nutriScoreInput: List[NutriScoreInput]): NutritionalInformation = {
    nutriScoreInput
      .map { n =>
        val weight100Gram = n.weightGram / 100.0
        NutritionalInformation(
          n.energyKiloCalories * weight100Gram,
          n.lipidGram * weight100Gram,
          n.saturatedFattyAcidGram * weight100Gram,
          n.carbohydrateGram * weight100Gram,
          n.sugarGram * weight100Gram,
          n.dietaryFiberGram * weight100Gram,
          n.proteinGram * weight100Gram,
          n.saltGram * weight100Gram,
          n.sodiumMilliGram * weight100Gram
        )
      }
      .reduce((n1, n2) =>
        NutritionalInformation(
          n1.energyKiloCalories + n2.energyKiloCalories,
          n1.lipidGram + n2.lipidGram,
          n1.saturatedFattyAcidGram + n2.saturatedFattyAcidGram,
          n1.carbohydrateGram + n2.carbohydrateGram,
          n1.sugarGram + n2.sugarGram,
          n1.dietaryFiberGram + n2.dietaryFiberGram,
          n1.proteinGram + n2.proteinGram,
          n1.saltGram + n2.saltGram,
          n1.sodiumMilliGram + n2.sodiumMilliGram
        )
      )
  }
}
