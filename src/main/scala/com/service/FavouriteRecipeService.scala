package com.service

import java.time.LocalDateTime

import com.model.recipe.Recipe
import com.storage.FavouriteRecipeStorage

import scala.concurrent.{ExecutionContext, Future}

object FavouriteRecipeService {

  def findFavouriteRecipe(token: String)(implicit executionContext: ExecutionContext):  Future[Either[String, List[Recipe]]] = {
    AccountService.decodeToken(token) match {
      case Left(exception) => Future(Left(exception))
      case Right(accountId) =>
        FavouriteRecipeStorage.selectFavouriteRecipe(accountId).map(_.left.map(throw _))
    }
  }

  def addFavouriteRecipe(token: String, recipeId: Int)(implicit executionContext: ExecutionContext):  Future[Either[String, Boolean]] = {
    AccountService.decodeToken(token) match {
      case Left(exception) => Future(Left(exception))
      case Right(accountId) =>
        FavouriteRecipeStorage.selectFavouriteRecipe(accountId).flatMap {
          case Left(psqlException) => throw psqlException
          case Right(recipes) if recipes.map(_.recipeId).contains(recipeId) => Future(Left("ALREADY_A_FAVOURITE"))
          case Right(_) =>
            FavouriteRecipeStorage.insertFavouriteRecipe(accountId, recipeId, LocalDateTime.now()).map {
              case Left(psqlException) => throw psqlException
              case Right(1) => Right(true)
              case Right(i) => throw new Error(s"Unexpected number of favourites created $i != 1")
            }
        }
      }
  }

  def deleteFavouriteRecipe(token: String, recipeId: Int)(implicit executionContext: ExecutionContext):  Future[Either[String, Boolean]] = {
    AccountService.decodeToken(token) match {
      case Left(exception) => Future(Left(exception))
      case Right(accountId) =>
        FavouriteRecipeStorage.selectFavouriteRecipe(accountId).flatMap {
          case Left(psqlException) => throw psqlException
          case Right(recipes) if !recipes.map(_.recipeId).contains(recipeId) => Future(Left("NOT_A_FAVOURITE"))
          case Right(_) =>
            FavouriteRecipeStorage.deleteFavouriteRecipe(accountId, recipeId).map {
              case Left(psqlException) => throw psqlException
              case Right(1) => Right(true)
              case Right(i) => throw new Error(s"Unexpected number of favourites deleted $i != 1")
            }
        }
    }
  }
}
