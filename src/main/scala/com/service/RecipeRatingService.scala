package com.service

import java.time.LocalDateTime

import com.model.Account
import com.model.recipe.{RecipeComment, RecipeRating}
import com.storage.RecipeRatingStorage

import scala.concurrent.{ExecutionContext, Future}

object RecipeRatingService {
  def addRecipeRating(recipeId: Int, rating: Int, token: String)(implicit executionContext: ExecutionContext): Future[Either[String, RecipeRating]] = {
    if(!List(1, 2, 3, 4, 5).contains(rating)) Future(Left("INVALID_RATING"))
    else {
      Account.decodeJsonWebToken(token) match {
        case Left(_) => Future(Left("INVALID_TOKEN"))
        case Right(accountId) =>
          RecipeService.recipeExists(recipeId).flatMap{
            case false => Future(Left("RECIPE_NOT_FOUND"))
            case true =>
              RecipeRatingStorage.deleteRating(recipeId, accountId).flatMap {
                case Left(psqlException) => throw psqlException
                case Right(_) =>
                  RecipeRatingStorage.insertRating(recipeId, accountId, LocalDateTime.now(), rating).flatMap {
                    case Left(psqlException) => throw psqlException
                    case Right(_) =>
                      RecipeRatingStorage.selectRecipeRating(recipeId).map {
                        case Left(psqlException) => throw psqlException
                        case Right(Nil) => Right(RecipeRating(None, 0))
                        case Right(recipeRating :: _) => Right(recipeRating)
                      }
                  }
              }
          }
      }
    }
  }

  def getRecipeRating(recipeId: Int)(implicit executionContext: ExecutionContext): Future[Either[String, RecipeRating]] = {
    RecipeService.recipeExists(recipeId).flatMap {
      case false => Future(Left("RECIPE_NOT_FOUND"))
      case true =>
        RecipeRatingStorage.selectRecipeRating(recipeId).map {
          case Left(psqlException) => throw psqlException
          case Right(Nil) => Right(RecipeRating(None, 0))
          case Right(recipeRating :: _) => Right(recipeRating)
        }
    }
  }

  def addRecipeComment(recipeId: Int, comment: String, token: String)(implicit executionContext: ExecutionContext): Future[Either[String, Boolean]] = {
    Account.decodeJsonWebToken(token) match {
      case Left(_) => Future(Left("INVALID_TOKEN"))
      case Right(accountId) =>
        RecipeService.recipeExists(recipeId).flatMap {
          case false => Future(Left("RECIPE_NOT_FOUND"))
          case true =>
            RecipeRatingStorage.insertComment(recipeId, accountId, LocalDateTime.now(), comment).map {
              case Left(psqlException) => throw psqlException
              case Right(_) => Right(true)
            }
        }
    }
  }

  def getRecipeComments(recipeId: Int)(implicit executionContext: ExecutionContext): Future[Either[String, List[RecipeComment]]] = {
    RecipeService.recipeExists(recipeId).flatMap {
      case false => Future(Left("RECIPE_NOT_FOUND"))
      case true =>
        RecipeRatingStorage.selectRecipeComment(recipeId).map {
          case Left(psqlException) => throw psqlException
          case Right(comments) => Right(comments)
        }
    }
  }
}
