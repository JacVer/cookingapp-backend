package com.service

import java.time.Month

import com.model.food.{Food, FoodUnit}
import com.model.nutrition.NutritionalInformation
import com.model.recipe.{NewRecipeIngredient, Recipe}
import com.model.Account
import com.storage.RecipeStorage

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

object RecipeService {

  private def parseMonth(month: String): Month = month match {
    case "january" => Month.JANUARY
    case "february" => Month.FEBRUARY
    case "march" => Month.MARCH
    case "april" => Month.APRIL
    case "may" => Month.MAY
    case "june" => Month.JUNE
    case "july" => Month.JULY
    case "august" => Month.AUGUST
    case "september" => Month.SEPTEMBER
    case "october" => Month.OCTOBER
    case "november" => Month.NOVEMBER
    case "december" => Month.DECEMBER
    case m => throw new Error(s"INVALID_MONTH:$m")
  }

  // TODO : limit number of results sent
  def listRecipes(
    title: Option[String],
    ingredients: Option[String],
    pseudo: Option[String],
    token: Option[String],
    pescetarian: Option[Boolean],
    vegetarian: Option[Boolean],
    vegan: Option[Boolean],
    glutenFree: Option[Boolean],
    month: Option[String]
  )(implicit executionContext: ExecutionContext): Future[Either[String, List[Recipe]]] = {

    if(title.isEmpty && ingredients.isEmpty && pseudo.isEmpty && month.isEmpty) Future(Left("NO_PARAMETERS"))
    else if(!ingredients.forall(_.split(",").map(s => Try(s.toInt)).forall(_.isSuccess))) Future(Left("INVALID_INGREDIENTS"))
    else if(!title.forall(Recipe.isTitleValid)) Future(Left("INVALID_RECIPE_TITLE"))
    else if(!pseudo.forall(Account.isPseudoValid)) Future(Left("INVALID_PSEUDO"))
    else if(!month.forall(m => List("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december").contains(m))) {
      Future(Left("INVALID_MONTH"))
    }
    else {
      val foodIds = ingredients.map(_.split(",").map(s => s.toInt).toList)

      token.map(Account.decodeJsonWebToken) match {
        case Some(Left(_)) => Future(Left("INVALID_TOKEN"))
        case Some(Right(accountId)) =>
          RecipeStorage
            .selectRecipe(
              title,
              pseudo,
              Some(accountId),
              foodIds = foodIds,
              pescetarian = pescetarian.getOrElse(false),
              vegetarian = vegetarian.getOrElse(false),
              vegan = vegan.getOrElse(false),
              glutenFree = glutenFree.getOrElse(false),
              month = month.map(parseMonth)
            )
            .map(_.left.map(throw _))
        case None =>
          RecipeStorage
            .selectRecipe(
              title,
              pseudo,
              foodIds = foodIds,
              pescetarian = pescetarian.getOrElse(false),
              vegetarian = vegetarian.getOrElse(false),
              vegan = vegan.getOrElse(false),
              glutenFree = glutenFree.getOrElse(false),
              month = month.map(parseMonth)
            )
            .map(_.left.map(throw _))
      }
    }
  }

  def createRecipe(
    token: String,
    title: String,
    description: String,
    instructions: List[String],
    ingredients: List[(Int, Int, Double)],
    servings: Int,
    preparationTimeMinutes: Double,
    sourceURL: Option[String],
    base64EncodedPicture: Option[String]
  )(implicit executionContext: ExecutionContext): Future[Either[String, Int]] = {

    val sortedIngredients = ingredients.sortBy(_._1)

    val duplicateFoodIds = Recipe.getDuplicatedFoodIdUnitId(ingredients.map { case (foodId, unitId, _) => (foodId, unitId) })
    if (!Recipe.isTitleValid(title)) Future(Left("INVALID_RECIPE_TITLE"))
    else if (duplicateFoodIds.nonEmpty) Future(Left(s"DUPLICATED_FOOD:${duplicateFoodIds.mkString(",")}"))
    else if (ingredients.isEmpty) Future(Left("NO_INGREDIENTS"))
    else if (instructions.isEmpty) Future(Left("NO_INSTRUCTIONS"))
    else if (servings <= 0) Future(Left("INVALID_SERVINGS"))
    else if (preparationTimeMinutes <= 0.0) Future(Left("INVALID_PREPARATION_TIME"))
    else {
      AccountService.decodeToken(token) match {
        case Left(exception) => Future(Left(exception))
        case Right(accountId) =>
          FoodService
            .getFoodWithUnit(ingredients.map { case (foodId, unitId, _) => (foodId, unitId) })
            .flatMap {
              case Left(exception) => Future(Left(exception))
              case Right(foods) =>
                val quantity = sortedIngredients.map(_._3)
                val ingredientsWithNutritionalInformation = foods.zip(quantity)
                val nutriScoreInput = ingredientsWithNutritionalInformation.map { case ((food, foodUnit), quantity) => food.toNutriScoreInput(foodUnit.weightGrams * quantity)}
                val recipeNutritionalInformation = NutritionService.computeRecipeNutritionalInformation(nutriScoreInput)
                val nutritionalScore = NutritionService.computeRecipeNutritionalScore(nutriScoreInput)
                val nutriScoreLabel = NutritionService.getNutriScoreLabel(nutritionalScore)

                RecipeStorage
                  .insertRecipe(
                    accountId,
                    title,
                    description,
                    instructions,
                    sourceURL,
                    servings,
                    nutriScoreLabel,
                    preparationTimeMinutes,
                    base64EncodedPicture,
                    recipeNutritionalInformation.energyKiloCalories,
                    recipeNutritionalInformation.lipidGram,
                    recipeNutritionalInformation.saturatedFattyAcidGram,
                    recipeNutritionalInformation.carbohydrateGram,
                    recipeNutritionalInformation.sugarGram,
                    recipeNutritionalInformation.dietaryFiberGram,
                    recipeNutritionalInformation.proteinGram,
                    recipeNutritionalInformation.saltGram,
                    recipeNutritionalInformation.sodiumMilliGram,
                    foods.forall(_._1.pescetarian),
                    foods.forall(_._1.vegetarian),
                    foods.forall(_._1.vegan),
                    foods.forall(_._1.glutenFree),
                    foods.forall(_._1.january),
                    foods.forall(_._1.february),
                    foods.forall(_._1.march),
                    foods.forall(_._1.april),
                    foods.forall(_._1.may),
                    foods.forall(_._1.june),
                    foods.forall(_._1.july),
                    foods.forall(_._1.august),
                    foods.forall(_._1.september),
                    foods.forall(_._1.october),
                    foods.forall(_._1.november),
                    foods.forall(_._1.december)
                  )
                  .flatMap {
                    case Left(error) => throw error
                    case Right(Nil) => throw new Error("Unexpected Nil when creating the recipe")
                    case Right(createdRecipeRecipeId :: _) =>
                      val newRecipeIngredients = ingredientsWithNutritionalInformation.map {
                        case ((food, foodUnit), quantity) => NewRecipeIngredient(createdRecipeRecipeId, food.foodId, foodUnit.unitId, quantity)
                      }
                      RecipeStorage.insertIngredients(newRecipeIngredients).map {
                        case Left(psqlException) => throw psqlException
                        case Right(i) if i == ingredients.length => Right(createdRecipeRecipeId)
                        case Right(i) => throw new Error(s"Unexpected number of ingredients created $i != ${ingredients.length}")
                      }
                  }
            }
      }
    }
  }

  def getNutritionalInformation(token: String, ingredients: List[(Int, Int, Double)])(implicit executionContext: ExecutionContext): Future[Either[String, NutritionalInformation]] = {

    val duplicateFoodIds = Recipe.getDuplicatedFoodIdUnitId(ingredients.map { case (foodId, unitId, _) => (foodId, unitId) })

    if (duplicateFoodIds.nonEmpty) Future(Left(s"DUPLICATED_FOOD:${duplicateFoodIds.mkString(",")}"))
    else {
      AccountService.decodeToken(token) match {
        case Left(exception) => Future(Left(exception))
        case Right(_) =>
          FoodService
            .getFoodWithUnit(ingredients.map { case (foodId, unitId, _) => (foodId, unitId) })
            .map(_.map { foods =>
              val sortedIngredients = ingredients.sortBy(_._1)
              val quantity = sortedIngredients.map(_._3)
              val ingredientsWithNutritionalInformation = foods.zip(quantity)
              val nutriScoreInput = ingredientsWithNutritionalInformation.map { case ((food, foodUnit), quantity) => food.toNutriScoreInput(foodUnit.weightGrams * quantity)}
              NutritionService.computeRecipeNutritionalInformation(nutriScoreInput)
            })
      }
    }
  }

  def updateRecipe(
    recipeId: Int,
    token: String,
    title: String,
    description: String,
    instructions: List[String],
    ingredients: List[(Int, Int, Double)],
    servings: Int,
    preparationTimeMinutes: Double,
    sourceURL: Option[String],
    base64EncodedPicture: Option[String]
  )(implicit executionContext: ExecutionContext): Future[Either[String, Boolean]] = {

    val sortedIngredients = ingredients.sortBy(_._1)

    val duplicateFoodIds = Recipe.getDuplicatedFoodIdUnitId(ingredients.map { case (foodId, unitId, _) => (foodId, unitId) })
    if (!Recipe.isTitleValid(title)) Future(Left("INVALID_RECIPE_TITLE"))
    else if (duplicateFoodIds.nonEmpty) Future(Left(s"DUPLICATED_FOOD:${duplicateFoodIds.mkString(",")}"))
    else if (ingredients.isEmpty) Future(Left("NO_INGREDIENTS"))
    else if (instructions.isEmpty) Future(Left("NO_INSTRUCTIONS"))
    else if (servings <= 0) Future(Left("INVALID_SERVINGS"))
    else if (preparationTimeMinutes <= 0.0) Future(Left("INVALID_PREPARATION_TIME"))
    else {
      AccountService.decodeToken(token) match {
        case Left(exception) => Future(Left(exception))
        case Right(accountId) =>
          RecipeStorage
            .selectRecipe(recipeId = Some(recipeId))
            .flatMap {
              case Left(psqlException) => throw psqlException
              case Right(Nil) => Future(Left("RECIPE_NOT_FOUND"))
              case Right(r :: _) if r.accountId != accountId => Future(Left("USER_NOT_OWNER_OF_RECIPE"))
              case Right(r :: _) if r.accountId == accountId =>
                RecipeStorage
                  .deleteRecipeIngredients(recipeId)
                  .flatMap {
                    case Left(psqlException) => throw psqlException
                    case Right(_) =>
                      // TODO : abstract this out this as it is used in create and simulate functions
                      FoodService
                        .getFoodWithUnit(ingredients.map { case (foodId, unitId, _) => (foodId, unitId) })
                        .flatMap {
                          case Left(exception) => Future(Left(exception))
                          case Right(foods) =>
                            val quantity = sortedIngredients.map(_._3)
                            val ingredientsWithNutritionalInformation = foods.zip(quantity)
                            val nutriScoreInput = ingredientsWithNutritionalInformation.map { case ((food, foodUnit), quantity) => food.toNutriScoreInput(foodUnit.weightGrams * quantity)}
                            val recipeNutritionalInformation = NutritionService.computeRecipeNutritionalInformation(nutriScoreInput)
                            val nutritionalScore = NutritionService.computeRecipeNutritionalScore(nutriScoreInput)
                            val nutriScoreLabel = NutritionService.getNutriScoreLabel(nutritionalScore)

                            RecipeStorage
                              .updateRecipe(
                                recipeId,
                                title,
                                description,
                                instructions,
                                sourceURL,
                                servings,
                                nutriScoreLabel,
                                preparationTimeMinutes,
                                base64EncodedPicture,
                                recipeNutritionalInformation.energyKiloCalories,
                                recipeNutritionalInformation.lipidGram,
                                recipeNutritionalInformation.saturatedFattyAcidGram,
                                recipeNutritionalInformation.carbohydrateGram,
                                recipeNutritionalInformation.sugarGram,
                                recipeNutritionalInformation.dietaryFiberGram,
                                recipeNutritionalInformation.proteinGram,
                                recipeNutritionalInformation.saltGram,
                                recipeNutritionalInformation.sodiumMilliGram,
                                foods.forall(_._1.pescetarian),
                                foods.forall(_._1.vegetarian),
                                foods.forall(_._1.vegan),
                                foods.forall(_._1.glutenFree),
                                foods.forall(_._1.january),
                                foods.forall(_._1.february),
                                foods.forall(_._1.march),
                                foods.forall(_._1.april),
                                foods.forall(_._1.may),
                                foods.forall(_._1.june),
                                foods.forall(_._1.july),
                                foods.forall(_._1.august),
                                foods.forall(_._1.september),
                                foods.forall(_._1.october),
                                foods.forall(_._1.november),
                                foods.forall(_._1.december)
                              )
                              .flatMap {
                                case Left(error) => throw error
                                case Right(0) => throw new Error("Unexpected zero when updating the recipe")
                                case Right(i) if i > 0 =>
                                  val newRecipeIngredients = ingredientsWithNutritionalInformation.map {
                                    case ((food, foodUnit), quantity) => NewRecipeIngredient(recipeId, food.foodId, foodUnit.unitId, quantity)
                                  }
                                  RecipeStorage.insertIngredients(newRecipeIngredients).map {
                                    case Left(psqlException) => throw psqlException
                                    case Right(i) if i == ingredients.length => Right(true)
                                    case Right(i) => throw new Error(s"Unexpected number of ingredients created $i != ${ingredients.length}")
                                  }
                              }
                        }
                  }

            }
      }
    }
  }

  def recipeExists(recipeId: Int)(implicit executionContext: ExecutionContext): Future[Boolean] = {
    RecipeStorage.selectRecipe(recipeId = Some(recipeId)).map {
      case Left(psqlException) => throw psqlException
      case Right(Nil) => false
      case Right(_) => true
    }
  }

  def getRecipe(recipeId: Int, maybeToken: Option[String])(implicit executionContext: ExecutionContext): Future[Either[String, Recipe]] = {
    // TODO : there's probably a better way to do this
    maybeToken.map(Account.decodeJsonWebToken) match {
      case Some(Left(_)) => Future(Left("INVALID_TOKEN"))
      case maybeAccountId =>
        RecipeStorage.selectRecipe(
          recipeId = Some(recipeId),
          accountId = maybeAccountId.map(_.getOrElse(throw new Exception("Unexpected Left found")))
        )
          .map {
            case Left(psqlException) => throw psqlException
            case Right(Nil) => Left("RECIPE_NOT_FOUND")
            case Right(recipe :: _) => Right(recipe)
          }
    }
  }

  def getRecipeIngredients(recipeId: Int)(implicit executionContext: ExecutionContext): Future[Either[String, List[(Food, FoodUnit, Double)]]] = {
    RecipeStorage
      .selectRecipeIngredients(recipeId)
      .map {
        case Left(psqlException) => throw psqlException
        case Right(Nil) => Left("RECIPE_NOT_FOUND")
        case Right(ingredients) => Right(ingredients)
      }
  }

  def deleteRecipe(token: String, recipeId: Int)(implicit executionContext: ExecutionContext): Future[Either[String, Boolean]] = {
    AccountService.decodeToken(token) match {
      case Left(exception) => Future(Left(exception))
      case Right(accountId) =>
        RecipeStorage.selectRecipe(recipeId = Some(recipeId)).flatMap {
          case Left(psqlException) => throw psqlException
          case Right(Nil) => Future(Left("RECIPE_NOT_FOUND"))
          case Right(recipe :: _) if recipe.accountId != accountId => Future(Left("USER_NOT_OWNER_OF_RECIPE"))
          case Right(recipe :: _) if recipe.accountId == accountId =>
            RecipeStorage.hideRecipe(recipeId).map {
              case Left(psqlException) => throw psqlException
              case Right(i) if i != 1 => throw new Error(s"Unexpected value returned when hiding recipe : $i")
              case Right(1) => Right(true)
            }
        }
      }
  }
}
