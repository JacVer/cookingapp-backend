package com.admin

import java.time.LocalDateTime

import com.model.Account
import com.model.food.{CalnutFood, NewFoodUnit}
import com.model.recipe.{NewRecipeIngredient, Recipe}
import com.storage.{AccountStorage, FavouriteRecipeStorage, FoodStorage, FoodUnitStorage, RecipeRatingStorage, RecipeStorage, ShoppingListStorage}

object SetupProductionDatabase {

  val batchSize = 200

  ShoppingListStorage.dropTables()
  RecipeRatingStorage.dropTables()
  FavouriteRecipeStorage.dropTables()
  RecipeStorage.dropTables()
  FoodUnitStorage.dropTables()
  FoodStorage.dropTables()
  AccountStorage.dropTables()

  AccountStorage.createTables()
  FoodStorage.createTables()
  val calnutCsvFileNameName = "food_base_data.csv"
  val calnutCsvFileNamePath: String = getClass.getResource("/" + calnutCsvFileNameName).getPath

  val foodToInsert: Iterator[CalnutFood] = FoodStorage.readCalnutCsv(calnutCsvFileNamePath)
    .map(c => c.copy(categoryNameFr = c.categoryNameFr.trim, foodNameFr = c.foodNameFr.trim))
  val foodToInsertInBatch: List[Seq[(CalnutFood, Int)]] = foodToInsert.zipWithIndex.grouped(batchSize).toList
  foodToInsertInBatch
    .map(batch => (batch.head._2, batch.last._2, batch.map(_._1)))
    .map { case (startIndex, endIndex, calnutFoodBatch) =>
      FoodStorage.insertCalnutFoods(LocalDateTime.now(), calnutFoodBatch.toList) match {
        case Left(error) => throw error
        case Right(_) => println(s"Inserted ingredients $startIndex to $endIndex")
      }
    }

  println("====")

  FoodUnitStorage.createTables()
  val foodUnitCsvFileName = "food_units.csv"
  val foodUnitCsvFilePath: String = getClass.getResource("/" + foodUnitCsvFileName).getPath
  val foodUnitToInsert: Iterator[NewFoodUnit] = FoodUnitStorage.readFoodUnitCsv(foodUnitCsvFilePath)
  val foodUnitToInsertInBatch: List[Seq[(NewFoodUnit, Int)]] = foodUnitToInsert.zipWithIndex.grouped(batchSize).toList
  foodUnitToInsertInBatch
    .map(batch => (batch.head._2, batch.last._2, batch.map(_._1)))
    .map { case (startIndex, endIndex, foodUnitBatch) =>
      FoodUnitStorage.insertFoodUnits(foodUnitBatch.toList) match {
        case Left(error) => throw error
        case Right(_) => println(s"Inserted food units $startIndex to $endIndex")
      }
    }

  println("===")

  RecipeStorage.createTables()
  FavouriteRecipeStorage.createTables()
  RecipeRatingStorage.createTables()
  ShoppingListStorage.createTables()


  val testUser: Account = Account(0, "pwal@pwal.com", "pwal", Account.encryptPassword("pwalipwalo"))

  val testRecipe1: Recipe = Recipe(
    0, testUser.accountId, testUser.pseudo,
    "Purée aux trois racines",
    "",
    List(
      "Peler panais, pommes de terre et patates douces et les couper en cubes de 2cm.",
      "Placer le panais, les pommes de terre, la patate douce, le sel et le lait dans le col. Cuire 30min/100°C/vitesse 1 avec le gobelet doseur.",
      "Insérer le fouet, ajouter le fromage frais et battre 15sec/vitesse 5",
      "Servir parsemé de persil"
    ),
    sourceURL = None,
    servings = 4,
    "B",
    preparationTimeMinutes = 45.0,
    pescetarian = true, vegetarian = true, vegan = false, glutenFree = true,
    january = false, february = false, march = false, april = false, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
    energyKiloCalories = 1185.25, lipidGram = 46.645, saturatedFattyAcidGram = 29.17, carbohydrateGram = 145.345, sugarGram = 49.12, dietaryFiberGram = 23.11, proteinGram = 34.05, saltGram = 2.195, sodiumMilliGram = 879.55,
    picture = None,
    isFavourite = false
  )

  val testRecipe1Ingredients: List[NewRecipeIngredient] = List(
    NewRecipeIngredient(testRecipe1.recipeId, 20133, 0, 250), // Panais
    NewRecipeIngredient(testRecipe1.recipeId, 4048, 0, 300), // Pommes de terre
    NewRecipeIngredient(testRecipe1.recipeId, 4102, 0, 250), // Patates douces
    NewRecipeIngredient(testRecipe1.recipeId, 19041, 0, 350), // Lait
    NewRecipeIngredient(testRecipe1.recipeId, 19530, 0, 100), // Fromage frais ail et fines herbes
  )

  val testRecipe2: Recipe = Recipe(
    1, testUser.accountId, testUser.pseudo,
    "Cake aux olives",
    "",
    List(
      "Préchauffer le four à 180°",
      "Dans un récipient, mélanger les oeufs, la farine et la levure.",
      "Ajouter le lait et l'huile. Mélanger jusqu'à obtention d'une pâte homogène. Incorporer le fromage râpé.",
      "Ciseler grossièrement le basilic. Couper la fêta en dés. Couper les tomates confites en morceaux.",
      "Ajouter l'ensemble des ingrédients à la pâte et remuer délicatement pour ne pas écraser la fêta.",
      "Saler poivrer et ajouter les herbes de Provence et les olives entières. Mélanger à nouveau",
      "Beurrer et fariner le moule à cake et y verser la préparation. Enfourner 40-45 minutes. Laisser refroidir et déguster"
    ),
    sourceURL = None,
    servings = 4,
    "D",
    preparationTimeMinutes = 55.0,
    pescetarian = true, vegetarian = true, vegan = false, glutenFree = false,
    january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true,
    energyKiloCalories = 1673.226, lipidGram = 93.812, saturatedFattyAcidGram = 35.0943, carbohydrateGram = 117.2576, sugarGram = 8.3862, dietaryFiberGram = 20.9975, proteinGram = 79.4153, saltGram = 6.10943, sodiumMilliGram = 4155.47,
    picture = None,
    isFavourite = false
  )
  val testRecipe2Ingredients: List[NewRecipeIngredient] = List(
    NewRecipeIngredient(testRecipe2.recipeId, 22000, 0, 195), // Oeuf
    NewRecipeIngredient(testRecipe2.recipeId, 9410, 0, 150), // Farine
    NewRecipeIngredient(testRecipe2.recipeId, 11046, 0, 12), // Levure chimique
    NewRecipeIngredient(testRecipe2.recipeId, 17270, 0, 10), // Huile d'olive
    NewRecipeIngredient(testRecipe2.recipeId, 19041, 0, 12), // Lait
    NewRecipeIngredient(testRecipe2.recipeId, 12118, 0, 100), // Emmental râpé
    NewRecipeIngredient(testRecipe2.recipeId, 13186, 0, 80), // Olives noires
    NewRecipeIngredient(testRecipe2.recipeId, 12061, 0, 50), // Fêta
    NewRecipeIngredient(testRecipe2.recipeId, 20256, 0, 50), // Tomates confites
    NewRecipeIngredient(testRecipe2.recipeId, 11033, 0, 5), // Basilic frais
    NewRecipeIngredient(testRecipe2.recipeId, 11060, 0, 5), // Herbes de Provence
  )

  val testRecipe3: Recipe = Recipe(
    2, testUser.accountId, testUser.pseudo,
    "Chou-fleur, Gnocchi, Epinard et pesto",
    "",
    List(
      "Mettre de l'eau à bouillir dans une casserole de taille moyenne et mettre le four à préchauffer à 180°C",
      "Couper le chou-fleur en florettes pendant que l'eau bout.",
      "Mettre les florettes à cuire dans l'eau pendant 5 minutes. Le sortir de l'eau et le réserver.",
      "Mettre les gnocchis dans l'eau. Les sortir de l'eau quand elle remonte à la surface puis les réserver.",
      "Dans une plaque de cuisson ou un plat , mettre les florettes, les gnocchis et les épinards, et les badigeonner de pesto.",
      "Enfourner pour trente minutes."
    ),
    sourceURL = None,
    servings = 4,
    "A",
    preparationTimeMinutes = 50.0,
    pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
    january = false, february = false, march = false, april = false, may = true, june = true, july = true, august = true, september = true, october = true, november = false, december = false,
    energyKiloCalories = 1490.3, lipidGram = 93.46, saturatedFattyAcidGram = 10.105, carbohydrateGram = 132.67, sugarGram = 24.81, dietaryFiberGram = 37.4, proteinGram = 50.542, saltGram = 10.532, sodiumMilliGram = 4254.0,
    picture = None,
    isFavourite = false,
  )
  val testRecipe3Ingredients: List[NewRecipeIngredient] = List(
    NewRecipeIngredient(testRecipe3.recipeId, 20017, 0, 1000), // Chou-fleur
    NewRecipeIngredient(testRecipe3.recipeId, 25579, 0, 300), // Gnocchi
    NewRecipeIngredient(testRecipe3.recipeId, 20060, 0, 300), // Épinard
    NewRecipeIngredient(testRecipe3.recipeId, 11179, 0, 180), // Pesto
  )

  AccountStorage.insertTestAccounts(List(testUser))
  RecipeStorage.insertTestRecipes(List(testRecipe1, testRecipe2, testRecipe3))
  RecipeStorage.insertTestIngredients(testRecipe1Ingredients ::: testRecipe2Ingredients ::: testRecipe3Ingredients)

  println("Inserted test recipes !")

  FoodUnitStorage.selectFoodIdWithoutUnit() match {
    case Left(psqlException) => throw psqlException
    case Right(Nil) =>
    case Right(foodIdsWithoutUnits) => throw new Error(s"FOOD_IDS_WITHOUT_UNIT:${foodIdsWithoutUnits.mkString(",")}")
  }
  FoodUnitStorage.selectFoodIdWithoutGramUnit() match {
    case Left(psqlException) => throw psqlException
    case Right(Nil) =>
    case Right(foodIdWithoutGramUnit) => throw new Error(s"FOOD_ID_WITHOUT_GRAM_UNIT:${foodIdWithoutGramUnit.mkString(",")}")
  }
  FoodUnitStorage.selectUnitIdWithMultipleUnitNameFr() match {
    case Left(psqlException) => throw psqlException
    case Right(Nil) =>
    case Right(unitIdWithMultipleNames) => throw new Error(s"UNIT_ID_WITH_MULTIPLE_UNIT_NAME_FR:${unitIdWithMultipleNames.mkString(",")}")
  }

  println("Data is checked !")
}
