package com.service

import java.time.LocalDateTime

import com.BaseTest
import com.model.food.{CalnutFood, Food, FoodUnit, NewFoodUnit}
import com.storage.{FoodStorage, FoodUnitStorage}
import org.scalatest._

import scala.concurrent.ExecutionContextExecutor

class FoodServiceTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = new Context {
    FoodStorage.createTables()
    FoodUnitStorage.createTables()
    FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood1, testCalnutFood2, testCalnutFood3))
    FoodUnitStorage.insertFoodUnits(List(
      testCalnutFood1Unit0,
      testCalnutFood1Unit1,
      testCalnutFood2Unit0,
      testCalnutFood2Unit1,
      testCalnutFood3Unit0
    ))
  }

  override def afterAll(): Unit = {
    FoodUnitStorage.dropTables()
    FoodStorage.dropTables()
  }

  "FoodService" when {
    "getFoodNutriScoreInformation" should {
      "return the food nutri score information if no foods are missing" in new Context {
        awaitForResult(FoodService.getFoodWithUnit(List((testCalnutFood1.foodId, testCalnutFood1Unit1.unitId), (testCalnutFood2.foodId, testCalnutFood2Unit1.unitId), (testCalnutFood3.foodId, testCalnutFood3Unit0.unitId)))) shouldBe
          Right(List(
            (
              testCalnutFood3.toFood,
              FoodUnit(testCalnutFood3Unit0.unitId, testCalnutFood3Unit0.unitNameFr, testCalnutFood3Unit0.weightGrams)
            ),
            (
              testCalnutFood2.toFood,
              FoodUnit(testCalnutFood2Unit1.unitId, testCalnutFood2Unit1.unitNameFr, testCalnutFood2Unit1.weightGrams)
            ),
            (
              testCalnutFood1.toFood,
              FoodUnit(testCalnutFood1Unit1.unitId, testCalnutFood1Unit1.unitNameFr, testCalnutFood1Unit1.weightGrams)
            ),
          ))
      }

      "return an error including the (foodIds, unitIds) of the missing foods" in new Context {
        awaitForResult(FoodService.getFoodWithUnit(List((testCalnutFood1.foodId, testCalnutFood1Unit1.unitId), (missingFoodId, 0), (testCalnutFood1.foodId, 56)))) shouldBe
          Left(s"FOOD_UNIT_NOT_FOUND:($missingFoodId,0),(${testCalnutFood1.foodId},56)")
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.now()

    val testCalnutFood1: CalnutFood = CalnutFood(24, "Légumes", 20303, "Brocoli", nutriScoreTEV = true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=false, february=false, march=false, april=false, may=false, june=true, july=true, august=true, september=true, october=true, november=false, december=false, 23.1, 93.8, 0.033, 13, 11, 39, 90, 43, 0.17, 0.33, 0.15, 0.18, 10, 10, 2.19, 1.03, 0.6, 0.3, 0.1, 0.1, 0.3, 0.1, 0.1, 0.18, 0.25, 3, 0.5, 0.14, 0.13, 0.18, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.1, 0.04, 0.12, 0.06, 0.12, 0.005, 0.005, 0.005, 0, 576, 0, 1.69, 49.6, 0, 18.1, 0.0075, 0.005, 0.05, 0.25, 0.048, 0, 24.7, 0, 0.053, 0)
    val testCalnutFood2: CalnutFood = CalnutFood(0, "Bouillons", 11001, "Bouillon de bœuf déshydraté", nutriScoreTEV = false, pescetarian=false, vegetarian=false, vegan=false, glutenFree=false, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 240, 2.6, 33, 13200, 21.9, 127, 344, 32.7, 0.1, 0.35, 0.12, 0.32, 4.6, 2.5, 10.2, 19.7, 3.95, 0.32, 0, 0, 0.27, 0.15, 3.36, 0, 0, 0.65, 13.3, 7.03, 2.6, 1.4, 0, 0, 0.013, 0.013, 0.038, 0.18, 5.42, 1.25, 2.6, 0.96, 0.038, 0, 0, 0, 0, 0, 0, 2.17, 0, 0, 0, 1.38, 0.23, 2.09, 0.48, 0.29, 0.34, 76.3, 0, 0, 0)
    val testCalnutFood3: CalnutFood = CalnutFood(4, "Produits céréaliers", 9341, "Quinoa cuit", nutriScoreTEV = false, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 148, 61.6, 0.015, 6, 71, 180, 220, 23, 0.7, 1.6, 0.21, 1.2, 10, 10, 4.66, 27.9, 0.87, 0.05, 0.05, 0.05, 0.05, 0.05, 0.87, 24.3, 0, 3.8, 1.1, 0.14, 0.31, 0.56, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.12, 0.02, 0.28, 0.49, 0.07, 0.005, 0.005, 0.005, 0, 2.5, 0, 0.88, 0.4, 0, 0, 0.14, 0.014, 0.2, 0.61, 0.089, 0, 49.9, 0, 0, 0)

    val testCalnutFood1Unit0: NewFoodUnit = NewFoodUnit(testCalnutFood1.foodId, 0, "grammes", 1)
    val testCalnutFood1Unit1: NewFoodUnit = NewFoodUnit(testCalnutFood1.foodId, 1, "brocoli moyen", 300)
    val testCalnutFood2Unit0: NewFoodUnit = NewFoodUnit(testCalnutFood2.foodId, 0, "grammes", 1)
    val testCalnutFood2Unit1: NewFoodUnit = NewFoodUnit(testCalnutFood2.foodId, 2, "cube", 10)
    val testCalnutFood3Unit0: NewFoodUnit = NewFoodUnit(testCalnutFood3.foodId, 0, "grammes", 1)

    val missingFoodId = 5345
  }

}