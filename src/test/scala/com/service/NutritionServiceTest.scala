package com.service

import com.BaseTest
import com.model.nutrition.{NutriScoreInput, NutritionalInformation}
import org.scalatest._

class NutritionServiceTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  "RecipeService" when {

    "computeEnergyScore" should {
      "compute the energy score" in new Context {
        NutritionService.computeEnergyScore(recipe1Ingredients) shouldBe 1
        NutritionService.computeEnergyScore(recipe2Ingredients) shouldBe 3
        NutritionService.computeEnergyScore(recipe3Ingredients) shouldBe 1
      }
    }

    "computeSaturatedFatScore" should {
      "compute the saturated fat score" in new Context {
        NutritionService.computeSaturatedFatScore(recipe1Ingredients) shouldBe 2
        NutritionService.computeSaturatedFatScore(recipe2Ingredients) shouldBe 5
        NutritionService.computeSaturatedFatScore(recipe3Ingredients) shouldBe 0
      }
    }

    "computeSugarScore" should {
      "compute the sugar score" in new Context {
        NutritionService.computeSugarScore(recipe1Ingredients) shouldBe 0
        NutritionService.computeSugarScore(recipe2Ingredients) shouldBe 0
        NutritionService.computeSugarScore(recipe3Ingredients) shouldBe 0
      }
    }

    "computeSodiumScore" should {
      "compute the sodium score" in new Context {
        NutritionService.computeSodiumScore(recipe1Ingredients) shouldBe 0
        NutritionService.computeSodiumScore(recipe2Ingredients) shouldBe 6
        NutritionService.computeSodiumScore(recipe3Ingredients) shouldBe 2
      }
    }

    "computeTEVScore" should {
      "compute the TEV score" in new Context {
        NutritionService.computeTEVScore(recipe1Ingredients) shouldBe 0
        NutritionService.computeTEVScore(recipe2Ingredients) shouldBe 0
        NutritionService.computeTEVScore(recipe3Ingredients) shouldBe 2
      }
    }

    "computeDietaryFiberScore" should {
      "compute the dietary fiber score" in new Context {
        NutritionService.computeDietaryFiberScore(recipe1Ingredients) shouldBe 1
        NutritionService.computeDietaryFiberScore(recipe2Ingredients) shouldBe 3
        NutritionService.computeDietaryFiberScore(recipe3Ingredients) shouldBe 2
      }
    }

    "computeProteinScore" should {
      "compute the protein score" in new Context {
        NutritionService.computeProteinScore(recipe1Ingredients) shouldBe 1
        NutritionService.computeProteinScore(recipe2Ingredients) shouldBe 5
        NutritionService.computeProteinScore(recipe3Ingredients) shouldBe 1
      }
    }

    "computeRecipeNutritionalScore" should {
      "compute the nutritional score of a recipe based on its ingredient" in new Context {
        NutritionService.computeRecipeNutritionalScore(recipe1Ingredients) shouldBe 1
        NutritionService.computeRecipeNutritionalScore(recipe2Ingredients) shouldBe 11
        NutritionService.computeRecipeNutritionalScore(recipe3Ingredients) shouldBe -2
      }
    }

    "getNutriScore" should {
      "get the right label based on the nutritional score" in {
        NutritionService.getNutriScoreLabel(1) shouldBe "B"
        NutritionService.getNutriScoreLabel(11) shouldBe "D"
        NutritionService.getNutriScoreLabel(-2) shouldBe "A"
      }
    }

    "getRecipeNutritionInformation" should {
       "return the aggregated nutritional information" in {
         NutritionService.computeRecipeNutritionalInformation(List(
           NutriScoreInput(isTEV = false, 50, 25, 10, 50, 0, 5, 14, 0.5, 0.15, 100.0),
         )) shouldBe NutritionalInformation(50, 25, 10, 50, 0, 5, 14, 0.5, 0.15)

         NutritionService.computeRecipeNutritionalInformation(List(
           NutriScoreInput(isTEV = false, 50, 25, 10, 50, 0, 5, 15, 0.5, 0.15, 10.0),
         )) shouldBe NutritionalInformation(5, 2.5, 1, 5, 0, 0.5, 1.5, 0.05, 0.015)

         NutritionService.computeRecipeNutritionalInformation(List(
           NutriScoreInput(isTEV = false, 50, 25, 10, 50, 0, 5, 15, 0.5, 0.15, 100.0),
           NutriScoreInput(isTEV = false, 100, 10, 20, 45, 25, 6, 2, 8, 2, 200.0),
         )) shouldBe NutritionalInformation(250, 45, 50, 140, 50, 17, 19, 16.5, 4.15)
       }
    }
  }

  trait Context {
    val recipe1Ingredients: List[NutriScoreInput] = List(
      NutriScoreInput(
        isTEV = true,
        energyKiloCalories = 67.8,
        lipidGram = 0.3,
        saturatedFattyAcidGram = 0.05,
        carbohydrateGram = 13.4,
        sugarGram = 4.8,
        dietaryFiberGram = 3.6,
        proteinGram = 1.32,
        saltGram = 0.025,
        sodiumMilliGram = 10,
        250.0
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 93.2,
        lipidGram = 1.37,
        saturatedFattyAcidGram = 0.4,
        carbohydrateGram = 17.2,
        sugarGram = 1.05,
        dietaryFiberGram = 1.96,
        proteinGram = 2.01,
        saltGram = 0.12,
        sodiumMilliGram = 48.8,
        350.0
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 62.8,
        lipidGram = 0.15,
        saturatedFattyAcidGram = 0.042,
        carbohydrateGram = 12.2,
        sugarGram = 6.11,
        dietaryFiberGram = 2.9,
        proteinGram = 1.69,
        saltGram = 0.079,
        sodiumMilliGram = 31.5,
        250.0
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 47.3,
        lipidGram = 1.55,
        saturatedFattyAcidGram = 1.04,
        carbohydrateGram = 4.83,
        sugarGram = 4.66,
        dietaryFiberGram = 0.0,
        proteinGram = 3.38,
        saltGram = 0.09,
        sodiumMilliGram = 36.0,
        350.0
      )
      ,
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 367,
        lipidGram = 35.3,
        saturatedFattyAcidGram = 23.9,
        carbohydrateGram = 4.24,
        sugarGram = 1.86,
        dietaryFiberGram = 0.0,
        proteinGram = 7.66,
        saltGram = 1.2,
        sodiumMilliGram = 479,
        100.0
      )
    )

    val recipe2Ingredients: List[NutriScoreInput] = List(
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 140,
        lipidGram = 9.83,
        saturatedFattyAcidGram = 2.64,
        carbohydrateGram = 0.27,
        sugarGram = 0.27,
        dietaryFiberGram = 0.0,
        proteinGram = 12.7,
        saltGram = 0.31,
        sodiumMilliGram = 124,
        195.0
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 340,
        lipidGram = 1.5,
        saturatedFattyAcidGram = 0.27,
        carbohydrateGram = 68.6,
        sugarGram = 1.6,
        dietaryFiberGram = 6.8,
        proteinGram = 9.61,
        saltGram = 0.063,
        sodiumMilliGram = 2.5, 
        150.0
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 153,
        lipidGram = 0.2,
        saturatedFattyAcidGram = 0.05,
        carbohydrateGram = 33.2,
        sugarGram = 0.1,
        dietaryFiberGram = 1.9,
        proteinGram = 1.96,
        saltGram = 0.56,
        sodiumMilliGram = 14200.0, 
        12.0
      ),
      NutriScoreInput(
        isTEV = true,
        energyKiloCalories = 900,
        lipidGram = 99.9,
        saturatedFattyAcidGram = 15.2,
        carbohydrateGram = 0.0,
        sugarGram = 0.0,
        dietaryFiberGram = 0.0,
        proteinGram = 0.25,
        saltGram = 0.0063,
        sodiumMilliGram = 2.5, 
        10.0
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 47.3,
        lipidGram = 4.55,
        saturatedFattyAcidGram = 1.04,
        carbohydrateGram = 4.83,
        sugarGram = 4.66,
        dietaryFiberGram = 0.0,
        proteinGram = 3.38,
        saltGram = 0.09,
        sodiumMilliGram = 36,
        12.0
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 370,
        lipidGram = 28.6,
        saturatedFattyAcidGram = 18.1,
        carbohydrateGram = 0.0,
        sugarGram = 0.13,
        dietaryFiberGram = 0.0,
        proteinGram = 28.1,
        saltGram = 0.75,
        sodiumMilliGram = 297.0, 
        100.0
      ),
      NutriScoreInput(
        isTEV = true,
        energyKiloCalories = 196,
        lipidGram = 19.5,
        saturatedFattyAcidGram = 2.35,
        carbohydrateGram = 0.05,
        sugarGram = 0.0,
        dietaryFiberGram = 6.97,
        proteinGram = 1.49,
        saltGram = 2.72,
        sodiumMilliGram = 1090.0, 
        80.0
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 280,
        lipidGram = 22.8,
        saturatedFattyAcidGram = 14.3,
        carbohydrateGram = 2.5,
        sugarGram = 1.13,
        dietaryFiberGram = 0.0,
        proteinGram = 14.8,
        saltGram = 2.59,
        sodiumMilliGram = 1040, 
        50.0
      ),
      NutriScoreInput(
        isTEV = true,
        energyKiloCalories = 187,
        lipidGram = 11.7,
        saturatedFattyAcidGram = 1.32,
        carbohydrateGram = 13.4,
        sugarGram = 6.5,
        dietaryFiberGram = 5.63,
        proteinGram = 4.27,
        saltGram = 2.21,
        sodiumMilliGram = 1020,
        50.0
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 34.8,
        lipidGram = 0.47,
        saturatedFattyAcidGram = 0.13,
        carbohydrateGram = 2.55,
        sugarGram = 0.37,
        dietaryFiberGram = 3.47,
        proteinGram = 3.35,
        saltGram = 0.028,
        sodiumMilliGram = 12,
        5
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 283,
        lipidGram = 7.2,
        saturatedFattyAcidGram = 1.88,
        carbohydrateGram = 23,
        sugarGram = 18.5,
        dietaryFiberGram = 40.1,
        proteinGram = 11.5,
        saltGram = 0.088,
        sodiumMilliGram = 35, 
        5
      )
    )

    val recipe3Ingredients: List[NutriScoreInput] = List(
      NutriScoreInput(
        isTEV = true,
        energyKiloCalories = 20.9,
        lipidGram = 0.46,
        saturatedFattyAcidGram = 0.097,
        carbohydrateGram = 1.6,
        sugarGram = 1.59,
        dietaryFiberGram = 2,
        proteinGram = 1.6,
        saltGram = 0.017,
        sodiumMilliGram = 19.5, 
        1000
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 181,
        lipidGram = 7.88,
        saturatedFattyAcidGram = 0.33,
        carbohydrateGram = 34,
        sugarGram = 0.65,
        dietaryFiberGram = 1.9,
        proteinGram = 6.34,
        saltGram = 1.06,
        sodiumMilliGram = 395,
        300
      ),
      NutriScoreInput(
        isTEV = true,
        energyKiloCalories = 24.1,
        lipidGram = 0.5,
        saturatedFattyAcidGram = 0.081,
        carbohydrateGram = 0.93,
        sugarGram = 0.4,
        dietaryFiberGram = 2.4,
        proteinGram = 2.81,
        saltGram = 0.81,
        sodiumMilliGram = 322, 
        300
      ),
      NutriScoreInput(
        isTEV = false,
        energyKiloCalories = 370,
        lipidGram = 35.4,
        saturatedFattyAcidGram = 4.39,
        carbohydrateGram = 6.6,
        sugarGram = 3.2,
        dietaryFiberGram = 2.5,
        proteinGram = 3.94,
        saltGram = 2.64,
        sodiumMilliGram = 1060,
        180
      )
    )
  }
}
