package com.service

import com.BaseTest
import com.model.Account
import com.storage.AccountStorage
import org.scalatest._

import scala.concurrent.ExecutionContextExecutor

class AccountServiceTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = new Context {
    AccountStorage.createTables()
    AccountStorage.insertTestAccounts(List(testUser1))
  }

  override protected def afterAll(): Unit = {
    AccountStorage.dropTables()
  }

  "UserService" when {

    "getUserFromUserId" should {
      "return the user if one exists with the given user_id" in new Context {
        awaitForResult(AccountService.getAccountByAccountId(testUser1.accountId)) shouldBe
          Some(testUser1)
      }

      "return None if no user exist with the given user_id" in new Context {
        awaitForResult(AccountService.getAccountByAccountId(missingUserId)) shouldBe
          None
      }
    }

    "getUserByPseudo" should {
      "return the user if one exists with the given pseudo" in new Context {
        awaitForResult(AccountService.getAccountByPseudo(testUser1.pseudo)) shouldBe
          Right(testUser1)
      }

      "return an error if the pseudo is not valid" in new Context {
        awaitForResult(AccountService.getAccountByPseudo("invalid;pseudo")) shouldBe
          Left("INVALID_PSEUDO")

        awaitForResult(AccountService.getAccountByPseudo("invalid pseudo")) shouldBe
          Left("INVALID_PSEUDO")

        awaitForResult(AccountService.getAccountByPseudo("invalid@pseudo")) shouldBe
          Left("INVALID_PSEUDO")
      }

      "return None if no user exist with the given pseudo" in new Context {
        awaitForResult(AccountService.getAccountByPseudo(missingPseudo)) shouldBe
          Left("PSEUDO_NOT_FOUND")
      }
    }
  }

  trait Context {
    val testUser1: Account = Account(5482, "pwal@ninja.com", "this_is_a_pseudo", "this is a password hash")
    val missingUserId = 42
    val missingPseudo = "kéké"
  }

}