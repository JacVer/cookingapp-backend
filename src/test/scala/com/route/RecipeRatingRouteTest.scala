package com.route

import java.time.LocalDateTime

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.BaseTest
import com.api.RecipeRatingApi.{RecipeComment, RecipeRating => RecipeRatingApi}
import com.model.Account
import com.model.recipe.Recipe
import com.storage.{AccountStorage, FoodStorage, FoodUnitStorage, RecipeRatingStorage, RecipeStorage}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContextExecutor

class RecipeRatingRouteTest extends WordSpec with BaseTest with Matchers with ScalatestRouteTest with BeforeAndAfterAll {

  val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global
  val timeAdded: LocalDateTime = LocalDateTime.now().withNano(0)

  override def beforeAll(): Unit = new Context { seed() }

  override def afterAll(): Unit = new Context { clean() }

  "RecipeRatingRoute" when {
    "PUT /api/recipe/?/rating" should {
      "return 200 and the new rating summary if the rating is added successfully" in new Context {
        Put(s"/api/recipe/1/rating", 3) ~> RawHeader("jwt", Account.getJsonWebToken(1)) ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[RecipeRatingApi] shouldBe RecipeRatingApi(Some(3.0), 1)
        }
      }

      "return 200 and the new rating summary if the rating is added successfully, overwriting any previous ratings" in new Context {
        Put(s"/api/recipe/1/rating", 5) ~> RawHeader("jwt", Account.getJsonWebToken(1)) ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[RecipeRatingApi] shouldBe RecipeRatingApi(Some(5.0), 1)
        }
      }

      "return 400 if the recipe doesn't exist" in new Context {
        Put(s"/api/recipe/58/rating", 5) ~> RawHeader("jwt", Account.getJsonWebToken(1)) ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 if the recipe is hidden" in new Context {
        Put(s"/api/recipe/4/rating", 5) ~> RawHeader("jwt", Account.getJsonWebToken(1)) ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 if the token is wrong" in new Context {
        Put(s"/api/recipe/1/rating", 5) ~> RawHeader("jwt", "pwal") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 if the rating is not 1, 2, 3, 4 or 5" in new Context {
        Put(s"/api/recipe/1/rating", 8) ~> RawHeader("jwt", Account.getJsonWebToken(1)) ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_RATING"
        }

        Put(s"/api/recipe/1/rating", -3) ~> RawHeader("jwt", Account.getJsonWebToken(1)) ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_RATING"
        }
      }
    }

    "GET /api/recipe/?/rating" should {
      "return 200 and the rating summary" in new Context {
        Get(s"/api/recipe/2/rating") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[RecipeRatingApi] shouldBe RecipeRatingApi(Some(2.5), 2)
        }
      }

      "return 200 and empty rating summary if the recipe doesn't have any ratings" in new Context {
        Get(s"/api/recipe/3/rating") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[RecipeRatingApi] shouldBe RecipeRatingApi(None, 0)
        }
      }

      "return 400 if the recipe is not found" in new Context {
        Get(s"/api/recipe/785/rating") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 if the recipe is hidden" in new Context {
        Get(s"/api/recipe/4/rating") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 if the recipe creator is deleted" in new Context {
        Get(s"/api/recipe/5/rating") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }
    }

    "PUT /api/recipe/?/comment" should {
      "return 200 if the comment is added successfully" in new Context {
        Put(s"/api/recipe/1/comment", "First") ~> RawHeader("jwt", Account.getJsonWebToken(1)) ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.OK
        }

        awaitForResult(RecipeRatingStorage.selectRecipeComment(1)).getOrElse(throw new Error("Should be a right")).length shouldBe 1
      }

      "return 400 if the recipe is hidden" in new Context {
        Put(s"/api/recipe/4/comment", "Second") ~> RawHeader("jwt", Account.getJsonWebToken(1)) ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 if the recipe doesn't exist" in new Context {
        Put(s"/api/recipe/458132/comment", "Second") ~> RawHeader("jwt", Account.getJsonWebToken(1)) ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 if the token if wrong" in new Context {
        Put(s"/api/recipe/1/comment", "Second") ~> RawHeader("jwt", "walleeeezzz") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }
    }

    "GET /api/recipe/?/comment" should {
      "return 200 and the recipe comments" in new Context {
        Get("/api/recipe/2/comment") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeComment]] shouldBe List(
            RecipeComment(timeAdded, Some("redrum"), "first"),
            RecipeComment(timeAdded.plusMinutes(5), Some("crawler"), "second")
          )
        }
      }

      "return 200 and hide the pseudo of the deleted accounts" in new Context {
        Get("/api/recipe/3/comment") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeComment]] shouldBe List(
            RecipeComment(timeAdded, Some("crawler"), "pwali"),
            RecipeComment(timeAdded.plusMinutes(10), None, "pwalo")
          )
        }
      }

      "return 400 if the recipe doesn't exist" in new Context {
        Get("/api/recipe/36453/comment") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 if the recipe is hidden" in new Context {
        Get("/api/recipe/4/comment") ~> recipeRatingRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }
    }
  }

  trait Context {
    val recipeRatingRoute: Route = RecipeRatingRoute().route

    def seed(): Unit = {
      AccountStorage.createTables()
      FoodStorage.createTables()
      FoodUnitStorage.createTables()
      RecipeStorage.createTables()
      RecipeRatingStorage.createTables()

      AccountStorage.insertTestAccounts(List(
        Account(1, "email@mitch", "redrum", "azertyuiop"),
        Account(2, "email@night", "crawler", "qsdfghjklm"),
        Account(3, "email@human", "rone", "azerthjklm"),
        Account(4, "boom@boom", "billx", "fgvhjsdgg"),
      ))

      RecipeStorage.insertTestRecipes(List(
        Recipe(1, 1, "redrum", "recipe_1", "", List("pwal"), None, 1, "A", 20.0, pescetarian = true, vegetarian = true, vegan = true, glutenFree = true, january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, None, isFavourite = false),
        Recipe(2, 1, "redrum", "recipe_2", "", List("pwal"), None, 1, "A", 20.0, pescetarian = true, vegetarian = true, vegan = true, glutenFree = true, january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, None, isFavourite = false),
        Recipe(3, 2, "crawler", "recipe_3", "", List("pwal"), None, 1, "A", 20.0, pescetarian = true, vegetarian = true, vegan = true, glutenFree = true, january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, None, isFavourite = false),
        Recipe(4, 2, "pwal", "recipe_4", "", List("pwal"), None, 1, "A", 20.0, pescetarian = true, vegetarian = true, vegan = true, glutenFree = true, january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, None, isFavourite = false),
        Recipe(5, 4, "pwalo", "recipe_5", "", List("pwal"), None, 1, "A", 20.0, pescetarian = true, vegetarian = true, vegan = true, glutenFree = true, january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, None, isFavourite = false),
      ))

      awaitForResult {
        RecipeRatingStorage.insertRating(2, 1, LocalDateTime.now(), 1)
        RecipeRatingStorage.insertRating(2, 2, LocalDateTime.now(), 4)
        RecipeRatingStorage.insertComment(2, 1, timeAdded, "first")
        RecipeRatingStorage.insertComment(2, 2, timeAdded.plusMinutes(5), "second")
        RecipeRatingStorage.insertComment(3, 2, timeAdded, "pwali")
        RecipeRatingStorage.insertComment(3, 4, timeAdded.plusMinutes(10), "pwalo")
        RecipeStorage.hideRecipe(4)
        AccountStorage.hideAccount(4)
      }
    }

    def clean(): Unit = {
      RecipeRatingStorage.dropTables()
      RecipeStorage.dropTables()
      FoodUnitStorage.createTables()
      FoodStorage.createTables()
      AccountStorage.dropTables()
    }
  }
}
