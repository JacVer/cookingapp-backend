package com.route

import java.time.LocalDateTime

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.BaseTest
import com.api.RecipeApi.{NewRecipe, NewRecipeId, RecipeIngredient, NewRecipeIngredient => NewRecipeIngredientApi, Recipe => RecipeApi}
import com.api.NutritionApi.{NutritionalInformation => NutritionalInformationApi}
import com.model.food.{CalnutFood, FoodUnit, NewFoodUnit}
import com.model.recipe.{NewRecipeIngredient, Recipe}
import com.model.Account
import com.storage.{AccountStorage, FavouriteRecipeStorage, FoodStorage, FoodUnitStorage, RecipeStorage}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import org.scalatest._

import scala.concurrent.ExecutionContextExecutor

class RecipeRouteTest
  extends WordSpec
    with BaseTest
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterAll {

  val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = {
    AccountStorage.createTables()
    FoodStorage.createTables()
    FoodUnitStorage.createTables()
    RecipeStorage.createTables()
    FavouriteRecipeStorage.createTables()

    new Context {
      AccountStorage.insertTestAccounts(List(testAccount1, testAccount2, testHiddenAccount))
      RecipeStorage.insertTestRecipes(List(testRecipe1, testRecipe2, testRecipe3, testRecipe4, testRecipe5, testRecipeWithHiddenAccount, recipeToDelete, testHiddenRecipe, testRecipeToUpdate))
      FoodStorage.insertCalnutFoods(timeAdded, List(onion, rice, chicken, mushroom, leek, potato, salmon)).left.map(throw _)
      FoodUnitStorage.insertFoodUnits(List(
        riceFoodUnit0,
        leekFoodUnit0,
        leekFoodUnit1,
        potatoFoodUnit0,
        potatoFoodUnit1,
        potatoFoodUnit2,
        potatoFoodUnit3,
        chickenFoodUnit0,
        chickenFoodUnit1,
        onionFoodUnit0,
        onionFoodUnit1,
        onionFoodUnit2,
        mushroomFoodUnit0,
        mushroomFoodUnit1,
        salmonFoodUnit0
      )).left.map(throw _)

      RecipeStorage.insertTestIngredients(List(
        testRecipe1Ingredient1,
        testRecipe1Ingredient2,
        testRecipe2Ingredient1,
        testRecipe3Ingredient1,
        testRecipe4Ingredient1,
        testRecipe5Ingredient1,
        testRecipe5Ingredient2,
        recipeToDeleteIngredient1,
        testRecipeWithHiddenAccountIngredient1,
        testHiddenRecipeIngredient,
        testRecipeToUpdateIngredient1,
        testRecipeToUpdateIngredient2
      ))

      FavouriteRecipeStorage.insertTestFavouriteRecipes(List(
        (testAccount1.accountId, testRecipe1.recipeId, timeAdded),
        (testAccount2.accountId, testRecipe3.recipeId, timeAdded)
      ))

      awaitForResult(AccountStorage.hideAccount(testHiddenAccount.accountId))
      awaitForResult(RecipeStorage.hideRecipe(testHiddenRecipe.recipeId))
    }
  }

  override def afterAll(): Unit = {
    FavouriteRecipeStorage.dropTables()
    RecipeStorage.dropTables()
    FoodUnitStorage.dropTables()
    FoodStorage.dropTables()
    AccountStorage.dropTables()
  }

  "RecipeRoute" when {

    "DELETE /api/recipe/?" should {
      "return 400 and an error if the token can't be deciphered" in new Context {
        Delete(s"/api/recipe/${recipeToDelete.recipeId}") ~> RawHeader("jwt", "walla") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 and an error if the user is not the owner of the recipe" in new Context {
        Delete(s"/api/recipe/${recipeToDelete.recipeId}") ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "USER_NOT_OWNER_OF_RECIPE"
        }
      }

      "return 400 and an error if the recipe doesn't exist" in new Context {
        Delete(s"/api/recipe/7452") ~> RawHeader("jwt", Account.getJsonWebToken(testAccount2.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 200 and success if the recipe has been deleted successfully" in new Context {
        Delete(s"/api/recipe/${recipeToDelete.recipeId}") ~> RawHeader("jwt", Account.getJsonWebToken(testAccount2.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        awaitForResult(RecipeStorage.selectRecipe(recipeId = Some(recipeToDelete.recipeId))) shouldBe
          Right(List())
      }
    }

    // TODO : how to deal with accents ?
    "GET /api/recipe?title" should {
      "return 200 and the recipes matching" in new Context {
        Get("/api/recipe?title=champignons") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe1, testRecipe2).map(_.toRecipeApi)
        }
      }

      "return 200 and the recipes matching and whether they are favourites" in new Context {
        Get(s"/api/recipe?title=champignons") ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe1.copy(isFavourite = true), testRecipe2).map(_.toRecipeApi)
        }
      }

      "return 200 and the recipes matching while not being case sensitive" in new Context {
        Get("/api/recipe?title=CHAMPIGNONS") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe1, testRecipe2).map(_.toRecipeApi)
        }
      }

      "return 200 and nothing if no recipe match the title" in new Context {
        Get("/api/recipe?title=estragon") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe Nil
        }
      }

      "return 400 and an error if the title contains a semi colon" in new Context {
        Get("/api/recipe?title=estr;agon") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_RECIPE_TITLE"
        }
      }
    }

    "GET /api/recipe/find?ingredients" should {
      "return 200 and the recipes containing the ingredient" in new Context {
        Get(s"/api/recipe?ingredients=${mushroom.foodId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipeToUpdate, testRecipe1, testRecipe2).map(_.toRecipeApi)
        }
      }

      "return 200 and only the recipes from non hidden accounts" in new Context {
        Get(s"/api/recipe?ingredients=${onion.foodId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipeToUpdate, testRecipe4).map(_.toRecipeApi)
        }
      }

      "return 200 and no recipes if no one matched" in new Context {
        Get(s"/api/recipe?ingredients=${rice.foodId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe Nil
        }
      }

      "return 200 and the recipes containing the ingredients" in new Context {
        Get(s"/api/recipe?ingredients=${mushroom.foodId},${chicken.foodId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe1).map(_.toRecipeApi)
        }
      }

      "return 400 and if the ingredients have an invalid shape" in new Context {
        Get(s"/api/recipe?ingredients=blabla") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_INGREDIENTS"
        }

        Get(s"/api/recipe?ingredients=bla,5") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_INGREDIENTS"
        }
      }
    }

    "GET /api/recipe?title&pseudo" should {

      "return 200 and the recipes matching the title from the account" in new Context {
        Get(s"/api/recipe?title=poulet&pseudo=${testAccount2.pseudo}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe3).map(_.toRecipeApi)
        }
      }

      "return 400 and an error if the title contains a semi colon" in new Context {
        Get(s"/api/recipe?title=estr;agon&pseudo=${testAccount2.pseudo}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_RECIPE_TITLE"
        }
      }

      "return 200 and Nil if the pseudo doesn't exist" in new Context {
        Get(s"/api/recipe?pseudo=missingPseudo&title=poulet") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe Nil
        }
      }

      "return 400 if the pseudo contains a semi colon" in new Context {
        Get(s"/api/recipe?pseudo=pseu;do&title=poulet") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PSEUDO"
        }
      }

      "return 400 if the pseudo has less than 4 characters" in new Context {
        Get(s"/api/recipe?pseudo=pse&title=poulet") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PSEUDO"
        }
      }
    }

    "GET /api/recipe?title&ingredients" should {

      "return 200 and the recipes matching the title with the ingredients" in new Context {
        Get(s"/api/recipe?title=poulet&ingredients=${mushroom.foodId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe1).map(_.toRecipeApi)
        }
      }

      "return 400 and an error if the title contains a semi colon" in new Context {
        Get(s"/api/recipe?title=estr;agon&ingredients=${mushroom.foodId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_RECIPE_TITLE"
        }
      }
    }

    "GET /api/recipe?pseudo&ingredients" should {

      "return 200 and the recipes with the ingredients from the account" in new Context {
        Get(s"/api/recipe?pseudo=${testAccount2.pseudo}&ingredients=${onion.foodId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe4).map(_.toRecipeApi)
        }
      }

      "return 200 and Nil if the pseudo doesn't exist" in new Context {
        Get(s"/api/recipe?pseudo=missingPseudo&ingredients=${onion.foodId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe Nil
        }
      }
    }

    "GET /api/recipe?pseudo&ingredients&title" should {

      "return 200 and the recipes with the ingredients from the account matching the title" in new Context {
        Get(s"/api/recipe?pseudo=${testAccount2.pseudo}&ingredients=${chicken.foodId}&title=poulet") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe3).map(_.toRecipeApi)
        }
      }

      "return 200 and the recipes with the ingredients from the account matching the title and whether they are favourites" in new Context {
        Get(s"/api/recipe?pseudo=${testAccount2.pseudo}&ingredients=${chicken.foodId}&title=poulet") ~>
          RawHeader("jwt", Account.getJsonWebToken(testAccount2.accountId)) ~>
          recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe3.copy(isFavourite = true)).map(_.toRecipeApi)
        }
      }

      "return 400 and an error if the title contains a semi colon" in new Context {
        Get(s"/api/recipe?pseudo=${testAccount1.pseudo}&ingredients=${mushroom.foodId}&title=estr;agon&") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_RECIPE_TITLE"
        }
      }

      "return 200 and Nil if the pseudo doesn't exist" in new Context {
        Get(s"/api/recipe?pseudo=missingPseudo&ingredients=${onion.foodId}&title=oignon") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe Nil
        }
      }

      "return 400 and an error if no parameter is defined" in new Context {
        Get(s"/api/recipe?") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "NO_PARAMETERS"
        }
      }
    }

    "GET /api/recipe?pseudo" should {
      "return 200 and Nil if the pseudo isn't found" in new Context {
        Get(s"/api/recipe?pseudo=missingpseudo") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe Nil
        }
      }

      "return 400 and an error if the pseudo is invalid" in new Context {
        Get(s"/api/recipe?pseudo=invalid;pseudo") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PSEUDO"
        }
      }

      "return 200 and the recipes of the account having this pseudo" in new Context {
        Get(s"/api/recipe?pseudo=${testAccount2.pseudo}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe3, testRecipe5, testRecipe4).map(_.toRecipeApi)
        }
      }

      "return 200 and the recipes of the account having this pseudo matching the recipe title" in new Context {
        Get(s"/api/recipe?pseudo=${testAccount2.pseudo}&title=oignon") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe4).map(_.toRecipeApi)
        }
      }

      "return 200 and the recipes of the account having this pseudo and containing the ingredients" in new Context {
        Get(s"/api/recipe?pseudo=${testAccount2.pseudo}&ingredients=${chicken.foodId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe3).map(_.toRecipeApi)
        }
      }
    }

    "GET /api/recipe?pescetarian&vegetarian&vegan&gluten_free" should {

      "return 200 and the recipes being filtered by dietary preferences" in new Context {
        Get(s"/api/recipe?title=poulet&pescetarian=true") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe Nil
        }

        Get(s"/api/recipe?ingredients=${salmon.foodId}&pescetarian=true") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe5.toRecipeApi)
        }

        Get(s"/api/recipe?ingredients=${salmon.foodId}&vegan=true") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe Nil
        }
      }
    }

    "GET /api/recipe?month" should {

      "return 200 and the recipes being filtered by season" in new Context {
        Get(s"/api/recipe?title=poulet&month=january") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe1.toRecipeApi, testRecipe3.toRecipeApi)
        }

        Get(s"/api/recipe?ingredients=${chicken.foodId}&month=june") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe3.toRecipeApi)
        }

        Get(s"/api/recipe?ingredients=${chicken.foodId}&month=febuary") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_MONTH"
        }
      }
    }

    "POST /api/recipe" should {

      "return 400 and an error if there is no ingredients" in new Context {
        Post(
          "/api/recipe",
          NewRecipe(
            newRecipe.title,
            newRecipe.description,
            newRecipe.instructions,
            Nil,
            newRecipe.servings,
            newRecipe.preparation_time_minutes,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "NO_INGREDIENTS"
        }
      }

      "return 400 and an error if there is no instructions" in new Context {
        Post(
          "/api/recipe",
          NewRecipe(
            newRecipe.title,
            newRecipe.description,
            Nil,
            newRecipe.ingredients,
            newRecipe.servings,
            newRecipe.preparation_time_minutes,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "NO_INSTRUCTIONS"
        }
      }

      "return 400 and an error if the token cannot be deciphered" in new Context {
        Post("/api/recipe", newRecipe) ~> RawHeader("jwt", "walla") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 and an error if the title contains a semi colon" in new Context {
        Post(
          "/api/recipe",
          NewRecipe(
            "Po;ulet",
            newRecipe.description,
            List("Mettre au four"),
            List(NewRecipeIngredientApi(120, 400, 45)),
            newRecipe.servings,
            newRecipe.preparation_time_minutes,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_RECIPE_TITLE"
        }
      }

      "return 400 and an error if one ingredient's (food_id, unit_id) combination can't be found" in new Context {
        Post(
          "/api/recipe",
          NewRecipe(
            "Soupe à l'oignon",
            "Jsais pas",
            List("Cuire les oignons pendant longtemps.", "Mettre les oignons dans le bouillon"),
            List(
              NewRecipeIngredientApi(onion.foodId, onionFoodUnit0.unitId, 35),
              NewRecipeIngredientApi(456, 300, 55)
            ),
            newRecipe.servings,
            newRecipe.preparation_time_minutes,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe s"FOOD_UNIT_NOT_FOUND:(456,300)"
        }
      }

      "return 400 and an error if the servings are zero or negative" in new Context {
        Post("/api/recipe", newRecipe.copy(servings = 0)) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_SERVINGS"
        }

        Post("/api/recipe", newRecipe.copy(servings = -10)) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_SERVINGS"
        }
      }

      "return 400 and an error if the preparation_time_minutes is zero or negative" in new Context {
        Post("/api/recipe", newRecipe.copy(preparation_time_minutes = 0.0)) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PREPARATION_TIME"
        }

        Post("/api/recipe", newRecipe.copy(preparation_time_minutes = -30.0)) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PREPARATION_TIME"
        }
      }

      "return 200 and the recipe_id if the recipe has been created successfully" in new Context {
        Post("/api/recipe", newRecipe) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[NewRecipeId] shouldBe createdRecipeId0
        }

        Get(s"/api/recipe/${createdRecipeId0.recipe_id}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[RecipeApi] shouldBe RecipeApi(
            createdRecipeId0.recipe_id,
            newRecipe.title,
            newRecipe.description,
            newRecipe.instructions,
            newRecipe.servings,
            "A",
            newRecipe.preparation_time_minutes,
            testAccount1.pseudo,
            pescetarian = true, vegetarian = false, vegan = false, gluten_free = true,
            january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
            NutritionalInformationApi(365.58 ,15.54 ,4.386 ,8.586 ,0.0 ,1.08 ,48.678 ,0.6336 ,253.2),
            newRecipe.base_64_encoded_picture,
            is_favourite = false
          )
        }
      }

      "return 200 and the recipe_id if the recipe has been created successfully with a source_url" in new Context {
        Post("/api/recipe", newRecipe.copy(sourceURL = Some("pwal.org"))) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[NewRecipeId] shouldBe createdRecipeId1
        }

        Get(s"/api/recipe/${createdRecipeId1.recipe_id}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[RecipeApi] shouldBe RecipeApi(
            createdRecipeId1.recipe_id,
            newRecipe.title,
            newRecipe.description,
            newRecipe.instructions,
            newRecipe.servings,
            "A",
            newRecipe.preparation_time_minutes,
            testAccount1.pseudo,
            pescetarian = true, vegetarian = false, vegan = false, gluten_free = true,
            january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
            NutritionalInformationApi(365.58 ,15.54 ,4.386 ,8.586 ,0.0 ,1.08 ,48.678 ,0.6336 ,253.2),
            newRecipe.base_64_encoded_picture,
            is_favourite = false
          )
        }
      }

      "return 400 and an error if there are duplicate combinations (food_id,unit_id)" in new Context {
        Post("/api/recipe", newRecipeWithDuplicateIngredients) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe s"DUPLICATED_FOOD:(${onion.foodId},${onionFoodUnit2.unitId})"
        }
      }
    }

    "GET /api/recipe/?" should {
      "return 200 and the recipe if it is found" in new Context {
        Get(s"/api/recipe/${testRecipe1.recipeId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[RecipeApi] shouldBe testRecipe1.toRecipeApi
        }
      }

      "return 200 and the recipe if it is found and if it is a favourite" in new Context {
        Get(s"/api/recipe/${testRecipe1.recipeId}") ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[RecipeApi] shouldBe testRecipe1.copy(isFavourite = true).toRecipeApi
        }
      }

      "return 400 and an error if the recipe is not found" in new Context {
        Get(s"/api/recipe/85212") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 and an error if the account owning the recipe is hidden" in new Context {
        Get(s"/api/recipe/${testRecipeWithHiddenAccount.recipeId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 and an error if the recipe is deleted" in new Context {
        Get(s"/api/recipe/${testHiddenRecipe.recipeId}") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

    }

    "PATCH /api/recipe/?" should {
      "return 200 and success if the recipe has been updated" in new Context {
        Patch(
          s"/api/recipe/${testRecipeToUpdate.recipeId}",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipe4Ingredient1).map(_.toNewRecipeIngredientApi),
            5,
            30.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        awaitForResult(RecipeStorage.selectRecipe(recipeId = Some(testRecipeToUpdate.recipeId))) shouldBe
          Right(List(Recipe(
            testRecipeToUpdate.recipeId,
            testAccount1.accountId,
            testAccount1.pseudo,
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            None,
            5,
            "A",
            preparationTimeMinutes = 30.0,
            pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
            january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true,
            energyKiloCalories = 205.2, lipidGram = 1.2, saturatedFattyAcidGram = 0.144, carbohydrateGram = 37.2, sugarGram = 24.0, dietaryFiberGram = 8.4, proteinGram = 7.8, saltGram = 0.126, sodiumMilliGram = 50.4,
            picture = None,
            isFavourite = false
          )))

        awaitForResult(RecipeStorage.selectRecipeIngredients(testRecipeToUpdate.recipeId)) shouldBe
          Right(List((onion.toFood, FoodUnit(onionFoodUnit2.unitId, onionFoodUnit2.unitNameFr, onionFoodUnit2.weightGrams), 4.0)))
      }

      "return 400 and an error if the recipe is hidden" in new Context {
        Patch(
          s"/api/recipe/${testHiddenRecipe.recipeId}",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipe4Ingredient1).map(_.toNewRecipeIngredientApi),
            5,
            30.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount2.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 and an error if the user in the token isn't the owner of the recipe" in new Context {
        Patch(
          s"/api/recipe/${testRecipeToUpdate.recipeId}",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipe4Ingredient1).map(_.toNewRecipeIngredientApi),
            5,
            30.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount2.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "USER_NOT_OWNER_OF_RECIPE"
        }
      }

      "return 400 and an error if there is no instructions" in new Context {
        Patch(
          s"/api/recipe/${testRecipeToUpdate.recipeId}",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            Nil,
            List(testRecipeToUpdateIngredient1, testRecipeToUpdateIngredient2).map(_.toNewRecipeIngredientApi),
            5,
            30.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "NO_INSTRUCTIONS"
        }
      }

      "return 400 and an error if the recipe doesn't exist" in new Context {
        Patch(
          s"/api/recipe/44153",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipeToUpdateIngredient1, testRecipeToUpdateIngredient2).map(_.toNewRecipeIngredientApi),
            5,
            30.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 and an error if the token can't be deciphered" in new Context {
        Patch(
          s"/api/recipe/${testRecipeToUpdate.recipeId}",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipeToUpdateIngredient1, testRecipeToUpdateIngredient2).map(_.toNewRecipeIngredientApi),
            5,
            30.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", "walla") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 and an error if the title contains a semi-colon" in new Context {
        Patch(
          s"/api/recipe/${testRecipeToUpdate.recipeId}",
          NewRecipe(
            "Daft Punk; risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipeToUpdateIngredient1, testRecipeToUpdateIngredient2).map(_.toNewRecipeIngredientApi),
            5,
            30.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_RECIPE_TITLE"
        }
      }

      "return an error if the servings is zero or negative" in new Context {
        Patch(
          s"/api/recipe/${testRecipeToUpdate.recipeId}",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipeToUpdateIngredient1, testRecipeToUpdateIngredient2).map(_.toNewRecipeIngredientApi),
            0,
            30.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_SERVINGS"
        }

        Patch(
          s"/api/recipe/${testRecipeToUpdate.recipeId}",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipeToUpdateIngredient1, testRecipeToUpdateIngredient2).map(_.toNewRecipeIngredientApi),
            -5,
            30.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_SERVINGS"
        }
      }

      "return 400 and an error if the preparation_time_minutes is zero or negative" in new Context {
        Patch(
          s"/api/recipe/${testRecipeToUpdate.recipeId}",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipeToUpdateIngredient1, testRecipeToUpdateIngredient2).map(_.toNewRecipeIngredientApi),
            5,
            0.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PREPARATION_TIME"
        }

        Patch(
          s"/api/recipe/${testRecipeToUpdate.recipeId}",
          NewRecipe(
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            List(testRecipeToUpdateIngredient1, testRecipeToUpdateIngredient2).map(_.toNewRecipeIngredientApi),
            5,
            -50.0,
            None,
            None
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PREPARATION_TIME"
        }
      }
    }

    "GET /api/recipe/?/ingredients" should {

      "return 200 and the recipe ingredients" in new Context {
        Get(s"/api/recipe/${testRecipe1.recipeId}/ingredients") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeIngredient]] shouldBe List(
            chicken.toFood.toRecipeIngredient(FoodUnit(chickenFoodUnit1.unitId, chickenFoodUnit1.unitNameFr, chickenFoodUnit1.weightGrams), testRecipe1Ingredient1.quantity),
            mushroom.toFood.toRecipeIngredient(FoodUnit(mushroomFoodUnit0.unitId, mushroomFoodUnit0.unitNameFr, mushroomFoodUnit0.weightGrams), testRecipe1Ingredient2.quantity)
          )
        }
      }

      "return 400 and an error if the recipe is not found" in new Context {
        Get(s"/api/recipe/85212/ingredients") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 and an error if the account owning the recipe is hidden" in new Context {
        Get(s"/api/recipe/${testRecipeWithHiddenAccount.recipeId}/ingredients") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }

      "return 400 and an error if the recipe is deleted" in new Context {
        Get(s"/api/recipe/${testHiddenRecipe.recipeId}/ingredients") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "RECIPE_NOT_FOUND"
        }
      }
    }

    "POST /api/recipe/simulate" should {

      "return 400 and an error if the token cannot be deciphered" in new Context {
        Post("/api/recipe/simulate", newRecipe.ingredients) ~> RawHeader("jwt", "walla") ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 and an error if one ingredient's (food_id, unit_id) combination can't be found" in new Context {
        Post(
          "/api/recipe/simulate",
          List(
            NewRecipeIngredientApi(onion.foodId, onionFoodUnit0.unitId, 35),
            NewRecipeIngredientApi(456, 300, 55)
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe s"FOOD_UNIT_NOT_FOUND:(456,300)"
        }
      }

      "return 400 and an error if there are duplicate combinations (food_id,unit_id)" in new Context {
        Post(
          "/api/recipe/simulate",
          newRecipeWithDuplicateIngredients.ingredients
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe s"DUPLICATED_FOOD:(${onion.foodId},${onionFoodUnit2.unitId})"
        }
      }

      "return 200 and the nutritional information of the ingredients" in new Context {
        Post(
          "/api/recipe/simulate",
          newRecipe.ingredients
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testAccount1.accountId)) ~> recipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[NutritionalInformationApi] shouldBe NutritionalInformationApi(365.58, 15.540000000000001, 4.386, 8.586, 0.0, 1.08, 48.678, 0.6336, 253.2)
        }
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.now()
    val recipeRoute: Route = RecipeRoute().route

    val testAccount1: Account = Account(22, "email@pwal.ninja", "Columbine", "pwal_hash")
    val testAccount2: Account = Account(33, "college@teenage.color", "Mamene", "music_hash")
    val testHiddenAccount: Account = Account(44, "hindoux@gan.di", "Modi", "japon_hash")

    val rice: CalnutFood = CalnutFood(4, "Produits céréaliers", 9120, "Riz basmati", nutriScoreTEV=false, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 127, 67.8, 0.005, 2.5, 7.2, 29, 11, 10, 0.26, 0.07, 0.07, 0.44, 10, 10, 2.86, 28.3, 0.1, 0.1, 0, 0.1, 0.1, 0.1, 0.1, 26.4, 0, 0.8, 0.13, 0.023, 0.024, 0.062, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0, 2.5, 0, 0.04, 0.4, 0, 0.25, 0.01, 0.005, 0.05, 0.16, 0.03, 0.048, 10.7, 0, 0, 1.11)
    val leek: CalnutFood = CalnutFood(24, "Légumes", 20040, "Poireau", nutriScoreTEV=true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=true, february=true, march=true, april=false, may=false, june=false, july=false, august=false, september=true, october=true, november=true, december=true, 24.2, 92, 0.015, 5.8, 8.45, 20, 151, 25.6, 0.075, 0.34, 0.12, 0.1, 5, 5, 1.1, 3, 2.11, 0.6, 0.1, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 3.2, 0.2, 0.027, 0.003, 0.11, 2.5E-05, 2.5E-05, 0.00012, 0.00011, 0.00033, 0.00039, 0.025, 0.001, 0.003, 0.02, 0.0067, 0.0033, 0.00013, 0.00018, 0, 94, 0, 0.5, 25.4, 0, 1.3, 0.025, 0.025, 0.025, 0.025, 0.06, 0, 21.2, 0, 0, 0)
    val potato: CalnutFood = CalnutFood(31, "Tubercules", 4048, "Pomme de terre", nutriScoreTEV=false, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 93.2, 76.4, 0.12, 48.8, 20.7, 43.7, 450, 9.62, 0.12, 0.43, 0.095, 0.24, 7.07, 6.78, 2.01, 17.2, 1.05, 0.35, 0.021, 0.065, 0.4, 0.065, 0.24, 14.5, 0.052, 1.96, 1.37, 0.4, 1.1, 0.23, 0.0058, 0.0037, 0.0037, 0.0062, 0.0086, 0.015, 0.17, 0.053, 0.91, 0.17, 0.019, 0.0031, 0.0027, 0.0027, 0, 3.68, 0.027, 0.12, 1.34, 0, 5.05, 0.07, 0.013, 1.08, 0.46, 0.24, 0.03, 18.4, 0, 0.1, 0)
    val chicken: CalnutFood = CalnutFood(35, "Viandes", 36005, "Blanc de poulet", nutriScoreTEV=false, pescetarian=false, vegetarian=false, vegan=false, glutenFree=true, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 213, 59.7, 0.4, 160, 39.6, 200, 414, 19.4, 0.013, 0.68, 0.05, 1.27, 5.66, 5, 28.9, 2.1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9.88, 2.78, 4.36, 2.19, 0, 0, 0, 0.0083, 0.025, 0.084, 2.15, 0.52, 3.47, 1.93, 0.13, 0.047, 0.009, 0.035, 47.5, 0, 1.5, 0.39, 2.4, 0, 0, 0.06, 0.16, 8.55, 0.97, 0.32, 0.52, 5, 0, 0, 117)
    val mushroom: CalnutFood = CalnutFood(24, "Légumes", 20125, "Champignon de paris", nutriScoreTEV=true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=true, february=true, march=true, april=true, may=false, june=false, july=false, august=false, september=true, october=true, november=true, december=true, 38.4, 88.4, 0.028, 11, 14, 140, 540, 2.7, 0.05, 0.31, 0.39, 0.81, 10, 10, 4.44, 4.53, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.18, 3, 0.9, 0.6, 0.18, 0.16, 0.18, 0.005, 0.005, 0.005, 0.005, 0.01, 0.01, 0.11, 0.05, 0.16, 0.18, 0.005, 0.005, 0.005, 0.005, 0, 2.5, 0.2, 0.04, 0.4, 0, 0.25, 0.062, 0.36, 4.53, 2.33, 0.029, 0, 14.3, 0, 0.028, 0)
    val onion: CalnutFood = CalnutFood(24, "Légumes", 20035, "Oignon", nutriScoreTEV=true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 34.2, 90.4, 0.021, 8.4, 13, 44, 220, 29.2, 0.12, 0.21, 0.077, 0.19, 5, 1, 1.3, 6.2, 4, 1.5, 0, 0.25, 1.3, 0.25, 1.2, 0.25, 0.25, 1.4, 0.2, 0.024, 0.02, 0.056, 0, 0, 0, 0, 0, 0.001, 0.021, 0.0015, 0.0067, 0.02, 0.0067, 0.0033, 0, 0, 0, 25, 0, 0.015, 0.4, 0, 2.1, 0.025, 0.025, 0.025, 0.13, 0.09, 0, 21, 0, 0, 0)
    val salmon: CalnutFood = chicken.copy(foodNameFr = "Salmon", foodId = 6541, pescetarian = true)

    val riceFoodUnit0: NewFoodUnit = NewFoodUnit(rice.foodId, 0, "grammes", 1)
    val leekFoodUnit0: NewFoodUnit = NewFoodUnit(leek.foodId, 0, "grammes", 1)
    val leekFoodUnit1: NewFoodUnit = NewFoodUnit(leek.foodId, 1, "poireau moyen", 150)
    val potatoFoodUnit0: NewFoodUnit = NewFoodUnit(potato.foodId, 0, "grammes", 1)
    val potatoFoodUnit1: NewFoodUnit = NewFoodUnit(potato.foodId, 2, "petite pomme de terre", 80)
    val potatoFoodUnit2: NewFoodUnit = NewFoodUnit(potato.foodId, 3, "pomme de terre moyenne", 160)
    val potatoFoodUnit3: NewFoodUnit = NewFoodUnit(potato.foodId, 4, "grosse pomme de terre", 320)
    val chickenFoodUnit0: NewFoodUnit = NewFoodUnit(chicken.foodId, 0, "grammes", 1)
    val chickenFoodUnit1: NewFoodUnit = NewFoodUnit(chicken.foodId, 5, "blanc de poulet moyen", 150)
    val mushroomFoodUnit0: NewFoodUnit = NewFoodUnit(mushroom.foodId, 0, "grammes", 1)
    val mushroomFoodUnit1: NewFoodUnit = NewFoodUnit(mushroom.foodId, 6, "champignon moyen", 20)
    val onionFoodUnit0: NewFoodUnit = NewFoodUnit(onion.foodId, 0, "grammes", 1)
    val onionFoodUnit1: NewFoodUnit = NewFoodUnit(onion.foodId, 7, "oignon", 100)
    val onionFoodUnit2: NewFoodUnit = NewFoodUnit(onion.foodId, 8, "gros oignon", 150)
    val salmonFoodUnit0: NewFoodUnit = NewFoodUnit(salmon.foodId, 0, "grammes", 1)

    val testRecipe1: Recipe = Recipe(
      555,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Poulet aux champignons",
      "la meilleure façon de manger du poulet.",
      List("Cuire le poulet et les champignons.", "Servir."),
      sourceURL = Some("chicken.com"),
      servings = 3,
      "A",
      preparationTimeMinutes = 20.0,
      pescetarian = false, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 300, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = None,
      isFavourite = false
    )
    val testRecipe2: Recipe = Recipe(
      666,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Soupe aux champignons",
      "Pour se réchauffer en plein hiver",
      List("Cuire les champignons.", "Mixer.", "Servir."),
      sourceURL = None,
      servings = 1,
      "A",
      preparationTimeMinutes = 15.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 180, lipidGram = 12, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("picture_hash"),
      isFavourite = false
    )
    val testRecipe3: Recipe = Recipe(
      777,
      testAccount2.accountId,
      testAccount2.pseudo,
      "Poulet braisé",
      "Du bon poulet au four",
      List("Cuire le poulet au four dans un plat pendant 40min"),
      sourceURL = None,
      servings = 4,
      "C",
      preparationTimeMinutes = 50.0,
      pescetarian = false, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 50, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 50, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("100%_encoded_picture_as_seen_on_tv"),
      isFavourite = false
    )
    val testRecipe4: Recipe = Recipe(
      888,
      testAccount2.accountId,
      testAccount2.pseudo,
      "Soupe à l'oignon y crouton",
      "La bonne soupe franchouillarde",
      List("Cuire les oignons lentement avec du bouillon cube.", "Rajouter de l''eau bouillante et des croutons."),
      sourceURL = Some("https://wintersoups.co.uk"),
      servings = 4,
      "A",
      preparationTimeMinutes = 30.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = false,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 150, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 2, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 1040,
      picture = None,
      isFavourite = false
    )
    val testRecipe5: Recipe = Recipe(
      5842,
      testAccount2.accountId,
      testAccount2.pseudo,
      "Saumon aux poireaux",
      "",
      List("Faire revenir les poireaux avec du beurre", "Ajouter le saumon"),
      sourceURL = None,
      servings = 3,
      nutriScore = "B",
      preparationTimeMinutes = 40.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = false,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 150, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 2, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 1040,
      picture = None,
      isFavourite = false
    )
    val testRecipeWithHiddenAccount: Recipe = Recipe(
      999,
      testHiddenAccount.accountId,
      testHiddenAccount.pseudo,
      "Mon créateur va disparaitre !",
      "Nooooooooooo",
      List("Disparaitre"),
      sourceURL = None,
      servings = 1,
      "A",
      preparationTimeMinutes = 1.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 25, lipidGram = 2, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 10, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = None,
      isFavourite = false
    )
    val recipeToDelete: Recipe = Recipe(
      1111,
      testAccount2.accountId,
      testAccount2.pseudo,
      "Soupe au poireau",
      "Une bonne soupe hivernale",
      List("Cuire les poireaux.", "Ajouter des patates.", "Mixer"),
      sourceURL = None,
      servings = 5,
      "A",
      preparationTimeMinutes = 50.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 300, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("picture_encoded_b64_pwal"),
      isFavourite = false
    )
    val testHiddenRecipe: Recipe = Recipe(
      1212,
      testAccount2.accountId,
      testAccount2.pseudo,
      "Poireau à la soupe",
      "UN bon poireau hivernal",
      List("Cuire les soupes.", "Ajouter des patates.", "Mixer"),
      sourceURL = None,
      servings = 5,
      "A",
      preparationTimeMinutes = 50.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 300, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("picture_encoded_b64_pwalo"),
      isFavourite = false
    )
    val testRecipeToUpdate: Recipe = Recipe(
      48,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Daft punk cake",
      "A cake in the shape of a Daft Punk head",
      List("Bake the cake", "Decorate it so it looks like a Daft Punk head"),
      None,
      10,
      "E",
      60.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 600, lipidGram = 20, saturatedFattyAcidGram = 18, carbohydrateGram = 30, sugarGram = 30, dietaryFiberGram = 0, proteinGram = 2, saltGram = 0.1, sodiumMilliGram = 0.05,
      picture = None,
      isFavourite = false
    )

    val testRecipe1Ingredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipe1.recipeId, chickenFoodUnit1.foodId, chickenFoodUnit1.unitId, 2)
    val testRecipe1Ingredient2: NewRecipeIngredient = NewRecipeIngredient(testRecipe1.recipeId, mushroomFoodUnit0.foodId, mushroomFoodUnit0.unitId, 400)
    val testRecipe2Ingredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipe2.recipeId, mushroomFoodUnit0.foodId, mushroomFoodUnit0.unitId, 600)
    val testRecipe3Ingredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipe3.recipeId, chickenFoodUnit1.foodId, chickenFoodUnit1.unitId, 4)
    val testRecipe4Ingredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipe4.recipeId, onionFoodUnit2.foodId, onionFoodUnit2.unitId, 4)
    val testRecipe5Ingredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipe5.recipeId, leek.foodId, leekFoodUnit0.unitId, 300)
    val testRecipe5Ingredient2: NewRecipeIngredient = NewRecipeIngredient(testRecipe5.recipeId, salmon.foodId, salmonFoodUnit0.unitId, 150)
    val testRecipeWithHiddenAccountIngredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipeWithHiddenAccount.recipeId, onionFoodUnit1.foodId, onionFoodUnit1.unitId, 4)
    val recipeToDeleteIngredient1: NewRecipeIngredient = NewRecipeIngredient(recipeToDelete.recipeId, leekFoodUnit1.foodId, leekFoodUnit1.unitId, 4)
    val testHiddenRecipeIngredient: NewRecipeIngredient = NewRecipeIngredient(testHiddenRecipe.recipeId, leekFoodUnit1.foodId, leekFoodUnit1.unitId, 4)
    val testRecipeToUpdateIngredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipeToUpdate.recipeId, onion.foodId, onionFoodUnit2.unitId, 2)
    val testRecipeToUpdateIngredient2: NewRecipeIngredient = NewRecipeIngredient(testRecipeToUpdate.recipeId, mushroom.foodId, mushroomFoodUnit0.unitId, 300)

    val newRecipeId = 1
    val newRecipe: NewRecipe = NewRecipe(
      "Risotto à l'asperge et au pwâl",
      "Trop bon le pwâl ; d'Automne",
      List("Cuire oignons et riz dans le bouillon; L'arroser de pwâl", "", "Servir chaud."),
      List(
        NewRecipeIngredientApi(mushroom.foodId, mushroomFoodUnit1.unitId, 6),
        NewRecipeIngredientApi(salmon.foodId, salmonFoodUnit0.unitId, 150)
      ),
      4,
      30.0,
      None,
      Some("whoa look at this awesome picture of pwâl")
    )

    val newRecipeWithDuplicateIngredients: NewRecipe = NewRecipe(
      "Risotto à l'asperge et au pwâl",
      "Trop bon le pwâl ; d'Automne",
      List("Cuire oignons et riz dans le bouillon; L'arroser de pwâl", "", "Servir chaud."),
      NewRecipeIngredientApi(onion.foodId, onionFoodUnit2.unitId, 45) :: NewRecipeIngredientApi(onion.foodId, onionFoodUnit2.unitId, 300) :: newRecipe.ingredients,
      4,
      30.0,
      None,
      None
    )

    val createdRecipeId0: NewRecipeId = NewRecipeId(newRecipeId)
    val createdRecipeId1: NewRecipeId = NewRecipeId(newRecipeId + 1)
  }
}