package com.route

import java.time.LocalDateTime

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.BaseTest
import com.api.RecipeApi.{Recipe => RecipeApi}
import com.model.Account
import com.model.recipe.Recipe
import com.storage.{AccountStorage, FavouriteRecipeStorage, FoodStorage, FoodUnitStorage, RecipeStorage}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContextExecutor

class FavouriteRecipeRouteTest extends WordSpec with BaseTest with Matchers with ScalatestRouteTest with BeforeAndAfterAll {

  val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = {
    AccountStorage.createTables()
    FoodStorage.createTables()
    FoodUnitStorage.createTables()
    RecipeStorage.createTables()
    FavouriteRecipeStorage.createTables()

    new Context {
      AccountStorage.insertTestAccounts(List(testUser1, testUser2, testUser3, testUser4))
      RecipeStorage.insertTestRecipes(List(testRecipe1, testRecipe2))
      FavouriteRecipeStorage.insertTestFavouriteRecipes(List(
        testFavouriteUser1Recipe1,
        testFavouriteUser1Recipe2,
        testFavouriteUser3Recipe1,
        testFavouriteUser4Recipe1,
        testFavouriteUser4Recipe2
      ))
    }
  }

  override def afterAll(): Unit = {
    FavouriteRecipeStorage.dropTables()
    RecipeStorage.dropTables()
    FoodUnitStorage.dropTables()
    FoodStorage.dropTables()
    AccountStorage.dropTables()
  }

  "FavouriteRecipeRoute" when {
    "GET /api/recipe/favourite" should {
      "return 400 and an error if the token can't be deciphered" in new Context {
        Get(s"/api/recipe/favourite") ~> RawHeader("jwt", "walla") ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 200 and an empty list if there is no favourites for this user" in new Context {
        Get(s"/api/recipe/favourite") ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe Nil
        }
      }

      "return 200 and the user's favourite recipes" in new Context {
        Get(s"/api/recipe/favourite") ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe1.copy(isFavourite = true), testRecipe2.copy(isFavourite = true)).map(_.toRecipeApi)
        }
      }
    }

    "PUT /api/recipe/favourite/?" should {
      "return 400 and an error if the token can't be deciphered" in new Context {
        Put(s"/api/recipe/favourite/${testRecipe2.recipeId}") ~> RawHeader("jwt", "walla") ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 and an error if the recipe is already added as favourite" in new Context {
        Put(s"/api/recipe/favourite/${testRecipe1.recipeId}") ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "ALREADY_A_FAVOURITE"
        }
      }

      "return 200 and success if the recipe is successfully added as favourite" in new Context {
        Put(s"/api/recipe/favourite/${testRecipe2.recipeId}") ~> RawHeader("jwt", Account.getJsonWebToken(testUser3.accountId)) ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        Get(s"/api/recipe/favourite") ~> RawHeader("jwt", Account.getJsonWebToken(testUser3.accountId)) ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe1.copy(isFavourite = true), testRecipe2.copy(isFavourite = true)).map(_.toRecipeApi)
        }
      }
    }

    "DELETE /api/recipe/favourite/?" should {
      "return 400 and an error if the token can't be deciphered" in new Context {
        Delete(s"/api/recipe/favourite/${testRecipe2.recipeId}") ~> RawHeader("jwt", "walla") ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 and an error if the recipe was not a favourite" in new Context {
        Delete(s"/api/recipe/favourite/${testRecipe2.recipeId}") ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "NOT_A_FAVOURITE"
        }
      }

      "return 200 and success if the recipe is successfully removed" in new Context {
        Delete(s"/api/recipe/favourite/${testRecipe2.recipeId}") ~> RawHeader("jwt", Account.getJsonWebToken(testUser4.accountId)) ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        Get(s"/api/recipe/favourite") ~> RawHeader("jwt", Account.getJsonWebToken(testUser4.accountId)) ~> favouriteRecipeRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[RecipeApi]] shouldBe List(testRecipe1.copy(isFavourite = true)).map(_.toRecipeApi)
        }
      }
    }
  }

  trait Context {
    val favouriteRecipeRoute: Route = FavouriteRecipeRoute().route

    val testUser1: Account = Account(3, "columbine@cache.cache", "Columbine", "foda c hash")
    val testUser2: Account = Account(4, "robert.parker@synth.wave", "RB", "85' again")
    val testUser3: Account = Account(5, "lore.nzo@rap.nul", "Mamène", "du sale")
    val testUser4: Account = Account(6, "mbk.rocker@vroum.vroum", "Rico", "empereur")

    val testRecipe1: Recipe = Recipe(
      5,
      testUser1.accountId,
      testUser1.pseudo,
      "Saumon au four",
      "La meilleure façon de cuire du saumon",
      List("Mettre le saumon dans un plat", "Au four à 180C pendant 30min"),
      sourceURL = Some("https://www.fishrecipes.com"),
      servings = 2,
      "B",
      preparationTimeMinutes = 30.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 300, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("this is a picture hash"),
      isFavourite = false
    )
    val testRecipe2: Recipe = Recipe(
      6,
      testUser1.accountId,
      testUser1.pseudo,
      "Daurade au four",
      "Une dorade croustillante comme jamais",
      List("Mettre la daurade dans un plat", "Au four à 180C pendant 30min"),
      sourceURL = None,
      servings = 1,
      "B",
      preparationTimeMinutes = 25.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 150, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = None,
      isFavourite = false
    )

    val testFavouriteUser1Recipe1: (Int, Int, LocalDateTime) = (testUser1.accountId, testRecipe1.recipeId, LocalDateTime.now())
    val testFavouriteUser1Recipe2: (Int, Int, LocalDateTime) = (testUser1.accountId, testRecipe2.recipeId, LocalDateTime.now())
    val testFavouriteUser3Recipe1: (Int, Int, LocalDateTime) = (testUser3.accountId, testRecipe1.recipeId, LocalDateTime.now())
    val testFavouriteUser4Recipe1: (Int, Int, LocalDateTime) = (testUser4.accountId, testRecipe1.recipeId, LocalDateTime.now())
    val testFavouriteUser4Recipe2: (Int, Int, LocalDateTime) = (testUser4.accountId, testRecipe2.recipeId, LocalDateTime.now())
  }
}
