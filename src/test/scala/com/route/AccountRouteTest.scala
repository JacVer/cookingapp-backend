package com.route

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.BaseTest
import com.api.AccountApi.{CreateAccount, Credentials, JsonWebToken}
import com.model.Account
import com.storage.AccountStorage
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import org.scalatest._

import scala.concurrent.ExecutionContextExecutor

class AccountRouteTest
  extends WordSpec
    with BaseTest
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterAll {

  val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = new Context {
    AccountStorage.createTables()
    AccountStorage.insertTestAccounts(List(
      Account(testUser1UserId, testUser1Email, testUser1Pseudo, testUser1PasswordHash),
      Account(userToDeleteUserId, userToDeleteEmail, userToDeletePseudo, userToDeletePasswordHash)
    ))
  }

  override protected def afterAll(): Unit = AccountStorage.dropTables()

  "UserRouteTest" when {
    // TODO : return the pseudo as well
    "POST /api/account/login" should {
      "return 200 and a token if email and password are correct" in new Context {
        Post("/api/account/login", Credentials(testUser1Email, testUser1Password)) ~> userRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[JsonWebToken] shouldBe JsonWebToken(testUser1JsonWebToken)
        }
      }

      "return 400 and an error if the email doesn't contain an @" in new Context {
        Post("/api/account/login", Credentials(emailWithoutAt, "pwal")) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_EMAIL"
        }
      }

      "return 400 and an error if the email contains a semi colon" in new Context {
        Post("/api/account/login", Credentials(emailWithSemiColon, "pwal")) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_EMAIL"
        }
      }

      "return 400 and an error if the user can't be found" in new Context {
        Post("/api/account/login", Credentials(missingEmail, "pwal")) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "EMAIL_NOT_FOUND"
        }
      }

      "return 400 and an error if the password is incorrect" in new Context {
        Post("/api/account/login", Credentials(testUser1Email, "wrong password")) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "WRONG_PASSWORD"
        }
      }
    }

    "POST /api/account/create" should {
      "return 200 and a token if the user is created successfully" in new Context {
        Post("/api/account/create", CreateAccount(userToCreateEmail, userToCreatePeudo, userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[JsonWebToken] shouldBe JsonWebToken(userToCreateToken)
        }

        Post("/api/account/login", Credentials(userToCreateEmail, userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[JsonWebToken] shouldBe JsonWebToken(userToCreateToken)
        }
      }

      "return 400 and an error if the email doesn't contain an @" in new Context {
        Post("/api/account/create", CreateAccount(emailWithoutAt, userToCreatePeudo, userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_EMAIL"
        }
      }

      "return 400 and an error if the email contains a semi colon" in new Context {
        Post("/api/account/create", CreateAccount(emailWithSemiColon, userToCreatePeudo, userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_EMAIL"
        }
      }

      "return 400 and an error if the password is too short" in new Context {
        Post("/api/account/create", CreateAccount(userToCreateEmail, userToCreatePeudo, "pwal")) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "PASSWORD_TOO_SHORT"
        }
      }

      "return 400 and an error if the email already exists" in new Context {
        Post("/api/account/create", CreateAccount(testUser1Email, userToCreatePeudo, userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "ALREADY_USED_EMAIL"
        }
      }

      "return 400 and an error if the pseudo already exists" in new Context {
        Post("/api/account/create", CreateAccount(missingEmail, testUser1Pseudo, userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "ALREADY_USED_PSEUDO"
        }
      }

      "return 400 and an error if the pseudo contains a semi colon" in new Context {
        Post("/api/account/create", CreateAccount(userToCreateEmail, "pseu;do", userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PSEUDO"
        }
      }

      "return 400 and an error if the pseudo contains a space" in new Context {
        Post("/api/account/create", CreateAccount(userToCreateEmail, "pseu do", userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PSEUDO"
        }
      }

      "return 400 and an error if the pseudo contains an @" in new Context {
        Post("/api/account/create", CreateAccount(userToCreateEmail, "pseu@do", userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PSEUDO"
        }
      }

      "return 400 and an error if the pseudo has less than 4 characters" in new Context {
        Post("/api/account/create", CreateAccount(userToCreateEmail, "aaa", userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PSEUDO"
        }

        Post("/api/account/create", CreateAccount(userToCreateEmail, "", userToCreatePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_PSEUDO"
        }
      }
    }

    "POST /api/account/delete" should {
      "return 400 and an error if the email doesn't contain an @" in new Context {
        Post("/api/account/delete", Credentials(emailWithoutAt, userToDeletePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_EMAIL"
        }
      }

      "return 400 and an error if the email contains a semi colon" in new Context {
        Post("/api/account/delete", Credentials(emailWithSemiColon, userToDeletePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_EMAIL"
        }
      }

      "return 400 and an error if the user can't be found" in new Context {
        Post("/api/account/delete", Credentials(missingEmail, userToDeletePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "EMAIL_NOT_FOUND"
        }
      }

      "return 400 and an error if the password is incorrect" in new Context {
        Post("/api/account/delete", Credentials(userToDeleteEmail, "wrong password")) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "WRONG_PASSWORD"
        }
      }

      "return 200 and Success if the user is deleted successfully" in new Context {
        Post("/api/account/delete", Credentials(userToDeleteEmail, userToDeletePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        Post("/api/account/login", Credentials(userToDeleteEmail, userToDeletePassword)) ~> userRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "EMAIL_NOT_FOUND"
        }
      }
    }
  }

  trait Context {
    val userRoute: Route = AccountRoute().route

    val testUser1UserId = 56
    val testUser1Email = "email@pwal.ninja"
    val testUser1Pseudo = "pwalo"
    val testUser1Password = "pwal password"
    val testUser1PasswordHash: String = Account.encryptPassword(testUser1Password)
    val testUser1JsonWebToken: String = Account.getJsonWebToken(testUser1UserId)

    val missingEmail = "notfoundemail@pwal.ninja"
    val emailWithoutAt = "pwal"
    val emailWithSemiColon = "pw;al"

    val userToDeleteUserId = 58
    val userToDeleteEmail = "emailtodelete@pwal.ninja"
    val userToDeletePseudo = "Mamène"
    val userToDeletePassword = "yolololo"
    val userToDeletePasswordHash: String = Account.encryptPassword(userToDeletePassword)

    val userToCreateUserId = 1
    val userToCreateEmail = "newUser@pwal.com"
    val userToCreatePeudo = "amongus"
    val userToCreatePassword = "new pwal password"
    val userToCreateToken: String = Account.getJsonWebToken(userToCreateUserId)
  }

}