package com.route

import java.time.LocalDateTime

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.BaseTest
import com.api.ShoppingListApi.{AddShoppingListItems, NewShoppingList, NewShoppingListItem, RenameShoppingList, ShoppingListCreatedId, ShoppingListSummary, ShoppingList => ShoppingListApi, ShoppingListItem => ShoppingListItemApi}
import com.model.{ShoppingList, ShoppingListItem, Account}
import com.storage.{ShoppingListStorage, AccountStorage}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContextExecutor

class ShoppingListRouteTest
  extends WordSpec
    with BaseTest
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterAll {

  val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = {
    AccountStorage.createTables()
    ShoppingListStorage.createTables()

    new Context {
      AccountStorage.insertTestAccounts(List(testUser1, testUser2, testUser3, testUser4))

      ShoppingListStorage.insertTestShoppingLists(List(testShoppingList1, testShoppingList2, testShoppingList3, testShoppingList4, testShoppingList5, testShoppingList6, testShoppingList7, testShoppingList8))
      ShoppingListStorage.insertTestShoppingListItems(testShoppingList1Items ::: testShoppingList3Items ::: testShoppingList4Items ::: testShoppingList5Items)
    }
  }

  override def afterAll(): Unit = {
    ShoppingListStorage.dropTables()
    AccountStorage.dropTables()
  }

  "ShoppingListRouteTest" when {
    "POST /api/shopping-list" should {

      "return 200 and the shopping_list_id if the list was successfully created" in new Context {
        Post("/api/shopping-list", NewShoppingList("New shopping list")) ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[ShoppingListCreatedId] shouldBe ShoppingListCreatedId(1)
        }

        awaitForResult(ShoppingListStorage.selectShoppingListByShoppingListId(1)).map(_.length) shouldBe
          Right(1)
      }

      "return 400 if the shopping-list name is empty" in new Context {
        Post("/api/shopping-list", NewShoppingList("")) ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "EMPTY_SHOPPING_LIST_TITLE"
        }
      }

      "return 400 if the token can't be deciphered" in new Context {
        Post("/api/shopping-list", NewShoppingList("New shopping list")) ~> RawHeader("jwt", "wallaa") ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }
    }

    "GET /api/shopping-list/?" should {

      "return 200 if the token is correct and the shopping list exists" in new Context {
        Get(s"/api/shopping-list/${testShoppingList1.shoppingListId}") ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[ShoppingListApi] shouldBe
            ShoppingListApi(
              testShoppingList1.shoppingListId,
              testShoppingList1.addedTime,
              testShoppingList1.title,
              testShoppingList1Items.map(i => ShoppingListItemApi(i.itemId, i.ticked, i.indented, i.text))
            )
        }
      }

      "return 400 if the token can't be deciphered" in new Context {
        Get(s"/api/shopping-list/${testShoppingList1.shoppingListId}") ~> RawHeader("jwt", "walla") ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 if the user_id in the token doesn't own this shopping list" in new Context {
        Get(s"/api/shopping-list/${testShoppingList1.shoppingListId}") ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "USER_NOT_OWNER_OF_SHOPPING_LIST"
        }
      }

      "return 400 if the shopping list can't be found" in new Context {
        Get(s"/api/shopping-list/4565") ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "SHOPPING_LIST_NOT_FOUND"
        }
      }
    }

    "POST /api/shopping-list/?/items" should {

      "return 200 and success if it succeeded" in new Context {
        Post(s"/api/shopping-list/${testShoppingList2.shoppingListId}/items",
          AddShoppingListItems(
            List(
              NewShoppingListItem(ticked = false, indented = false, "Galette bretonne"),
              NewShoppingListItem(ticked = false, indented = true, "Jambon"),
              NewShoppingListItem(ticked = false, indented = true, "Fromage râpé"),
              NewShoppingListItem(ticked = false, indented = true, "Galette de sarrasin")
            )
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }
      }

      "return 400 if the token can't be deciphered" in new Context {
        Post(
          s"/api/shopping-list/${testShoppingList2.shoppingListId}/items",
          AddShoppingListItems(
            List(
              NewShoppingListItem(ticked = false, indented = false, "pwal")
            )
          )
        ) ~> RawHeader("jwt", "walla") ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 if the user_id in the token doesn't own this shopping list" in new Context {
        Post(
          s"/api/shopping-list/${testShoppingList2.shoppingListId}/items",
          AddShoppingListItems(
            List(
              NewShoppingListItem(ticked = false, indented = false, "pwal")
            )
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "USER_NOT_OWNER_OF_SHOPPING_LIST"
        }
      }

      "return 400 if the shopping list can't be found" in new Context {
        Post(
          s"/api/shopping-list/4556/items",
          AddShoppingListItems(
            List(
              NewShoppingListItem(ticked = false, indented = false, "pwal")
            )
          )
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "SHOPPING_LIST_NOT_FOUND"
        }
      }

      "return 400 if the list of items is empty" in new Context {
        Post(
          s"/api/shopping-list/${testShoppingList2.shoppingListId}/items",
          AddShoppingListItems(Nil)
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "EMPTY_ITEM_LIST"
        }
      }
    }

    "DELETE /api/shopping-list/?/items/?" should {

      "return 200 and success if it succeeded" in new Context {
        Delete(s"/api/shopping-list/${testShoppingList3.shoppingListId}/items/2") ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList3.shoppingListId)) shouldBe Right(
          testShoppingList3Items.filter(_.itemId != 2)
        )
      }

      "return 400 if the token can't be deciphered" in new Context {
        Delete(s"/api/shopping-list/${testShoppingList3.shoppingListId}/items/2") ~> RawHeader("jwt", "walla") ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 if the user_id in the token doesn't own this shopping list" in new Context {
        Delete(s"/api/shopping-list/${testShoppingList3.shoppingListId}/items/2") ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "USER_NOT_OWNER_OF_SHOPPING_LIST"
        }
      }

      "return 400 if the shopping list can't be found" in new Context {
        Delete(s"/api/shopping-list/7852/items/2") ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "SHOPPING_LIST_NOT_FOUND"
        }
      }

      "return 400 if the item_id can't be found in the shopping list" in new Context {
        Delete(s"/api/shopping-list/${testShoppingList3.shoppingListId}/items/45678") ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "ITEM_NOT_FOUND"
        }
      }

    }

    "DELETE /api/shopping-list/?" should {

      "return 400 if the token can't be deciphered" in new Context {
        Delete(s"/api/shopping-list/${testShoppingList4.shoppingListId}") ~> RawHeader("jwt", "walla") ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 if the user_id in the token doesn't own this shopping list" in new Context {
        Delete(s"/api/shopping-list/${testShoppingList4.shoppingListId}") ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "USER_NOT_OWNER_OF_SHOPPING_LIST"
        }
      }

      "return 400 if the shopping list can't be found" in new Context {
        Delete(s"/api/shopping-list/752") ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "SHOPPING_LIST_NOT_FOUND"
        }
      }

      "return 200 and success if it succeeded" in new Context {
        Delete(s"/api/shopping-list/${testShoppingList4.shoppingListId}") ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        awaitForResult(ShoppingListStorage.selectShoppingListByShoppingListId(testShoppingList4.shoppingListId)) shouldBe
          Right(Nil)

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList4.shoppingListId)) shouldBe
          Right(Nil)
      }
    }

    "PATCH /api/shopping-list/?/items/?" should {

      "return 400 if the token can't be deciphered" in new Context {
        Patch(
          s"/api/shopping-list/${testShoppingList5.shoppingListId}/items/2?ticked=false&indented=false"
        ) ~> RawHeader("jwt", "wallaaa") ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 if the user_id in the token doesn't own this shopping list" in new Context {
        Patch(
          s"/api/shopping-list/${testShoppingList5.shoppingListId}/items/2?ticked=false&indented=false"
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "USER_NOT_OWNER_OF_SHOPPING_LIST"
        }
      }

      "return 400 if the shopping list can't be found" in new Context {
        Patch(
          s"/api/shopping-list/4568/items/2?ticked=false&indented=false"
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "SHOPPING_LIST_NOT_FOUND"
        }
      }

      "return 400 if the item_id can't be found in the shopping list" in new Context {
        Patch(
          s"/api/shopping-list/${testShoppingList5.shoppingListId}/items/7852?ticked=false&indented=false"
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "ITEM_NOT_FOUND"
        }
      }

      "return 400 if neither indented, ticked or text are defined" in new Context {
        Patch(
          s"/api/shopping-list/${testShoppingList5.shoppingListId}/items/2"
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "AT_LEAST_ONE_PARAMETER_MUST_BE_DEFINED"
        }
      }

      "return 200 if the item was updated successfully" in new Context {
        Patch(
          s"/api/shopping-list/${testShoppingList5.shoppingListId}/items/0?indented=true",
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        Patch(
          s"/api/shopping-list/${testShoppingList5.shoppingListId}/items/1?ticked=false"
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId))~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        Patch(
          s"/api/shopping-list/${testShoppingList5.shoppingListId}/items/2?ticked=false&indented=true"
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        Patch(
          s"/api/shopping-list/${testShoppingList5.shoppingListId}/items/4",
          "en fait tout va bien"
        ) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList5.shoppingListId)) shouldBe
          Right(List(
            ShoppingListItem(testShoppingList5.shoppingListId, 0, ticked = false, indented = true, "toujours pas d'idée"),
            ShoppingListItem(testShoppingList5.shoppingListId, 1, ticked = false, indented = false, "toujours autant de pluie chez moi"),
            ShoppingListItem(testShoppingList5.shoppingListId, 2, ticked = false, indented = true, "pourtant il fait beau"),
            ShoppingListItem(testShoppingList5.shoppingListId, 3, ticked = true, indented = false, "tout va bien"),
            ShoppingListItem(testShoppingList5.shoppingListId, 4, ticked = false, indented = true, "en fait tout va bien")
          ))
      }
    }

    "PATCH /api/shopping-list/?" should {

      "return 400 if the token can't be deciphered" in new Context {
        Patch(s"/api/shopping-list/${testShoppingList5.shoppingListId}", RenameShoppingList("blé")) ~> RawHeader("jwt", "walla") ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }

      "return 400 if the user_id in the token doesn't own this shopping list" in new Context {
        Patch(s"/api/shopping-list/${testShoppingList5.shoppingListId}", RenameShoppingList("blé")) ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "USER_NOT_OWNER_OF_SHOPPING_LIST"
        }
      }

      "return 400 if the shopping list can't be found" in new Context {
        Patch(s"/api/shopping-list/7586", RenameShoppingList("blé")) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "SHOPPING_LIST_NOT_FOUND"
        }
      }

      "return 400 if the shopping list name is empty" in new Context {
        Patch(s"/api/shopping-list/${testShoppingList5.shoppingListId}", RenameShoppingList("")) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "EMPTY_SHOPPING_LIST_TITLE"
        }
      }

      "return 200 and success if it succeeded" in new Context {
        Patch(s"/api/shopping-list/${testShoppingList5.shoppingListId}", RenameShoppingList("List de course 5bis")) ~> RawHeader("jwt", Account.getJsonWebToken(testUser2.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[Boolean] shouldBe true
        }

        awaitForResult(ShoppingListStorage.selectShoppingListByShoppingListId(testShoppingList5.shoppingListId)).map(_.head.title) shouldBe
          Right("List de course 5bis")
      }
    }

    "GET /api/shopping-list" should {

      "return 400 if the token can't be deciphered" in new Context {
        Get(s"/api/shopping-list?ordering=alphabetical") ~> RawHeader("jwt", "walla") ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "INVALID_TOKEN"
        }
      }
      "return 400 if the ordering is something else than 'alphabetical' or 'created_time'" in new Context {
        Get(s"/api/shopping-list?ordering=pwal") ~> RawHeader("jwt", Account.getJsonWebToken(testUser1.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "UNKNOWN_ORDERING"
        }
      }

      "return 200 and nothing if the user doesn't have any shopping lists" in new Context {
        Get(s"/api/shopping-list?ordering=alphabetical") ~> RawHeader("jwt", Account.getJsonWebToken(testUser3.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[ShoppingListSummary]] shouldBe Nil
        }
      }

      "return 200 and the user shopping lists ordered by alphabetical" in new Context {
        Get(s"/api/shopping-list?ordering=alphabetical") ~> RawHeader("jwt", Account.getJsonWebToken(testUser4.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[ShoppingListSummary]] shouldBe List(testShoppingList8, testShoppingList6, testShoppingList7).map(s => ShoppingListSummary(s.shoppingListId, s.addedTime, s.title))
        }
      }

      "return 200 and the user shopping lists ordered by created time " in new Context {
        Get(s"/api/shopping-list?ordering=created_time") ~> RawHeader("jwt", Account.getJsonWebToken(testUser4.accountId)) ~> shoppingListRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[ShoppingListSummary]] shouldBe List(testShoppingList8, testShoppingList7, testShoppingList6).map(s => ShoppingListSummary(s.shoppingListId, s.addedTime, s.title))
        }
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.of(2021, 2, 14, 23, 33)
    val shoppingListRoute: Route = ShoppingListRoute().route

    val testUser1: Account = Account(22, "les@prélis.fr", "border", "line_hash")
    val testUser2: Account = Account(33, "moby@love.fr", "Pursuit", "dududududududududu")
    val testUser3: Account = Account(44, "moby@88.fr", "After", "life_dududu")
    val testUser4: Account = Account(55, "88@moby.fr", "String", "electro_hash")

    val testShoppingList1: ShoppingList = ShoppingList(testUser1.accountId, 5, timeAdded, "Liste de course 1")
    val testShoppingList1Items: List[ShoppingListItem] = List(
      ShoppingListItem(testShoppingList1.shoppingListId, 0, ticked = false, indented = false, "Pizza"),
      ShoppingListItem(testShoppingList1.shoppingListId, 1, ticked = false, indented = true, "Sauce tomate"),
      ShoppingListItem(testShoppingList1.shoppingListId, 2, ticked = true, indented = true, "Olives noires"),
      ShoppingListItem(testShoppingList1.shoppingListId, 3, ticked = true, indented = true, "Champignons")
    )
    val testShoppingList2: ShoppingList = ShoppingList(testUser1.accountId, 6, timeAdded, "Liste de course 2")
    val testShoppingList3: ShoppingList = ShoppingList(testUser2.accountId, 7, timeAdded, "Liste de course 3")
    val testShoppingList3Items: List[ShoppingListItem] = List(
      ShoppingListItem(testShoppingList3.shoppingListId, 0, ticked = false, indented = false, "Galette complète"),
      ShoppingListItem(testShoppingList3.shoppingListId, 1, ticked = false, indented = true, "Oeufs"),
      ShoppingListItem(testShoppingList3.shoppingListId, 2, ticked = false, indented = true, "Jambon"),
      ShoppingListItem(testShoppingList3.shoppingListId, 3, ticked = false, indented = true, "Fromage râpé")
    )
    val testShoppingList4: ShoppingList = ShoppingList(testUser1.accountId, 8, timeAdded, "Liste de course 4")
    val testShoppingList4Items: List[ShoppingListItem] = List(
      ShoppingListItem(testShoppingList4.shoppingListId, 0, ticked = false, indented = false, "g pa didé"),
      ShoppingListItem(testShoppingList4.shoppingListId, 1, ticked = false, indented = true, "lala"),
      ShoppingListItem(testShoppingList4.shoppingListId, 2, ticked = false, indented = true, "nanana")
    )
    val testShoppingList5: ShoppingList = ShoppingList(testUser2.accountId, 9, timeAdded, "Liste de course 5")
    val testShoppingList5Items: List[ShoppingListItem] = List(
      ShoppingListItem(testShoppingList5.shoppingListId, 0, ticked = false, indented = false, "toujours pas d'idée"),
      ShoppingListItem(testShoppingList5.shoppingListId, 1, ticked = true, indented = false, "toujours autant de pluie chez moi"),
      ShoppingListItem(testShoppingList5.shoppingListId, 2, ticked = true, indented = false, "pourtant il fait beau"),
      ShoppingListItem(testShoppingList5.shoppingListId, 3, ticked = true, indented = false, "tout va bien"),
      ShoppingListItem(testShoppingList5.shoppingListId, 4, ticked = false, indented = true, "tout ne va pas bien"),
    )

    val testShoppingList6: ShoppingList = ShoppingList(testUser4.accountId, 10, timeAdded, "Attention")
    val testShoppingList7: ShoppingList = ShoppingList(testUser4.accountId, 11, timeAdded.plusMinutes(2), "Bahaha")
    val testShoppingList8: ShoppingList = ShoppingList(testUser4.accountId, 12, timeAdded.plusMinutes(5), "AAAAAHH")
  }
}
