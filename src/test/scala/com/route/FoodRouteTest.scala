package com.route

import java.time.LocalDateTime

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.BaseTest
import com.api.FoodApi.{Food => FoodApi, FoodUnit => FoodUnitApi}
import com.model.food.{CalnutFood, NewFoodUnit}
import com.storage.{FoodStorage, FoodUnitStorage}
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import org.scalatest._

import scala.concurrent.ExecutionContextExecutor

class FoodRouteTest extends WordSpec with BaseTest with Matchers with ScalatestRouteTest with BeforeAndAfterAll {

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = new Context {
    FoodStorage.createTables()
    FoodUnitStorage.createTables()
    FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood0, testCalnutFood1))
    FoodUnitStorage.insertFoodUnits(List(testCalnutFood0Unit0, testCalnutFood0Unit1))
  }

  override def afterAll(): Unit = {
    FoodUnitStorage.dropTables()
    FoodStorage.dropTables()
  }

  "FoodRoute" when {

    "GET /api/food?name" should {

      "return 200 and the food items matching" in new Context {
        Get(s"/api/food?name=o") ~> foodRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[FoodApi]] shouldBe List(testCalnutFood1, testCalnutFood0).map(_.toFood.toFoodApi)
        }
      }

      "return 200 and the food items matching with a token" in new Context {
        Get(s"/api/food?name=o") ~> RawHeader("jwt", "pwal") ~> foodRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[FoodApi]] shouldBe List(testCalnutFood1, testCalnutFood0).map(_.toFood.toFoodApi)
        }
      }

      "return 200 and no food items if no one matches" in new Context {
        Get(s"/api/food?name=fromage") ~> foodRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[FoodApi]] shouldBe Nil
        }
      }

      "return 400 and an error if the name contains a semi colon" in new Context {
        Get(s"/api/food?name=fro;mage") ~> foodRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe
            "INVALID_FOOD_NAME"
        }
      }
    }

    "GET /api/food/?" should {

      "return 200 and the food with the given food_id" in new Context {
        Get(s"/api/food/${testCalnutFood0.foodId}") ~> foodRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[FoodApi] shouldBe testCalnutFood0.toFood.toFoodApi
        }
      }

      "return 200 and the food with the food_id and its units exist with a token" in new Context {
        Get(s"/api/food/${testCalnutFood0.foodId}") ~> RawHeader("jwt", "pwal") ~> foodRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[FoodApi] shouldBe testCalnutFood0.toFood.toFoodApi
        }
      }

      "return 400 and an error if the food doesn't exist" in new Context {
        Get(s"/api/food/4568") ~> foodRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "FOOD_NOT_FOUND"
        }
      }
    }

    "GET /api/food/?/units" should {

      "return 200 and the units of the food" in new Context {
        Get(s"/api/food/${testCalnutFood0.foodId}/units") ~> foodRoute ~> check {
          status shouldBe StatusCodes.OK
          responseAs[List[FoodUnitApi]] shouldBe List(
            FoodUnitApi(testCalnutFood0Unit0.unitId, testCalnutFood0Unit0.unitNameFr, testCalnutFood0Unit0.weightGrams),
            FoodUnitApi(testCalnutFood0Unit1.unitId, testCalnutFood0Unit1.unitNameFr, testCalnutFood0Unit1.weightGrams)
          )
        }
      }

      "return 400 and an error if the food doesn't exist" in new Context {
        Get(s"/api/food/78452/units") ~> foodRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "FOOD_UNITS_NOT_FOUND"
        }
      }

      "return 400 and an error if the food exist but doesn't have any units" in new Context {
        Get(s"/api/food/${testCalnutFood1.foodId}/units") ~> foodRoute ~> check {
          status shouldBe StatusCodes.BadRequest
          responseAs[String] shouldBe "FOOD_UNITS_NOT_FOUND"
        }
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.now()
    val foodRoute: Route = FoodRoute().route

    val testCalnutFood0: CalnutFood = CalnutFood(24, "Légumes", 20303, "Brocoli", nutriScoreTEV = true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=false, february=false, march=false, april=false, may=false, june=true, july=true, august=true, september=true, october=true, november=false, december=false, 23.1, 93.8, 0.033, 13, 11, 39, 90, 43, 0.17, 0.33, 0.15, 0.18, 10, 10, 2.19, 1.03, 0.6, 0.3, 0.1, 0.1, 0.3, 0.1, 0.1, 0.18, 0.25, 3, 0.5, 0.14, 0.13, 0.18, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.1, 0.04, 0.12, 0.06, 0.12, 0.005, 0.005, 0.005, 0, 576, 0, 1.69, 49.6, 0, 18.1, 0.0075, 0.005, 0.05, 0.25, 0.048, 0, 24.7, 0, 0.053, 0)
    val testCalnutFood1: CalnutFood = CalnutFood(0, "Bouillons", 11001, "Bouillon de bœuf déshydraté", nutriScoreTEV = false, pescetarian=false, vegetarian=false, vegan=false, glutenFree=false, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 240, 2.6, 33, 13200, 21.9, 127, 344, 32.7, 0.1, 0.35, 0.12, 0.32, 4.6, 2.5, 10.2, 19.7, 3.95, 0.32, 0, 0, 0.27, 0.15, 3.36, 0, 0, 0.65, 13.3, 7.03, 2.6, 1.4, 0, 0, 0.013, 0.013, 0.038, 0.18, 5.42, 1.25, 2.6, 0.96, 0.038, 0, 0, 0, 0, 0, 0, 2.17, 0, 0, 0, 1.38, 0.23, 2.09, 0.48, 0.29, 0.34, 76.3, 0, 0, 0)

    val testCalnutFood0Unit0: NewFoodUnit = NewFoodUnit(testCalnutFood0.foodId, 0, "grammes", 1)
    val testCalnutFood0Unit1: NewFoodUnit = NewFoodUnit(testCalnutFood0.foodId, 1, "brocoli moyen", 300)
  }
}
