package com.route

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.ContentNegotiator.Alternative.ContentType
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.parser.decode
import org.scalatest.{Matchers, WordSpec}

class HttpRouteTest extends WordSpec with Matchers with ScalatestRouteTest {

  "HttpRoute" when {
    "GET /heartbeat" should {
      "return 200 - I'm looking for a heartbeat !" in new Context {
        Get("/heartbeat") ~> httpRoute ~> check {
          status shouldBe StatusCodes.OK
        }
      }
    }
  }

  trait Context {
    val httpRoute: Route = HttpRoute.route
  }
}
