package com.admin

import java.time.LocalDateTime

import com.BaseTest
import com.model.food.{CalnutFood, NewFoodUnit}
import com.storage.{FoodStorage, FoodUnitStorage}
import org.scalatest.{Matchers, WordSpec}

class DataCheckTest  extends WordSpec with BaseTest with Matchers {

  "Data check functions" when {
    "selectFoodIdWithoutUnit" should {
      "raise an error containing the foodIds without units if there are some" in new Context {
        FoodStorage.createTables()
        FoodUnitStorage.createTables()

        FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood0, testCalnutFood1, testCalnutFood2, testCalnutFood3, testCalnutFood4))
        FoodUnitStorage.insertFoodUnits(List(testCalnutFood0Unit0, testCalnutFood0Unit1, testCalnutFood1Unit0, testCalnutFood1Unit1))

        FoodUnitStorage.selectFoodIdWithoutUnit() shouldBe Right(List(testCalnutFood2.foodId, testCalnutFood3.foodId, testCalnutFood4.foodId))

        FoodUnitStorage.dropTables()
        FoodStorage.dropTables()
      }

      "return Nil if each food has at least one unit" in new Context {
        FoodStorage.createTables()
        FoodUnitStorage.createTables()

        FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood0, testCalnutFood1))
        FoodUnitStorage.insertFoodUnits(List(testCalnutFood0Unit0, testCalnutFood0Unit1, testCalnutFood1Unit0, testCalnutFood1Unit1))

        FoodUnitStorage.selectFoodIdWithoutUnit() shouldBe Right(Nil)

        FoodUnitStorage.dropTables()
        FoodStorage.dropTables()
      }
    }

    "selectUnitIdWithMoreThanOneName" should {
      "raise an error if the same unit_id is used with more than one unit_name_fr" in new Context {

        FoodStorage.createTables()
        FoodUnitStorage.createTables()

        FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood0, testCalnutFood1))
        FoodUnitStorage.insertFoodUnits(List(testCalnutFood0Unit0, testCalnutFood0Unit1, testCalnutFood1Unit0, testCalnutFood1Unit1, testCalnutFood1ErrorUnit))

        FoodUnitStorage.selectUnitIdWithMultipleUnitNameFr() shouldBe Right(List(testCalnutFood1ErrorUnit.unitId))

        FoodUnitStorage.dropTables()
        FoodStorage.dropTables()
      }

      "return Nil if each unit_id is unique to each unit_name_fr" in new Context {
        FoodStorage.createTables()
        FoodUnitStorage.createTables()

        FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood0, testCalnutFood1))
        FoodUnitStorage.insertFoodUnits(List(testCalnutFood0Unit0, testCalnutFood0Unit1, testCalnutFood1Unit0, testCalnutFood1Unit1))

        FoodUnitStorage.selectUnitIdWithMultipleUnitNameFr() shouldBe Right(Nil)

        FoodUnitStorage.dropTables()
        FoodStorage.dropTables()
      }
    }

    "selectFoodIdWithoutGramUnit" should {
      "return the food_id of the food that don't have grams as a unit" in new Context {
        FoodStorage.createTables()
        FoodUnitStorage.createTables()

        FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood0, testCalnutFood1))
        FoodUnitStorage.insertFoodUnits(List(testCalnutFood0Unit0, testCalnutFood0Unit1, testCalnutFood1Unit1))

        FoodUnitStorage.selectFoodIdWithoutGramUnit() shouldBe Right(List(testCalnutFood1.foodId))

        FoodUnitStorage.dropTables()
        FoodStorage.dropTables()
      }

      "return Nil if all food have a corresponding grammes unit" in new Context {
        FoodStorage.createTables()
        FoodUnitStorage.createTables()

        FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood0, testCalnutFood1))
        FoodUnitStorage.insertFoodUnits(List(testCalnutFood0Unit0, testCalnutFood0Unit1, testCalnutFood1Unit0, testCalnutFood1Unit1))

        FoodUnitStorage.selectFoodIdWithoutGramUnit() shouldBe Right(Nil)

        FoodUnitStorage.dropTables()
        FoodStorage.dropTables()
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.now()

    val testCalnutFood0: CalnutFood = CalnutFood(24, "Légumes", 20303, "Brocoli", nutriScoreTEV = true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=false, february=false, march=false, april=false, may=false, june=true, july=true, august=true, september=true, october=true, november=false, december=false, 23.1, 93.8, 0.033, 13, 11, 39, 90, 43, 0.17, 0.33, 0.15, 0.18, 10, 10, 2.19, 1.03, 0.6, 0.3, 0.1, 0.1, 0.3, 0.1, 0.1, 0.18, 0.25, 3, 0.5, 0.14, 0.13, 0.18, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.1, 0.04, 0.12, 0.06, 0.12, 0.005, 0.005, 0.005, 0, 576, 0, 1.69, 49.6, 0, 18.1, 0.0075, 0.005, 0.05, 0.25, 0.048, 0, 24.7, 0, 0.053, 0)
    val testCalnutFood1: CalnutFood = testCalnutFood0.copy(foodId = 44, foodNameFr = "Wallez")
    val testCalnutFood2: CalnutFood = testCalnutFood0.copy(foodId = 55, foodNameFr = "Pwal")
    val testCalnutFood3: CalnutFood = testCalnutFood0.copy(foodId = 66, foodNameFr = "Pwali")
    val testCalnutFood4: CalnutFood = testCalnutFood0.copy(foodId = 77, foodNameFr = "Pwalo")

    val testCalnutFood0Unit0: NewFoodUnit = NewFoodUnit(testCalnutFood0.foodId, 0, "gramme", 1)
    val testCalnutFood0Unit1: NewFoodUnit = NewFoodUnit(testCalnutFood0.foodId, 1, "brocoli moyen", 300)
    val testCalnutFood1Unit0: NewFoodUnit = NewFoodUnit(testCalnutFood1.foodId, 0, "gramme", 1)
    val testCalnutFood1Unit1: NewFoodUnit = NewFoodUnit(testCalnutFood1.foodId, 2, "wallez moyen", 150)
    val testCalnutFood1ErrorUnit: NewFoodUnit = NewFoodUnit(testCalnutFood1.foodId, 1, "gros wallez", 150)
  }
}
