package com

import akka.http.scaladsl.testkit.RouteTestTimeout

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

trait BaseTest {
  def awaitForResult[T](futureResult: Future[T]): T = {
    Await.result(futureResult, 5.seconds)
  }

  implicit val timeout: RouteTestTimeout = RouteTestTimeout(3.seconds)
}
