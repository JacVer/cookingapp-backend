package com.storage

import java.time.LocalDateTime

import com.BaseTest
import com.model.{Alphabetical, CreatedDateTime, ShoppingList, ShoppingListItem, Account}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContextExecutor

class ShoppingListStorageTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = {
    AccountStorage.createTables()
    ShoppingListStorage.createTables()

    new Context {
      AccountStorage.insertTestAccounts(List(testUser1, testUser2))
      ShoppingListStorage.insertTestShoppingLists(List(testShoppingList1, testShoppingList2, testShoppingList3, testShoppingList4, testShoppingList5, testShoppingList6))
      ShoppingListStorage.insertTestShoppingListItems(testShoppingList1Items ::: testShoppingList2Items ::: testShoppingList4Items ::: testShoppingList5Items ::: testShoppingList6Items)
    }
  }

  override def afterAll(): Unit = {
    ShoppingListStorage.dropTables()
    AccountStorage.dropTables()
  }

  "ShoppingListStorage" when {
    "insertShoppingList" should {
      "return an error if the user doesn't exist in the user table" in new Context {
        awaitForResult(ShoppingListStorage.insertShoppingList(-25, shoppingListToCreate.addedTime, shoppingListToCreate.title))
          .left.get.getMessage should include(s"""insert or update on table "${ShoppingListStorage.shoppingListTableName}" violates foreign key constraint "${ShoppingListStorage.shoppingListTableName}_account_id_fkey"""")
      }

      "return the shopping_list_id of the created shopping list if the shopping list was inserted" in new Context {
        awaitForResult(ShoppingListStorage.insertShoppingList(shoppingListToCreate.accountId, shoppingListToCreate.addedTime, shoppingListToCreate.title)) shouldBe
          Right(List(shoppingListToCreate.shoppingListId))
      }
    }

    "insertShoppingListItems" should {
      "return an error if the shopping_list_id doesn't exist in the shopping_list table" in new Context {
        awaitForResult(ShoppingListStorage.insertShoppingListItems(testShoppingList1Items.map(i => i.copy(shoppingListId = -25))))
          .left.get.getMessage should include(s"""insert or update on table "${ShoppingListStorage.shoppingListItemsTableName}" violates foreign key constraint "${ShoppingListStorage.shoppingListItemsTableName}_shopping_list_id_fkey"""")
      }

      "return the number of items inserted if the items were inserted" in new Context {
        awaitForResult(ShoppingListStorage.insertShoppingListItems(shoppingListToCreateItems)) shouldBe
          Right(shoppingListToCreateItems.length)
      }
    }

    // TODO : refactor this test so that it doesn't rely on previous tests to succeed (it is using shoppingListToCreate)
    "selectShoppingListByuserId" should {
      "return nothing if the user_id doesn't have any shopping list" in new Context {
        awaitForResult(ShoppingListStorage.selectShoppingListByAccountId(-25, Alphabetical)) shouldBe Right(Nil)
      }

      "return the shopping lists matching the user_id in alphabetical order" in new Context {
        awaitForResult(ShoppingListStorage.selectShoppingListByAccountId(testUser1.accountId, Alphabetical)) shouldBe
          Right(List(testShoppingList1, shoppingListToCreate))
      }

      "return the shopping lists matching the user_id ordered by descending created time " in new Context {
        awaitForResult(ShoppingListStorage.selectShoppingListByAccountId(testUser1.accountId, CreatedDateTime)) shouldBe
          Right(List(shoppingListToCreate, testShoppingList1))
      }
    }

    "selectShoppingListItems" should {
      "return nothing if the shopping_list_id doesn't exist" in new Context {
        awaitForResult(ShoppingListStorage.selectShoppingListItems(-45)) shouldBe Right(Nil)
      }

      "return the items of the shopping list with the given shopping_list_id" in new Context {
        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList1.shoppingListId)) shouldBe
          Right(testShoppingList1Items)
      }
    }

    "selectShoppingListByShoppingListId" should {
      "return the shopping list if it is found" in new Context {
        awaitForResult(ShoppingListStorage.selectShoppingListByShoppingListId(testShoppingList1.shoppingListId)) shouldBe
          Right(List(testShoppingList1))
      }

      "return nothing if it doesn't exist" in new Context {
        awaitForResult(ShoppingListStorage.selectShoppingListByShoppingListId(-88)) shouldBe
          Right(Nil)
      }
    }

    "selectMaxItemIdByShoppingListId" should {
      "return nothing if the shopping_list_id doesn't exist" in new Context {
        awaitForResult(ShoppingListStorage.selectMaxItemIdByShoppingListId(-78)) shouldBe
          Right(Nil)
      }

      "return the maximum item_id found with this shopping_list_id" in new Context {
        awaitForResult(ShoppingListStorage.selectMaxItemIdByShoppingListId(testShoppingList1.shoppingListId)) shouldBe
          Right(List((testShoppingList1.shoppingListId, testShoppingList1.accountId, testShoppingList1Items.length - 1)))
      }

      "return 0 if the shopping_list doesn't contain any item" in new Context {
        awaitForResult(ShoppingListStorage.selectMaxItemIdByShoppingListId(testShoppingList3.shoppingListId)) shouldBe
          Right(List((testShoppingList3.shoppingListId, testShoppingList3.accountId, 0)))
      }
    }

    "deleteShoppingListItem" should {
      "return zero if the (shopping_list_id, itemId) combination doesn't exist" in new Context {
        awaitForResult(ShoppingListStorage.deleteShoppingListItem(-54, 0)) shouldBe
          Right(0)

        awaitForResult(ShoppingListStorage.deleteShoppingListItem(0, -58)) shouldBe
          Right(0)
      }

      "return one if the item is successfully deleted" in new Context {
        awaitForResult(ShoppingListStorage.deleteShoppingListItem(testShoppingList4.shoppingListId, 0)) shouldBe
          Right(1)
      }
    }

    "deleteShoppingListItems" should {
      "do nothing if the shopping_list_id doesn't exist" in new Context {
        awaitForResult(ShoppingListStorage.deleteShoppingListItems(-45)) shouldBe
          Right(0)
      }

      "remove all items with this shopping_list_id" in new Context {
        awaitForResult(ShoppingListStorage.deleteShoppingListItems(testShoppingList5.shoppingListId)) shouldBe
          Right(testShoppingList5Items.length)

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList5.shoppingListId)) shouldBe
          Right(Nil)
      }
    }

    "deleteShoppingList" should {
      "do nothing if the shopping_list_id doesn't exist" in new Context {
        awaitForResult(ShoppingListStorage.deleteShoppingList(-745)) shouldBe
          Right(0)
      }

      "delete the shopping list" in new Context {
        awaitForResult(ShoppingListStorage.deleteShoppingList(testShoppingList5.shoppingListId)) shouldBe
          Right(1)

        awaitForResult(ShoppingListStorage.selectShoppingListByShoppingListId(testShoppingList5.shoppingListId)) shouldBe
          Right(Nil)
      }
    }

    "updateShoppingListItem" should {
      "return an error if neither indented or ticked is defined" in new Context {
        the [IllegalArgumentException] thrownBy ShoppingListStorage.updateShoppingListItem(testShoppingList6.shoppingListId, 0, None, None, None) should have message "indented, ticked or text must be defined"
          Right()

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList6.shoppingListId)) shouldBe
          Right(testShoppingList6Items)
      }

      "do nothing if the (shopping_list_id, item_id) isn't in the table" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListItem(testShoppingList6.shoppingListId, -25, Some(true), None, None)) shouldBe
          Right(0)

        awaitForResult(ShoppingListStorage.updateShoppingListItem(-404, 0, None, Some(false), None)) shouldBe
          Right(0)
      }

      "update the ticked field of the item" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListItem(testShoppingList6.shoppingListId, 0, Some(true), None, None)) shouldBe
          Right(1)

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList6.shoppingListId)).map(_.filter(_.itemId == 0).head) shouldBe
          Right(ShoppingListItem(testShoppingList6.shoppingListId, 0, ticked = true, indented = false, "je vais être tické"))
      }

      "update the indented field of the item" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListItem(testShoppingList6.shoppingListId, 1, None, Some(true), None)) shouldBe
          Right(1)

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList6.shoppingListId)).map(_.filter(_.itemId == 1).head) shouldBe
          Right(ShoppingListItem(testShoppingList6.shoppingListId, 1, ticked = false, indented = true, "je vais être indenté"))
      }

      "update the ticked and indented fields of the item" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListItem(testShoppingList6.shoppingListId, 2, Some(false), Some(false), None)) shouldBe
          Right(1)

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList6.shoppingListId)).map(_.filter(_.itemId == 2).head) shouldBe
          Right(ShoppingListItem(testShoppingList6.shoppingListId, 2, ticked = false, indented = false, "moi je ne serais plus rien :o "))
      }

      "update the ticked and text fields of the item" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListItem(testShoppingList6.shoppingListId, 3, Some(true), None, Some("ma renommée sera mondiale"))) shouldBe
          Right(1)

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList6.shoppingListId)).map(_.filter(_.itemId == 3).head) shouldBe
          Right(ShoppingListItem(testShoppingList6.shoppingListId, 3, ticked = true, indented = false, "ma renommée sera mondiale"))
      }

      "update the indented and text fields of the item" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListItem(testShoppingList6.shoppingListId, 4, None, Some(true), Some("ma renommée sera identée"))) shouldBe
          Right(1)

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList6.shoppingListId)).map(_.filter(_.itemId == 4).head) shouldBe
          Right(ShoppingListItem(testShoppingList6.shoppingListId, 4, ticked = false, indented = true, "ma renommée sera identée"))
      }

      "update the text field of the item" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListItem(testShoppingList6.shoppingListId, 5, None, None, Some("ma renommée sera ... renommée"))) shouldBe
          Right(1)

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList6.shoppingListId)).map(_.filter(_.itemId == 5).head) shouldBe
          Right(ShoppingListItem(testShoppingList6.shoppingListId, 5, ticked = false, indented = false, "ma renommée sera ... renommée"))
      }

      "update the indented, ticked and text fields of the item" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListItem(testShoppingList6.shoppingListId, 6, Some(true), Some(false), Some("ma renommée sera tooouuut"))) shouldBe
          Right(1)

        awaitForResult(ShoppingListStorage.selectShoppingListItems(testShoppingList6.shoppingListId)).map(_.filter(_.itemId == 6).head) shouldBe
          Right(ShoppingListItem(testShoppingList6.shoppingListId, 6, ticked = true, indented = false, "ma renommée sera tooouuut"))
      }
    }

    "updateShoppingListTitle" should {
      "do nothing if the shopping_list_id doesn't exist" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListTitle(-45, "pwal")) shouldBe
          Right(0)
      }

      "rename the shopping list" in new Context {
        awaitForResult(ShoppingListStorage.updateShoppingListTitle(testShoppingList6.shoppingListId, "et oui tu t'es fait renommer")) shouldBe
          Right(1)

        awaitForResult(ShoppingListStorage.selectShoppingListByShoppingListId(testShoppingList6.shoppingListId)).map(_.head.title) shouldBe
          Right("et oui tu t'es fait renommer")
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.of(2021, 2, 8, 21, 58)

    val testUser1: Account = Account(1, "casseur@flowteurs.fr", "Orelsan", "Gringe_hash")
    val testUser2: Account = Account(2, "orselan@malerbe.fr", "Greenje", "regarde_comme_il_fait_beau")

    val testShoppingList1: ShoppingList = ShoppingList(testUser1.accountId, 0, timeAdded, "Liste de course 08/02/2021 ;\" pwal")
    val testShoppingList1Items = List(
      ShoppingListItem(testShoppingList1.shoppingListId, 0, ticked = false, indented = false, "Poulet 200g"),
      ShoppingListItem(testShoppingList1.shoppingListId, 1, ticked = false, indented = false, "Deux choux-fleurs"),
      ShoppingListItem(testShoppingList1.shoppingListId, 2, ticked = true, indented = false, "Un demi oeuf")
    )
    val testShoppingList2: ShoppingList = ShoppingList(testUser2.accountId, 5, timeAdded, "trobien la cuisine $$$")
    val testShoppingList2Items = List(
      ShoppingListItem(testShoppingList2.shoppingListId, 0, ticked = false, indented = false, "Poulet au citron"),
      ShoppingListItem(testShoppingList2.shoppingListId, 1, ticked = false, indented = true, "Deux citrons"),
      ShoppingListItem(testShoppingList2.shoppingListId, 2, ticked = false, indented = true, "Olives vertes")
    )
    val testShoppingList3: ShoppingList = ShoppingList(testUser2.accountId, 10, timeAdded, "pourquoi je suis vide ?")
    val shoppingListToCreate: ShoppingList = ShoppingList(testUser1.accountId, 2, timeAdded.plusMinutes(25), "pourquoi c cher les légumes ; \"bio\'")
    val shoppingListToCreateItems = List(
      ShoppingListItem(shoppingListToCreate.shoppingListId, 0, ticked = false, indented = false, "Courgette bio"),
      ShoppingListItem(shoppingListToCreate.shoppingListId, 1, ticked = false, indented = false, "Aubergine bio"),
      ShoppingListItem(shoppingListToCreate.shoppingListId, 2, ticked = true, indented = false, "PQ")
    )
    val testShoppingList4: ShoppingList = ShoppingList(testUser2.accountId, 11, timeAdded, "wesh je me fais vider ?")
    val testShoppingList4Items: List[ShoppingListItem] = List(
      ShoppingListItem(testShoppingList4.shoppingListId, 0, ticked = false, indented = false, "je vais être supprimé"),
      ShoppingListItem(testShoppingList4.shoppingListId, 1, ticked = false, indented = false, "pas moi")
    )
    val testShoppingList5: ShoppingList = ShoppingList(testUser2.accountId, 12, timeAdded, "ah et moi je me fais supprimer ?")
    val testShoppingList5Items: List[ShoppingListItem] = List(
      ShoppingListItem(testShoppingList5.shoppingListId, 0, ticked = false, indented = false, "je vais être supprimé"),
      ShoppingListItem(testShoppingList5.shoppingListId, 1, ticked = false, indented = false, "moi aussi cette fois :( ")
    )
    val testShoppingList6: ShoppingList = ShoppingList(testUser2.accountId, 13, timeAdded, "moi je me fait update ? enfin pas moi mais mes items")
    val testShoppingList6Items: List[ShoppingListItem] = List(
      ShoppingListItem(testShoppingList6.shoppingListId, 0, ticked = false, indented = false, "je vais être tické"),
      ShoppingListItem(testShoppingList6.shoppingListId, 1, ticked = false, indented = false, "je vais être indenté"),
      ShoppingListItem(testShoppingList6.shoppingListId, 2, ticked = true, indented = true, "moi je ne serais plus rien :o "),
      ShoppingListItem(testShoppingList6.shoppingListId, 3, ticked = false, indented = false, "moi je serais renommé et tické"),
      ShoppingListItem(testShoppingList6.shoppingListId, 4, ticked = false, indented = false, "et moi je ne serais plus rien :o "),
      ShoppingListItem(testShoppingList6.shoppingListId, 5, ticked = false, indented = false, "et moi je ne serais plus rien :o "),
      ShoppingListItem(testShoppingList6.shoppingListId, 6, ticked = false, indented = false, "et moi je ne serais plus rien :o ")
    )
  }
}
