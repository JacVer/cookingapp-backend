package com.storage

import java.time.LocalDateTime

import com.BaseTest
import com.model.Account
import com.model.recipe.{Recipe, RecipeComment, RecipeRating}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContextExecutor

class RecipeRatingStorageTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = {
    AccountStorage.createTables()
    FoodStorage.createTables()
    FoodUnitStorage.createTables()
    RecipeStorage.createTables()
    RecipeRatingStorage.createTables()

    new Context {
      AccountStorage.insertTestAccounts(List(testAccount1, testAccount2, testAccount3, testAccount4))
      AccountStorage.hideAccount(testAccount4.accountId)
      RecipeStorage.insertTestRecipes(List(testRecipe1, testRecipe2, testRecipe3))
      awaitForResult(RecipeRatingStorage.insertRating(testRecipe1.recipeId, testAccount1.accountId, timeAdded, 1))
      awaitForResult(RecipeRatingStorage.insertRating(testRecipe1.recipeId, testAccount2.accountId, timeAdded, 4))
      awaitForResult(RecipeRatingStorage.insertRating(testRecipe1.recipeId, testAccount3.accountId, timeAdded, 5))
      awaitForResult(RecipeRatingStorage.insertRating(testRecipe1.recipeId, testAccount4.accountId, timeAdded, 2))
      awaitForResult(RecipeRatingStorage.insertRating(testRecipe3.recipeId, testAccount4.accountId, timeAdded, 5))
      awaitForResult(RecipeRatingStorage.insertComment(testRecipe1.recipeId, testAccount4.accountId, timeAdded, "Je suis caché"))
      awaitForResult(RecipeRatingStorage.insertComment(testRecipe2.recipeId, testAccount1.accountId, timeAdded, "Pwal"))
      awaitForResult(RecipeRatingStorage.insertComment(testRecipe2.recipeId, testAccount1.accountId, timeAdded.plusMinutes(5), "Pwali"))
      awaitForResult(RecipeRatingStorage.insertComment(testRecipe2.recipeId, testAccount2.accountId, timeAdded.plusMinutes(10), "Pwalo"))
    }
  }

  override def afterAll(): Unit = {
    RecipeRatingStorage.dropTables()
    RecipeStorage.dropTables()
    FoodUnitStorage.createTables()
    FoodStorage.createTables()
    AccountStorage.dropTables()
  }

  "RecipeRatingStorage" when {
    "insertRating" should {
      "insert the rating in the database" in new Context {
        awaitForResult(RecipeRatingStorage.insertRating(testRecipe2.recipeId, testAccount1.accountId, timeAdded, 5)) shouldBe
          Right(1)

        awaitForResult(RecipeRatingStorage.selectRecipeRating(testRecipe2.recipeId)) shouldBe
          Right(List(RecipeRating(Some(5.0), 1)))
      }

      "return an error if the user has already put a rating for this recipe" in new Context {
        awaitForResult(RecipeRatingStorage.insertRating(testRecipe2.recipeId, testAccount2.accountId, timeAdded, 5)) shouldBe
          Right(1)

        awaitForResult(RecipeRatingStorage.insertRating(testRecipe2.recipeId, testAccount2.accountId, timeAdded, 5))
          .swap.getOrElse(throw new Error("This should be a Left")).getMessage should include("""ERROR: duplicate key value violates unique constraint "recipe_rating_pkey"""")
      }

      "return an error if the recipe_id doesn't exist" in new Context {
        awaitForResult(RecipeRatingStorage.insertRating(852, testAccount2.accountId, timeAdded, 5))
          .swap.getOrElse(throw new Error("This should be a Left")).getMessage should include("""ERROR: insert or update on table "recipe_rating" violates foreign key constraint "recipe_rating_recipe_id_fkey"""")
      }

      "return an error if the account_id doesn't exist" in new Context {
        awaitForResult(RecipeRatingStorage.insertRating(testRecipe2.recipeId, 7851, timeAdded, 5))
          .swap.getOrElse(throw new Error("This should be a Left")).getMessage should include("""ERROR: insert or update on table "recipe_rating" violates foreign key constraint "recipe_rating_account_id_fkey"""")
      }

      "return an error if the rating is not 1, 2, 3, 4 or 5" in new Context {
        awaitForResult(RecipeRatingStorage.insertRating(testRecipe2.recipeId, testAccount2.accountId, timeAdded, 6))
          .swap.getOrElse(throw new Error("This should be a Left")).getMessage should include("""ERROR: new row for relation "recipe_rating" violates check constraint "recipe_rating_rating_check"""")

        awaitForResult(RecipeRatingStorage.insertRating(testRecipe2.recipeId, testAccount2.accountId, timeAdded, -5))
          .swap.getOrElse(throw new Error("This should be a Left")).getMessage should include("""ERROR: new row for relation "recipe_rating" violates check constraint "recipe_rating_rating_check"""")
      }
    }

    "deleteRating" should {
      "delete the rating if it exists" in new Context {
        awaitForResult(RecipeRatingStorage.deleteRating(testRecipe3.recipeId, testAccount4.accountId)) shouldBe Right(1)

        awaitForResult(RecipeRatingStorage.selectRecipeRating(testRecipe3.recipeId)) shouldBe
          Right(Nil)
      }

      "do nothing if the rating doesn't exist" in new Context {
        awaitForResult(RecipeRatingStorage.deleteRating(852, testAccount4.accountId)) shouldBe Right(0)
      }
    }

    "selectRecipeRating" should {
      "get the rating of a recipe" in new Context {
        awaitForResult(RecipeRatingStorage.selectRecipeRating(testRecipe1.recipeId)) shouldBe
          Right(List(RecipeRating(Some(3.0), 4)))
      }

      "return Nil if the recipe has no ratings" in new Context {
        awaitForResult(RecipeRatingStorage.selectRecipeRating(testRecipe3.recipeId)) shouldBe
          Right(Nil)
      }
    }

    "selectRecipeComment" should {
      "return Nil if the recipe has no comments" in new Context {
        awaitForResult(RecipeRatingStorage.selectRecipeComment(testRecipe3.recipeId)) shouldBe Right(Nil)
      }

      "return the comments of the recipe" in new Context {
        awaitForResult(RecipeRatingStorage.selectRecipeComment(testRecipe2.recipeId)) shouldBe Right(List(
          RecipeComment(testRecipe2.recipeId, Some(testAccount1.pseudo), timeAdded, "Pwal"),
          RecipeComment(testRecipe2.recipeId, Some(testAccount1.pseudo), timeAdded.plusMinutes(5), "Pwali"),
          RecipeComment(testRecipe2.recipeId, Some(testAccount2.pseudo), timeAdded.plusMinutes(10), "Pwalo")
        ))
      }

      "hide the pseudo if the user is deleted" in new Context {
        awaitForResult(RecipeRatingStorage.selectRecipeComment(testRecipe1.recipeId)) shouldBe Right(List(
          RecipeComment(testRecipe1.recipeId, None, timeAdded, "Je suis caché")
        ))
      }
    }

    "insertComment" should {
      "insert the comment in the database" in new Context {
        awaitForResult(RecipeRatingStorage.insertComment(testRecipe1.recipeId, testAccount1.accountId, timeAdded, "Best recipe ever")) shouldBe Right(1)
      }

      "return an error if the recipe_id doesn't exist" in new Context {
        awaitForResult(RecipeRatingStorage.insertComment(85315, testAccount1.accountId, timeAdded, "Best recipe ever"))
          .swap.getOrElse(throw new Error("Should be a Right")).getMessage should include("""ERROR: insert or update on table "recipe_comment" violates foreign key constraint "recipe_comment_recipe_id_fkey"""")
      }

      "return an error if the account_id doesn't exist" in new Context {
        awaitForResult(RecipeRatingStorage.insertComment(testRecipe1.recipeId, 321132, timeAdded, "Best recipe ever"))
          .swap.getOrElse(throw new Error("Should be a Right")).getMessage should include("""ERROR: insert or update on table "recipe_comment" violates foreign key constraint "recipe_comment_account_id_fkey"""")
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.of(2021, 5, 6, 21, 27, 0)

    val testAccount1: Account = Account(3, "anoraa@a.k", "nightdrive", "with_you")
    val testAccount2: Account = Account(4, "robert.parker@synth.wave", "RB", "85' again")
    val testAccount3: Account = Account(5, "carp.en@te.r", "Brut", "anarchy_road")
    val testAccount4: Account = Account(6, "make.you@mo.ve", "DJ E-Maxx", "dum_dumdumdumdumdum")

    val testRecipe1: Recipe = Recipe(
      5,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Saumon au four",
      "La meilleure façon de manger du Saumon",
      List("Mettre le saumon dans un plat", "Au four à 180C pendant 30min"),
      sourceURL = Some("https://roastfish.nl"),
      servings = 2,
      "A",
      preparationTimeMinutes = 20.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 180, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = None,
      isFavourite = true
    )
    val testRecipe2: Recipe = Recipe(
      6,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Daurade au four",
      "Aussi pour la dorade",
      List("Mettre la daurade dans un plat", "Au four à 180C pendant 30min"),
      sourceURL = None,
      servings = 4,
      "A",
      preparationTimeMinutes = 25.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 300, lipidGram = 2.5, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 500,
      picture = Some("picture encoded in base 64"),
      isFavourite = true
    )
    val testRecipe3: Recipe = Recipe(
      7,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Crevette au four",
      "Mitigé pour les crevettes",
      List("Mettre la crevette dans un plat", "Au four à 180C pendant 30min"),
      sourceURL = Some("https://www.idontlikeseafood.wtf"),
      servings = 1,
      "A",
      preparationTimeMinutes = 5.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 300, lipidGram = 25, saturatedFattyAcidGram = 30, carbohydrateGram = 25, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("picture encoded in base 64"),
      isFavourite = true
    )
  }
}
