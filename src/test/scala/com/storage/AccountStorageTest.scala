package com.storage

import com.BaseTest
import com.model.Account
import org.scalatest._

class AccountStorageTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  override def beforeAll(): Unit = new Context {
    AccountStorage.createTables()
    AccountStorage.insertTestAccounts(List(testUser1, userToDelete, userToHide))
  }

  override protected def afterAll(): Unit = {
    AccountStorage.dropTables()
  }

  "UserStorage" when {
    "selectUserWithEmail" should {
      "return a user with the email" in new Context {
        awaitForResult(AccountStorage.selectAccountWithEmail(testUser1.email)) shouldBe
          Right(List(testUser1))
      }

      "return an empty list if the user isn't found" in new Context {
        awaitForResult(AccountStorage.selectAccountWithEmail(missingUserEmail)) shouldBe
          Right(Nil)
      }
    }

    "selectUserWithPseudo" should {
      "return a user with the pseudo" in new Context {
        awaitForResult(AccountStorage.selectAccountWithPseudo(testUser1.pseudo)) shouldBe
          Right(List(testUser1))
      }

      "return Nil if no user has this pseudo" in new Context {
        awaitForResult(AccountStorage.selectAccountWithPseudo(missingPseudo)) shouldBe
          Right(Nil)
      }
    }

    "selectUserWithUserId" should {
      "return a user if its found" in new Context {
        awaitForResult(AccountStorage.selectAccountWithAccountId(testUser1.accountId)) shouldBe
          Right(List(testUser1))
      }

      "return an empty list if the user isn't found" in new Context {
        awaitForResult(AccountStorage.selectAccountWithAccountId(missingUserId)) shouldBe
          Right(Nil)
      }
    }

    "createUser" should {
      "create a user in the database" in new Context {
        awaitForResult(AccountStorage.insertAccount(userToInsert.email, userToInsert.pseudo, userToInsert.passwordHash)) shouldBe
          Right(List(userToInsert))

        awaitForResult(AccountStorage.selectAccountWithEmail(userToInsert.email)) shouldBe
          Right(List(userToInsert))
      }

      "return an error if the user already exists" in new Context {
        awaitForResult(AccountStorage.insertAccount(userToInsertTwice.email, userToInsertTwice.pseudo, userToInsertTwice.passwordHash)) shouldBe
          Right(List(userToInsertTwice))

        awaitForResult(AccountStorage.insertAccount(userToInsertTwice.email, userToInsertTwice.pseudo, userToInsertTwice.passwordHash))
          .left.map(_.getMessage should include(s"""duplicate key value violates unique constraint "${AccountStorage.accountTableName}_email_key""""))
      }
    }

    "deleteUser" should {
      "delete the user in the database" in new Context {
        awaitForResult(AccountStorage.deleteAccount(userToDelete.email)) shouldBe
          Right(1)

        awaitForResult(AccountStorage.selectAccountWithEmail(userToDelete.email)) shouldBe
          Right(Nil)
      }

      "do nothing if the user doesn't exist" in new Context {
        awaitForResult(AccountStorage.deleteAccount(missingUserEmail)) shouldBe
          Right(0)
      }
    }

    "hideUser" should {
      "hide the user in the database" in new Context {
        awaitForResult(AccountStorage.hideAccount(userToHide.accountId)) shouldBe
          Right(1)

        awaitForResult(AccountStorage.selectAccountWithAccountId(userToHide.accountId)) shouldBe
          Right(Nil)
      }

      "do nothing if the user doesn't exist" in new Context {
        awaitForResult(AccountStorage.hideAccount(missingUserId)) shouldBe
          Right(0)
      }
    }
  }

  trait Context {
    val testUser1: Account = Account(22, "email@pwal.ninja", "pwali", "pwal hash")
    val userToHide: Account = Account(33, "columbine@cache.cache", "columbine", "petit hash")
    val userToInsert: Account = Account(1, "newUser@pwal.ninja", "Philibert", "12345")
    val userToInsertTwice: Account = Account(2, "newUser2@pwal.ninja","Mr. Freeze" ,"12345")
    val userToDelete: Account = Account(53, "contrepwal@pwal.ninja","Mamène", "azerty")
    val missingUserEmail = "pwal@pwal.ninja"
    val missingPseudo = "not a pseudo"
    val missingUserId = 424242
  }
}