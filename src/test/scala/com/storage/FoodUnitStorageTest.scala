package com.storage

import java.time.LocalDateTime

import com.BaseTest
import com.model.food.{CalnutFood, FoodUnit, NewFoodUnit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContextExecutor

class FoodUnitStorageTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = new Context {
    FoodStorage.createTables()
    FoodUnitStorage.createTables()

    FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood0, testCalnutFood1, testCalnutFood2, testCalnutFood3, testCalnutFood4))
    FoodUnitStorage.insertFoodUnits(List(testCalnutFood0Unit0, testCalnutFood0Unit1, testCalnutFood1Unit0, testCalnutFood1Unit1))
  }

  override protected def afterAll(): Unit = {
    FoodUnitStorage.dropTables()
    FoodStorage.dropTables()
  }

  "FoodUnitStorage" when {
    "readFoodUnitStorage" should {
      "parse the data from the food_units CSV" in new Context {
        FoodUnitStorage.readFoodUnitCsv(foodUnitCsvFilePath).toList shouldBe List(
          NewFoodUnit(11001, 0, "gramme", 1),
          NewFoodUnit(11001, 1, "cube", 10),
          NewFoodUnit(11041, 0, "gramme", 1),
          NewFoodUnit(11041, 1, "cube", 10)
        )
      }
    }

    "insertFoodUnits" should {
      "insert the food units in the database" in new Context {
        FoodUnitStorage.insertFoodUnits(List(
          NewFoodUnit(testCalnutFood3.foodId, 0, "gramme", 1),
          NewFoodUnit(testCalnutFood3.foodId, 1, "cube", 10),
          NewFoodUnit(testCalnutFood4.foodId, 0, "gramme", 1),
          NewFoodUnit(testCalnutFood4.foodId, 2, "pouce", 20)
        )) shouldBe Right(4)

        awaitForResult(FoodUnitStorage.selectFoodUnit(testCalnutFood3.foodId)) shouldBe Right(List(
          FoodUnit(0, "gramme", 1),
          FoodUnit(1, "cube", 10)
        ))
      }

      "return an error if the combination (foodId, unitId) already exists in the database" in new Context {
        FoodUnitStorage.insertFoodUnits(List(
          NewFoodUnit(testCalnutFood2.foodId, 0, "gramme", 1),
          NewFoodUnit(testCalnutFood2.foodId, 1, "cube", 10),
        )) shouldBe Right(2)

        FoodUnitStorage.insertFoodUnits(List(
          NewFoodUnit(testCalnutFood2.foodId, 0, "gramme", 1),
          NewFoodUnit(testCalnutFood2.foodId, 1, "cube", 10),
        )).swap.getOrElse(throw new Error("Should be a Right")).getMessage should include(s"""ERROR: duplicate key value violates unique constraint "${FoodUnitStorage.foodUnitTableName}_pkey"""")
      }

      // TODO : how to enforce this in PostgresSQL ?
//      "return an error if two unitId have a different unitNameFr" in new Context {
//        FoodUnitStorage.insertFoodUnits(List(
//          FoodUnit(testCalnutFood3.foodId, 0, "gramme", 1),
//          FoodUnit(testCalnutFood4.foodId, 0, "cube", 10),
//        )).left.get.getMessage should include(s"""ERROR: duplicate key value violates unique constraint "${FoodUnitStorage.foodUnitTableName}_pkey"""")
//      }

      "return an error if a foodId isn't in the food table" in new Context {
        FoodUnitStorage.insertFoodUnits(List(NewFoodUnit(missingFoodId, 0, "gramme", 1))).swap.getOrElse(throw new Error("Should be a Right")).getMessage should
          include(s"""ERROR: insert or update on table "food_unit" violates foreign key constraint "${FoodUnitStorage.foodUnitTableName}_food_id_fkey"""")
      }
    }

    "selectFoodUnits" should {
      "return food units if they exist" in new Context {
        awaitForResult(FoodUnitStorage.selectFoodUnit(testCalnutFood0.foodId)) shouldBe Right(List(
          FoodUnit(testCalnutFood0Unit0.unitId, testCalnutFood0Unit0.unitNameFr, testCalnutFood0Unit0.weightGrams),
          FoodUnit(testCalnutFood0Unit1.unitId, testCalnutFood0Unit1.unitNameFr, testCalnutFood0Unit1.weightGrams)
        ))
      }

      "return Nil if they don't exist" in new Context {
        awaitForResult(FoodUnitStorage.selectFoodUnit(missingFoodId)) shouldBe Right(Nil)
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.now()

    val foodUnitCsvFileName = "food_units_test.csv"
    val foodUnitCsvFilePath: String = getClass.getResource("/" + foodUnitCsvFileName).getPath

    val testCalnutFood0: CalnutFood = CalnutFood(24, "Légumes", 20303, "Brocoli", nutriScoreTEV = true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=false, february=false, march=false, april=false, may=false, june=true, july=true, august=true, september=true, october=true, november=false, december=false, 23.1, 93.8, 0.033, 13, 11, 39, 90, 43, 0.17, 0.33, 0.15, 0.18, 10, 10, 2.19, 1.03, 0.6, 0.3, 0.1, 0.1, 0.3, 0.1, 0.1, 0.18, 0.25, 3, 0.5, 0.14, 0.13, 0.18, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.1, 0.04, 0.12, 0.06, 0.12, 0.005, 0.005, 0.005, 0, 576, 0, 1.69, 49.6, 0, 18.1, 0.0075, 0.005, 0.05, 0.25, 0.048, 0, 24.7, 0, 0.053, 0)
    val testCalnutFood1: CalnutFood = testCalnutFood0.copy(foodId = 44, foodNameFr = "Wallez")
    val testCalnutFood2: CalnutFood = testCalnutFood0.copy(foodId = 55, foodNameFr = "Pwal")
    val testCalnutFood3: CalnutFood = testCalnutFood0.copy(foodId = 66, foodNameFr = "Pwali")
    val testCalnutFood4: CalnutFood = testCalnutFood0.copy(foodId = 77, foodNameFr = "Pwalo")

    val testCalnutFood0Unit0: NewFoodUnit = NewFoodUnit(testCalnutFood0.foodId, 0, "grammes", 1)
    val testCalnutFood0Unit1: NewFoodUnit = NewFoodUnit(testCalnutFood0.foodId, 1, "brocoli moyen", 300)
    val testCalnutFood1Unit0: NewFoodUnit = NewFoodUnit(testCalnutFood1.foodId, 0, "grammes", 1)
    val testCalnutFood1Unit1: NewFoodUnit = NewFoodUnit(testCalnutFood1.foodId, 1, "wallez moyen", 150)

    val missingFoodId = 5
  }
}
