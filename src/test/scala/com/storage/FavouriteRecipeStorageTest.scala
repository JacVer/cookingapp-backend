package com.storage

import java.time.LocalDateTime

import com.BaseTest
import com.model.Account
import com.model.recipe.Recipe
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContextExecutor

class FavouriteRecipeStorageTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = {
    AccountStorage.createTables()
    FoodStorage.createTables()
    FoodUnitStorage.createTables()
    RecipeStorage.createTables()
    FavouriteRecipeStorage.createTables()

    new Context {
      AccountStorage.insertTestAccounts(List(testAccount1, testAccount2, testAccount3, testAccount4, testAccount5))
      RecipeStorage.insertTestRecipes(List(testRecipe1, testRecipe2, testRecipe3))
      awaitForResult(RecipeStorage.hideRecipe(testRecipe3.recipeId))
      FavouriteRecipeStorage.insertTestFavouriteRecipes(List(
        testFavouriteAccount1Recipe1,
        testFavouriteAccount1Recipe2,
        testFavouriteAccount3Recipe1,
        testFavouriteAccount4Recipe1,
        testFavouriteAccount4Recipe2
      ))
    }
  }

  override def afterAll(): Unit = {
    FavouriteRecipeStorage.dropTables()
    RecipeStorage.dropTables()
    FoodUnitStorage.dropTables()
    FoodStorage.dropTables()
    AccountStorage.dropTables()
  }

  "FavouriteRecipeStorage" when {
    "addFavouriteRecipe" should {
      "return an error if the favourite recipe already exists" in new Context {
        awaitForResult(FavouriteRecipeStorage.insertFavouriteRecipe(
          testFavouriteAccount1Recipe1._1, testFavouriteAccount1Recipe1._2, testFavouriteAccount1Recipe1._3
        )).swap.getOrElse(throw new Error("Should be a Left")).getMessage should include (s"""duplicate key value violates unique constraint "${FavouriteRecipeStorage.favouriteRecipeTableName}_pkey"""")
      }

      "return the number of favourite successfully inserted" in new Context {
        awaitForResult(FavouriteRecipeStorage.insertFavouriteRecipe(
          testAccount3.accountId, testRecipe2.recipeId, LocalDateTime.now()
        )) shouldBe Right(1)

        awaitForResult(FavouriteRecipeStorage.selectFavouriteRecipe(testAccount3.accountId)) shouldBe
          Right(List(testRecipe1, testRecipe2))
      }
    }

    "deleteFavouriteRecipe" should {
      "return zero if there is no favourite to delete" in new Context {
        awaitForResult(FavouriteRecipeStorage.deleteFavouriteRecipe(testAccount1.accountId, missingRecipeId)) shouldBe
          Right(0)
      }

      "return the number of favourites deleted" in new Context {
        awaitForResult(FavouriteRecipeStorage.deleteFavouriteRecipe(testAccount4.accountId, testFavouriteAccount4Recipe1._2)) shouldBe
          Right(1)

        awaitForResult(FavouriteRecipeStorage.selectFavouriteRecipe(testAccount4.accountId)) shouldBe
          Right(List(testRecipe2))
      }
    }

    "findFavouriteRecipe" should {
      "return Nil if there is no favourite" in new Context {
        awaitForResult(FavouriteRecipeStorage.selectFavouriteRecipe(testAccount2.accountId)) shouldBe
          Right(Nil)
      }

      "return the favourite recipes ordered by added datetime" in new Context {
        awaitForResult(FavouriteRecipeStorage.selectFavouriteRecipe(testAccount1.accountId)) shouldBe
          Right(List(testRecipe1, testRecipe2))
      }

      "return Nil if there is a favourite but it is deleted" in new Context {
        awaitForResult(FavouriteRecipeStorage.selectFavouriteRecipe(testAccount5.accountId)) shouldBe
          Right(Nil)
      }
    }
  }

  trait Context {
    val testAccount1: Account = Account(3, "columbine@cache.cache", "Columbine", "foda c hash")
    val testAccount2: Account = Account(4, "robert.parker@synth.wave", "RB", "85' again")
    val testAccount3: Account = Account(5, "lore.nzo@rap.nul", "Mamène", "du sale")
    val testAccount4: Account = Account(6, "mbk.rocker@vroum.vroum", "Rico", "empereur")
    val testAccount5: Account = Account(7, "power.ranger@lo.lo", "Power", "Rangers")

    val testRecipe1: Recipe = Recipe(
      5,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Saumon au four",
      "La meilleure façon de manger du Saumon",
      List("Mettre le saumon dans un plat", "Au four à 180C pendant 30min"),
      sourceURL = Some("https://roastfish.nl"),
      servings = 2,
      "A",
      preparationTimeMinutes = 20.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 180, lipidGram = 25, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = None,
      isFavourite = true
    )
    val testRecipe2: Recipe = Recipe(
      6,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Daurade au four",
      "Aussi pour la dorade",
      List("Mettre la daurade dans un plat", "Au four à 180C pendant 30min"),
      sourceURL = None,
      servings = 4,
      "A",
      preparationTimeMinutes = 25.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 300, lipidGram = 2.5, saturatedFattyAcidGram = 3, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 500,
      picture = Some("picture encoded in base 64"),
      isFavourite = true
    )
    val testRecipe3: Recipe = Recipe(
      7,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Crevette au four",
      "Mitigé pour les crevettes",
      List("Mettre la crevette dans un plat", "Au four à 180C pendant 30min"),
      sourceURL = Some("https://www.idontlikeseafood.wtf"),
      servings = 1,
      "A",
      preparationTimeMinutes = 5.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true,  june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 300, lipidGram = 25, saturatedFattyAcidGram = 30, carbohydrateGram = 25, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("picture encoded in base 64"),
      isFavourite = true
    )

    val testFavouriteAccount1Recipe1: (Int, Int, LocalDateTime) = (testAccount1.accountId, testRecipe1.recipeId, LocalDateTime.now())
    val testFavouriteAccount1Recipe2: (Int, Int, LocalDateTime) = (testAccount1.accountId, testRecipe2.recipeId, LocalDateTime.now())
    val testFavouriteAccount3Recipe1: (Int, Int, LocalDateTime) = (testAccount3.accountId, testRecipe1.recipeId, LocalDateTime.now())
    val testFavouriteAccount4Recipe1: (Int, Int, LocalDateTime) = (testAccount4.accountId, testRecipe1.recipeId, LocalDateTime.now())
    val testFavouriteAccount4Recipe2: (Int, Int, LocalDateTime) = (testAccount4.accountId, testRecipe2.recipeId, LocalDateTime.now())
    val testFavouriteAccount5Recipe3: (Int, Int, LocalDateTime) = (testAccount5.accountId, testRecipe3.recipeId, LocalDateTime.now())

    val missingRecipeId = 42
  }
}
