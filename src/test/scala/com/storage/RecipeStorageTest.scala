package com.storage

import java.time.{LocalDateTime, Month}

import com.BaseTest
import com.model._
import com.model.food.{CalnutFood, FoodUnit, NewFoodUnit}
import com.model.recipe.{NewRecipeIngredient, Recipe}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContextExecutor

class RecipeStorageTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = {
    AccountStorage.createTables()
    FoodStorage.createTables()
    FoodUnitStorage.createTables()
    RecipeStorage.createTables()
    FavouriteRecipeStorage.createTables()

    new Context {
      AccountStorage.insertTestAccounts(List(testAccount1, testAccount2, testAccount3, testHiddenAccount))
      FoodStorage.insertCalnutFoods(timeAdded, List(chicken, mushroom, onion, salmon)).left.map(throw _)
      FoodUnitStorage.insertFoodUnits(List(
        chickenFoodUnit0,
        chickenFoodUnit1,
        mushroomFoodUnit0,
        mushroomFoodUnit1,
        onionFoodUnit0,
        onionFoodUnit1,
        onionFoodUnit2,
        salmonFoodUnit0
      )).left.map(throw _)
      RecipeStorage.insertTestRecipes(List(
        testRecipe1,
        testRecipe2,
        testRecipe3,
        testRecipe4,
        recipeToHide,
        testHiddenRecipe,
        testRecipeWithHiddenAccount,
        testRecipeToUpdate
      ))
      RecipeStorage.insertTestIngredients(List(
        testRecipe1Ingredient1,
        testRecipe1Ingredient2,
        testRecipe2Ingredient1,
        testRecipe3Ingredient1,
        testRecipe3Ingredient2,
        testRecipe4Ingredient1,
        testRecipe4Ingredient2,
        testHiddenRecipeIngredient1,
        testHiddenRecipeIngredient2,
        testRecipeWithHiddenAccountIngredient,
        testRecipeToUpdateIngredient1,
        testRecipeToUpdateIngredient2
      ))

      awaitForResult(RecipeStorage.hideRecipe(testHiddenRecipe.recipeId))
      awaitForResult(AccountStorage.hideAccount(testHiddenAccount.accountId))

      FavouriteRecipeStorage.insertTestFavouriteRecipes(List(
        (testAccount1.accountId, testRecipe3.recipeId, timeAdded),
        (testAccount2.accountId, testRecipe1.recipeId, timeAdded)
      )
      )
    }
  }

  override def afterAll(): Unit = {
    FavouriteRecipeStorage.dropTables()
    RecipeStorage.dropTables()
    FoodUnitStorage.dropTables()
    FoodStorage.dropTables()
    AccountStorage.dropTables()
  }


  "RecipeStorage" when {

    "selectRecipeWithoutIngredients" should {
      "return recipes containing title the title" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("soupe"))) shouldBe
          Right(List(testRecipe2))
      }

      "return recipes containing the title while not being case sensitive" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("POULET"))) shouldBe
          Right(List(testRecipe1))
      }

      "return recipes containing the title while not being case sensitive and if it is a favourite" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("POULET"), accountId = Some(testAccount2.accountId))) shouldBe
          Right(List(testRecipe1.copy(isFavourite = true)))
      }

      "return Nil if no recipe contains the title" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("estragon"))) shouldBe
          Right(Nil)
      }

      "return recipes created by the account_id" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(pseudo = Some(testAccount1.pseudo))) shouldBe
          Right(List(testRecipe1, testRecipe2))
      }

      "return Nil if no recipe were created by this account_id" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(pseudo = Some("missing_pseudo"))) shouldBe
          Right(Nil)
      }

      "return recipes containing the title and created by the account_id" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("champignon"), pseudo = Some(testAccount2.pseudo))) shouldBe
          Right(List(testRecipe3))
      }

      "return Nil if no recipe created by the account_id contains the title" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("risotto"), pseudo = Some(testAccount1.pseudo))) shouldBe
          Right(Nil)
      }

      "return the recipe with this recipe_id" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(recipeId = Some(testRecipe1.recipeId))) shouldBe
          Right(List(testRecipe1))
      }

      "return the recipe with this recipe_id and whether it is a favourite" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(recipeId = Some(testRecipe1.recipeId), accountId = Some(testAccount2.accountId))) shouldBe
          Right(List(testRecipe1.copy(isFavourite = true)))

        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(recipeId = Some(testRecipe1.recipeId), accountId = Some(testAccount1.accountId))) shouldBe
          Right(List(testRecipe1))
      }

      "return Nil if no recipe exist with this recipe_id" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(recipeId = Some(missingRecipeId))) shouldBe
          Right(Nil)
      }

      "return recipes based on dietary preferences" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("poulet"), vegan = true)) shouldBe Right(Nil)
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("SAUMON"), pescetarian = true)) shouldBe Right(List(testRecipe4))
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("SAUMON"), vegetarian = true)) shouldBe Right(Nil)
      }

      "return recipes bassed on seasonal information" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("poulet"), month = Some(Month.JANUARY))) shouldBe Right(List(testRecipe1))
        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(title = Some("poulet"), month = Some(Month.JUNE))) shouldBe Right(Nil)
      }
    }

    "selectRecipeWithIngredients" should {
      "return recipes containing the passed food_ids" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(chicken.foodId, mushroom.foodId))) shouldBe
          Right(List(testRecipe1))
      }

      "return Nil if there is no recipe containing the food_ids" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(missingFoodId))) shouldBe
          Right(Nil)
      }

      "return recipes containing the food_ids and containing the title" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(mushroom.foodId), title = Some("risotto"))) shouldBe
          Right(List(testRecipe3))
      }

      "return recipes containing the food_ids and containing the title and whether it is a favourite" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(mushroom.foodId), title = Some("risotto"), accountId = Some(testAccount2.accountId))) shouldBe
          Right(List(testRecipe3))

        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(mushroom.foodId), title = Some("risotto"), accountId = Some(testAccount1.accountId))) shouldBe
          Right(List(testRecipe3.copy(isFavourite = true)))
      }
      "return Nil if no recipe with the food_ids contain the title" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(missingFoodId), title = Some("champignon"))) shouldBe
          Right(Nil)
      }

      "return recipes containing the food_ids created by the account_id" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(mushroom.foodId), pseudo = Some(testAccount1.pseudo))) shouldBe
          Right(List(testRecipe1, testRecipe2))
      }

      "return Nil if no recipe created by the account_id contains the food_ids" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(mushroom.foodId), pseudo = Some("missing_pseudo"))) shouldBe
          Right(Nil)
      }

      "return recipes containing the food_ids, containing the title and created by the account_id" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(mushroom.foodId), title = Some("Poulet"), pseudo = Some(testAccount1.pseudo))) shouldBe
          Right(List(testRecipe1))
      }

      "return Nil if no recipe containing the ingredient created by the account_id the account contains the title" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(List(mushroom.foodId), title = Some("Poulet"), pseudo = Some(testAccount2.pseudo))) shouldBe
          Right(List())
      }

      "return recipes based on dietary preferences" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(foodIds = List(chicken.foodId), vegan = true)) shouldBe Right(Nil)
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(foodIds = List(salmon.foodId), pescetarian = true)) shouldBe Right(List(testRecipe4))
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(foodIds = List(salmon.foodId), vegetarian = true)) shouldBe Right(Nil)
      }

      "return recipes based on seasonal information" in new Context {
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(foodIds = List(chicken.foodId), month = Some(Month.JANUARY))) shouldBe Right(List(testRecipe1))
        awaitForResult(RecipeStorage.selectRecipeWithIngredients(foodIds = List(chicken.foodId), month = Some(Month.JUNE))) shouldBe Right(Nil)
      }
    }

    "insertRecipe" should {

      "insert a recipe in the database" in new Context {
        awaitForResult(
          RecipeStorage.insertRecipe(
            insertedRecipe1.accountId,
            insertedRecipe1.title,
            insertedRecipe1.description,
            insertedRecipe1.instructions,
            insertedRecipe1.sourceURL,
            insertedRecipe1.servings,
            insertedRecipe1.nutriScore,
            insertedRecipe1.preparationTimeMinutes,
            insertedRecipe1.picture,
            insertedRecipe1.energyKiloCalories,
            insertedRecipe1.lipidGram,
            insertedRecipe1.saturatedFattyAcidGram,
            insertedRecipe1.carbohydrateGram,
            insertedRecipe1.sugarGram,
            insertedRecipe1.dietaryFiberGram,
            insertedRecipe1.proteinGram,
            insertedRecipe1.saltGram,
            insertedRecipe1.sodiumMilliGram,
            insertedRecipe1.pescetarian,
            insertedRecipe1.vegetarian,
            insertedRecipe1.vegan,
            insertedRecipe1.glutenFree,
            insertedRecipe1.january,
            insertedRecipe1.february,
            insertedRecipe1.march,
            insertedRecipe1.april,
            insertedRecipe1.may,
            insertedRecipe1.june,
            insertedRecipe1.july,
            insertedRecipe1.august,
            insertedRecipe1.september,
            insertedRecipe1.october,
            insertedRecipe1.november,
            insertedRecipe1.december
          )
        ) shouldBe
          Right(List(insertedRecipe1.recipeId))

        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(recipeId = Some(insertedRecipe1.recipeId))) shouldBe
          Right(List(insertedRecipe1))
      }

      "insert a recipe in the database with no source URL" in new Context {
        awaitForResult(
          RecipeStorage.insertRecipe(
            insertedRecipe2.accountId,
            insertedRecipe2.title,
            insertedRecipe2.description,
            insertedRecipe2.instructions,
            insertedRecipe2.sourceURL,
            insertedRecipe2.servings,
            insertedRecipe2.nutriScore,
            insertedRecipe2.preparationTimeMinutes,
            insertedRecipe2.picture,
            insertedRecipe2.energyKiloCalories,
            insertedRecipe2.lipidGram,
            insertedRecipe2.saturatedFattyAcidGram,
            insertedRecipe2.carbohydrateGram,
            insertedRecipe2.sugarGram,
            insertedRecipe2.dietaryFiberGram,
            insertedRecipe2.proteinGram,
            insertedRecipe2.saltGram,
            insertedRecipe2.sodiumMilliGram,
            insertedRecipe2.pescetarian,
            insertedRecipe2.vegetarian,
            insertedRecipe2.vegan,
            insertedRecipe2.glutenFree,
            insertedRecipe2.january,
            insertedRecipe2.february,
            insertedRecipe2.march,
            insertedRecipe2.april,
            insertedRecipe2.may,
            insertedRecipe2.june,
            insertedRecipe2.july,
            insertedRecipe2.august,
            insertedRecipe2.september,
            insertedRecipe2.october,
            insertedRecipe2.november,
            insertedRecipe2.december
          )
        ) shouldBe
          Right(List(insertedRecipe2.recipeId))

        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(recipeId = Some(insertedRecipe2.recipeId))) shouldBe
          Right(List(insertedRecipe2))
      }

      "return an error if the account_id doesn't exist in the accounts table" in new Context {
        awaitForResult(
          RecipeStorage.insertRecipe(
            424242,
            insertedRecipe1.title,
            insertedRecipe1.description,
            insertedRecipe1.instructions,
            insertedRecipe1.sourceURL,
            insertedRecipe1.servings,
            insertedRecipe1.nutriScore,
            insertedRecipe1.preparationTimeMinutes,
            insertedRecipe1.picture,
            insertedRecipe1.energyKiloCalories,
            insertedRecipe1.lipidGram,
            insertedRecipe1.saturatedFattyAcidGram,
            insertedRecipe1.carbohydrateGram,
            insertedRecipe1.sugarGram,
            insertedRecipe1.dietaryFiberGram,
            insertedRecipe1.proteinGram,
            insertedRecipe1.saltGram,
            insertedRecipe1.sodiumMilliGram,
            insertedRecipe1.pescetarian,
            insertedRecipe1.vegetarian,
            insertedRecipe1.vegan,
            insertedRecipe1.glutenFree,
            insertedRecipe1.january,
            insertedRecipe1.february,
            insertedRecipe1.march,
            insertedRecipe1.april,
            insertedRecipe1.may,
            insertedRecipe1.june,
            insertedRecipe1.july,
            insertedRecipe1.august,
            insertedRecipe1.september,
            insertedRecipe1.october,
            insertedRecipe1.november,
            insertedRecipe1.december
          )
        ).swap.getOrElse(throw new Error("Shouldn't be a Right")).getMessage should
          include(s"""insert or update on table "${RecipeStorage.recipeTableName}" violates foreign key constraint "${RecipeStorage.recipeTableName}_account_id_fkey"""")
      }

      "return an error if the number of serving is zero or negative" in new Context {
        awaitForResult(
          RecipeStorage.insertRecipe(
            insertedRecipe2.accountId,
            insertedRecipe2.title,
            insertedRecipe2.description,
            insertedRecipe2.instructions,
            insertedRecipe2.sourceURL,
            0,
            insertedRecipe2.nutriScore,
            insertedRecipe2.preparationTimeMinutes,
            insertedRecipe2.picture,
            insertedRecipe2.energyKiloCalories,
            insertedRecipe2.lipidGram,
            insertedRecipe2.saturatedFattyAcidGram,
            insertedRecipe2.carbohydrateGram,
            insertedRecipe2.sugarGram,
            insertedRecipe2.dietaryFiberGram,
            insertedRecipe2.proteinGram,
            insertedRecipe2.saltGram,
            insertedRecipe2.sodiumMilliGram,
            insertedRecipe2.pescetarian,
            insertedRecipe2.vegetarian,
            insertedRecipe2.vegan,
            insertedRecipe2.glutenFree,
            insertedRecipe2.january,
            insertedRecipe2.february,
            insertedRecipe2.march,
            insertedRecipe2.april,
            insertedRecipe2.may,
            insertedRecipe2.june,
            insertedRecipe2.july,
            insertedRecipe2.august,
            insertedRecipe2.september,
            insertedRecipe2.october,
            insertedRecipe2.november,
            insertedRecipe2.december
          )
        ) .swap.getOrElse(throw new Error("Shouldn't be a Right")).getMessage should
          include("ERROR: new row for relation \"recipe\" violates check constraint \"recipe_servings_check\"")

        awaitForResult(
          RecipeStorage.insertRecipe(
            insertedRecipe2.accountId,
            insertedRecipe2.title,
            insertedRecipe2.description,
            insertedRecipe2.instructions,
            insertedRecipe2.sourceURL,
            -5,
            insertedRecipe2.nutriScore,
            insertedRecipe2.preparationTimeMinutes,
            insertedRecipe2.picture,
            insertedRecipe2.energyKiloCalories,
            insertedRecipe2.lipidGram,
            insertedRecipe2.saturatedFattyAcidGram,
            insertedRecipe2.carbohydrateGram,
            insertedRecipe2.sugarGram,
            insertedRecipe2.dietaryFiberGram,
            insertedRecipe2.proteinGram,
            insertedRecipe2.saltGram,
            insertedRecipe2.sodiumMilliGram,
            insertedRecipe2.pescetarian,
            insertedRecipe2.vegetarian,
            insertedRecipe2.vegan,
            insertedRecipe2.glutenFree,
            insertedRecipe2.january,
            insertedRecipe2.february,
            insertedRecipe2.march,
            insertedRecipe2.april,
            insertedRecipe2.may,
            insertedRecipe2.june,
            insertedRecipe2.july,
            insertedRecipe2.august,
            insertedRecipe2.september,
            insertedRecipe2.october,
            insertedRecipe2.november,
            insertedRecipe2.december
          )
        ) .swap.getOrElse(throw new Error("Shouldn't be a Right")).getMessage should
          include("ERROR: new row for relation \"recipe\" violates check constraint \"recipe_servings_check\"")
      }

      "return an error if the preparation time in minutes is zero or negative" in new Context {
        awaitForResult(
          RecipeStorage.insertRecipe(
            insertedRecipe2.accountId,
            insertedRecipe2.title,
            insertedRecipe2.description,
            insertedRecipe2.instructions,
            insertedRecipe2.sourceURL,
            insertedRecipe2.servings,
            insertedRecipe2.nutriScore,
            0.0,
            insertedRecipe2.picture,
            insertedRecipe2.energyKiloCalories,
            insertedRecipe2.lipidGram,
            insertedRecipe2.saturatedFattyAcidGram,
            insertedRecipe2.carbohydrateGram,
            insertedRecipe2.sugarGram,
            insertedRecipe2.dietaryFiberGram,
            insertedRecipe2.proteinGram,
            insertedRecipe2.saltGram,
            insertedRecipe2.sodiumMilliGram,
            insertedRecipe2.pescetarian,
            insertedRecipe2.vegetarian,
            insertedRecipe2.vegan,
            insertedRecipe2.glutenFree,
            insertedRecipe2.january,
            insertedRecipe2.february,
            insertedRecipe2.march,
            insertedRecipe2.april,
            insertedRecipe2.may,
            insertedRecipe2.june,
            insertedRecipe2.july,
            insertedRecipe2.august,
            insertedRecipe2.september,
            insertedRecipe2.october,
            insertedRecipe2.november,
            insertedRecipe2.december
          )
        ) .swap.getOrElse(throw new Error("Shouldn't be a Right")).getMessage should
          include("ERROR: new row for relation \"recipe\" violates check constraint \"recipe_preparation_time_minutes_check\"")

        awaitForResult(
          RecipeStorage.insertRecipe(
            insertedRecipe2.accountId,
            insertedRecipe2.title,
            insertedRecipe2.description,
            insertedRecipe2.instructions,
            insertedRecipe2.sourceURL,
            insertedRecipe2.servings,
            insertedRecipe2.nutriScore,
            -5.0,
            insertedRecipe2.picture,
            insertedRecipe2.energyKiloCalories,
            insertedRecipe2.lipidGram,
            insertedRecipe2.saturatedFattyAcidGram,
            insertedRecipe2.carbohydrateGram,
            insertedRecipe2.sugarGram,
            insertedRecipe2.dietaryFiberGram,
            insertedRecipe2.proteinGram,
            insertedRecipe2.saltGram,
            insertedRecipe2.sodiumMilliGram,
            insertedRecipe2.pescetarian,
            insertedRecipe2.vegetarian,
            insertedRecipe2.vegan,
            insertedRecipe2.glutenFree,
            insertedRecipe2.january,
            insertedRecipe2.february,
            insertedRecipe2.march,
            insertedRecipe2.april,
            insertedRecipe2.may,
            insertedRecipe2.june,
            insertedRecipe2.july,
            insertedRecipe2.august,
            insertedRecipe2.september,
            insertedRecipe2.october,
            insertedRecipe2.november,
            insertedRecipe2.december
          )
        ) .swap.getOrElse(throw new Error("Shouldn't be a Right")).getMessage should
          include("ERROR: new row for relation \"recipe\" violates check constraint \"recipe_preparation_time_minutes_check\"")
      }
    }

    "hideRecipe" should {
      "hide a recipe if it exists" in new Context {
        awaitForResult(RecipeStorage.hideRecipe(recipeToHide.recipeId)) shouldBe
          Right(1)

        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(recipeId = Some(recipeToHide.recipeId))) shouldBe
          Right(Nil)
      }

      "do nothing if the recipe is not found" in new Context {
        awaitForResult(RecipeStorage.hideRecipe(missingRecipeId)) shouldBe
          Right(0)
      }
    }

    "updateRecipe" should {
      "update a recipe information if it exists" in new Context {
        awaitForResult(
          RecipeStorage.updateRecipe(
            testRecipeToUpdate.recipeId,
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            None,
            5,
            "B",
            30.0,
            None,
            400, 10, 2, 40, 0, 10, 5, 1, 0.2,
            pescetarian = true, vegetarian = true, vegan = false, glutenFree = false,
            january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true,
          )
        ) shouldBe Right(1)

        awaitForResult(RecipeStorage.selectRecipeWithoutIngredients(recipeId = Some(testRecipeToUpdate.recipeId))) shouldBe
         Right(List(
           Recipe(
             testRecipeToUpdate.recipeId,
             testAccount3.accountId,
             testAccount3.pseudo,
             "Daft Punk risotto",
             "Changed my mind, a Daft Punk risotto is funnier",
             List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
             None,
             servings = 5,
             nutriScore =  "B",
             preparationTimeMinutes = 30.0,
             pescetarian = true, vegetarian = true, vegan = false, glutenFree = false,
             january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true,
             energyKiloCalories = 400, lipidGram =  10, saturatedFattyAcidGram =  2, carbohydrateGram =  40, sugarGram =  0, dietaryFiberGram =  10, proteinGram =  5, saltGram =  1, sodiumMilliGram =  0.2,
             picture = None,
             isFavourite = false
           )
         ))
      }

      "do nothing if the recipe doesn't exist" in new Context {
        awaitForResult(
          RecipeStorage.updateRecipe(
            -7852,
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            None,
            5,
            "B",
            30.0,
            None,
            400, 10, 2, 40, 0, 10, 5, 1, 0.2,
            pescetarian = true, vegetarian = true, vegan = true, glutenFree = false,
            january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true,
          )
        ) shouldBe Right(0)
      }

      "do nothing if the recipe is hidden" in new Context {
        awaitForResult(
          RecipeStorage.updateRecipe(
            testHiddenRecipe.recipeId,
            "Daft Punk risotto",
            "Changed my mind, a Daft Punk risotto is funnier",
            List("Cook the rice and the onion.", "Use them to make a shape of a Daft Punk head (good luck !)"),
            None,
            5,
            "B",
            30.0,
            None,
            400, 10, 2, 40, 0, 10, 5, 1, 0.2,
            pescetarian = true, vegetarian = true, vegan = true, glutenFree = false,
            january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true,
          )
        ) shouldBe Right(0)
      }
    }

    "selectRecipeIngredients" should {
      "return Nil if no ingredient exists with this recipe_id" in new Context {
        awaitForResult(RecipeStorage.selectRecipeIngredients(missingRecipeId)) shouldBe
          Right(Nil)
      }

      "return Nil if the recipe is hidden" in new Context {
        awaitForResult(RecipeStorage.selectRecipeIngredients(testHiddenRecipe.recipeId)) shouldBe
          Right(Nil)
      }

      "return Nil if the recipe owner is hidden" in new Context {
        awaitForResult(RecipeStorage.selectRecipeIngredients(testRecipeWithHiddenAccount.recipeId)) shouldBe
          Right(Nil)
      }

      "return the ingredient and food information with this recipe_id" in new Context {
        awaitForResult(RecipeStorage.selectRecipeIngredients(testRecipe1.recipeId)) shouldBe
          Right(List(
            (
              chicken.toFood,
              FoodUnit(chickenFoodUnit1.unitId, chickenFoodUnit1.unitNameFr, chickenFoodUnit1.weightGrams),
              testRecipe1Ingredient1.quantity
            ),
            (
              mushroom.toFood,
              FoodUnit(mushroomFoodUnit0.unitId, mushroomFoodUnit0.unitNameFr, mushroomFoodUnit0.weightGrams),
              testRecipe1Ingredient2.quantity
            )
          ))
      }
    }

    "insertIngredients" should {
      "return an error if one of the ingredient's food_id doesn't exist in the food table" in new Context {
        awaitForResult(RecipeStorage.insertIngredients(List(NewRecipeIngredient(testRecipe1.recipeId, onion.foodId, 1, 300))))
          .swap.getOrElse(throw new Error("Shouldn't be a Right")).getMessage should include(s"""ERROR: insert or update on table "${RecipeStorage.ingredientTableName}" violates foreign key constraint "${RecipeStorage.ingredientTableName}_food_id_unit_id_fkey"""")
      }

      "return an error if the ingredients' recipe_id doesn't exist in the recipe table" in new Context {
        awaitForResult(RecipeStorage.insertIngredients(List(NewRecipeIngredient(missingRecipeId, chicken.foodId, chickenFoodUnit0.unitId, 300))))
          .swap.getOrElse(throw new Error("Shouldn't be a Right")).getMessage should include(s"""ERROR: insert or update on table "${RecipeStorage.ingredientTableName}" violates foreign key constraint "${RecipeStorage.ingredientTableName}_recipe_id_fkey"""")
      }

      "return an error if the couple (recipe, food, unit) already exists" in new Context {
        awaitForResult(RecipeStorage.insertIngredients(List(testRecipe1Ingredient1)))
          .swap.getOrElse(throw new Error("Shouldn't be a Right")).getMessage should include(s"""duplicate key value violates unique constraint "${RecipeStorage.ingredientTableName}_pkey"""")
      }

      "return the number of ingredients inserted if successfully inserted" in new Context {
        awaitForResult(RecipeStorage.insertIngredients(List(
          NewRecipeIngredient(testRecipe1.recipeId, onion.foodId, onionFoodUnit1.unitId, 3)
        ))) shouldBe Right(1)
      }
    }

    "deleteRecipeIngredients" should {
      "delete a recipe ingredients" in new Context {
        awaitForResult(RecipeStorage.deleteRecipeIngredients(testRecipeToUpdate.recipeId)) shouldBe Right(2)

        awaitForResult(RecipeStorage.selectRecipeIngredients(testRecipeToUpdate.recipeId)) shouldBe Right(Nil)
      }

      "do nothing if the recipe doesn't exist" in new Context {
        awaitForResult(RecipeStorage.deleteRecipeIngredients(-784)) shouldBe Right(0)
      }

      "do nothing if  the recipe is hidden" in new Context {
        awaitForResult(RecipeStorage.deleteRecipeIngredients(testHiddenRecipe.recipeId)) shouldBe Right(0)
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.now()

    val testAccount1: Account = Account(22, "email@pwal.ninja", "polytopia", "pwal hash")
    val testAccount2: Account = Account(23, "poly@topia.win", "computer", "pwalash")
    val testAccount3: Account = Account(25, "columbine@cache.cache", "dunnolol", "mdphash")
    val testHiddenAccount: Account = Account(26, "pwaloooooooo@oooo.oo", "stilldunnolmao", "ronehash")

    val chicken: CalnutFood = CalnutFood(35, "Viandes", 36005, "Blanc de poulet", nutriScoreTEV=false, pescetarian=false, vegetarian=false, vegan=false, glutenFree=true, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 213, 59.7, 0.4, 160, 39.6, 200, 414, 19.4, 0.013, 0.68, 0.05, 1.27, 5.66, 5, 28.9, 2.1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9.88, 2.78, 4.36, 2.19, 0, 0, 0, 0.0083, 0.025, 0.084, 2.15, 0.52, 3.47, 1.93, 0.13, 0.047, 0.009, 0.035, 47.5, 0, 1.5, 0.39, 2.4, 0, 0, 0.06, 0.16, 8.55, 0.97, 0.32, 0.52, 5, 0, 0, 117)
    val mushroom: CalnutFood = CalnutFood(24, "Légumes", 20125, "Champignon de paris", nutriScoreTEV=true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=true, february=true, march=true, april=true, may=false, june=false, july=false, august=false, september=true, october=true, november=true, december=true, 38.4, 88.4, 0.028, 11, 14, 140, 540, 2.7, 0.05, 0.31, 0.39, 0.81, 10, 10, 4.44, 4.53, 0, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.18, 3, 0.9, 0.6, 0.18, 0.16, 0.18, 0.005, 0.005, 0.005, 0.005, 0.01, 0.01, 0.11, 0.05, 0.16, 0.18, 0.005, 0.005, 0.005, 0.005, 0, 2.5, 0.2, 0.04, 0.4, 0, 0.25, 0.062, 0.36, 4.53, 2.33, 0.029, 0, 14.3, 0, 0.028, 0)
    val onion: CalnutFood = CalnutFood(24, "Légumes", 20035, "Oignon", nutriScoreTEV=true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 34.2, 90.4, 0.021, 8.4, 13, 44, 220, 29.2, 0.12, 0.21, 0.077, 0.19, 5, 1, 1.3, 6.2, 4, 1.5, 0, 0.25, 1.3, 0.25, 1.2, 0.25, 0.25, 1.4, 0.2, 0.024, 0.02, 0.056, 0, 0, 0, 0, 0, 0.001, 0.021, 0.0015, 0.0067, 0.02, 0.0067, 0.0033, 0, 0, 0, 25, 0, 0.015, 0.4, 0, 2.1, 0.025, 0.025, 0.025, 0.13, 0.09, 0, 21, 0, 0, 0)
    val salmon: CalnutFood = chicken.copy(foodNameFr = "Saumon", foodId = 50, pescetarian = true)

    val chickenFoodUnit0: NewFoodUnit = NewFoodUnit(chicken.foodId, 0, "grammes", 1)
    val chickenFoodUnit1: NewFoodUnit = NewFoodUnit(chicken.foodId, 1, "blanc de poulet moyen", 150)
    val mushroomFoodUnit0: NewFoodUnit = NewFoodUnit(mushroom.foodId, 0, "grammes", 1)
    val mushroomFoodUnit1: NewFoodUnit = NewFoodUnit(mushroom.foodId, 2, "champignon moyen", 20)
    val onionFoodUnit0: NewFoodUnit = NewFoodUnit(onion.foodId, 0, "grammes", 1)
    val onionFoodUnit1: NewFoodUnit = NewFoodUnit(onion.foodId, 3, "oignon", 100)
    val onionFoodUnit2: NewFoodUnit = NewFoodUnit(onion.foodId, 4, "gros oignon", 150)
    val salmonFoodUnit0: NewFoodUnit = NewFoodUnit(salmon.foodId, 0, "grammes", 1)

    val testRecipe1: Recipe = Recipe(
      56,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Poulet aux champignons",
      "Un assortiment de goûts exquis",
      List("Cuire le poulet et les champignons.", "Servir."),
      sourceURL = None,
      servings = 3,
      "B",
      preparationTimeMinutes = 30.0,
      pescetarian = false, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 30, lipidGram = 50, saturatedFattyAcidGram = 45, carbohydrateGram = 20, sugarGram = 0.2, dietaryFiberGram = 5, proteinGram = 6, saltGram = 78, sodiumMilliGram = 200,
      isFavourite = false,
      picture = None
    )
    val testRecipe1Ingredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipe1.recipeId, chicken.foodId, chickenFoodUnit1.unitId, 2)
    val testRecipe1Ingredient2: NewRecipeIngredient = NewRecipeIngredient(testRecipe1.recipeId, mushroom.foodId, mushroomFoodUnit0.unitId, 300)
    val testRecipe2: Recipe = Recipe(
      58,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Soupe aux champignons",
      "Une soupe pour vous tenir chaud l'hiver",
      List("Cuire les champignons.", "Mixer.", "Servir."),
      sourceURL = Some("www.mesrecettes.com"),
      servings = 1,
      "A",
      preparationTimeMinutes = 10.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 30, lipidGram = 15, saturatedFattyAcidGram = 12, carbohydrateGram = 20, sugarGram = 0.5, dietaryFiberGram = 45, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("picture_encoded_b64"),
      isFavourite = false
    )
    val testRecipe2Ingredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipe2.recipeId, mushroom.foodId, mushroomFoodUnit0.unitId, 500)
    val testRecipe3: Recipe = Recipe(
      59,
      testAccount2.accountId,
      testAccount2.pseudo,
      "Risotto aux champignons sans riz",
      "Un risotto revisité façon flemme",
      List("Mettre les oignons à Risotter.", "Ajouter les champignons coupés en petits morceaux."),
      sourceURL = Some("www.goodnutrition.ninja/weird-risotto"),
      servings = 2,
      "A",
      preparationTimeMinutes = 45.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 30, lipidGram = 5, saturatedFattyAcidGram = 6, carbohydrateGram = 10, sugarGram = 0.5, dietaryFiberGram = 10, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("believe_it_or_not_this_is_a_picture_encoded_b64"),
      isFavourite = false
    )
    val testRecipe3Ingredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipe3.recipeId, onion.foodId, onionFoodUnit2.unitId, 3)
    val testRecipe3Ingredient2: NewRecipeIngredient = NewRecipeIngredient(testRecipe3.recipeId, mushroom.foodId, mushroomFoodUnit0.unitId, 300)
    val testRecipe4: Recipe = Recipe(
      615,
      testAccount2.accountId,
      testAccount2.pseudo,
      "Risotto aux oignons et saumon",
      "Un risotto revisité façon poisson",
      List("Mettre les oignons à Risotter avec le saumon.", "Ajouter les champignons coupés en petits morceaux."),
      sourceURL = Some("www.goodnutrition.ninja/insane-fish-risotto"),
      servings = 2,
      "A",
      preparationTimeMinutes = 45.0,
      pescetarian = true, vegetarian = false, vegan = false, glutenFree = true,
      january = true, february = true, march = true, april = true, may = true, june = true, july = true, august = true, september = true, october = true, november = true, december = true,
      energyKiloCalories = 30, lipidGram = 5, saturatedFattyAcidGram = 6, carbohydrateGram = 10, sugarGram = 0.5, dietaryFiberGram = 10, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("believe_it_or_not_this_is_a_picture_encoded_b64"),
      isFavourite = false
    )
    val testRecipe4Ingredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipe4.recipeId, onion.foodId, onionFoodUnit2.unitId, 3)
    val testRecipe4Ingredient2: NewRecipeIngredient = NewRecipeIngredient(testRecipe4.recipeId, salmon.foodId, salmonFoodUnit0.unitId, 300)
    val testHiddenRecipe: Recipe = Recipe(
      62,
      testAccount2.accountId,
      testAccount2.pseudo,
      "Risotto aux champignons sans riz sans risotto",
      "Un risotto revisité façon grosse flemme",
      List("Mettre les oignons à Risotter.", "Ajouter les champignons coupés en petits morceaux."),
      sourceURL = Some("www.goodnutrition.ninja/weirdo-risotto"),
      servings = 2,
      "A",
      preparationTimeMinutes = 45.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 30, lipidGram = 5, saturatedFattyAcidGram = 6, carbohydrateGram = 10, sugarGram = 0.5, dietaryFiberGram = 10, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("believe_it_or_not_this_is_a_picture_encoded_b64"),
      isFavourite = false
    )
    val testHiddenRecipeIngredient1: NewRecipeIngredient = NewRecipeIngredient(testHiddenRecipe.recipeId, onion.foodId, onionFoodUnit2.unitId, 3)
    val testHiddenRecipeIngredient2: NewRecipeIngredient = NewRecipeIngredient(testHiddenRecipe.recipeId, mushroom.foodId, mushroomFoodUnit0.unitId, 300)
    val testRecipeWithHiddenAccount: Recipe = Recipe(
      63,
      testHiddenAccount.accountId,
      testHiddenAccount.pseudo,
      "Risotto sans champignon",
      "Un risotto revisité façon énorme flemme",
      List("Mettre les oignons à Risotter.", "Ajouter les champignons coupés en petits morceaux."),
      sourceURL = Some("www.goodnutrition.ninja/weirder-risotto"),
      servings = 2,
      "A",
      preparationTimeMinutes = 45.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 30, lipidGram = 5, saturatedFattyAcidGram = 6, carbohydrateGram = 10, sugarGram = 0.5, dietaryFiberGram = 10, proteinGram = 5, saltGram = 1, sodiumMilliGram = 50,
      picture = Some("believe_it_or_not_this_is_a_picture_encoded_b64_or_base64"),
      isFavourite = false
    )
    val testRecipeWithHiddenAccountIngredient: NewRecipeIngredient = NewRecipeIngredient(testRecipeWithHiddenAccount.recipeId, mushroom.foodId, mushroomFoodUnit0.unitId, 300)
    val testRecipeToUpdate: Recipe = Recipe(
      64,
      testAccount3.accountId,
      testAccount3.pseudo,
      "Daft punk cake",
      "A cake in the shape of a Daft Punk head",
      List("Bake the cake", "Decorate it so it looks like a Daft Punk head"),
      sourceURL = None,
      servings = 10,
      nutriScore = "E",
      preparationTimeMinutes = 60.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = false,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 600, lipidGram = 20, saturatedFattyAcidGram = 18, carbohydrateGram = 30, sugarGram = 30, dietaryFiberGram = 0, proteinGram = 2, saltGram = 0.1, sodiumMilliGram = 0.02,
      picture = None,
      isFavourite = false
    )
    val testRecipeToUpdateIngredient1: NewRecipeIngredient = NewRecipeIngredient(testRecipeToUpdate.recipeId, onion.foodId, onionFoodUnit2.unitId, 4)
    val testRecipeToUpdateIngredient2: NewRecipeIngredient = NewRecipeIngredient(testRecipeToUpdate.recipeId, mushroom.foodId, mushroomFoodUnit0.unitId, 300)

    val recipeToHide: Recipe = Recipe(
      60,
      testAccount3.accountId,
      testAccount3.pseudo,
      "Recette cachée",
      "Personne ne doit me voir",
      List("Cacher", "La", "Recette"),
      sourceURL = None,
      servings = 1,
      nutriScore = "E",
      preparationTimeMinutes = 1.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 600, lipidGram = 20, saturatedFattyAcidGram = 18, carbohydrateGram = 30, sugarGram = 30, dietaryFiberGram = 0, proteinGram = 2, saltGram = 0.1, sodiumMilliGram = 0.02,
      picture = Some("hidden_picture_encoded_b64"),
      isFavourite = false
    )

    val insertedRecipe1: Recipe = Recipe(
      1,
      testAccount1.accountId,
      testAccount1.pseudo,
      "Risotto",
      "Je viens d'être créée",
      List("Cuire oignons et riz dans le bouillon.", "Servir."),
      sourceURL = Some("https://www.shady_website.com"),
      servings = 2,
      nutriScore = "C",
      preparationTimeMinutes = 15.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 600, lipidGram = 20, saturatedFattyAcidGram = 18, carbohydrateGram = 30, sugarGram = 30, dietaryFiberGram = 0, proteinGram = 2, saltGram = 0.1, sodiumMilliGram = 0.02,
      picture = None,
      isFavourite = false
    )
    val insertedRecipe2: Recipe = Recipe(
      2,
      testAccount2.accountId,
      testAccount2.pseudo,
      "Risotto bis",
      "Je viens d'être créée bis",
      List("Cuire oignons et riz dans le bouillon.", "Ne pas servir."),
      sourceURL = None,
      servings = 3,
      nutriScore = "E",
      preparationTimeMinutes = 20.0,
      pescetarian = true, vegetarian = true, vegan = true, glutenFree = true,
      january = true, february = true, march = true, april = true, may = false, june = false, july = false, august = false, september = true, october = true, november = true, december = true,
      energyKiloCalories = 600, lipidGram = 20, saturatedFattyAcidGram = 18, carbohydrateGram = 30, sugarGram = 30, dietaryFiberGram = 0, proteinGram = 2, saltGram = 0.1, sodiumMilliGram = 0.02,
      picture =  Some("whoa _a_new_picture_encoded_b64"),
      isFavourite = false
    )
    val missingRecipeId = 1212
    val missingFoodId = 45
  }
}