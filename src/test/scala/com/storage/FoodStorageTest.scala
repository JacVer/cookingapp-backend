package com.storage

import java.time.LocalDateTime

import com.BaseTest
import com.model.food.{CalnutFood, FoodUnit, NewFoodUnit}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContextExecutor

class FoodStorageTest extends WordSpec with BaseTest with Matchers with BeforeAndAfterAll {

  implicit val executionContext: ExecutionContextExecutor = scala.concurrent.ExecutionContext.global

  override def beforeAll(): Unit = new Context {
    FoodStorage.createTables()
    FoodUnitStorage.createTables()
    FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood0))
    FoodUnitStorage.insertFoodUnits(List(testCalnutFood0Unit0))
  }

  override def afterAll(): Unit = {
    FoodUnitStorage.dropTables()
    FoodStorage.dropTables()
  }

  "FoodStorage" when {
    "readCalnutCsv" should {
      "parse the data from the Calnut data" in new Context {
        FoodStorage.readCalnutCsv(calnutCsvFilePath).toList shouldBe calnutFoodInCsvFile
      }
    }

    "insertCalnutFoods" should {
      "insert the food items in the database" in new Context {
        FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood1)) shouldBe Right(1)

        awaitForResult(FoodStorage.selectFoodWithFoodId(testCalnutFood1.foodId)) shouldBe Right(List(testCalnutFood1.toFood))
      }

      "return an error if the foodId already exists in the database" in new Context {
        FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood2, testCalnutFood3))
          .swap.getOrElse(throw new Error("This should be a Right")).getMessage should include(
          s"""ERROR: duplicate key value violates unique constraint "${FoodStorage.foodTableName}_pkey""""
        )
      }

      "return an error if the food_name_fr already exists in the database" in new Context {
        FoodStorage.insertCalnutFoods(timeAdded, List(testCalnutFood4, testCalnutFood5))
          .swap.getOrElse(throw new Error("This should be a Right")).getMessage should include(
          s"""ERROR: duplicate key value violates unique constraint "${FoodStorage.foodTableName}_food_name_fr_key""""
        )
      }
    }

    "selectFoodByPartialNotCaseSensitiveName" should {
      "return the food items if some match" in new Context {
        awaitForResult(FoodStorage.selectFoodWithName(testCalnutFood0.foodNameFr)) shouldBe
          Right(List(testCalnutFood0.toFood))
      }

      "return Nil if no food item match" in new Context {
        awaitForResult(FoodStorage.selectFoodWithName("Lentilles")) shouldBe
          Right(Nil)
      }
    }

    "selectFoodByFoodId" should {
      "return the food items with the food_id" in new Context {
        awaitForResult(FoodStorage.selectFoodWithFoodId(testCalnutFood0.foodId)) shouldBe
          Right(List(testCalnutFood0.toFood))
      }

      "return Nil if no food has this food_id" in new Context {
        awaitForResult(FoodStorage.selectFoodWithFoodId(missingFoodId)) shouldBe
          Right(Nil)
      }
    }

    "selectFoodNutriScoreInformationByFoodId" should {
      "return the existing food_id if they are here" in new Context {
        awaitForResult(FoodStorage.selectFoodWithFoodUnit(List((testCalnutFood0.foodId, testCalnutFood0Unit0.unitId)))) shouldBe Right(List(
          (
            testCalnutFood0.toFood,
            FoodUnit(testCalnutFood0Unit0.unitId, testCalnutFood0Unit0.unitNameFr, testCalnutFood0Unit0.weightGrams)
          )
        ))
      }

      "return Nil if no (food_id, unit_id) combination are found" in new Context {
        awaitForResult(FoodStorage.selectFoodWithFoodUnit(List((missingFoodId, 0)))) shouldBe Right(Nil)
        awaitForResult(FoodStorage.selectFoodWithFoodUnit(List((testCalnutFood0.foodId, 50)))) shouldBe Right(Nil)
      }
    }
  }

  trait Context {
    val timeAdded: LocalDateTime = LocalDateTime.now()
    val calnutCsvFileName = "food_base_data_test.csv"
    val calnutCsvFilePath: String = getClass.getResource("/" + calnutCsvFileName).getPath

    val calnutFoodInCsvFile: List[CalnutFood] = List(
      CalnutFood(0, "Bouillons", 11001, "Bouillon de bœuf déshydraté", nutriScoreTEV = false, pescetarian=false, vegetarian=false, vegan=false, glutenFree=false, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 240, 2.6, 33, 13200, 21.9, 127, 344, 32.7, 0.1, 0.35, 0.12, 0.32, 4.6, 2.5, 10.2, 19.7, 3.95, 0.32, 0, 0, 0.27, 0.15, 3.36, 0, 0, 0.65, 13.3, 7.03, 2.6, 1.4, 0, 0, 0.013, 0.013, 0.038, 0.18, 5.42, 1.25, 2.6, 0.96, 0.038, 0, 0, 0, 0, 0, 0, 2.17, 0, 0, 0, 1.38, 0.23, 2.09, 0.48, 0.29, 0.34, 76.3, 0, 0, 0),
      CalnutFood(4, "Produits céréaliers", 9341, "Quinoa cuit", nutriScoreTEV = false, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=true, february=true, march=true, april=true, may=true, june=true, july=true, august=true, september=true, october=true, november=true, december=true, 148, 61.6, 0.015, 6, 71, 180, 220, 23, 0.7, 1.6, 0.21, 1.2, 10, 10, 4.66, 27.9, 0.87, 0.05, 0.05, 0.05, 0.05, 0.05, 0.87, 24.3, 0, 3.8, 1.1, 0.14, 0.31, 0.56, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.12, 0.02, 0.28, 0.49, 0.07, 0.005, 0.005, 0.005, 0, 2.5, 0, 0.88, 0.4, 0, 0, 0.14, 0.014, 0.2, 0.61, 0.089, 0, 49.9, 0, 0, 0),
    )

    val testCalnutFood0: CalnutFood = CalnutFood(24, "Légumes", 20303, "Brocoli", nutriScoreTEV = true, pescetarian=true, vegetarian=true, vegan=true, glutenFree=true, january=false, february=false, march=false, april=false, may=false, june=true, july=true, august=true, september=true, october=true, november=false, december=false, 23.1, 93.8, 0.033, 13, 11, 39, 90, 43, 0.17, 0.33, 0.15, 0.18, 10, 10, 2.19, 1.03, 0.6, 0.3, 0.1, 0.1, 0.3, 0.1, 0.1, 0.18, 0.25, 3, 0.5, 0.14, 0.13, 0.18, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.1, 0.04, 0.12, 0.06, 0.12, 0.005, 0.005, 0.005, 0, 576, 0, 1.69, 49.6, 0, 18.1, 0.0075, 0.005, 0.05, 0.25, 0.048, 0, 24.7, 0, 0.053, 0)
    val testCalnutFood1: CalnutFood = testCalnutFood0.copy(foodId = 44, foodNameFr = "Wallez")
    val testCalnutFood2: CalnutFood = testCalnutFood0.copy(foodId = 55, foodNameFr = "Pwal")
    val testCalnutFood3: CalnutFood = testCalnutFood0.copy(foodId = 55, foodNameFr = "Pwali")
    val testCalnutFood4: CalnutFood = testCalnutFood0.copy(foodId = 66, foodNameFr = "Pwalo")
    val testCalnutFood5: CalnutFood = testCalnutFood0.copy(foodId = 77, foodNameFr = "Pwalo")
    val missingFoodId: Int = -25

    val testCalnutFood0Unit0: NewFoodUnit = NewFoodUnit(testCalnutFood0.foodId, 0, "grammes", 1)
  }
}