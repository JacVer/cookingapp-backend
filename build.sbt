name := "CookingAppBackend"

import com.typesafe.sbt.packager.docker.DockerVersion

enablePlugins(JavaAppPackaging, AshScriptPlugin, DockerPlugin)

version := "0.7"

val akkaHttpVersion = "10.2.2"
val akkaVersion = "2.6.8"
val doobieVersion = "0.8.8"
val catsVersion = "2.2.0"
val circeVersion = "0.12.3"
val swaggerVersion = "2.1.7"

dockerBaseImage := "openjdk:8-jre-alpine"
packageName in Docker := "cookingapp-backend"

lazy val root = (project in file("."))
  .settings(
    inThisBuild(List(
      organization    := "com",
      scalaVersion    := "2.13.3"
    )),
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "ch.megard" %% "akka-http-cors" % "1.1.1",
      "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,

      "com.typesafe" % "config" % "1.4.0",
      "ch.qos.logback" % "logback-classic" % "1.2.3",

      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion,
      "de.heikoseeberger" %% "akka-http-circe" % "1.35.3",

      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test,
      "org.scalatest" %% "scalatest" % "3.0.8" % Test,

      "org.tpolecat" %% "doobie-core"      % doobieVersion,
      "org.tpolecat" %% "doobie-postgres"  % doobieVersion,          // Postgres driver 42.2.9 + type mappings.

      "org.typelevel" %% "cats-core" % catsVersion,
      "org.typelevel" %% "cats-effect" % catsVersion,

      "org.mindrot" % "jbcrypt" % "0.3m",
      "com.pauldijou" %% "jwt-circe" % "5.0.0",

      "javax.ws.rs" % "javax.ws.rs-api" % "2.0.1",
      "com.github.swagger-akka-http" %% "swagger-akka-http" % "2.4.0",
      "com.github.swagger-akka-http" %% "swagger-scala-module" % "2.3.0",
//      "com.github.swagger-akka-http" %% "swagger-enumeratum-module" % "2.1.0",
      "io.swagger.core.v3" % "swagger-core" % swaggerVersion,
      "io.swagger.core.v3" % "swagger-annotations" % swaggerVersion,
      "io.swagger.core.v3" % "swagger-models" % swaggerVersion,
      "io.swagger.core.v3" % "swagger-jaxrs2" % swaggerVersion,

      "com.github.tototoshi" %% "scala-csv" % "1.3.6",
    )
  )

dockerExposedPorts ++= Seq(8080)

dockerVersion := Some(DockerVersion(19, 3, 5, Some("ce")))
