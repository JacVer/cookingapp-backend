# Antisèche acides gras

*10-01-21*

*Jacques Vergine*

*Complétude : 100%*



## Sources

* Lisa Patissier, Diététicienne et Chargée de Projet en Nutrition
* [Wikipédia](https://fr.wikipedia.org/wiki/Liste_des_acides_gras_satur%C3%A9s)



## Sommaire

| Nom de colonne   | S/I     | Nom français             | Nom anglais           |
| ---------------- | ------- | ------------------------ | --------------------- |
| ag_04_0_g        | S       | Acide butyrique          | Butyric acid          |
| ag_06_0_g        | S       | Acide caproïque          | Caproic acid          |
| ag_08_0_g        | S       | Acide caprylique         | Caorylic acid         |
| ag_10_0_g        | S       | Acide caprique           | Capric acid           |
| ag_12_0_g        | S       | Acide laurique           | Lauric acid           |
| ag_14_0_g        | S       | Acide myristique         | Mystiric acid         |
| ag_16_0_g        | S       | Acide palmitique         | Palmitic acid         |
| ag_18_0_g        | S       | Acide stéarique          | Stearic acid          |
| ag_18_1_ole_g    | MI (O9) | Acide oléique            | Oleic acid            |
| ag_18_2_lino_g   | PI (O6) | Acide linoléique         | Linoleic acid         |
| ag_18_3_a_lino_g | PI (O6) | Acide alpha-linolénique  | alpha-Linolenic acid  |
| ag_20_4_ara_g    | PI (O6) | Acide arachidonique      | Arachidonic acid      |
| ag_20_5_epa_g    | PI (O3) | Acide eicosapentaénoïque | Eicosapentaenoic acid |
| ag_22_6_dha_g    | PI (O3) | Acide docosahexaénoïque  | Docosahexaenoic acid  |

* Details
  * S/I : Saturé (S), Monoinsaturé (MI) ou Polyinsaturé (PI)
  * O3 : Oméga-3
  * O6 : Oméga-6
  * O9 : Oméga-9



## Acides Gras Saturés (AGS)

| Origine animale           | Origine végétale  |
| ------------------------- | ----------------- |
| Viande                    | Huile de palme    |
| Beurre                    | Huile de coprah   |
| Crème et Laitages entiers | Huile de palmiste |
| Charcuterie               | Huile d'arachide  |
| Jaune œuf                 |                   |
| Saindoux                  |                   |
| Suif                      |                   |
| Fromages                  |                   |

#### Acide butyrique C4:0

* Rôle préventif dans le cancer du colon
* Trouvé dans :
  * Beurre rance 
  * Le colon suite à la dégradation de fibres par les bactéries 



#### Acide caproïque C6:0, caprylique C8:0 et caprique C10:0

* Facile à digérer
* Se dégradent rapidement et ne sont pas stockés dans les cellules adipeuses
* N'augmentent pas le cholestérol, pas associé au risque cardiovasculaire 



#### Acide laurique C12:0, myristique C14:0, palmitique C16:0 et stéarique C18:0

* L'acide Myristique participe a la myristoylation des protéines (fixe une protéine sur la membrane d'une cellule), processus nécessaire pour exercer leurs fonctions.
* L'acide Palmitique est synthétisé par le corps et est trouvé dans les produits industriels car composant principal de l'huile de palme.
* L'acide Stéarique peut se transformer en acide oléique par désaturation.
* Ces quatre acides gras sont des composants majeurs de la gaine de myéline au niveau du système nerveux cérébral
* <u>En excès</u>, les acides Laurique, Myristique et Palmitique augmentent les risques cardiovasculaires en raison d'effets 
  * Athérogènes : plaques de graisses dans les artères causant [l'athérosclérose](https://fr.wikipedia.org/wiki/Ath%C3%A9roscl%C3%A9rose), première cause de mortalité dans les pays développés
  * Thrombogènes : formation de caillots de sang dans veine ou artère bloquant la circulation du sang
  * Hypercholestérolémiants : augmente le taux de cholestérol dans le sang



## Acides Gras Mono-Insaturés (AGMI) ou Oméga9

| Origine animale                     | Origine végétale     |
| ----------------------------------- | -------------------- |
| Graisse d'oie                       | Huile d'olive        |
| Graisses de volailles (oie, canard) | Avocat               |
|                                     | Graines oléagineuses |

#### Acide oléique C18:1

* Compose la totalité des AGMI
* Synthétisé majoritairement par l'organisme 
* Rôle préventif sur l'apparition des maladies cardiovasculaires 
* Ils composent 15 à 20% de l'Apport Énergétique Total Quotidien (AETQ) 



## Acides Gras Poly-Insaturés (AGPI)

| Origine animale | Origine végétale                                             |
| --------------- | ------------------------------------------------------------ |
| Saumon          | Toutes les huiles sauf palme, palmiste et coprah qui en contiennent moins |
| Thon            |                                                              |
| Hareng          |                                                              |
| Flétan          |                                                              |
| Maquereau       |                                                              |
| Truite          |                                                              |
| Sardine         |                                                              |
| Anchois         |                                                              |

* Certains de ces acides gras sont appelés <u>indispensables</u> car l'organisme ne peut pas les synthétiser, il est donc important d'en avoir dans son alimentation.

* Les autres acides gras peuvent être synthétisés à partir des acides gras indispensables, ils sont donc appelés <u>essentiels</u>.



### Oméga 6

* Hypocholestérolémiants : réduisent le cholestérol dans le sang
* Pro-inflammatoire : peuvent créer des inflammations 
* Participent à la coagulation donc diminuent la fluidité du sang et contribuent à la vasoconstriction
* Participent au maintien de l'intégrité de l'épiderme 
* Favorisent la fertilité ou la gestation

#### Acide linoléique C18:2 

* Acide indispensable 
* Intervient dans la création de la membrane cellulaire 

#### Acide arachidonique C20:4 

* Acide essentiel
* Rôle dans l'apparition de l'obésité car stimule la prise alimentaire et inhibe la lipolyse (dégradation des lipides en acides gras)

#### Acide dihomo gamma linolénique C20:3 

* Acide essentiel
* Formé à partir de l'acide linoléique 
* Possède les même propriétés que la famille des Oméga 3



### Oméga 3

* Hypotriglycéridémiants : réduit le risque hypertriglycéridémie et donc de maladies cardiovasculaires
* Anti-inflammatoires 
* Limites la coagulation donc favorisent la fluidité du sang
* Augmentent le [HDL cholestérol](https://fr.wikipedia.org/wiki/Lipoprot%C3%A9ine_de_haute_densit%C3%A9) "bon" cholestérol éliminant le cholestérol des vaisseaux sanguins
* Améliorent la vision car protègent la rétine et jouent un rôle protecteur dans l'apparition de la [DMLA](https://fr.wikipedia.org/wiki/D%C3%A9g%C3%A9n%C3%A9rescence_maculaire_li%C3%A9e_%C3%A0_l%27%C3%A2ge)

* Participent à la prévention des maladies neurodégénératives (Alzheimer, Parkinson)
* Ralentissent la croissance tumorale (notamment colorectale et prostatique)

#### Acide alpha-linolénique C18:3

* Acide indispensable
* Sert à synthétiser les autres Oméga 3

#### Acide eicosapentaénoïque  C20:5

* Acide essentiel
* Synthétisé à partir d'acide alpha-linolénique, en consommer plus augmentera sa synthèse. 
* Particulièrement impliqué dans la limitation de la coagulation.

#### Acide docosahexaénoïque C22:6

* Acide essentiel
* Rôle essentiel dans la vision en protégeant les photorécepteurs rétiniens.
* Particulièrement impliqué dans le processus d'apoptose (suicide cellulaire programmé)
* Synthétisé à partir d'acide alpha-linolénique, mais en consommer plus d'augmentera pas sa synthèse. Il faut donc être apporté quotidiennement 

