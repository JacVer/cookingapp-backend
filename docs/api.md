# Cooking App API

## Summary

* **Test route** 
  * GET : /heartbeat - Test that the backend is alive
* **Shopping list routes**
  * POST : /api/account/login - Log in a user and get a JWT
  * POST : /api/account/create - Create an account and get a JWT
  * POST : /api/account/delete - Delete an account
* **Recipe routes** 
  * GET : /api/recipe/find? - Find recipes using pseudo, name or ingredients
  * POST : /api/recipe/create - Create a new recipe
  * GET : /api/recipe/get? - Get one recipe by its recipe_id
  * POST : /api/recipe/delete - Delete a recipe
* **Food routes** 
  * GET : /api/food/find? - Get foods by name
  * GET : /api/food/get? - Get one food by food_id
* **Favourite recipes routes** 
  * GET : /api/recipe/favourite/get? - Get favourite recipes by token
  * POST : /api/recipe/favourite/add - Add a recipe to the favourites
  * POST : /api/recipe/favourite/delete - Remove a recipe from the favourites
* **Shopping list routes**
  * POST : /api/shopping-list/create - Create a new shopping list
  * POST : /api/shopping-list/delete - Delete an existing shopping list
  * POST : /api/shopping-list/rename- Rename an existing shopping list
  * GET : /api/shopping-list/get? - Get one shopping list 
  * GET : /api/shopping-list/find? - Get user's shopping list summaries 
  * POST : /api/shopping-list/items/add - Add an item to a shopping list
  * POST : /api/shopping-list/items/delete - Delete an item from a shopping list
  * POST : /api/shopping-list/items/update - Update an item in a shopping list



## Test routes

* **GET /heartbeat**
  * No Input shape
  * Outputs : "I'm looking for a heartbeat"



## Account routes 

### POST /api/account/login

* Input shape

  ```
  {"email": String, "password": String}
  ```

* Output shapes

| Reason             | Code | Shape                         |
| ------------------ | ---- | ----------------------------- |
| Login successful   | 200  | {"token": String}             |
| Invalid email      | 400  | {"error" : "INVALID_EMAIL"}   |
| User not found     | 400  | {"error" : "EMAIL_NOT_FOUND"} |
| Incorrect password | 400  | {"error" : "WRONG_PASSWORD"}  |



### POST /api/account/create

* Input shape

  ```
  {"email": String, "pseudo": String, "password": String}
  ```

* Output shapes

| Reason                       | Code | Shape                             |
| ---------------------------- | ---- | --------------------------------- |
| Account created successfully | 200  | {"token": String}                 |
| Invalid email                | 400  | {"error" : "INVALID_EMAIL"}       |
| Invalid pseudo               | 400  | {"error" : "INVALID_PSEUDO"}      |
| Password too short           | 400  | {"error" : "PASSWORD_TOO_SHORT"}  |
| Email is already used        | 400  | {"error" : "ALREADY_USED_EMAIL"}  |
| Pseudo is already used       | 400  | {"error" : "ALREADY_USED_PSEUDO"} |
| Incorrect password           | 400  | {"error" : "WRONG_PASSWORD"}      |



### POST /api/account/delete

* Input shape

  ```
  {"email": String, "password": String}
  ```

* Output shapes

| Reason                       | Code | Shape                             |
| ---------------------------- | ---- | --------------------------------- |
| Account deleted successfully | 200  | {"success": true}                 |
| Invalid email                | 400  | {"error" : "INVALID_EMAIL"}       |
| Email is not found           | 400  | {"error" : "EMAIL_NOT_FOUND"}     |
| Pseudo is already used       | 400  | {"error" : "ALREADY_USED_PSEUDO"} |
| Incorrect password           | 400  | {"error" : "WRONG_PASSWORD"}      |



## Recipe routes 

### GET /api/recipe/find?

* Input shape

<<<<<<< Updated upstream
    * title: Option[String]
    * pseudo: Option[String]
    * ingredients: Option[String]
  * token: Option[String]
  
* Outputs :
=======
  * title: Option[String]
  * pseudo: Option[String]
  * ingredients: Option[String]
  * token: Option[String]

* Output shapes
>>>>>>> Stashed changes

  * Recipe : 
  
      ```
      {
          "recipe_id": Int, 
          "title": String, 
          "instructions": List[String], 
          "nutri_score": String in (A, B, C, D, E),
          "creator_pseudo": String,
          "is_favourite": Boolean
      }
      ```

| Reason               | Code | Shape                             |
| -------------------- | ---- | --------------------------------- |
| Research successful  | 200  | {"recipes": List[Recipe]}         |
| Invalid title        | 400  | {"error": "INVALID_RECIPE_TITLE"} |
| Invalid pseudo       | 400  | {"error": "INVALID_PSEUDO"}       |
| Pseudo not found     | 400  | {"error": "PSEUDO_NOT_FOUND"}     |
| No parameter defined | 400  | {"error": "NO_PARAMETERS"}        |



### POST /api/recipe/create

* Input shape

  * Ingredient : 

    ```
  {"food_id": Int, "unit_id": Int, "quantity": Double}
    ```

  * RecipeToCreate : 

    ````
    {
    	"token": String,
  		"recipe": {
            "title": String, 
          	"instructions": List[String], 
            "ingredients": List[Ingredient]
        }
    }
    ````
  
* Output shapes 

| Reason                         | Code | Shape                                            |
| ------------------------------ | ---- | ------------------------------------------------ |
| Recipe created successfully    | 200  | {"recipe_id": Int}                               |
| No ingredients provided        | 400  | {"error": "NO_INGREDIENTS"}                      |
| No instructions provided       | 400  | {"error": "NO_INSTRUCTIONS"}                     |
| Invalid token                  | 400  | {"error": "INVALID_TOKEN"}                       |
| User not found                 | 400  | {"error": "USER_NOT_FOUND"}                      |
| Invalid recipe title           | 400  | {"error": "INVALID_RECIPE_TITLE"}                |
| Wrong food in ingredient       | 400  | {"error": "FOOD_NOT_FOUND:$wrong_food_ids"}      |
| Duplicate foods in ingredients | 400  | {"error": "DUPLICATED_FOOD:$duplicate_food_ids"} |



### GET /api/recipe/get?

* Input shape

<<<<<<< Updated upstream
    * recipe_id: Int
  * token: Option[String]
  
* Outputs :
=======
  * recipe_id: Int
  * token: Option[String]

* Output shapes
>>>>>>> Stashed changes

  * IngredientInformation :
  
    ```
    {
        "food_id": Int, 
        "name": String, 
        "unit_name": String,
        "unit_grams": Double,
        "quantity": Double, 
        "energy_kilo_calories": Double,
        "lipid": Double,
        "saturated_fat": Double,
        "carbohydrate": Double,
    	"sugar": Double,
        "salt": Double
    }
    ```
  * RecipeInformation :
  
    ```
    {
        "recipe_id": Int, 
        "title": String, 
        "instructions": List[String], 
        "ingredients": List[IngredientInformation],
        "nutri_score": String (A, B, C, D, E),
        "creator_pseudo": String,
        "is_favourite": Boolean
    }
    ```

| Reason                        | Code | Shape                         |
| ----------------------------- | ---- | ----------------------------- |
| Get recipe is successful      | 200  | RecipeInformation             |
| No recipe with this recipe_id | 400  | {"error": "RECIPE_NOT_FOUND"} |



### POST /api/recipe/delete

* Input shape

  * token: String
  * recipe_id: Int

* Output shapes


| Reason                           | Code | Shape                                 |
| -------------------------------- | ---- | ------------------------------------- |
| Delete recipe is successful      | 200  | {"success": TRUE}                     |
| Invalid token                    | 400  | {"error": "INVALID_TOKEN"}            |
| User not found                   | 400  | {"error": "USER_NOT_FOUND"}           |
| User not the owner of the recipe | 400  | {"error": "USER_NOT_OWNER_OF_RECIPE"} |
| Recipe doesn't exist             | 400  | {"error": "RECIPE_NOT_FOUND"}         |



## Food routes 

### GET /api/food/find?

* Input shape

<<<<<<< Updated upstream
    * name: String
  * token: Option[String]
  
* Outputs :
=======
  * name: String
  * token: Option[String]

* Output shapes 
>>>>>>> Stashed changes

  * FoodSummary:
  
    ```
    {"food_id": Int, "name": String}
    ```

| Reason                       | Code | Shape                          |
| ---------------------------- | ---- | ------------------------------ |
| Food found matching the name | 200  | {"foods": List[FoodSummary]}   |
| Invalid food name            | 400  | {"error": "INVALID_FOOD_NAME"} |



### GET /api/food/get?

* Input shape

<<<<<<< Updated upstream
    * food_id: Int
  * token: Option[String]
  
* Outputs :
=======
  * food_id: Int
  * token: Option[String]

* Output shapes
>>>>>>> Stashed changes

  * FoodUnit
  
    ```
    {"unit_id": Int, "name": String, "grams": Double}
    ```
    
  * FoodInformation :
  
    ```
    {
    	"food_id": Int, 
    	"name": String, 
    	"energy_kilo_calories": Double,
    	"lipid": Double,
    	"saturated_fat": Double,
    	"carbohydrate": Double,
    	"sugar": Double,
    	"salt": Double,
    	"units": List[FoodUnit]
    }
    ```


| Reason                          | Code | Shape                       |
| ------------------------------- | ---- | --------------------------- |
| Food found matching the food_id | 200  | FoodInformation             |
| No food found with the food_id  | 400  | {"error": "FOOD_NOT_FOUND"} |



## Favourite recipes routes

### GET /api/recipe/favourite/get?

* Input shape

  * token: String

* Output shapes

  * Recipe : 

  ```
  {"recipe_id": Int, title: String, "instructions": List[String]}
  ```

| Reason             | Code | Shape                       |
| ------------------ | ---- | --------------------------- |
| Get all favourites | 200  | {"recipes": List[Recipe]}   |
| Invalid token      | 400  | {"error": "INVALID_TOKEN"}  |
| User not found     | 400  | {"error": "USER_NOT_FOUND"} |



### POST /api/recipe/favourite/add

* Input shape

  * ```
    {"token": String, "recipe_id": Int}
    ```

* Output shapes

| Reason                                 | Code | Shape                            |
| -------------------------------------- | ---- | -------------------------------- |
| Recipe successfully added as favourite | 200  | {"success": TRUE}                |
| Invalid token                          | 400  | {"error": "INVALID_TOKEN"}       |
| User not found                         | 400  | {"error": "USER_NOT_FOUND"}      |
| Recipe was already a favourite         | 400  | {"error": "ALREADY_A_FAVOURITE"} |



### POST /api/recipe/favourite/delete

* Input shape

  ```
  {"token": String, "recipe_id": Int}
  ```

* Output shapes

| Reason                                      | Code | Shape                        |
| ------------------------------------------- | ---- | ---------------------------- |
| Recipe successfully deleted from favourites | 200  | {"success": TRUE}            |
| Invalid token                               | 400  | {"error": "INVALID_TOKEN"}   |
| User not found                              | 400  | {"error": "USER_NOT_FOUND"}  |
| Recipe was not a favourite                  | 400  | {"error": "NOT_A_FAVOURITE"} |



## Shopping list routes 

### POST /api/shopping-list/create

* Input shape 

````
{"token": String, "shopping_list_title": String}
````

* Output shapes 

| Reason                             | Code | Shape                                  |
| ---------------------------------- | ---- | -------------------------------------- |
| Shopping list successfully created | 200  | {"shopping_list_id": Int}              |
| Invalid token                      | 400  | {"error": "INVALID_TOKEN"}             |
| User not found                     | 400  | {"error": "USER_NOT_FOUND"}            |
| Empty shopping list title          | 400  | {"error": "EMPTY_SHOPPING_LIST_TITLE"} |



### POST /api/shopping-list/delete

* Input shape 

  ````
  {"token": String, "shopping_list_id": Int}
  ````

* Output shapes

  | Reason                                  | Code | Shape                                        |
  | --------------------------------------- | ---- | -------------------------------------------- |
  | Shopping list successfully deleted      | 200  | {"success": Boolean}                         |
  | Invalid token                           | 400  | {"error": "INVALID_TOKEN"}                   |
  | User not found                          | 400  | {"error": "USER_NOT_FOUND"}                  |
  | User not the owner of the shopping list | 400  | {"error": "USER_NOT_OWNER_OF_SHOPPING_LIST"} |
  | Shopping list not found                 | 400  | {"error": "SHOPPING_LIST_NOT_FOUND"}         |



### POST /api/shopping-list/rename

* Input shape 

  ````
  {"token": String, "shopping_list_id": Int, "shopping_list_title": String}
  ````

* Output shapes 

| Reason                                  | Code | Shape                                        |
| --------------------------------------- | ---- | -------------------------------------------- |
| Shopping list successfully renamed      | 200  | {"success": Boolean}                         |
| Invalid token                           | 400  | {"error": "INVALID_TOKEN"}                   |
| User not found                          | 400  | {"error": "USER_NOT_FOUND"}                  |
| User not the owner of the shopping list | 400  | {"error": "USER_NOT_OWNER_OF_SHOPPING_LIST"} |
| Shopping list not found                 | 400  | {"error": "SHOPPING_LIST_NOT_FOUND"}         |
| Empty shopping list title               | 400  | {"error": "EMPTY_SHOPPING_LIST_TITLE"}       |



### GET /api/shopping-list/get?

* Input shape

  * token: String
  * shopping_list_id: Int

* Output shapes 

  * ShoppingListItem

  ````
  {"item_id": Int, "ticked": Boolan, "indented": Boolean, "text": String}
  ````

  * ShoppingList

  ````
  {
  	"shopping_list_id": Int,
  	"time_created": DateTime,
  	"title": String,
  	"items": List[ShoppingListItem]
  }
  ````

  | Reason                                      | Code | Shape                                        |
  | ------------------------------------------- | ---- | -------------------------------------------- |
  | Shopping list matching the shopping_list_id | 200  | ShoppingList                                 |
  | Invalid token                               | 400  | {"error": "INVALID_TOKEN"}                   |
  | User not found                              | 400  | {"error": "USER_NOT_FOUND"}                  |
  | Empty shopping list title                   | 400  | {"error": "EMPTY_SHOPPING_LIST_TITLE"}       |
  | User not the owner of the shopping list     | 400  | {"error": "USER_NOT_OWNER_OF_SHOPPING_LIST"} |
  | Shopping list not found                     | 400  | {"error": "SHOPPING_LIST_NOT_FOUND"}         |



### GET /api/shopping-list/find?

* Input shape 

  * token: String
  * ordering: String in ["alphabetical", "created_time"]

* Output shapes

  * ShoppingListSummary

  ````
  {"shopping_list_id": Int, "time_created": DateTime, "title": String}
  ````

  | Reason                                    | Code | Shape                         |
  | ----------------------------------------- | ---- | ----------------------------- |
  | All shopping lists from the user          | 200  | List[ShoppingList]            |
  | Invalid token                             | 400  | {"error": "INVALID_TOKEN"}    |
  | User not found                            | 400  | {"error": "USER_NOT_FOUND"}   |
  | Ordering not alphabetical or created_time | 400  | {"error": "UNKNOWN_ORDERING"} |



### POST /api/shopping-list/items/add

* Input shape 

  * NewShoppingListItem

  ````
  {"ticked": Boolean, "indented": Boolean, "text": String}
  ````

  ````
  {
  	"token": String,
  	"shopping_list_id": Int,
  	"items": List[NewShoppingListItem]
  }
  ````

* Output shapes 

| Reason                                  | Code | Shape                                        |
| --------------------------------------- | ---- | -------------------------------------------- |
| Items successfully added                | 200  | {"success": Boolean}                         |
| Invalid token                           | 400  | {"error": "INVALID_TOKEN"}                   |
| User not found                          | 400  | {"error": "USER_NOT_FOUND"}                  |
| Empty item list                         | 400  | {"error": "EMPTY_ITEM_LIST"}                 |
| User not the owner of the shopping list | 400  | {"error": "USER_NOT_OWNER_OF_SHOPPING_LIST"} |
| Shopping list not found                 | 400  | {"error": "SHOPPING_LIST_NOT_FOUND"}         |



### POST /api/shopping-list/items/delete

* Input shape 

  ````
  {"token": String, "shopping_list_id": Int, "item_id": Int}
  ````

* Output shapes 

| Reason                                  | Code | Shape                                        |
| --------------------------------------- | ---- | -------------------------------------------- |
| Items successfully removed              | 200  | {"success": Boolean}                         |
| Invalid token                           | 400  | {"error": "INVALID_TOKEN"}                   |
| User not found                          | 400  | {"error": "USER_NOT_FOUND"}                  |
| Item not found                          | 400  | {"error": "ITEM_NOT_FOUND"}                  |
| User not the owner of the shopping list | 400  | {"error": "USER_NOT_OWNER_OF_SHOPPING_LIST"} |
| Shopping list not found                 | 400  | {"error": "SHOPPING_LIST_NOT_FOUND"}         |



### POST /api/shopping-list/items/update

* Input shape 

  ````
  {
  	"token": String, 
  	"shopping_list_id": Int, 
  	"item_id": Int,
  	"ticked": null or Boolean
  	"indented": null or Boolean
  }
  ````

* Output shapes 

  | Reason                                  | Code | Shape                                           |
  | --------------------------------------- | ---- | ----------------------------------------------- |
  | Items successfully updated              | 200  | {"success": Boolean}                            |
  | Invalid token                           | 400  | {"error": "INVALID_TOKEN"}                      |
  | User not found                          | 400  | {"error": "USER_NOT_FOUND"}                     |
  | Item not found                          | 400  | {"error": "ITEM_NOT_FOUND"}                     |
  | User not the owner of the shopping list | 400  | {"error": "USER_NOT_OWNER_OF_SHOPPING_LIST"}    |
  | Shopping list not found                 | 400  | {"error": "SHOPPING_LIST_NOT_FOUND"}            |
  | Indented and ticked are null            | 400  | {"error": "INDENTED_OR_TICKED_MUST_BE_DEFINED"} |



## Glossary

JWT : Json Web Token