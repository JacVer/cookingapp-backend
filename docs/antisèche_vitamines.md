# Antisèche Vitamines 

*18-12-2020*

*Jacques Vergine*

*Complétude : 50%*

*À faire : réviser et compléter avec de meilleures sources, mettre en forme*



## Sources

* [Série d'articles sur Doctissimo par Florence Daine](https://www.doctissimo.fr/html/nutrition/vitamines_mineraux/vitamine_k.htm)



## Définition

Une vitamine est une substance organique nécessaire en faible quantité (moins de 100milligram par jour) au métabolisme d'un organisme vivant, qui ne peut être synthétisé en quantité suffisante par cette organisme.



## Vitamine A

* Indispensable à la vision : rôle dans les flux nerveux vers les nerfs optiques 
* Essentiel à la croissance 
* Aide à maintenir le système immunitaire 

### Rétinol

* Vitamine A pure 

### Bêta-Carotène

* Précurseur de la vitamine A, ou provitamine A
* Action anti-oxydante en synergie avec d'autres nutriments : vitamines C, E, sélénium ...



## Vitamine B1

* Essentiel à l'activité des enzymes transformant les glucides en énergie
* Participe à la dégradation de l'alcool
* Nécessaire au fonctionnement du système nerveux et des muscles 



## Vitamine B2

* Participe au métabolisme (stockage, utilisation ...) des glucides, lipides et protéines
* Impliquée dans la production de kératine, protéine indispensable au bon état des cheveux, ongles et peau
* Intervient dans la vision



## Vitamine B3

* Participe à la production d'énergie au sein des cellules 
* Permet de réparer l'ADN si endommagé
* Intervient dans le fonctionnement du système nerveux



## Vitamine B5

* Participe au métabolisme (stockage, utilisation ...) des glucides, lipides et protéines
* Participe à la synthèse de certaines hormones : cortisol, aldostérone, adrénaline ...
* Essentielle à la croissance des tissus, cicatrisation de la peau et pousse des cheveux 



## Vitamine B6

* TODO



## Vitamine B9

* TODO



## Vitamine B12

* TODO



## Vitamine C

* Action anti-oxydante 
* Essentielle aux défenses immunitaires 
* Contribue à la production de 
  * Neurotransmetteurs : dopamine  
  * Vasoconstricteurs : noradrénaline 
  * Os, cartilage, tendons, ligaments : collagène
  * Energie à partir d'acide gras : carnitine 



## Vitamine D

* Est produite par la peau quand exposée aux rayons solaires 
* Aide a l'absorption du calcium et du phosphore dans l'intestin et leur réabsorption par les reins
* Aide à fixer les minéraux dans le tissus osseux (prévention de l'ostéoporose)



## Vitamine E

* Fonction anti-oxydante 
* Prévention des maladies cardiovasculaires : protège du mauvais cholestérol 



## Vitamine K

* Rôle dans la coagulation du sang 
* Essentielle à la croissance et santé des os (prévention de l'ostéroporose)

### Vitamine K1

* Origine végétale

### Vitamine K2

* Origine animale





