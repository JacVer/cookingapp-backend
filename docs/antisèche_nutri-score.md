# Antisèche Nutri-Score

*10-01-21*

*Jacques Vergine*



## Sources

* [Santé Publique France](https://www.santepubliquefrance.fr/determinants-de-sante/nutrition-et-activite-physique/articles/nutri-score)
  * Plus particulièrement, l'annexe 1 du [Règlement d'usage du Nutri-Score](https://www.santepubliquefrance.fr/media/files/02-determinants-de-sante/nutrition-et-activite-physique/nutri-score/reglement-usage)



## Définition

Le Nutri-Score est un logo qui informe sur la qualité nutritionnelle des produits sous une forme simplifiée. Il est basé sur une échelle de 5 couleurs, du vert foncé au orange foncé, associées à des lettres allant de A à E.

Le logo est attribué sur la base d'un score prenant en compte, pour 100 grammes ou 100 millilitres de produits, la teneur :

* en nutriments et aliments à favoriser  (fibres, protéines, fruits, légumes, légumineuses, fruits à coques,  huile de colza, de noix et d’olive),
* et en nutriments à limiter (énergie, acides gras saturés, sucres, sel).

Après calcul, le score obtenu par un produit permet de lui attribuer une lettre et une couleur.

Note : Le Nutri-Score n'est pas adapté aux aliments infantiles destinés aux enfants de 0 à 3 ans.



## Calcul du Nutri-Score 

Pour paraphraser l'annexe du Règlement d'usage du Nutri-Score qui l'explique déjà très clairement.

### 1. Calcul du score nutritionnel des aliments 

Il est calculé de façon identique pour tous les aliments, sauf pour les fromages, les matières grasses végétales ou animales, les boissons. Voir les exceptions au 1.b plus bas pour ces catégories d’aliments.



#### 1-a Cas Général

Le score nutritionnel des aliments repose sur deux composantes :

* Une composante dite "négative" **N**
* Une composante dite "positive" **P**



**La composante N** prend en compte les éléments nutritionnels dont il est recommandé de limiter la consommation : densité énergétique (kJ/100g), teneur en acide gras saturés, en sucres et en sodium. 

Pour chacun de ces éléments, on attribue des points de 1 à 10 selon le tableau ci-dessous, que l'on additionne pour obtenir la composante N, qui variera donc entre 0 à 40.

| Points | Densité énergétique<br />(kJ/100g) | Graisses saturées<br />(g/100g) | Sucres<br />(g/100g) | Sodium*<br />(mg/100g) |
| ------ | ---------------------------------- | :------------------------------ | -------------------- | ---------------------- |
| 0      | <= 335                             | <= 1​                            | <= 4.5               | <= 90                  |
| 1      | > 335​                              | > 1​                             | > 4.5​                | > 90​                   |
| 2      | > 670​                              | > 2​                             | > 9​                  | > 180​                  |
| 3      | > 1005​                             | > 3​                             | > 13.5​               | > 270​                  |
| 4      | > 1340​                             | > 4​                             | > 18​                 | > 360​                  |
| 5      | > 1675​                             | > 5​                             | > 22.5​               | > 450​                  |
| 6      | > 2010​                             | >6​                              | > 27​                 | > 540​                  |
| 7      | > 2345​                             | > 7​                             | > 31​                 | > 630​                  |
| 8      | > 2680​                             | > 8​                             | > 36​                 | > 720​                  |
| 9      | > 3015​                             | > 9​                             | > 40​                 | > 810​                  |
| 10     | > 3350​                             | > 10​                            | > 45​                 | > 900​                  |

***** La teneur en Sodium (mg/100g) correspond à a teneur en sel (g/100g) divisée par 2,5.



**La composante P** prend en compte les éléments à favoriser dans son alimentation : la teneur en fruits, légumes, légumineuses, fruits à coque et huiles de colza, de noix et d'olive, au titre des vitamines qu'ils contiennent, en fibres et en protéines.

Pour chacun de ces éléments, on attribue des points de 1 à 5 selon le tableau ci-dessous, que l'on additionne pour obtenir la composante P, qui variera donc entre 0 à 15.

| Points | Fruits, légumes, légumineuses, fruits à coque, <br />huiles de colza, de noix et d'olive* (%) | Fibres (g/100g)<br />Méthode AOAC | Protéines<br />(g/100g) |
| ------ | ------------------------------------------------------------ | --------------------------------- | ----------------------- |
| 0      | <= 40                                                        | <= 0.9​                            | <= 1.6                  |
| 1      | > 40​                                                         | > 0.9​                             | > 1.6​                   |
| 2      | > 60​                                                         | > 1.9​                             | > 3.2​                   |
| 3      | -                                                            | > 2.8​                             | > 4.8​                   |
| 4      | -                                                            | > 3.7​                             | > 6.4​                   |
| 5      | > 80​                                                         | > 4.7                             | > 8​                     |

***** Les fruits et légumes, légumineuses et fruits à coque comprennent de nombreuses vitamines (en particulier les vitamines E, C, B1, B2, N3, B6 et B9 ainsi que la provitamine A (bêta-carotène))

La teneur en fruits, légumes, légumineuses, fruits à coque, huiles de colza, de noix et d'olive, sera abréviée TEV pour "Teneur En Végétaux"

**Le score nutritionnel final** est calculé selon les formule suivante, en prenant en compte des règles spécifiques définies.

* Si la composante N est inférieure à 11 points
  * $ Score \space nutritionnel = Total \space Points \space N - Total \space Points \space P$
* Si la composante N est supérieure ou égale à 11 points et si le TEV est égal à 5
  * $ Score \space nutritionnel = Total \space Points \space N - Total \space Points \space P$

* Si la composante N est supérieure ou égale à 11 points et si le TEV est inférieur à 5
  * Dans ce cas, les points attribués par les protéines dans P ne sont pas pris en compte 
  * $ Score \space nutritionnel = Total \space Points \space N - Points\space Fibres - Points \space TEV$



La note finale est donc comprise entre une valeur théorique de -15 (le plus favorable sur le plan nutritionnel) et une valeur théorique de +40 (le plus défavorable sur le plan nutritionnel).



#### 1-b Cas particuliers 

Les cas particuliers des fromages et des matières grasses ajoutées ne nous concerne pas dans notre cas de recettes de cuisines faites maisons. Plus d'information sont disponibles dans les sources.

**Pour les boissons** il suffit de remplacer les colonnes ci-dessous dans les tableaux ci-dessus, les autres colonnes restent les même.

| Points | Densité énergétique <br />(kJ/100g ou 100mL) | Sucres<br />(g/100g ou 100mL) | Fruits, légumes, légumineuses, fruits à coque, <br />huiles de colza, de noix et d'olive (%) |
| ------ | -------------------------------------------- | ----------------------------- | ------------------------------------------------------------ |
| 0      | <= 0​                                         | <= 0                          | <= 40                                                        |
| 1      | <= 30​                                        | <= 1.5​                        | -                                                            |
| 2      | <= 60​                                        | <= 3                          | > 40​                                                         |
| 3      | <= 90​                                        | <= 4.5                        | -                                                            |
| 4      | <= 120​                                       | <= 6                          | > 60​                                                         |
| 5      | <= 150​                                       | <= 7.5                        | -                                                            |
| 6      | <= 180​                                       | <= 9                          | -                                                            |
| 7      | <= 210​                                       | <= 10.5                       | -                                                            |
| 8      | <= 240​                                       | <= 12                         | -                                                            |
| 9      | <= 270​                                       | <= 13.5                       | -                                                            |
| 10     | > 270                                        | > 13.5                        | > 80​                                                         |



### 2. Attribution d'une note à l'aliment selon son score

#### 2-a Cas d'un aliment 

| Classe | Bornes du score | Couleur      |
| ------ | --------------- | ------------ |
| A      | Minimum à -1    | Vert foncé   |
| B      | 0 à 2           | Vert clair   |
| C      | 3 à 10          | Orange clair |
| D      | 11 à 18         | Orange moyen |
| E      | 19 à Maximum    | Orange foncé |



#### 2-b Cas d'une boisson 

| Classe | Bornes du score | Couleur      |
| ------ | --------------- | ------------ |
| A      | Eaux            | Vert foncé   |
| B      | Minimum à 1     | Vert clair   |
| C      | 2 à 5           | Orange clair |
| D      | 6 à 9           | Orange moyen |
| E      | 10 à Maximum    | Orange foncé |



